AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "Gyropod"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Zoooooooooooooooooooooooooooooooooooooooooooooooooooooom."
ENT.Instructions = "Place on top of ship facing backwards, link pod, fly into map wall."

ENT.Spawnable = false
ENT.AdminOnly = false
ENT.IsSCModule = true
ENT.IsSCGyropod = true

local base = scripted_ents.Get("base_moduleentity")
local scbase = scripted_ents.Get("base_lsentity")
hook.Add( "InitPostEntity", "sc_gyropod_post_entity_init", function()
    base = scripted_ents.Get("base_moduleentity")
    scbase = scripted_ents.Get("base_lsentity")
end)

function ENT:SharedInit()
    scbase.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Enabled", "bool", false)
end

if not SERVER then return end

require("quaternion")

ENT.Leveling  = false
ENT.Velocity  = Vector(0,0,0)
ENT.TurnVelocity  = Angle(0,0,0)
ENT.On        = false
ENT.ForwardMovement = 0
ENT.RightMovement = 0
ENT.UpMovement = 0
ENT.Acceleration = 0
ENT.AccelerationMult = Vector(1, 0.5, 0.25)
ENT.SpeedLimit = 0
ENT.TurnSpeed = 0
ENT.TurnAcceleration = 0
ENT.TurnMult = Vector(1, 1, 1)
ENT.MaxThrottleAngle = 45
ENT.Deadzone = 5
ENT.Pitch = 0
ENT.Yaw = 0
ENT.Roll = 0
ENT.UseKeysForTurning = false
ENT.CurrentAngle = Angle(0, 0, 0)
ENT.RollLock = true
ENT.WireRollLock = false
ENT.WireLeveling = false
ENT.TargetPos = Vector(0, 0, 0)
ENT.CurrentPos = Vector(0, 0, 0)
ENT.MoveThrottle = Vector(0, 0, 0)
ENT.TurnThrottle = Vector(0, 0, 0)
ENT.ShouldAutoParent = true
ENT.UserMaxSpeed = -1
ENT.UserMaxTurnSpeed = -1

-- Ship speed values
local ShipSpeeds = {
    Drone = 3500,
    Fighter = 3000,
    Frigate = 2200,
    Cruiser = 1350,
    Battlecruiser = 850,
    Battleship = 500,
    Dreadnaught = 250,
    Titan = 125
}

local ShipTurnSpeeds = {
    Drone = 115,
    Fighter = 100,
    Frigate = 55,
    Cruiser = 35,
    Battlecruiser = 26,
    Battleship = 16,
    Dreadnaught = 8,
    Titan = 5
}

local ShipAcceleration = {
    Drone = 0.035,
    Fighter = 0.03,
    Frigate = 0.02,
    Cruiser = 0.015,
    Battlecruiser = 0.005,
    Battleship = 0.002,
    Dreadnaught = 0.0015,
    Titan = 0.001
}

local ShipTurnAcceleration = {
    Drone = 4,
    Fighter = 3.5,
    Frigate = 1,
    Cruiser = 0.65,
    Battlecruiser = 0.45,
    Battleship = 0.35,
    Dreadnaught = 0.12,
    Titan = 0.05
}

util.PrecacheSound("ambient/atmosphere/outdoor2.wav")
util.PrecacheSound("ambient/atmosphere/indoor1.wav")
util.PrecacheSound("buttons/button1.wav")
util.PrecacheSound("buttons/button18.wav")
util.PrecacheSound("buttons/button6.wav")
util.PrecacheSound("buttons/combine_button3.wav")
util.PrecacheSound("buttons/combine_button2.wav")
util.PrecacheSound("buttons/lever7.wav")

function ENT:Initialize()
    scbase.Initialize(self)

    self:SetName("Gyropod")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_VPHYSICS)

    self:SetTurnOnSound("buttons/button1.wav")
    self:SetTurnOffSound("buttons/button18.wav")
    self:SetLoopSound("ambient/atmosphere/outdoor2.wav")

    self.KeysDown = {}

    --Inputs
    if WireLib then
        WireLib.CreateSpecialInputs(self, {"AimAtTarget", "TargetPos", "UseWireIO", "Active", "RollLock", "MoveThrottle", "TurnThrottle", "Level", "MaxSpeed", "MaxTurnSpeed"}, {"NORMAL", "VECTOR", "NORMAL", "NORMAL", "NORMAL", "VECTOR", "VECTOR", "NORMAL", "NORMAL", "NORMAL"})
        WireLib.CreateSpecialOutputs(self, {"On", "Leveling", "RollLocked", "Velocity", "TurnVelocity"}, {"NORMAL", "NORMAL", "NORMAL", "VECTOR", "ANGLE"})
    end

    -- Input hooks
    hook.Add("PlayerButtonDown", "GyroKeyPress_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not IsValid(self.Pod) or not self.On then return end
        if ply:GetVehicle() == self.Pod then
            self.KeysDown[key] = true
        end
    end)

    hook.Add("PlayerButtonUp", "GyroKeyRelease_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not IsValid(self.Pod) or not self.On then return end
        if ply:GetVehicle() == self.Pod then
            self.KeysDown[key] = nil
        end
    end)
end

--
-- Code for parenting based on the auto parenter tool. Works slightly differently to prevent lag from ship core updates when parenting and unparenting.
--

-- Constraint types to remove
local ConstraintTypes = {
    "Weld",
    "Elastic",
    "Axis",
    "Rope",
    "Slider",
    "Winch",
    "Hydraulic",
    "Keepupright",
    "Motor",
    "Muscle",
    "Pulley",
    "Ballsocket",
    "AdvBallsocket"
}

-- Entities can have a table on them with the following format:
--  ENTITY.SCAutoParenter = {
--      ["DisableConstraintRemoval"] = false,
--      ["IgnoreSpecialConstraints"] = false,
--      ["IgnoredConstraints"] = {}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
--      ["DisableWeighting"] = false,
--      ["DisableWelding"] = false,
--      ["DisableParenting"] = false
--  }

function ENT:ParentShip()
    if self.Parenting or self.Parented or not IsValid(self:GetProtector()) then return end

    local ShipCore = self:GetProtector()
    local ConstrainedEnts = table.Copy(ShipCore.ConWeldTable)
    local EntPositions = {}
    local EntsToWeld = {}

    -- Set a variable so we know we're waiting for the parenting to finish
    self.Parenting = true

    -- Remove the constraints from any entity that has more than 1 weld.
    for i,k in pairs(ConstrainedEnts) do
        if k ~= self and IsValid(k) and IsValid(k:GetPhysicsObject()) then
            --get the physics
            local kphys = k:GetPhysicsObject()

            --freeze the contraption and nullify all collisions(we put these back later!)
            kphys:EnableCollisions(false)
            kphys:EnableMotion(false)
            kphys:Sleep()

            -- Record the position of the physics object for later
            local Pos, Ang = WorldToLocal(kphys:GetPos(), kphys:GetAngles(), self:GetPos(), self:GetAngles())
            EntPositions[k] = {["Pos"] = Pos, ["Ang"] = Ang}

            --unconstrain everything
            if not k.SCAutoParenter or not (k.SCAutoParenter.DisableConstraintRemoval or k.SCAutoParenter.IgnoreSpecialConstraints) then
                if table.Count(constraint.GetTable(k)) ~= 1 or not IsValid(constraint.Find(k, self, "Weld", 0, 0)) then
                    EntsToWeld[k] = true
                    constraint.RemoveAll(k)
                end
            elseif k.SCAutoParenter.IgnoreSpecialConstraints and k.SCAutoParenter.IgnoredConstraints ~= nil then
                for _,c in pairs(ConstraintTypes) do
                    if not table.HasValue(k.SCAutoParenter.IgnoredConstraints, c) then
                        constraint.RemoveConstraints(k, c)
                    end
                end
            end
        end
    end

    self.ParentedEntPositions = EntPositions

	--this part needs to be delayed a bit sadly
	timer.Simple(0.1, function()
		for i,k in pairs(ConstrainedEnts) do
			if IsValid(k) then
				--get the physics
				local kphys = k:GetPhysicsObject()

				--make sure we don't do this to the parent
				if k ~= self then
					--find out if we want to ignore anything
					local doweld, doparent = true, true
					if k.SCAutoParenter then
						doweld = not k.SCAutoParenter.DisableWelding
						doparent = not k.SCAutoParenter.DisableParenting
                    end

                    if doweld then
                        doweld = EntsToWeld[k] ~= nil
                    end

					--reweld everything
					if doweld then constraint.Weld(k, self, 0, 0, 0, false, true) end

					--parent that shit up
					if doparent and not IsValid(self:GetParent()) and not k:IsVehicle() then
						k:SetParent(self)
					end
				end

				--undo the funky shit
				kphys:EnableCollisions(true)
				kphys:Sleep()
			end
        end

        -- Let the rest of the code know we're done
        self.Parenting = false
        self.Parented = true
	end)
end

local function SetAngles(self, Angles)
    local phys = self:GetPhysicsObject()
    if IsValid(phys) then
        phys:SetAngles(Angles)
        phys:Wake()

        if not phys:IsMoveable() then
            phys:EnableMotion(true)
            phys:EnableMotion(false)
        end
    else
        self:SetAngles(Angles)
    end
end

local function SetPos(self, Pos)
    local phys = self:GetPhysicsObject()
    if IsValid(phys) then
        phys:SetPos(Pos)
        phys:Wake()

        if not phys:IsMoveable() then
            phys:EnableMotion(true)
            phys:EnableMotion(false)
        end
    else
        self:SetPos(Pos)
    end
end

function ENT:UnparentShip()
    if (not self.Parented) or self.Parenting then return end

    for i,k in pairs(self.ParentedEntPositions) do
        if IsValid(i) then
            i:SetParent(nil)
            local Pos, Ang = LocalToWorld(k.Pos, k.Ang, self:GetPos(), self:GetAngles())
            SetPos(i, Pos)
            SetAngles(i, Ang)
        end
    end

    self.Parented = false
end

--
-- End parenting code
--

function ENT:OnTurnOn()
    self:SoundPlay("TurnOnSound")
    self:SoundPlay("LoopSound")
end

function ENT:OnTurnOff()
    if self.Parented then
        self:UnparentShip()
    end

    self:SoundStop("LoopSound")
    self:SoundPlay("TurnOffSound")

    self.KeysDown = {}
end

function ENT:CanEnable()
    return IsValid(self:GetProtector()) and self:GetProtector():CheckClass(self:GetFitting()) and self:GetProtector():CheckFitting(self:GetFitting())
end

function ENT:OnModuleEnabled()
    return base.OnModuleEnabled(self)
end

function ENT:OnModuleDisabled()
    return base.OnModuleDisabled(self)
end

function ENT:EnableModule()
    return base.EnableModule(self)
end

function ENT:DisableModule(DisableModifierUpdate)
    return base.DisableModule(self, DisableModifierUpdate)
end

function ENT:OnCoreUpdated()
    local Core = self:GetProtector()
    if IsValid(Core) then
        local MassMod = ((Core.ArmorData.MassModifier + Core.HullData.HealthModifier) * 0.5)
        local SpeedMod = ((MassMod - 1) * 0.25) + 1
        self.Acceleration = ShipAcceleration[Core:GetShipClass()] / MassMod
        self.TurnSpeed = ShipTurnSpeeds[Core:GetShipClass()] / SpeedMod
        self.SpeedLimit = ShipSpeeds[Core:GetShipClass()] / SpeedMod
        self.TurnAcceleration = ShipTurnAcceleration[Core:GetShipClass()] / MassMod
    end
end

function ENT:OnRemove()
    -- Remove input hooks
    hook.Remove("PlayerButtonDown", "GyroKeyPress_"..self:EntIndex())
    hook.Remove("PlayerButtonUp", "GyroKeyRelease_"..self:EntIndex())
end

function ENT:DoTurnAcceleration(Input, Current, TurnMult, TurnMax)
    local MinTurn = TurnMult * 0.001
    if math.abs(Input) > 0.01 and math.abs(Current) < TurnMax then
        return Current + (TurnMult * Input)
    else
        if Current > MinTurn then
            return Current - math.min(TurnMult, math.abs(Current))
        elseif Current < -MinTurn then
            return Current + math.min(TurnMult, math.abs(Current))
        else
            return 0
        end
    end
end

function ENT:ApplyTurning(Parent, TurnThrottle)
    local TurnSpeed = self.TurnSpeed
    if self.UserMaxTurnSpeed >= 0 then
        TurnSpeed = math.min(TurnSpeed, self.UserMaxTurnSpeed)
    end

    TurnThrottle.x = math.Clamp(TurnThrottle.x, -1, 1)
    TurnThrottle.y = math.Clamp(TurnThrottle.y, -1, 1)
    TurnThrottle.z = math.Clamp(TurnThrottle.z, -1, 1)

    self.Pitch = self:DoTurnAcceleration(-TurnThrottle.x, self.Pitch, self.TurnMult.x * self.TurnAcceleration, TurnSpeed * math.abs(TurnThrottle.x))
    self.Yaw = self:DoTurnAcceleration(-TurnThrottle.y, self.Yaw, self.TurnMult.y * self.TurnAcceleration, TurnSpeed * math.abs(TurnThrottle.y))
    self.Roll = self:DoTurnAcceleration(TurnThrottle.z, self.Roll, self.TurnMult.z * self.TurnAcceleration, TurnSpeed * math.abs(TurnThrottle.z))

    local CurrentRotation = quaternion.newQuat(Parent:GetAngles())
    self.TurnVelocity = Angle(self.Pitch, self.Yaw, self.Roll)
    local TurnRotation = quaternion.newQuat(self.TurnVelocity * FrameTime())
    local DesiredRotation = CurrentRotation * TurnRotation

    self.CurrentAngle = DesiredRotation:forward():AngleEx(DesiredRotation:up())
    SetAngles(Parent, self.CurrentAngle)
end

function ENT:ApplyMovement(Parent, MoveThrottle)
    local SpeedLimit = self.SpeedLimit
    if self.UserMaxSpeed >= 0 then
        SpeedLimit = math.min(SpeedLimit, self.UserMaxSpeed)
    end

    if math.abs(MoveThrottle.x) < 0.001 then
        self.ForwardMovement = self.ForwardMovement * (1 - (self.Acceleration * self.AccelerationMult.x))
    else
        self.ForwardMovement = self.ForwardMovement + (-MoveThrottle.x * SpeedLimit * self.AccelerationMult.x * self.Acceleration)
    end

    if math.abs(MoveThrottle.y) < 0.001 then
        self.RightMovement = self.RightMovement * (1 - (self.Acceleration * self.AccelerationMult.y))
    else
        self.RightMovement = self.RightMovement + (MoveThrottle.y * SpeedLimit * self.AccelerationMult.y * self.Acceleration)
    end

    if math.abs(MoveThrottle.z) < 0.001 then
        self.UpMovement = self.UpMovement * (1 - (self.Acceleration * self.AccelerationMult.z))
    else
        self.UpMovement = self.UpMovement + (-MoveThrottle.z * SpeedLimit * self.AccelerationMult.z * self.Acceleration)
    end

    self.ForwardMovement = math.Clamp(self.ForwardMovement, -SpeedLimit, SpeedLimit)
    self.RightMovement = math.Clamp(self.RightMovement, -SpeedLimit, SpeedLimit)
    self.UpMovement = math.Clamp(self.UpMovement, -SpeedLimit, SpeedLimit)

    local LocalVelocity = Vector(self.ForwardMovement, self.RightMovement, self.UpMovement)

    self.Velocity = LocalVelocity
    self.Velocity:Rotate(self.CurrentAngle)

    if self.Velocity:LengthSqr() > 0.001 then
        self.CurrentPos = Parent:GetPos() - (self.Velocity * FrameTime())
        SetPos(Parent, self.CurrentPos)
    else
        self.CurrentPos = Parent:GetPos()
    end
end

local rad2deg = 180 / math.pi
local function Elevation(ent, vec)
	if not IsValid(ent) then return 0 end

	local pos = ent:WorldToLocal(vec)
	local len = pos:Length()
	return rad2deg*math.asin(pos.z / len)
end

local function Bearing(ent, vec)
	if not IsValid(ent) then return 0 end

	local pos = ent:WorldToLocal(vec)
	return rad2deg*-math.atan2(pos.y, pos.x)
end

function ENT:Think()
    local OldOn = self.On
    if not IsValid(self:GetProtector()) then
        if not self.Parenting then
            self.On = false

            if OldOn then
                self:OnTurnOff()
            end
        end

        return
    end

    local Parent = self:GetProtector():GetParent()
    local OldDriver = self.Driver
    if IsValid(self.Pod) then
        self.Driver = self.Pod:GetDriver()
    else
        self.Driver = NULL
    end

    if not self.UseWireIO then
        -- If we're not using Wire IO then we shouldn't turn on without a pod linked
        if not IsValid(self.Pod) then
            self.On = false

            if OldOn then
                self:OnTurnOff()
            end

            return
        end

        -- Check for a driver
        local Driver = self.Pod:GetDriver()
        self.On = IsValid(Driver)
    else
        if IsValid(self.Driver) and not self.On and OldDriver ~= self.Driver then
            self.Driver:ChatPrint("[Space Combat 2 - Gyropod] - This ship uses wire to fly, please activate the Gyropod!")
        end
    end

    if self.On and not OldOn then
        self:OnTurnOn()
    elseif not self.On and OldOn then
        self:OnTurnOff()
    end

    if self.On and not (self.Parented or self.Parenting) then
        if self.ShouldAutoParent then
            self:ParentShip()
        else
            if IsValid(self.Driver) and OldDriver ~= self.Driver then
                self.Driver:ChatPrint("[Space Combat 2 - Gyropod] - Please parent your ship or enable auto parenting to fly!")
            end
        end
    end

    if not self.On or not IsValid(Parent) or not self:GetFitting().CanRun or self.Parenting then
        self:UpdateOutputs()
        return
    end

    local MoveThrottle = Vector()
    local TurnThrottle = Vector()

    -- Deal with wire inputs
    if self.UseWireIO then
        MoveThrottle = self.MoveThrottle
        TurnThrottle = self.TurnThrottle
        self.Leveling = self.WireLeveling
        self.RollLock = self.WireRollLock

    -- Handle user input
    else
        local Driver = self.Pod:GetDriver()

        -- Get Roll Lock and Aim w/ Keys from player
        self.RollLock = (Driver:GetInfoNum("sc_gyropod_rolllock", 0) == 1) or self.WireRollLock
        self.UseKeysForTurning = Driver:GetInfoNum("sc_gyropod_usekeysforturning", 0) == 1

        -- Leveling
        if self.KeysDown[Driver.GyropodKeys.Level] or self.WireLeveling then
            self.Leveling = true
        else
            self.Leveling = false
        end

        -- Forward Movement
        if self.KeysDown[Driver.GyropodKeys.Forward] then
            MoveThrottle.x = 1
        elseif self.KeysDown[Driver.GyropodKeys.Back] then
            MoveThrottle.x = -1
        end

        -- Lateral Movement
        if self.KeysDown[Driver.GyropodKeys.Right] then
            MoveThrottle.y = 1
        elseif self.KeysDown[Driver.GyropodKeys.Left] then
            MoveThrottle.y = -1
        end

        -- Vertical Movement
        if self.KeysDown[Driver.GyropodKeys.Up] then
            MoveThrottle.z = 1
        elseif self.KeysDown[Driver.GyropodKeys.Down] then
            MoveThrottle.z = -1
        end

        if not self.KeysDown[Driver.GyropodKeys.Look] then
            if self.UseKeysForTurning then
                -- Pitch
                if not self.Leveling then
                    if self.KeysDown[Driver.GyropodKeys.PitchUp] then
                        TurnThrottle.x = 1
                    elseif self.KeysDown[Driver.GyropodKeys.PitchDown] then
                        TurnThrottle.x = -1
                    end
                end

                -- Yaw
                if self.KeysDown[Driver.GyropodKeys.YawRight] then
                    TurnThrottle.y = 1
                elseif self.KeysDown[Driver.GyropodKeys.YawLeft] then
                    TurnThrottle.y = -1
                end

                -- Roll
                if not self.Leveling and not self.RollLock then
                    if self.KeysDown[Driver.GyropodKeys.RollRight] then
                        TurnThrottle.z = 1
                    elseif self.KeysDown[Driver.GyropodKeys.RollLeft] then
                        TurnThrottle.z = -1
                    end
                end
            else
                local RelativeAngles = Parent:WorldToLocalAngles(self.Pod:GetDriver():EyeAngles())

                -- Pitch
                if not self.Leveling then
                    if RelativeAngles.p > self.Deadzone then
                        TurnThrottle.x =  (-RelativeAngles.p + self.Deadzone) / self.MaxThrottleAngle
                    elseif RelativeAngles.p < -self.Deadzone then
                        TurnThrottle.x =  (-RelativeAngles.p - self.Deadzone) / self.MaxThrottleAngle
                    end
                end

                -- Yaw
                if RelativeAngles.y > self.Deadzone then
                    TurnThrottle.y = (-RelativeAngles.y + self.Deadzone) / self.MaxThrottleAngle
                elseif RelativeAngles.y < -self.Deadzone then
                    TurnThrottle.y = (-RelativeAngles.y - self.Deadzone) / self.MaxThrottleAngle
                end

                -- Roll
                if not self.Leveling and not self.RollLock then
                    if self.KeysDown[Driver.GyropodKeys.RollRight] then
                        TurnThrottle.z = 1
                    elseif self.KeysDown[Driver.GyropodKeys.RollLeft] then
                        TurnThrottle.z = -1
                    end
                end
            end
        end
    end

    if self.Leveling then
        local ParentAng = Parent:GetAngles()
        if math.abs(ParentAng.p) > math.Clamp(self.Deadzone / 2, 0.05, 5) then
            TurnThrottle.x = (ParentAng.p / self.MaxThrottleAngle)
        else
            TurnThrottle.x = 0
        end
    end

    -- Aim at a target
    if self.AimAtTarget then
        -- Pitch
        local Elev = Elevation(Parent, self.TargetPos)
        if math.abs(Elev) > math.Clamp(self.Deadzone / 2, 0.05, 5) then
            TurnThrottle.x = (Elev / self.MaxThrottleAngle)
        else
            TurnThrottle.x = 0
        end

        -- Yaw
        local Bear = Bearing(Parent, self.TargetPos)
        if math.abs(Bear) > math.Clamp(self.Deadzone / 2, 0.05, 5) then
            TurnThrottle.y = (Bear / self.MaxThrottleAngle)
        else
            TurnThrottle.y = 0
        end

        -- Roll
        if not self.Leveling and not self.RollLock and not self.UseWireIO then
            local Driver = self.Pod:GetDriver()

            if IsValid(Driver) then
                if self.KeysDown[Driver.GyropodKeys.RollRight] then
                    TurnThrottle.z = 1
                elseif self.KeysDown[Driver.GyropodKeys.RollLeft] then
                    TurnThrottle.z = -1
                end
            end
        end
    end

    if self.Leveling or self.RollLock then
        local ParentAng = Parent:GetAngles()
        if math.abs(ParentAng.r) > math.Clamp(self.Deadzone / 2, 0.5, 5) then
            TurnThrottle.z = (-ParentAng.r / self.MaxThrottleAngle)
        else
            TurnThrottle.z = 0
        end
    end

    -- Save vehicle positions for later
    local Vehicles = {}
    for i,k in pairs(self:GetProtector().Vehicles) do
        local Pos, Rot = WorldToLocal(k:GetPos(), k:GetAngles(), Parent:GetPos(), Parent:GetAngles())
        table.insert(Vehicles, {Ent=k, Pos=Pos, Rot=Rot})
    end

    -- Do Turning
    self:ApplyTurning(Parent, TurnThrottle)

    -- Do Movement
    self:ApplyMovement(Parent, MoveThrottle)

    -- Update Sounds
    if self.LoopSound and self.LoopSound:IsPlaying() then
        self.LoopSound:ChangeVolume(math.Clamp(self.Velocity:Length(), self.SpeedLimit * 0.01, self.SpeedLimit) / self.SpeedLimit)
        self.LoopSound:ChangePitch(math.Clamp((math.Clamp(self.Velocity:Length(), self.SpeedLimit * 0.01, self.SpeedLimit) / self.SpeedLimit) * 255, 60, 255))
    end

    -- Apply new vehicle positions
    for i,k in pairs(Vehicles) do
        local Pos, Rot = LocalToWorld(k.Pos, k.Rot, self.CurrentPos, self.CurrentAngle)
        SetPos(k.Ent, Pos)
        SetAngles(k.Ent, Rot)
    end

    self:UpdateOutputs()

    self:NextThink(CurTime())
    return true
end

function ENT:SetTurnOnSound(path)
    if self.TurnOnSound then
        self.TurnOnSound:Stop()
    end

    self.TurnOnSound = CreateSound(self, Sound(path))
    self.TurnOnSound_path = path
end

function ENT:SetTurnOffSound(path)
    if self.TurnOffSound then
        self.TurnOffSound:Stop()
    end

    self.TurnOffSound = CreateSound(self, Sound(path))
    self.TurnOffSound_path = path
end

function ENT:SetLoopSound(path)
    if self.LoopSound then
        self.LoopSound:Stop()
    end

    self.LoopSound = CreateSound(self, Sound(path))
    self.LoopSound_path = path
end

function ENT:SoundPlay(which)
    if self.Muted then return end

    if self[which] then
        self[which]:Stop()
        self[which]:Play()
    elseif self[which.."_path"] ~= nil then
        local setfunc = self["Set" .. which]
        local path = self[which .. "_path"]

        setfunc(self, path)
    end
end

function ENT:SoundStop(which)
    if self[which] then
        self[which]:Stop()
    end
end

function ENT:SetMuted(b)
    self.Muted = b

    if b then
        self:SoundStop("TurnOnSound")
        self:SoundStop("TurnOffSound")
        self:SoundStop("LoopSound")
    elseif self.On then
        self:SoundPlay("LoopSound")
    end
end

duplicator.RegisterEntityClass("sc_gyropod", GAMEMODE.MakeEnt, "Data")
function ENT:ApplySCDupeInfo(DupeInfo, GetEntByID)
    local Info = DupeInfo or {}

    if Info.Pod ~= nil then
        self.Pod = GetEntByID(Info.Pod)
    end

    if Info.TurnMult ~= nil then
        self.TurnMult = Vector(math.Clamp(Info.TurnMult.X, 0, 1), math.Clamp(Info.TurnMult.Y, 0, 1), math.Clamp(Info.TurnMult.Z, 0, 1))
    end

    if Info.Deadzone ~= nil then
        self.Deadzone = Info.Deadzone
    end

    if Info.AccelerationMult ~= nil then
        self.AccelerationMult = Vector(math.Clamp(Info.AccelerationMult.X, 0, 1), math.Clamp(Info.AccelerationMult.Y, 0, 1), math.Clamp(Info.AccelerationMult.Z, 0, 1))
    end

    if Info.ShouldAutoParent ~= nil then
        self.ShouldAutoParent = Info.ShouldAutoParent
    end

    if Info.UserMaxSpeed ~= nil then
        self.UserMaxSpeed = Info.UserMaxSpeed
    else
        self.UserMaxSpeed = -1
    end

    if Info.UserMaxTurnSpeed ~= nil then
        self.UserMaxTurnSpeed = Info.UserMaxTurnSpeed
    else
        self.UserMaxTurnSpeed = -1
    end
end

function ENT:SaveSCInfo()
    local Data = {}

    Data["Pod"] = IsValid(self.Pod) and self.Pod:EntIndex() or nil
    Data["TurnMult"] = self.TurnMult
    Data["Deadzone"] = self.Deadzone
    Data["AccelerationMult"] = self.AccelerationMult
    Data["ShouldAutoParent"] = self.ShouldAutoParent
    Data["UserMaxSpeed"] = self.UserMaxSpeed
    Data["UserMaxTurnSpeed"] = self.UserMaxTurnSpeed

    return Data
end

function ENT:LinkPod(NewPod)
    if IsValid(NewPod) and NewPod:IsVehicle() then
        self.Pod = NewPod
        return true
    end

    return false
end

function ENT:TriggerInput(Name, Value)
    if Name == "TargetPos" then
        self.TargetPos = Value
    elseif Name == "AimAtTarget" then
        self.AimAtTarget = (Value == 1)
    elseif Name == "UseWireIO" then
        self.UseWireIO = (Value == 1)
    elseif Name == "Level" then
        self.WireLeveling = (Value == 1)
    elseif Name == "RollLock" then
        self.WireRollLock = (Value == 1)
    elseif Name == "MaxSpeed" then
        self.UserMaxSpeed = Value
    elseif Name == "MaxTurnSpeed" then
        self.UserMaxTurnSpeed = Value
    elseif self.UseWireIO then
        if Name == "Active" then
            self.On = (Value == 1)
        elseif Name == "MoveThrottle" then
            self.MoveThrottle = Value
        elseif Name == "TurnThrottle" then
            self.TurnThrottle = Value
        end
    end
end

function ENT:UpdateOutputs()
    if WireLib then
        WireLib.TriggerOutput(self, "On", self.On and 1 or 0)
        WireLib.TriggerOutput(self, "Velocity", self.Velocity)
        WireLib.TriggerOutput(self, "TurnVelocity", self.TurnVelocity)
        WireLib.TriggerOutput(self, "Leveling", self.Leveling and 1 or 0)
        WireLib.TriggerOutput(self, "RollLock", self.RollLock and 1 or 0)
    end
end