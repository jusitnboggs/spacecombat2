AddCSLuaFile()

local base = scripted_ents.Get("sc_resource_pump")
hook.Add("InitPostEntity", "base_gspump_post_entity_init", function()
	base = scripted_ents.Get("sc_resource_pump")
end)

ENT.Type = "anim"
ENT.Base = "sc_resource_pump"
ENT.PrintName = "Global Storage Pump"
ENT.Author = "Steeveeo + Ama"
ENT.Purpose = "Moves resources from a network to global storage, and vice versa."
ENT.Instructions = "Press Use to open the interface, select any in-range pumps, and queue up transfers."
ENT.Category = "Spacebuild 4"
ENT.Spawnable = false
ENT.LocalStorageNameOverride = "Global Storage"
if CLIENT then

-- Override function to stop the pully from spawning on the gs pump
function ENT:Initialize() end

elseif SERVER then
	hook.Add("CanDrive", "CanDriveGlobalPumps", function(ply, ent) --No driving GS Pumps.
		if ent:GetClass() == "sc_resource_pump_global" then
			return false
		end
	end)

	hook.Add("CanProperty", "CanPropertyGlobalPumps", function(ply, property, ent) --No using the context menu on GS Pumps either.
		if ent:GetClass() == "sc_resource_pump_global" then
			return false
		end
	end)

    -- The maximum range of a global storage pump
    if not ConVarExists("sc_resourcepump_gs_maxrange") then
	    CreateConVar("sc_resourcepump_gs_maxrange", 1000, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
    end

    if not ConVarExists("sc_resourcepump_gs_enabled") then
	    CreateConVar("sc_resourcepump_gs_enabled",1,{FCVAR_ARCHIVE, FCVAR_NOTIFY})
    end

	function ENT:Initialize()
	    self:PhysicsInit(SOLID_VPHYSICS)
	    self:SetMoveType(MOVETYPE_VPHYSICS)
	    self:SetSolid(SOLID_VPHYSICS)
	    self:DrawShadow(false)
		self:SetModel("models/props_lab/tpplugholder_single.mdl")
		self:SetSkin(4)

		base.Initialize(self)

		local phys = self:GetPhysicsObject()
	    if IsValid(phys) then
		    phys:Sleep()
		    phys:EnableGravity(false)
		    phys:EnableCollisions(true)
		    phys:EnableMotion(false)
		    phys:SetMass(50000)
	    end

		self.InboundPlayer = nil
		self.OutboundPlayer = nil

	    self.SC_Immune = true --make weapons ignore me
	    self.SB_Ignore = true --make spacebuild ignore me
	    self.Untouchable = true
	    self.Unconstrainable = true
	    self.PhysgunDisabled = true
	    self.CanTool = function()
		    return false
	    end

		--Ownership crap
	    if NADMOD then --If we're using NADMOD PP, then use its function
		    NADMOD.SetOwnerWorld(self)
	    elseif CPPI then --If we're using SPP, then use its function
		    self:SetNWString("Owner", "World")
	    else --Well fuck it, lets just use our own!
		    self.Owner = game.GetWorld()
	    end
	end

    function ENT:GetNetworkResources(connected)
        local types = {}
        local connected = self:GetOutboundPump() or connected
	    for resname in pairs(connected:GetPlayer():GetGlobalStorage()) do
		    types[resname] = true
	    end

        return types
    end

    function ENT:GetNetworkResourceAmount(name, connected)
        local connected = self:GetOutboundPump() or connected
        return connected:GetPlayer():GetGlobalStorage()[name]
    end

    function ENT:IsLinked()
        return true
    end

	function ENT:Unlink() end

	function ENT:TransferSpeedCalc() end

	function ENT:SetDesiredTransferSpeed(speed) end

    function ENT:GetMaxRange()
        return GetConVarNumber("sc_resourcepump_gs_maxrange")
    end

	function ENT:OnInboundConnect(pump)
		self.InboundPlayer = pump:GetPlayer()
	end

	function ENT:OnOutboundConnect(pump)
		self.OutboundPlayer = pump:GetPlayer()
	end

	function ENT:OnInboundDisconnect(pump)
		self.InboundPlayer = nil
	end

	function ENT:OnOutboundDisconnect(pump)
		self.OutboundPlayer = nil
	end

    function ENT:GetStoredAndEmpty(res, inbound)
		local ply = self.OutboundPlayer
		if inbound then
			ply = self.InboundPlayer
		end

        -- TODO: Replace large number with actual storage maximum
        return ply:GetGlobalStorage()[res] or 0, 1000000000 - (ply:GetGlobalStorage()[res] or 0)
    end

    -- FIXME: This function needs to make sure the resources are actually there before taking them
    function ENT:ConsumeResource(res, amount)
        local tbl = {}
		tbl[res] = amount

        self:GetOutboundPump():GetPlayer():TakeItemsFromStorage(tbl)

        return true
    end

    function ENT:SupplyResource(res, amount)
    	local tbl = {}
		tbl[res] = amount

        self:GetInboundPump():GetPlayer():AddItemsToStorage(tbl)

        return true
    end

	function ENT:GetTransferSpeed()
		if IsValid(self:GetOutboundPump()) then
			return self:GetOutboundPump():GetTransferSpeed()
		elseif IsValid(self:GetInboundPump()) then
			return self:GetInboundPump():GetTransferSpeed()
		else
			return 0
		end
	end

    local LoadedSpawnData
    local function GetSpawnData()
        if not LoadedSpawnData then
		    local FilePath = SC.DataFolder .. "/globalstorage/" .. game.GetMap():lower() .. ".txt"

		    if file.Exists(FilePath, "DATA") then
			    local rawString = file.Read(FilePath, "DATA")
                LoadedSpawnData = util.JSONToTable(rawString)
            end
        end

        return LoadedSpawnData or {}
    end

    local function SaveSpawnData(SpawnData)
        local FilePath = SC.DataFolder .. "/globalstorage/" .. game.GetMap():lower() .. ".txt"
        file.Write(FilePath, util.TableToJSON(SpawnData))
    end

    local function SpawnPump(Name, Pos)
        local Pump = ents.Create("sc_resource_pump_global")
		Pump:SetPos(Pos + Vector(0,0,72))
		Pump:Spawn()
		Pump:SetPumpName(Name)
        return Pump
    end

    local GSSpawned = false
    local function GSSpawn(enabled)
	    if enabled then
		    if not GSSpawned then
				local SpawnData = GetSpawnData()

                if SpawnData then
				    for k,v in pairs(SpawnData) do
					    SpawnPump(k, v)
				    end
                end

			    GSSpawned = true
		    end
	    else
		    if GSSpawned then
			    for k,v in pairs(ents.FindByClass("sc_resource_pump_global")) do
				    v:Remove()
			    end

			    GSSpawned = false
		    end
	    end
    end

    hook.Add("InitPostEntity", "sc_globalpumpspawn", function()
	    if GetConVarNumber("sc_resourcepump_gs_enabled") ~= 1 then return end

	    timer.Create("GSSpawnCheck", 10, 0, function()
		    if GetConVarNumber("sc_resourcepump_gs_enabled") == 1 then
			    GSSpawn(true)
		    else
			    GSSpawn(false)
		    end
	    end)
    end)
	local Admin = SC.Administration
	hook.Add("RegisterSC2Commands", "RegisterGlobalPumpCommands", function()
		-- Register any permissions we need for these commands
		Admin.RegisterPermission("AdministrateGlobalPumps", "The ability to create, modify, and remove global pumps.")

		-- Register commands
		Admin.RegisterCommand("GetGlobalPumpList", "Gets a list of pumps spawned on the map.",
			-- Permissions
			{
				"AdministrateGlobalPumps"
			},

			-- Arguments
			{},

			-- Callback
			function(Executor, Arguments)
				local SpawnData = GetSpawnData()

				if table.Count(SpawnData) > 0 then
					Executor:ChatPrint("- Global Storage Pump List - ")

					for k in pairs(SpawnData) do
						Executor:ChatPrint(k)
					end
				else
					Executor:ChatPrint("[Space Combat 2 - Global Storage] - No Global Storage Pumps have been added to this map!")
				end
			end)

		Admin.RegisterCommand("SpawnGlobalPump", "Creates a new global pump.",
			-- Permissions
			{
				"AdministrateGlobalPumps"
			},

			-- Arguments
			{
				{
					Name = "PumpName",
					Type = "string"
				}
			},

			-- Callback
			function(Executor, Arguments)
				local PumpName = Arguments.PumpName
				local SpawnData = GetSpawnData()

				if SpawnData[PumpName] then
					Executor:ChatPrint("[Space Combat 2 - Global Storage] - A pump with this name already exists, please choose another!")
					return
				else
					SpawnData[PumpName] = Executor:GetPos()
				end

				SpawnPump(PumpName, Executor:GetPos())
				SaveSpawnData(SpawnData)

				Executor:ChatPrint("[Space Combat 2 - Global Storage] - New Global Storage Pump spawn created!")
			end)

		Admin.RegisterCommand("RemoveGlobalPump", "Creates a new global pump.",
			-- Permissions
			{
				"AdministrateGlobalPumps"
			},

			-- Arguments
			{
				{
					Name = "PumpName",
					Type = "string"
				}
			},

			-- Callback
			function(Executor, Arguments)
				local PumpName = Arguments.PumpName
				local SpawnData = GetSpawnData()

				if not SpawnData then return end

				if SpawnData[PumpName] then
					SpawnData[PumpName] = nil

					for k,v in pairs(ents.FindByClass("sc_resource_pump_global")) do
						if k == PumpName then
							v:Remove()
							break
						end
					end

					SaveSpawnData(SpawnData)

					Executor:ChatPrint("[Space Combat 2 - Global Storage] - Global Storage Pump spawn removed!")
				else
					Executor:ChatPrint("[Space Combat 2 - Global Storage] - Global Storage Pump spawn doesn't exist!")
				end
            end)
        end)
end