﻿AddCSLuaFile()

ENT.Type 			    = "anim"
ENT.Base 			    = "base_moduleentity"
ENT.PrintName		    = "Ship Reactor"
ENT.Author			    = "Lt.Brandon"
ENT.Category 		    = "Space Combat"

ENT.Spawnable 		    = false
ENT.AdminSpawnable 	    = false
ENT.PGBonus             = 0
ENT.MaxTemperature      = 0
ENT.DisableAlarm        = false
ENT.CoolantToSet        = 1
ENT.ReactionToSet       = 1
ENT.TemperatureToSet    = -1
ENT.UpdateAfterCycle    = false
ENT.IsSCReactor         = true

local base = scripted_ents.Get("base_moduleentity")
local function InitReactors()
	base = scripted_ents.Get("base_moduleentity")

    local GM = GAMEMODE

    -- Fusion Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Fusion Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Harness the power of a miniature sun inside your tinfoil containment unit to produce power!")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/crylaser_small.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Fusion Reactor

    -- Fission Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Fission Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Produces Energy via the power of radioactivity. As a side effect, kind of deadly to humans. And life in general.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/battery_medium.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Fission Reactor

    -- Graviton Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Graviton Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Makes use of the seemingly magical properties of this particle that no one knows the properties of to produce Energy without radiation.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/tiberium/small_chemical_storage.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Graviton Reactor

    -- Antimatter Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Antimatter Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Collides Hydrogen and Antihydrogen inside of a powerful containment field to produce massive quantities of power.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/mandrac/energy_cell/small_cell.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Antimatter Reactor

    -- Auxiliary Fusion Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Fusion Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Harness the power of a miniature sun inside your tinfoil containment unit to produce power!")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/crylaser_small.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Fusion Reactor

    -- Fission Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Fission Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Produces Energy via the power of radioactivity. As a side effect, kind of deadly to humans. And life in general.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/battery_medium.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Fission Reactor

    -- Graviton Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Graviton Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Makes use of the seemingly magical properties of this particle that no one knows the properties of to produce Energy without radiation.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/tiberium/small_chemical_storage.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Graviton Reactor

    -- Antimatter Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Antimatter Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Collides Hydrogen and Antihydrogen inside of a powerful containment field to produce massive quantities of power.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/mandrac/energy_cell/small_cell.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Antimatter Reactor
end
hook.Remove("InitPostEntity", "sc_reactor_post_entity_init")
hook.Add("InitPostEntity", "sc_reactor_post_entity_init", InitReactors)
hook.Remove("OnReloaded", "sc_reactor_post_reloaded")
hook.Add("OnReloaded", "sc_reactor_post_reloaded", InitReactors)

local ReactorTypes = {
    "Antimatter",
    "Graviton",
    "Fusion",
    "Fission"
}

local ReactorSizes = {
    Drone = 0.5,
    Fighter = 1,
    Frigate = 2,
    Cruiser = 4,
    Battlecruiser = 6,
    Battleship = 10,
    Dreadnaught = 12,
    Titan = 14
}

local ReactorPGModifiers = {
    Antimatter = 1,
    Graviton = 1.2,
    Fusion = 0.8,
    Fission = 1.4
}

local Reactions = {}
local function AddReaction(DisplayName, ReactionType, EfficientTemperature, PowerVariable, CycleTime, Inputs, Outputs)
    local NewReaction = {}
    NewReaction.DisplayName = DisplayName
    NewReaction.ReactionType = ReactionType
    NewReaction.EfficientTemperature = EfficientTemperature
    NewReaction.PowerVariable = PowerVariable
    NewReaction.CycleTime = CycleTime
    NewReaction.Inputs = Inputs
    NewReaction.Outputs = Outputs or {}

    if not Reactions[ReactionType] then
        Reactions[ReactionType] = {}
    end

    table.insert(Reactions[ReactionType], NewReaction)
end

local Coolants = {}
local function AddCoolant(CoolantName, OutResource, HeatDissipation, AmountNeeded)
    local NewCoolant = {}
    NewCoolant.CoolantName = CoolantName
    NewCoolant.OutResource = OutResource
    NewCoolant.HeatDissipation = HeatDissipation
    NewCoolant.AmountNeeded = AmountNeeded

    table.insert(Coolants, NewCoolant)
end

-- Antimatter
AddReaction("AM + H", "Antimatter", 15000, 240, 60, {Antihydrogen=720, Hydrogen=720})
AddReaction("AM + An", "Antimatter", 24000, 320, 60, {Antianubium=80, Anubium=80})

-- Graviton
AddReaction("An + E -> -An", "Graviton", 15500, 200, 60, {Anubium=100, Energy=5000}, {Antianubium=5})
AddReaction("H + E -> -H", "Graviton", 14000, 120, 30, {Hydrogen=5000, Energy=7500}, {Antihydrogen=15})

-- Fusion
AddReaction("H + H -> D", "Fusion", 2140, 32, 120, {Hydrogen=3600}, {Deuterium=1800})
AddReaction("H + Li6 -> He4 + T", "Fusion", 4550, 12, 120, {Hydrogen=1800, ["Lithium-6"]=1800}, {Tritium=1800, ["Helium-4"]=1800})
AddReaction("H + D -> He3", "Fusion", 1380, 24, 120, {Hydrogen=1800, Deuterium=1800}, {["Helium-3"]=1800})
AddReaction("H + T -> He3", "Fusion", 4700, 8, 120, {Hydrogen=1800, Tritium=1800}, {["Helium-3"]=1800})
AddReaction("D + D -> He3 + T + H", "Fusion", 4780, 56, 120, {Deuterium=3600}, {["Helium-3"]=900, Tritium=900, Hydrogen=900})
AddReaction("T + T -> He4", "Fusion", 3875, 24, 120, {Tritium=3600}, {["Helium-4"]=1800})
AddReaction("D + T -> He4", "Fusion", 670, 80, 120, {Deuterium=1800, Tritium=1800}, {["Helium-4"]=1800})
AddReaction("He3 + He3 -> He4 + 2H", "Fusion", 6800, 48, 120, {["Helium-3"]=3600}, {["Helium-4"]=1800, Hydrogen=3600})
AddReaction("He3 + Li6 -> 2He4 + H", "Fusion", 8800, 56, 120, {["Helium-3"]=1800, ["Lithium-6"]=1800}, {["Helium-4"]=3600, Hydrogen=1800})
AddReaction("H + He3 -> He4", "Fusion", 4820, 8, 120, {["Helium-3"]=1800, Hydrogen=1800}, {["Helium-4"]=1800})

-- Fission
AddReaction("U - > DU", "Fission", 572, 16, 360, {Uranium=360}, {["Depleted Uranium"]=360})
AddReaction("DU -> Pu", "Fission", 1600, 14, 360, {Uranium=90, ["Depleted Uranium"]=360}, {Plutonium=36})
AddReaction("Pu", "Fission", 834, 20, 360, {Plutonium=240}, {})
AddReaction("P", "Fission", 773, 12, 360, {Polonium=360})
AddReaction("Pn", "Fission", 750, 48, 240, {["Polonium Nitrate"]=3600})

-- Coolant
AddCoolant("Water", "Radioactive Coolant", 4000, 80)
AddCoolant("Nitrogen", "Nitrogen", 12000, 1800)
AddCoolant("Helium-4", "Helium-4", 14400, 1500)
AddCoolant("Helium-3", "Helium-3", 16000, 1450)
AddCoolant("Nubium", "Nubium", 25000, 500)

SC = SC or {}
SC.Reactors = {}

function SC.Reactors.GetReactorSizeForClass(Class)
    return ReactorSizes[Class]
end

function SC.Reactors.GetCoolantInfo(CoolantID)
    return Coolants[CoolantID]
end

function SC.Reactors.GetCoolantTypes()
    return Coolants
end

function SC.Reactors.GetReactionTypes()
    return Reactions
end

function SC.Reactors.GetReactionInfo(ReactionType, ReactionID)
    return Reactions[ReactionType][ReactionID]
end

function SC.Reactors.GetReactorTypes()
    return ReactorTypes
end

local BasePowerGeneration = 1000
local e = 2.7182818285
function ENT:CalculatePowerGeneration(Temperature, EfficientTemperature, Size, PowerVariable)
    local Efficiency = 7.42*(e^(-Temperature/EfficientTemperature) + math.tanh(Temperature/EfficientTemperature) - 1)

    return math.floor(Efficiency*BasePowerGeneration*Size*PowerVariable), Efficiency
end

function ENT:GetBonusText()
    if not (self:GetPGBonus() > 0) or not self:GetCycling() then return end

    local Text = "-Output-\nPG: "..self:GetPGBonus()

    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    local Energy, Efficiency = self:CalculatePowerGeneration(self:GetTemperature(), Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)
    local Coolant = Coolants[self:GetReactorCoolant()]
    local CoolantAmount = math.ceil(self:GetReactorSize() * Coolant.AmountNeeded)

    Text = Text.."\n"..Energy.." Energy/s ("..(math.floor(Efficiency*10000)*0.01).."% efficiency)"
    Text = Text.."\n"..CoolantAmount.." "..Coolant.OutResource.."/s\n"

    Text = Text.."\n\nReaction Outputs:"
    for i,k in pairs(Reaction.Outputs) do
        Text = Text.."\n"..(k * self:GetReactorSize()).." "..i
    end

    return Text.."\n"
end

function ENT:GetFittingText()
    local Text = base.GetFittingText(self)

    if CLIENT then
        Text = string.format("%s\n\nPress [%s] to open menu\n", Text, string.upper(input.LookupBinding("+use")))
    end

    return Text
end

function ENT:GetActivationCostText()
    if not (self:GetPGBonus() > 0) then return end

    local Cost = self:GetCycleActivationCost()
    local Text = "\n-Input-\nPer Tick:\n"

    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    local Coolant = Coolants[self:GetReactorCoolant()]
    local CoolantAmount = math.ceil(self:GetReactorSize() * Coolant.AmountNeeded)

    Text = Text..Coolant.CoolantName..": "..CoolantAmount.."/s\n"

    if next(Cost) then
        Text = Text.."\nPer Cycle:\n"
        for i,k in pairs(Cost) do
            Text = Text..i..": "..k.."\n"
        end
    end

    Text = Text.."\nReactor Temperature: "..self:GetTemperature().."K"

    return Text
end

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "IsAuxiliary", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Temperature", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorSize", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorType", "number", 3)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorCoolant", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorReaction", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "PGBonus", "number", 0)
end

function ENT:UpdateCycleResources()
    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    local Resources = {}

    for i,k in pairs(Reaction.Inputs) do
        Resources[i] = self:GetReactorSize() * k
    end

    self:SetCycleActivationCost(Resources)
end

function ENT:ChangeReaction(NewReaction)
    if self:GetCycling() or not Reactions[ReactorTypes[self:GetReactorType()]][NewReaction] then return end

    self:SetReactorReaction(NewReaction)
    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    self:SetCycleDuration(Reaction.CycleTime)
    self:UpdateCycleResources()

    self.MaxTemperature = Reaction.EfficientTemperature

    if SERVER then
        local WIRETABLE = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
        local Production = table.Copy(WIRETABLE)
        local Consumption = table.Copy(WIRETABLE)
        local Mult = self:GetReactorSize()

        for i,k in pairs(Reaction.Outputs) do
            Production.s[i] = k * Mult
            Production.stypes[i] = "n"
        end

        for i,k in pairs(Reaction.Inputs) do
            Consumption.s[i] = k * Mult
            Consumption.stypes[i] = "n"
        end

        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Cycle Duration", Reaction.CycleTime)
            WireLib.TriggerOutput(self, "Reaction Outputs", Production)
            WireLib.TriggerOutput(self, "Reaction Inputs", Consumption)
        end
    end
end

function ENT:SetupGenerator(Generator)
    local Name = Generator:GetName()
    self:SetModuleName(Name)

    for i, k in ipairs(ReactorTypes) do
        if Name:find(k) then
            self:SetReactorType(i)

            if Name:find("Auxiliary") then
                self:SetIsAuxiliary(true)
            else
                self:SetIsAuxiliary(false)
            end

            self:SetSlot(self:GetIsAuxiliary() and "Auxiliary Reactor" or "Primary Reactor")
            self:ChangeReaction(1)

            break
        end
    end
end

if CLIENT then
    function ENT:Think()
        self:UpdateUI()
        return base.Think(self)
    end

    function ENT:SendUpdate(Active, Recipe, Coolant, MaxTemperature)
	    net.Start("sc_module_reactor_ui")
	    net.WriteEntity(self)
	    net.WriteInt(Recipe or 1, 8)
        net.WriteInt(Coolant or 1, 8)
        net.WriteFloat(MaxTemperature)
        net.WriteBit(Active or false)
	    net.SendToServer()
    end

    function ENT:UpdateUI()
        if not self:IsUIOpened() then return end

        local CurrentReaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
        self.UIPanel.TemperatureGauge.Value = self:GetTemperature() / (CurrentReaction.EfficientTemperature * 2)

        local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self.UIPanel.SelectedReaction]
        local Coolant = Coolants[self.UIPanel.SelectedCoolant]
        local Energy, Efficiency = self:CalculatePowerGeneration(Reaction.EfficientTemperature, Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)
        local CoolantAmount = math.ceil(self:GetReactorSize() * Coolant.AmountNeeded)

        local Text = Reaction.DisplayName
        Text = Text.."\n\nOutputs\n--------------\nPG: "..(math.floor(10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]))
        Text = Text.."\n"..Energy.." Energy/s ("..(math.floor(Efficiency*10000)*0.01).."% efficiency)"
        Text = Text.."\n"..CoolantAmount.." "..Coolant.OutResource.."/s\n"

        local Outputs = Reaction.Outputs
        if next(Outputs) then
            Text = Text.."\nPer Cycle:\n"
            for i,k in pairs(Outputs) do
                Text = Text..i..": "..(k * self:GetReactorSize()).."\n"
            end
        end

        Text = Text.."\n\nInputs\n--------------\n"
        Text = Text..Coolant.CoolantName..": "..CoolantAmount.."/s\n"

        local Cost = Reaction.Inputs
        if next(Cost) then
            Text = Text.."\nPer Cycle:\n"
            for i,k in pairs(Cost) do
                Text = Text..i..": "..(k * self:GetReactorSize()).."\n"
            end
        end

        self.UIPanel.RecipeInfoLabel:SetText(Text)
    end

    function ENT:OpenUI()
	    local InfoPanel = vgui.Create("DFrame")
	    InfoPanel:SetPos(100,100)
	    InfoPanel:SetSize(680,420)
	    InfoPanel:SetTitle("Reactor Control Panel")
	    InfoPanel:MakePopup()

	    local StatPan = vgui.Create("DPanel", InfoPanel)
	    StatPan:SetSize(220, 0)
	    StatPan:Dock(RIGHT)

	    local RecipeInfoLabel = vgui.Create("DLabel", StatPan)
	    RecipeInfoLabel:SetText("Reaction Information")
	    RecipeInfoLabel:SizeToContents()
        RecipeInfoLabel:Dock(TOP)
        RecipeInfoLabel:DockMargin(10, 10, 10, 10)

	    local RecipeInfoLabel2 = vgui.Create("DLabel", StatPan)
	    RecipeInfoLabel2:SetPos(30, 30)
        RecipeInfoLabel2:Dock(FILL)
        RecipeInfoLabel2:DockMargin(10, 10, 10, 10)
        RecipeInfoLabel2:SetContentAlignment(7)
        InfoPanel.RecipeInfoLabel = RecipeInfoLabel2

	    local RecipeList = vgui.Create("DListView", InfoPanel)
	    RecipeList:Clear()
	    RecipeList:SetMultiSelect(false)
	    RecipeList:AddColumn("Reaction")
	    RecipeList:AddColumn("Cycle Time")
	    RecipeList:AddColumn("Efficient Temperature")

        local SelectedReaction = self:GetReactorReaction()
        local SelectedCoolant = self:GetReactorCoolant()

        InfoPanel.SelectedReaction = SelectedReaction
	    local ListLines = {}
	    for i,k in pairs(Reactions[ReactorTypes[self:GetReactorType()]]) do
		    ListLines[i] = RecipeList:AddLine(k.DisplayName, k.CycleTime, k.EfficientTemperature.."K")
	    end
	    RecipeList:SelectItem(ListLines[self:GetReactorReaction()])
	    RecipeList.OnRowSelected = function(panel, line)
            if not IsValid(self) then return end
            SelectedReaction = line
            InfoPanel.SelectedReaction = line
            local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][SelectedReaction]
            self:SendUpdate(self:GetCycling(), SelectedReaction, SelectedCoolant, InfoPanel.MaxTemperatureGauge.Value * Reaction.EfficientTemperature * 2)
	    end

	    RecipeList:Dock(FILL)

        local CoolantList = vgui.Create("DListView", InfoPanel)
	    CoolantList:Clear()
	    CoolantList:SetMultiSelect(false)
        CoolantList:SetTall(100)
	    CoolantList:AddColumn("Coolant")
	    CoolantList:AddColumn("Output")
	    CoolantList:AddColumn("Heat Dissipation")

        InfoPanel.SelectedCoolant = SelectedCoolant
	    local CoolantListLines = {}
	    for i,k in pairs(Coolants) do
		    CoolantListLines[i] = CoolantList:AddLine(k.CoolantName, k.OutResource, k.HeatDissipation.."K per "..k.AmountNeeded)
	    end
	    CoolantList:SelectItem(CoolantListLines[self:GetReactorCoolant()])
	    CoolantList.OnRowSelected = function(panel, line)
            if not IsValid(self) then return end
            SelectedCoolant = line
            InfoPanel.SelectedCoolant = line
            local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][SelectedReaction]
            self:SendUpdate(self:GetCycling(), SelectedReaction, SelectedCoolant, InfoPanel.MaxTemperatureGauge.Value * Reaction.EfficientTemperature * 2)
	    end

	    CoolantList:Dock(TOP)

	    local StartProd, StopProd
        StartProd = vgui.Create("DButton", InfoPanel)
        StartProd:SetDisabled(self.SC_Active or self:GetCycling())
	    StartProd:SetText("Start Reactor")
	    StartProd.DoClick = function()
            if not IsValid(self) then return end
            local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][SelectedReaction]
		    self:SendUpdate(true, SelectedReaction, SelectedCoolant, InfoPanel.MaxTemperatureGauge.Value * Reaction.EfficientTemperature * 2)
            StopProd:SetDisabled(false)
            StartProd:SetDisabled(true)
	    end

	    StartProd:Dock(BOTTOM)

	    StopProd = vgui.Create("DButton", InfoPanel)
        StopProd:SetDisabled(not self.SC_Active or not self:GetCycling())
	    StopProd:SetText("Stop Reactor")
	    StopProd.DoClick = function()
            if not IsValid(self) then return end
            local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][SelectedReaction]
		    self:SendUpdate(false, SelectedReaction, SelectedCoolant, InfoPanel.MaxTemperatureGauge.Value * Reaction.EfficientTemperature * 2)
            StartProd:SetDisabled(false)
            StopProd:SetDisabled(true)
	    end

	    StopProd:Dock(BOTTOM)

        local CurrentReaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]

        local GradientUp = Material("vgui/gradient-u")
        local MaxTemperatureGauge = vgui.Create("DPanel", InfoPanel)
        MaxTemperatureGauge:Dock(LEFT)
        MaxTemperatureGauge:SetWide(30)
        MaxTemperatureGauge.Value = self.MaxTemperature / (CurrentReaction.EfficientTemperature * 2)
        function MaxTemperatureGauge:Paint(w, h)
            surface.SetDrawColor(Color(0, 150, 0, 255))
            surface.SetMaterial(GradientUp)
            surface.DrawTexturedRect(0, 0, w, h)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawTexturedRect(0, 0, w, h * 0.5)

            surface.SetDrawColor(255, 255, 0, 255)
            surface.DrawRect(0, 0.5 * h - 7, w, 14)

            surface.SetDrawColor(0, 0, 0, 250)
            self:DrawOutlinedRect()
            surface.DrawRect(0, (1 - self.Value) * h - 2, w, 3)

            surface.SetDrawColor(255, 255, 255, 250)
            surface.DrawRect(0, (1 - self.Value) * h - 1, w, 1)
        end

        MaxTemperatureGauge.OnCursorMoved = function(p, x, y)
            if not IsValid(self) then return end
            if not input.IsMouseDown(MOUSE_LEFT) then return end
            MaxTemperatureGauge.Value = 1 - math.Clamp(y / MaxTemperatureGauge:GetTall(), 0, 1)

            local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
		    self:SendUpdate(self:GetCycling(), SelectedReaction, SelectedCoolant, MaxTemperatureGauge.Value * Reaction.EfficientTemperature * 2)
        end

        function MaxTemperatureGauge:OnMousePressed()
            self:MouseCapture(true)
            self:OnCursorMoved(self:CursorPos())
        end

        function MaxTemperatureGauge:OnMouseReleased()
            self:MouseCapture(false)
            self:OnCursorMoved(self:CursorPos())
        end

        local TemperatureGauge = vgui.Create("DPanel", InfoPanel)
        TemperatureGauge:Dock(LEFT)
        TemperatureGauge:SetWide(30)
        TemperatureGauge.Value = self:GetTemperature() / (CurrentReaction.EfficientTemperature * 2)
        TemperatureGauge.Paint = function(Panel, w, h)
            surface.SetDrawColor(Color(0, 150, 0, 255))
            surface.SetMaterial(GradientUp)
            surface.DrawTexturedRect(0, 0, w, h)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawTexturedRect(0, 0, w, h * 0.5)

            surface.SetDrawColor(255, 255, 0, 255)
            surface.DrawRect(0, (1 - MaxTemperatureGauge.Value) * h - 7, w, 14)

            surface.SetDrawColor(0, 0, 0, 250)
            Panel:DrawOutlinedRect()
            surface.DrawRect(0, (1 - Panel.Value) * h - 2, w, 3)

            surface.SetDrawColor(255, 255, 255, 250)
            surface.DrawRect(0, (1 - Panel.Value) * h - 1, w, 1)
        end

        function TemperatureGauge:OnMousePressed()
            MaxTemperatureGauge:MouseCapture(true)
            MaxTemperatureGauge:OnCursorMoved(self:CursorPos())
        end

        function TemperatureGauge:OnMouseReleased()
            MaxTemperatureGauge:MouseCapture(false)
            MaxTemperatureGauge:OnCursorMoved(self:CursorPos())
        end

        InfoPanel.TemperatureGauge = TemperatureGauge
        InfoPanel.MaxTemperatureGauge = MaxTemperatureGauge

        self.UIPanel = InfoPanel
    end

    function ENT:CloseUI()
        self.UIPanel:Remove()
    end

    function ENT:IsUIOpened()
        return IsValid(self.UIPanel)
    end

    net.Receive("sc_module_reactor_ui", function()
	    local self = net.ReadEntity()
        local MaxTemp = net.ReadFloat()
        local SC_Active = net.ReadBool()

	    if not IsValid(self) then return end

        self.MaxTemperature = MaxTemp
        self.SC_Active = SC_Active

	    if not self:IsUIOpened() then
            self:OpenUI()
        end
    end)
else
    function ENT:Initialize()
        self.IsSupplyingBonuses = false
        self.LastTime = 0

        base.Initialize(self)
    end

    function ENT:Explode()
        local Core = self:GetProtector()

        if IsValid(Core) then
            local DamageMultiplier = (self:GetReactorSize() * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]) ^ 2.75
            Core:DamageHull(
                {
                    EM = 20000 * DamageMultiplier,
                    EXP = 70000 * DamageMultiplier,
                    THERM = 50000 * DamageMultiplier
                },
                self.Owner
            )
        else
            local Size = self:GetReactorSize() * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]
            local DamageMultiplier = Size ^ 2.75
            local Damage = {
                EM = 40000 * DamageMultiplier,
                EXP = 140000 * DamageMultiplier,
                THERM = 100000 * DamageMultiplier
            }

            SC.Explode(self:GetPos(), 250 * Size, 500 * Size, 12000, Damage, self.Owner, self)
        end

        local EffectData = {}
        EffectData.Origin = self:GetPos()
        EffectData.Normal = self:GetAngles():Forward()
        EffectData.Scale = 0.05 * self:GetReactorSize()

        SC.CreateEffect("impact_bigbertha", EffectData)

        timer.Simple(0, function()
            self:Remove()
        end)
    end

    duplicator.RegisterEntityClass("sc_module_reactor", GAMEMODE.MakeEnt, "Data")
    function ENT:LoadSCInfo(Info)
        self.SCDupeInfo = Info or {}

        self:SetIsAuxiliary(Info.IsAuxiliary or false)
        self:SetModuleName(Info.ReactorName or "Reactor")
        self:SetReactorType(Info.ReactorType or 3)
        self:ChangeReaction(Info.ReactorReaction or 1)
        self:SetReactorCoolant(Info.ReactorCoolant or 1)
        self:SetSlot(self:GetIsAuxiliary() and "Auxiliary Reactor" or "Primary Reactor")

        if IsValid(self:GetProtector()) then
		    self:GetProtector():UpdateModifiers()
	    end
    end

    function ENT:SaveSCInfo()
        local Data = {}
        Data["IsAuxiliary"] = self:GetIsAuxiliary()
        Data["ReactorName"] = self:GetModuleName()
        Data["ReactorType"] = self:GetReactorType()
        Data["ReactorReaction"] = self:GetReactorReaction()
        Data["ReactorCoolant"] = self:GetReactorCoolant()

		return Data
	end

    function ENT:StartSupplyingBonuses()
        if not self.IsSupplyingBonuses then
            local Core = self:GetProtector()

            if IsValid(Core) then
                Core.Fitting.PG = Core.Fitting.PG + self:GetPGBonus()
                self.IsSupplyingBonuses = true
            end
        end
    end

    function ENT:StopSupplyingBonuses()
        if self.IsSupplyingBonuses then
            local Core = self:GetProtector()

            if IsValid(Core) then
                Core.Fitting.PG = Core.Fitting.PG - self:GetPGBonus()
            end

            self.IsSupplyingBonuses = false
        end
    end

    function ENT:OnModuleEnabled()
        if self.IsSupplyingBonuses then
            self.IsSupplyingBonuses = false
            self:StartSupplyingBonuses()
        end

        local Mod = 1

        if self:GetIsAuxiliary() then
            Mod = 0.25
        end

        self:SetReactorSize(ReactorSizes[self:GetProtector():GetShipClass()] * Mod)
        self:SetPGBonus(math.floor(10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]))
        self:ChangeReaction(self:GetReactorReaction())
    end

    function ENT:OnModuleDisabled()
        self:StopSupplyingBonuses()
        self:SetReactorSize(ReactorSizes.Drone)
        self:SetPGBonus(0)
        self:ChangeReaction(self:GetReactorReaction())
    end

    function ENT:OnStartedCycling()
        -- Called when the module starts a cycle
        self:StartSupplyingBonuses()
        self:GetProtector():UpdateModifiers()
    end

    function ENT:OnCycleUpdated(TimeRemaining)
        -- Called when a module's cycle is updated
        local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
        local Energy, Efficiency = self:CalculatePowerGeneration(self:GetTemperature(), Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)

        self:SupplyResource("Energy", Energy)

        if not self.ManualTempControl then
            self.MaxTemperature = Reaction.EfficientTemperature
        end

        local Temp = (Reaction.EfficientTemperature / (Reaction.CycleTime * 0.5))
        local Coolant = Coolants[self:GetReactorCoolant()]
        local CoolantAmount = math.ceil(self:GetReactorSize() * Coolant.AmountNeeded)

        local HadCoolant = false

        if self:GetTemperature() > self.MaxTemperature * 0.95 then
            HadCoolant = self:ConsumeResource(Coolant.CoolantName, CoolantAmount)
            if HadCoolant then
                Temp = (Temp - (Coolant.HeatDissipation / (Reaction.CycleTime * 0.5)))
                self:SupplyResource(Coolant.OutResource, CoolantAmount)
            end
        end

        self:SetTemperature(self:GetTemperature() + math.floor(Temp))

        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Efficiency", Efficiency)
            WireLib.TriggerOutput(self, "Energy Output", Energy)
            WireLib.TriggerOutput(self, "Coolant Usage", HadCoolant and CoolantAmount or 0)
        end

        if self:GetTemperature() > (Reaction.EfficientTemperature * 1.2) then
            if not self.DisableAlarm then
                self:EmitSound("buttons/button_synth_negative_01.wav", 130, 50)
                self:EmitSound("buttons/button10.wav", 130, 50)
                self:EmitSound("buttons/button8.wav", 130, 75)

                timer.Simple(0.3, function()
                    if not IsValid(self) then return end
                    self:EmitSound("buttons/button10.wav", 130, 100)
                    self:EmitSound("buttons/button8.wav", 130, 125)
                end)
            end

            if self:GetTemperature() > (Reaction.EfficientTemperature * 2) then
                self:Explode()
            end
        end
    end

    function ENT:OnStoppedCycling()
        -- Called when the module finishes it's cycle.
        if not self.SC_Active or not self:CanCycle() then
            self:StopSupplyingBonuses()
        end
    end

    function ENT:OnCycleFinished()
        local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
        for i,k in pairs(Reaction.Outputs) do
            self:SupplyResource(i, k * self:GetReactorSize())
        end

        if self.UpdateAfterCycle then
            self:ChangeReaction(self.ReactionToSet)
            self:SetReactorCoolant(self.CoolantToSet)

            if math.abs(self.TemperatureToSet - self.MaxTemperature) > 50 then
                self.MaxTemperature = self.TemperatureToSet
                self.ManualTempControl = true
            end

            self.UpdateAfterCycle = false
        end
    end

    function ENT:OnCoreUpdated()
        if not self:IsProtected() then SC.Error("sc_module_reactor::OnCoreUpdated Core doesn't exist yet? WTF?", 5) return end

        -- Called when the core finishes recalculating. Commonly used to apply bonuses to modules.
        local Mod = 1

        if self:GetIsAuxiliary() then
            Mod = 0.25
        end

        self:SetReactorSize(ReactorSizes[self:GetProtector():GetShipClass()] * Mod)
        self:SetPGBonus(math.floor(10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]))
        self:ChangeReaction(self:GetReactorReaction())
    end

    function ENT:SetSlot(Slot)
        local Fitting = self:GetFitting()
        Fitting.Slot = Slot
        self:SetFitting(Fitting)
    end

    function ENT:GetWirePorts()
        return {
                "On",
                "Manual Temperature Control",
                "Maximum Temperature",
                "Disable Alarm"
            },
            {
                "Cycle Duration",
                "Cycle Percent",
                "Temperature",
                "Efficiency",
                "Energy Output",
                "Coolant Usage",
                "Reaction Inputs [TABLE]",
                "Reaction Outputs [TABLE]"
            }
    end

    function ENT:TriggerInput(Input, Value)
        if Input == "Manual Temperature Control" then
            self.ManualTempControl = Value == 1

            if not self.ManualTempControl then
                local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
                self.MaxTemperature = Reaction.EfficientTemperature
            end
        elseif Input == "Maximum Temperature" then
            self.MaxTemperature = Value
        elseif Input == "Disable Alarm" then
            self.DisableAlarm = Value == 1
        end

        base.TriggerInput(self, Input, Value)
    end

    function ENT:UpdateOutputs()
        if WireLib ~= nil then
            if not self:GetCycling() then
                WireLib.TriggerOutput(self, "Efficiency", 0)
                WireLib.TriggerOutput(self, "Energy Output", 0)
                WireLib.TriggerOutput(self, "Coolant Usage", 0)
            end

            WireLib.TriggerOutput(self, "Temperature", self:GetTemperature())
        end
    end

    function ENT:Think()
        if not self:GetCycling() and self:GetTemperature() > 0 then
            self:SetTemperature(self:GetTemperature() - 25)

            if self:GetTemperature() < 0 then
                self:SetTemperature(0)
            end
        end

        self:UpdateOutputs()

        return base.Think(self)
    end

    function ENT:Use(ply)
        if not IsValid(ply) or not ply:IsPlayer() or (self.CPPICanTool and not self:CPPICanTool(ply)) then return end

	    net.Start("sc_module_reactor_ui")
	    net.WriteEntity(self)
        net.WriteFloat(self.MaxTemperature)
        net.WriteBool(self.SC_Active)
	    net.Send(ply)
    end

    net.Receive("sc_module_reactor_ui", function(Length, Player)
        if not IsValid(Player) then return end

	    local self = net.ReadEntity()
        if not IsValid(self) then return end
        if self.CPPICanTool and not self:CPPICanTool(Player) then return end

        local Reaction = net.ReadInt(8)
        local Coolant = net.ReadInt(8)
        local MaxTemperature = net.ReadFloat()

        self:TriggerInput("On", net.ReadBit())

        if not self:GetCycling() then
            self:ChangeReaction(Reaction)
            self:SetReactorCoolant(Coolant)
        elseif Coolant ~= self:GetReactorCoolant() or Reaction ~= self:GetReactorReaction() then
            self.CoolantToSet = Coolant
            self.ReactionToSet = Reaction
            self.TemperatureToSet = MaxTemperature
            self.UpdateAfterCycle = true

            return
        end

        if math.abs(MaxTemperature - self.MaxTemperature) > 50 then
            self.MaxTemperature = MaxTemperature
            self.ManualTempControl = true
        end
    end)
end