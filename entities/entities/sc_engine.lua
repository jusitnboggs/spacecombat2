AddCSLuaFile()

DEFINE_BASECLASS("base_scentity")

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName	= "Vehicle Engine"
ENT.Author	= "Lt.Brandon"

ENT.Spawnable	= false
ENT.AdminSpawnable = false
ENT.IsDriveLineComponent = false
ENT.IsDriveBeltComponent = false
ENT.IsEngine = true
ENT.IsMechanical = true

-- RPM
ENT.RedlineRPM = 5200
ENT.LimitRPM = 6000
ENT.IdleRPM = 700
ENT.CurrentRPM = 0
ENT.EngineDrag = ENT.IdleRPM * 1.5

-- Torque
ENT.CurrentTorque = 0
ENT.IdleTorque = 160
ENT.LowTorque = 285
ENT.LowTorqueRPM = 1400
ENT.PeakTorque = 345
ENT.PeakTorqueRPM = 2200
ENT.HighTorque = 330
ENT.HighTorqueRPM = 4400
ENT.RedlineTorque = 210

-- Flywheel
ENT.FlywheelWeight = 2.6
ENT.FlywheelRadius = 4
ENT.FlywheelInertia = 0

-- Throttle
ENT.ThrottleSpeed = 5
ENT.Throttle = 0
ENT.WantedThrottle = 0

-- Sounds
ENT.RunningSound = "vehicles/airboat/fan_motor_fullthrottle_loop1.wav"
ENT.IgnitionSound = ""

-- Connections
ENT.DriveBeltComponents = {}
ENT.DriveLineComponents = {}

local base = scripted_ents.Get("base_scentity")
hook.Remove("SC.Config.PostLoad", "sc_engine_init")
hook.Add("SC.Config.PostLoad", "sc_engine_init", function()
    base = scripted_ents.Get("base_scentity")

    local GM = GAMEMODE

    GM.Mechanical = GM.Mechanical or {}
    GM.Mechanical.Engines = {}
    local Engines = GM.Mechanical.Engines

    function GM.Mechanical.RegisterEngine(ID, Data)
        -- TODO: Validate incoming data
        Engines[ID] = Data
    end

    GM.Mechanical.RegisterEngine("Default", {
        Name = "SC Engine",
        Description = "Default engine, for testing.",
        Model = "models/engines/i8med.mdl",
        IgnitionSound = "",
        RunningSound = "vehicles/airboat/fan_motor_fullthrottle_loop1.wav",
        RedlineRPM = 5200,
        LimitRPM = 6000,
        IdleRPM = 700,
        EngineDrag = 1.5,
        IdleTorque = 160,
        LowTorque = 285,
        LowTorqueRPM = 1400,
        PeakTorque = 345,
        PeakTorqueRPM = 2200,
        HighTorque = 330,
        HighTorqueRPM = 4400,
        RedlineTorque = 210,
        FlywheelWeight = 2.6,
        FlywheelRadius = 4,
        ThrottleSpeed = 5,
    })

    hook.Run("SC.RegisterEngines")
    hook.Run("SC.PostRegisterEngines")
end)

function ENT:SharedInit()
    SC.NWAccessors.CreateNWAccessor(self, "EngineType", "cachedstring", "Default", 1, function(self, Name, Value)
        if not CLIENT then return end
        if IsValid(self) then
            self:SetupEngine(Value)
        end
    end)
end

function ENT:Initialize()
    base.Initialize(self)

    if CLIENT then return end

    self.DriveBeltComponents = self.DriveBeltComponents or {}
    self.DriveLineComponents = self.DriveLineComponents or {}

    WireLib.CreateSpecialInputs(self, {"Throttle"}, {"NORMAL"})
    WireLib.CreateSpecialOutputs(self, {"Throttle", "RPM", "Torque"}, {"NORMAL", "NORMAL", "NORMAL"})
end

function ENT:OnRemove()
    if self.EngineSound then
        self.EngineSound:Stop()
        self.EngineSound = nil
    end
end

local OverlayTextFormat = [[
- %s -

Power: %s HP
Peak Torque %s lb-ft
Powerband: %s - %s RPM
Powerband Peak: %s RPM
Redline: %s RPM
]]

function ENT:SetupEngine(EngineType)
    local EngineData = GAMEMODE.Mechanical.Engines[EngineType]
    if not EngineData then
        SC.Error(string.format("Invalid engine type %s, changing to default engine", tostring(EngineType)), 5)
        EngineData = GAMEMODE.Mechanical.Engines["Default"]
        EngineType = "Default"
    end

    self.IgnitionSound = EngineData.IgnitionSound
    self.RunningSound = EngineData.RunningSound
    self.RedlineRPM = EngineData.RedlineRPM
    self.LimitRPM = EngineData.LimitRPM
    self.IdleRPM = EngineData.IdleRPM
    self.IdleTorque = EngineData.IdleTorque
    self.LowTorque = EngineData.LowTorque
    self.LowTorqueRPM = EngineData.LowTorqueRPM
    self.PeakTorque = EngineData.PeakTorque
    self.PeakTorqueRPM = EngineData.PeakTorqueRPM
    self.HighTorque = EngineData.HighTorque
    self.HighTorqueRPM = EngineData.HighTorqueRPM
    self.RedlineTorque = EngineData.RedlineTorque
    self.FlywheelWeight = EngineData.FlywheelWeight
    self.FlywheelRadius = EngineData.FlywheelRadius
    self.ThrottleSpeed = EngineData.ThrottleSpeed
    self.EngineDrag = EngineData.EngineDrag

    -- FIXME: I'm 90% sure this math is wrong, and I don't remember where/how I got it, but it works so screw it for now.
    self.FlywheelInertia = 0.5 * (((self.FlywheelWeight * 4.4482) / 9.8) * 2.2046) * self.FlywheelRadius * math.pi

    if SERVER then
        self:SetModel(EngineData.Model)
        self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
        self:SetUseType(SIMPLE_USE)

        self:SetEngineType(EngineType)

        self.EngineSound = CreateSound(self.Entity, self.RunningSound)
        self.EngineSound:Play()

        self:SetOverlayText(string.format(
            OverlayTextFormat,
            EngineData.Name,
            math.Round(self.PeakTorque * self.PeakTorqueRPM / 5252),
            self.PeakTorque,
            self.LowTorqueRPM,
            self.HighTorqueRPM,
            self.PeakTorqueRPM,
            self.RedlineRPM
        ))
    end

    self.SetupFinished = true
end

function ENT:IsLinked(Linked)
    if not IsValid(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        return table.HasValue(self.DriveLineComponents, Linked)
    elseif Linked.IsDriveBeltComponent then
        return table.HasValue(self.DriveBeltComponents, Linked)
    else
        return false
    end
end

function ENT:Link(Linked)
    if not IsValid(Linked) then
        return false
    end

    if self:IsLinked(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        table.insert(self.DriveLineComponents, Linked)
    elseif Linked.IsDriveBeltComponent then
        table.insert(self.DriveBeltComponents, Linked)
    else
        return false
    end

    return true
end

function ENT:Unlink(Linked)
    if not self:IsLinked(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        table.RemoveByValue(self.DriveLineComponents, Linked)
    elseif Linked.IsDriveBeltComponent then
        table.RemoveByValue(self.DriveBeltComponents, Linked)
    else
        return false
    end

    return true
end

function ENT:Think()
    if not SERVER then return end
    if not self.SetupFinished then return end

    self.Throttle = Lerp(FrameTime() * self.ThrottleSpeed, self.Throttle, self.WantedThrottle)

    -- All of the calculations are based on RPM
    -- So multiply Time by 60 to get the correct numbers
    local Time = FrameTime() * 60

    -- Determine the current engine torque output
    if self.CurrentRPM >= self.RedlineRPM then
        EngineTorque = Lerp((self.CurrentRPM - self.RedlineRPM) / (self.LimitRPM - self.RedlineRPM), self.RedlineTorque, 0)
    elseif self.CurrentRPM >= self.HighTorqueRPM then
        EngineTorque = Lerp((self.CurrentRPM - self.HighTorqueRPM) / (self.RedlineRPM - self.HighTorqueRPM), self.HighTorque, self.RedlineTorque)
    elseif self.CurrentRPM >= self.PeakTorqueRPM then
        EngineTorque = Lerp((self.CurrentRPM - self.PeakTorqueRPM) / (self.HighTorqueRPM - self.PeakTorqueRPM), self.PeakTorque, self.HighTorque)
    elseif self.CurrentRPM >= self.LowTorqueRPM then
        EngineTorque = Lerp((self.CurrentRPM - self.LowTorqueRPM) / (self.PeakTorqueRPM - self.LowTorqueRPM), self.LowTorque, self.PeakTorque)
    else
        EngineTorque = Lerp((self.CurrentRPM - self.IdleRPM) / (self.LowTorqueRPM - self.IdleRPM), self.IdleTorque, self.LowTorque)
    end

    self.CurrentRPM = self.CurrentRPM + (EngineTorque * Time / self.FlywheelInertia)
    self.CurrentTorque = EngineTorque

    local SoundPitch = 35 + Lerp(self.CurrentRPM / self.LimitRPM + (self.Throttle * 0.2), 0, 90)
    self.EngineSound:ChangePitch(SoundPitch, 0)

    -- Determine how much torque is needed to run all of our components
    local AvailableInertia = math.max(self.CurrentRPM - (self.IdleRPM * 0.9), 0) * self.FlywheelInertia
    local UsedTorque = 0

    -- Update all of the components with their alloted torque
    for Index, Component in pairs(self.DriveBeltComponents) do
        if Component.OnEngineUpdate then
            UsedTorque = UsedTorque + (Component:OnEngineUpdate(Time, self.CurrentRPM, AvailableInertia) or 0)
        end
    end

    for Index, Component in pairs(self.DriveLineComponents) do
        if Component.OnEngineUpdate then
            UsedTorque = UsedTorque + (Component:OnEngineUpdate(Time, self.CurrentRPM, AvailableInertia) or 0)
        end
    end

    local Drag
    -- If we go over the RPM limit then always give the full amount of drag
    if self.CurrentRPM > self.LimitRPM then
        Drag = math.max(EngineTorque, self.IdleTorque) * self.EngineDrag * Time

    -- If we go under idle then just don't apply any drag..
    -- This isn't super realistic or anything,
    -- but if we go under this then we're probably going to stall anyways
    elseif self.CurrentRPM < self.IdleRPM then
        Drag = 0

    -- Otherwise, just apply drag based on how much throttle is applied
    -- Again, not super realistic, but it works as an approximation
    else
        Drag = math.max(EngineTorque, self.IdleTorque) * self.EngineDrag * (1 - math.max(self.Throttle, 0.1)) * Time
    end

    -- Consume the used intertia from the flywheel
    self.CurrentRPM = math.max(self.CurrentRPM - ((UsedTorque + Drag) / self.FlywheelInertia), self.IdleRPM * 0.1)

    self:UpdateOutputs()

    self:NextThink(CurTime())
	return true
end

function ENT:TriggerInput(Name, Value)
    if Name == "Throttle" then
        self.WantedThrottle = math.Clamp(Value / 100, 0, 1)
    end
end

function ENT:UpdateOutputs()
    if WireLib then
        WireLib.TriggerOutput(self, "Throttle", self.Throttle)
        WireLib.TriggerOutput(self, "RPM", self.CurrentRPM)
        WireLib.TriggerOutput(self, "Torque", self.CurrentTorque)
    end
end

function ENT:SaveSCInfo()
    local DriveLineComponentIDs = {}
    for _, Component in ipairs(self.DriveLineComponents) do
        if IsValid(Component) then
            table.insert(DriveLineComponentIDs, Component:EntIndex())
        end
    end

    local DriveBeltComponentIDs = {}
    for _, Component in ipairs(self.DriveBeltComponents) do
        if IsValid(Component) then
            table.insert(DriveBeltComponentIDs, Component:EntIndex())
        end
    end

    return {
        EngineType = self:GetEngineType(),
        DriveLineComponents = DriveLineComponentIDs,
        DriveBeltComponents = DriveBeltComponentIDs
    }
end

function ENT:ApplySCDupeInfo(Info, GetEntByID)
    self:SetupEngine(Info.EngineType or "Default")

    if Info.DriveLineComponents then
        self.DriveLineComponents = {}
        for _, EntIndex in ipairs(Info.DriveLineComponents) do
            local Entity = GetEntByID(EntIndex)
            if IsValid(Entity) and Entity.IsDriveLineComponent then
                table.insert(self.DriveLineComponents, Entity)
            end
        end
    end

    if Info.DriveBeltComponents then
        self.DriveBeltComponents = {}
        for _, EntIndex in ipairs(Info.DriveBeltComponents) do
            local Entity = GetEntByID(EntIndex)
            if IsValid(Entity) and Entity.IsDriveBeltComponent then
                table.insert(self.DriveBeltComponents, Entity)
            end
        end
    end
end

duplicator.RegisterEntityClass("sc_engine", GAMEMODE.MakeEnt, "Data")
