AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

-- TODO: Move this to a config file
if SERVER then
	if not ConVarExists("sc_core_canrefillsuit") then
		CreateConVar("sc_core_canrefillsuit", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
	end
end

local MaxShieldHitFXPerSecond = 25 --Capped to prevent collisions from spawning thousands of shield hits within a few seconds, flooding the system
local BaseEnergyCost = 250
local EnergyPerPlayer = 75
local OxygenPerPlayer = 10
local CO2PerPlayer = 5
local LSCheckRange = 1000	--Both up and down this distance, from the player's feet (origin)

function ENT:Initialize()
    self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
    self:SharedInit()

	self.timer = CurTime()
	self.OneSecond = CurTime() + 1

	if WireLib ~= nil then
        self.Inputs = WireLib.CreateInputs(self, {
            "ShieldOn",
            "LifeSupportOn"
        })

		self.Outputs = WireLib.CreateOutputs( self, {
            "Shield",
            "Armor",
            "Hull",
            "Capacitor",
            "Max Shield",
            "Max Armor",
            "Max Hull",
            "Max Capacitor",
            "% Shield",
            "% Armor",
            "% Hull",
            "% Capacitor"
		})
	end

	self.Slots = {}

	self.Fitting = {
		CPU		= 50,
		CPU_R	= 1.00,
		CPU_U	= 0,
		PG		= 50,
		PG_R	= 1.00,
		PG_U	= 0,
	}

	self.MaxDirectDamageMultiplier = 10
	self.MinDirectDamageMultiplier = 1.5
	self.DirectDamageMultiplier = 1.5
	self.DirectDamageResistance = 0
	self.DirectHitCounter = 0
	self.DirectHitsForMaxDamage = 15
	self.DirectHitResetTime = 5
	self.LastDirectHit = 0

	self.IsAlive = true

	self.ToolSettings = {}

	self.sigrad = 1

	self.ShieldOn = true
	self.ShieldRegenAllowed = true

    self.DeathTime = 3
	self.CooldownEnd = 0
	self.PendingRecalc = false
    self.PendingRecalcLite = false
    self.DisableLinkUpdate = false
	self:SetOctreeDelay(CurTime())
	self.LastPos = vector_origin
	self.LastThink = CurTime()

	self.DamageEffects = {0, 0, 0} 	--Low, Mid, High; Used to limit the number of damage effects (smoke/fire) on a ship
	self.DamageEffectPercents = {}		--Keeps track of when particle effects would clean themselves up
    self.EffectCounts = {0, 0, 0}
	self.ShieldHitsLastSecond = 0

    self.ConWeldTable = {self}
    self.Vehicles = {}
    self.ManagedPlayers = {}

	self.IsNode = true
    self.resource_deltas = {}
    self.LastStorageSync = CurTime()

    self.ShieldData = SC.ShieldTypes["Hybrid"]
	self.ArmorData = SC.ArmorTypes["Standard"]
	self.HullData = SC.HullTypes["Standard"]
end

--Deprecated wrapper functions for capacitor
function ENT:SupplyCap(amount)
    return self:SupplyResource("Energy", amount)
end

function ENT:ConsumeCap(amount)
    return self:ConsumeResource("Energy", amount)
end

function ENT:SetCapAmount(amount)
    return self:SetAmount("Energy", amount)
end

function ENT:SetCapMax(amount)
    self:GetStorageOfType("Energy"):Resize(amount)
    self:GetStorageOfType("Energy"):SetMaxAmount("Energy", amount)
end

function ENT:ApplySettings(Options, NoRecalculate)
	self.ToolOptions = Options or {}

	--Apply settings from tool
	self.ShieldColor = self.ToolOptions.ShieldColor or Color(255,255,255,255)
	self.ShieldType = self.ToolOptions.ShieldType or "Hybrid"
	self.ArmorType = self.ToolOptions.ArmorType or "Standard"
	self.HullType = self.ToolOptions.HullType or "Standard"
	self:SetShipName(self.ToolOptions.ShipName or "Shippy McShipface")

	self.ShieldData = SC.ShieldTypes[self.ShieldType] or SC.ShieldTypes["Hybrid"]
	self.ArmorData = SC.ArmorTypes[self.ArmorType] or SC.ArmorTypes["Standard"]
	self.HullData = SC.HullTypes[self.HullType] or SC.HullTypes["Standard"]

	self.BaseShieldRes = self.ShieldData.Resistances
	self.BaseArmorRes = self.ArmorData.Resistances
	self.BaseHullRes = self.HullData.Resistances

	self.HullRes = table.Copy(self.BaseHullRes)
	self.ArmorRes = table.Copy(self.BaseArmorRes)
	self.ShieldRes = table.Copy(self.BaseShieldRes)

    self:SetIsStation(self.HullType == "Station")

	if self:GetIsStation() then
		self:CoreCooldown(30)
		self.LastPos = self:GetPos()
    else
        self:CoreCooldown(0)
	end

	--Force a recalculation
	if not NoRecalculate then
		self:UpdateConstraints()
    else
        self:UpdateModifiers()
	end
end

function ENT:UpdateConstraints()
	self.ConstraintsChanged = true
	self:CalculateOctree()
end

function ENT:OnRemove()
	local mul = math.Clamp(self.Hull.Percent,0.01,1)

	for _,i in pairs(self.ConWeldTable) do
		if IsValid(i) and i.SC_Health and i.SC_MaxHealth then --Don't want errors if the ents been removed or messed up
			i.SC_Health = math.Clamp(i.SC_MaxHealth * mul, SC.MinHealth(), SC.MaxHealth())
            i:SetProtector(nil)
		end
	end

	self:UnlinkAll()

	if WireLib ~= nil then WireLib.Remove(self) end
end

function ENT:SaveSCInfo()
	local Info = {}

	Info.tooloptions = self.ToolOptions
    Info.range = self:GetNodeRadius()

	return Info
end

function ENT:ApplySCDupeInfo(Info, GetEntByID)
    if not Info then
        self:SetNodeRadius(16000)
        self:ApplySettings({}, true)
        self.Owner:ChatPrint("[Space Combat 2] - Please update your ship core!")
        return
    end

	self:SetNodeRadius(Info.range or 16000)
	self:ApplySettings(Info.tooloptions, true)
end

hook.Add("AdvDupe_FinishPasting", "SC_CoreDupe", function(TimedPasteData, TimedPasteDataCurrent)
	local Entities = TimedPasteData[TimedPasteDataCurrent].CreatedEntities
	for i,k in pairs(Entities) do
		if IsValid(k) and k.UpdateConstraints ~= nil then
			k:UpdateConstraints()
		end
	end
end)

--Core menu
function ENT:Use(ply,call)
	local data = {Core = self,
				  Owner = self.Owner,
				  Fitting = self.Fitting,
				  Slots = self.Slots,
				  HullRes = self.HullRes,
				  ArmorRes = self.ArmorRes,
				  ShieldRes = self.ShieldRes,
				  Mods = self.Mods,
				  Linked = self:GetLinks()} --TODO: replace with clientside code, this is already networked

	net.Start("Ship_Core_UMSG")
		net.WriteTable(data)
	net.Send(ply)
end

net.Receive("Ship_Core_Update", function(len,ply)
	local ent = net.ReadEntity()
	local data = net.ReadTable()

	if data.ShieldOn ~= nil then
		ent:SetShieldActive(data.ShieldOn)
	end

	if data.Name ~= nil then
        ent:SetShipName(data.Name)
        ent.ToolOptions = ent.ToolOptions or {}
        ent.ToolOptions.ShipName = ent:GetShipName()
	end

	if data.HitEffect ~= nil then
		ent.HitEffect = data.HitEffect
	end
end)

--Default Explosion Function
function ENT:ExplosionFunction()
	ErrorNoHalt("WARN: Default Explosion Function for class " .. self:GetShipClass() .. ", replace me!\n")

    --Make it all boom :)
	local effectdata = {}
	effectdata.Magnitude = 1

	local Pos = self:GetPos()

	effectdata.Origin = Pos
	local scale = math.Clamp(((self.Hull.Max + self.Armor.Max + self.Shield.Max)/3)/125000,0.2,2.5)
	effectdata.Scale = 1 * scale

	SC.CreateEffect("warpcore_breach", effectdata)

	--Make a large boom sound
	self:EmitSound( "explode_9" )

    self:PostExplode()

	--Damage bitches from boom
	SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
end

--Check if any damage effects can be "cleaned up"
--Note: Server has no control over particle effects other than dispatching
--		them to clients, this just checks to see if any of them would have
--		cleaned themselves up due to repairs.
local function DamageEffectCleanupCheck(self)
	local curHP = (self:GetArmorAmount() + self:GetHullAmount()) / (self:GetArmorMax() + self:GetHullMax())

	for k,v in pairs(self.DamageEffectPercents) do
		local percent = v[1]
		local level = v[2]

		if curHP >= percent then
			--Tick down counter and remove this tracker
			self.DamageEffects[level] = self.DamageEffects[level] - 1
			table.remove(self.DamageEffectPercents, k)
		end
	end
end

--Spawn a random damage effect on the hull
local function HullDamageEffect(self, level, cutoffPercent)
	if not IsValid(self) then return end

	local trace = SC.GetRandomHullTrace(self)

	if trace.Hit and IsValid(trace.Entity) and trace.Entity:GetProtector() == self then
		--Setup Effect
		local effect = {}
		effect.Origin = trace.HitPos
		effect.Normal = trace.HitNormal
		effect.Entity = self
		effect.Scale = math.Clamp(20 * ((self.sigrad/39.3700787) / 385), 1, 20) --Maximum scale of 20; 385m is the sigrad of the Eversor, because fuck reasonability
		effect.Magnitude = cutoffPercent

		--Light Damage
		if not level or level == 1 then
			SC.CreateEffect("sc_hulldamage_smoke", effect)
		--Medium Explosion
		elseif level == 2 then
			SC.CreateEffect("sc_hulldamage_sparks", effect)
		--Large Explosion
		elseif level == 3 then
			SC.CreateEffect("sc_hulldamage_fire", effect)
		end
	end
end

--Calculate how many effects to spawn for a given health range (level = decrements of 25%, i.e. 75%, 50%, 25%)
local function DoDamageEffect(self, level, hpPercent)
	if self.DamageEffects[level] < self.EffectCounts[level] then
		local stride = 0.25 / self.EffectCounts[level]

		local curHP = hpPercent - (0.25 * (level - 1))
		local effectCount = self.EffectCounts[level] - math.floor(self.EffectCounts[level] * (curHP / 0.25))

		for i = self.DamageEffects[level], effectCount do
			--No more effects can be spawned, stop
			if self.DamageEffects[level] >= self.EffectCounts[level] then break end

			local cutoffPercent = ((self.EffectCounts[level] - i) * stride) + (0.75 - (0.25 * level))
			HullDamageEffect(self, level, cutoffPercent * 100)

			--Record to tracker tables
			self.DamageEffects[level] = self.DamageEffects[level] + 1
			table.insert(self.DamageEffectPercents, { cutoffPercent, level })
		end
	end
end

--Shows a shield hit effect at the given pos
local function DoShieldHitEffect(hitdata, damage)
	if damage == nil or damage == {} then return end
	if hitdata == nil or hitdata == {} then return end

	--Calculate total damage
	local totalDamage = SC.TotalDamage(damage)
	if totalDamage <= 0 then return end

	--Scale effect based on damage
	local scale = math.Clamp(10 * (totalDamage / 500000), 1, 10)

	--------------------------
	-- Determine Effect Type
	--------------------------

	--Get primary damage type
	local bigDamageType = nil
	local bigDamageAmount = nil
	for type, amount in pairs(damage) do
		if bigDamageType == nil or bigDamageAmount < amount then
			bigDamageType = type
			bigDamageAmount = amount
		end
	end

	--Set type
	local effectType = 0
	if bigDamageType == "KIN" then effectType = 1
	elseif bigDamageType == "THERM" then effectType = 2
	elseif bigDamageType == "EXP" then effectType = 3
	elseif bigDamageType == "EM" then effectType = 4
	end

	--Spawn effect
	local effect = {}
	effect.Origin = hitdata.HitPos
	effect.Normal = hitdata.HitNormal
	effect.Entity = hitdata.Entity
	effect.Scale = scale
	effect.Magnitude = effectType

	SC.CreateEffect("sc_shield_hit", effect)
end

--Plays Armor Hit Sounds
local MIN_DAMAGE = 24000		--Fighter Autocannon
local MAX_DAMAGE = 6000000		--Capital Beam Laser (scaling based on Capital MACs makes all the small weapons sound like rattling a tin can)
local function DoArmorHitSound(self, hitdata, damage)
	if damage == nil or damage == {} then return end
	if hitdata == nil or hitdata == {} then return end

	--Calculate total damage
	local totalDamage = SC.TotalDamage(damage)
	if totalDamage <= 0 then return end

	--Get primary damage type
	local bigDamageType = nil
	local bigDamageAmount = nil
	for type, amount in pairs(damage) do
		if bigDamageType == nil or bigDamageAmount < amount then
			bigDamageType = type
			bigDamageAmount = amount
		end
	end

	--Play Sound
	if SC.ArmorSounds[bigDamageType] then
		if not self.SC_ArmorSound then
			local sound = table.Random(SC.ArmorSounds[bigDamageType])

			--This makes sense if you graph it out
			local fraction = ((totalDamage - MIN_DAMAGE) / (MAX_DAMAGE - MIN_DAMAGE))
			local pitch = -1 * (math.log(fraction, 10)) * 75
			pitch = math.Clamp(pitch + math.random(-10, 10), 30, 225)

			self:EmitSound(sound, 100, pitch)
			if IsValid(hitdata.Entity) then
				hitdata.Entity:EmitSound(sound, 100, pitch)
			end

			self.SC_ArmorSound = true
			timer.Simple(0.05, function() if IsValid(self) then self.SC_ArmorSound = false end end)
		end
	end
end

function ENT:DamageHull(damage, attacker, hitdata)
	--And now we get to dig into the hull!
	local HullDamageTable = {}
	local HullDamage = 0
	for i,k in pairs(damage) do
		HullDamageTable[i] = k * (1 - (self.HullRes[i] or 0))
		HullDamage = HullDamage + HullDamageTable[i]
	end

	--did we live?
	if self.Hull.Amount > HullDamage then
		self:ConsumeHull(HullDamage)
	else
        self:SetHullAmount(0)
		self:ExplodeShip(0, attacker)
	end
end

function ENT:DamageArmor(damage, attacker, hitdata)
	--How much damage are we going to do to the armor?
	local ArmorDamageTable = {}
	local ArmorDamage = 0
    local TotalDamage = 0

    --Clang!
	DoArmorHitSound(self, hitdata, damage)

	for i,k in pairs(damage) do
		ArmorDamageTable[i] = k * (1 - (self.ArmorRes[i] or 0))
		ArmorDamage = ArmorDamage + ArmorDamageTable[i]
        TotalDamage = TotalDamage + k
	end

	--Can we tank it yet?
	if self.Armor.Amount >= ArmorDamage then
		self:ConsumeArmor(ArmorDamage)
	else
	--If not then redo the previous calculation to find the leftover
		if self.Armor.Amount > 0 then
			local leftover = (self.Armor.Amount - ArmorDamage) * -1

			--Recalc the Damage of each type that the armor didn't take
			local RetDamage = {}
			for i,k in pairs(damage) do
				RetDamage[i] = (k / TotalDamage) * (leftover / (1 - (self.ArmorRes[i] or 0)))
			end

			damage = RetDamage

			--Nuke that armor hard
			self:SetArmorAmount(0)
		end

		self:DamageHull(damage, attacker, hitdata)
	end
end

function ENT:DamageShield(hitent, damage, attacker, hitdata)
	--How much of the mainly flavors of pain do we use?
	local ResistedDamageTable = {}
	local ShieldDamageTable = {}
	local ShieldDamage = 0
	local TotalDamage = 0
	for i,k in pairs(damage) do
		ResistedDamageTable[i] = k * (self.ShieldRes[i] or 0)
		ShieldDamageTable[i] = k * (1 - (self.ShieldRes[i] or 0))
		ShieldDamage = ShieldDamage + ShieldDamageTable[i]
		TotalDamage = TotalDamage + k
	end

	if self.ShieldOn and self.ShieldRegenAllowed and self.ShieldData.Efficiency < 0 then
		local EnergyMultipliers = {
			EM = 1.0,
			KIN = 0.25,
			EXP = 0.15,
			THERM = 0.45
		}

		local Energy = 0
		for i,k in pairs(EnergyMultipliers) do
			if ResistedDamageTable[i] then
				Energy = Energy + (k * ResistedDamageTable[i] * math.abs(self.ShieldData.Efficiency))
			end
		end

		self:SupplyResource("Energy", Energy)
	end

	--If the shield can tank us then just subtract from the shield health and play a sound
	if self.ShieldOn and self.Shield.Amount > ShieldDamage then
		self:ConsumeShield(ShieldDamage)

		if self.ShieldHitsLastSecond < MaxShieldHitFXPerSecond then
			DoShieldHitEffect(hitdata, damage)
			self.ShieldHitsLastSecond = self.ShieldHitsLastSecond + 1
		end

		if not hitent.SC_ShieldSound then
			hitent:EmitSound(SC.ShieldSounds[math.random(1,#SC.ShieldSounds)], 90, math.Rand(90,110))
			hitent.SC_ShieldSound = true
			timer.Simple(0.025, function() if IsValid(hitent) then hitent.SC_ShieldSound = false end end)
		end

		if not self.SC_ShieldSound then
			self:EmitSound(SC.ShieldSounds[math.random(1,#SC.ShieldSounds)], 90, math.Rand(90,110))
			self.SC_ShieldSound = true
			timer.Simple(0.025, function() if IsValid(self) then self.SC_ShieldSound = false end end)
		end

	--If it can't lets dig into their armor!
	else
		if self.Shield.Amount >= 0 and self.ShieldOn then
			--After we hit the shield how much damage is left over?
			local leftover = math.abs(self.Shield.Amount - ShieldDamage)

			--Recalc the Damage of each type that the shield didn't take
			local RetDamage = {}
			for i,k in pairs(damage) do
				RetDamage[i] = (k / TotalDamage) * (leftover / (1 - (self.ShieldRes[i] or 0)))
			end

			damage = RetDamage

			--Set the shield health to 0
			self:SetShieldAmount(0)

			--TODO: Shield break effect?
			DoShieldHitEffect(hitdata, damage)

			--Store the previous settings for ShieldOn
			local ShieldOn = self.ShieldOn

			--Prevent the shield from recharging for a bit and stop the effect from rendering
			self.ShieldRegenAllowed = false
			self.ShieldOn = false

			--Setup a timer to enable the regen and effect again
			timer.Simple(math.max(self:GetShieldRechargeTime() * 0.05, 5), function()
				if IsValid(self) then
					if ShieldOn then
						local Amount = self.Shield.Max / 10
						local EnergyNeeded
						if self.ShieldData.Efficiency < 0 then
							EnergyNeeded = Amount * (1 - self.ShieldData.Efficiency)
						else
							EnergyNeeded = Amount * self.ShieldData.Efficiency
						end

						if self:HasResource("Energy", EnergyNeeded) then
							self:ConsumeResource("Energy", EnergyNeeded)
							self:SetShieldAmount(Amount)
						end
					end

					self.ShieldRegenAllowed = true
					self.ShieldOn = ShieldOn
				end
			end)
		end

		self:DamageArmor(damage, attacker, hitdata)
	end
end

function ENT:ApplyDamage(damage, attacker, inflictor, hitdata)
    local hitent = hitdata.Entity
	if hitent == self then
		self.DirectDamageMultiplier = math.Remap(self.DirectHitCounter / self.DirectHitsForMaxDamage, 0, 1, self.MinDirectDamageMultiplier, self.MaxDirectDamageMultiplier)
		self.DirectHitCounter = math.min(self.DirectHitCounter + 1, self.DirectHitsForMaxDamage)
		self.LastDirectHit = CurTime()

		for i,k in pairs(damage) do
			damage[i] = k * self.DirectDamageMultiplier * (1 - self.DirectDamageResistance)
		end
	end

	if self:GetOnCooldown() then
		self:DamageHull(damage, attacker, hitdata)
	else
		self:DamageShield(hitent, damage, attacker, hitdata)
	end

	--Get percent of armor+hull HP for calculating damage effects.
	--Factoring in hull as well as armor so as to work for hull tanks as well
	local curHP = (self:GetHullAmount() + self:GetArmorAmount()) / (self:GetHullMax() + self:GetArmorMax())

	--Light Damage Effects
	if curHP <= 0.75 then
		DoDamageEffect(self, 1, curHP)
	end
	--Medium Damage Effects
	if curHP <= 0.5 then
		DoDamageEffect(self, 2, curHP)
	end
	--Heavy Damage Effects
	if curHP <= 0.25 then
		DoDamageEffect(self, 3, curHP)
	end
end

local function StopSelfDestructSounds(self)
	if self.SelfDestructSounds then
		for k,v in pairs(self.SelfDestructSounds) do
			v:Stop()
			table.remove(self.SelfDestructSounds, k)
		end
	end
end

function ENT:ExplodeShip(time, attacker)
	--If we're not alive, we can't die!
	if not self.IsAlive then return end

	--This is the actual explosion function, this is only a local function because I wanted to be able to control when a ship explodes ^.- -Lt.Brandon
	local function explode()
		if not IsValid(self) then return end
		self.IsAlive = false

		--Make it so players cannot blow up a dead ship, causing errors.
		for k,v in pairs(self.ConWeldTable) do
			v.SC_Immune = true
		end

		--We died, spam a kill message to everyone D:
		if IsValid(attacker) then
			local KillMessage = attacker:GetName() .. " killed a core \n"
			local owner
			if CPPI then
				owner = self:CPPIGetOwner()
				if IsValid(owner) then
					KillMessage = attacker:GetName() .. " killed a core owned by " .. owner:GetName() .. "\n"
				end
			end
			hook.Run("OnShipKilled", self, attacker, owner)
			MsgAll(KillMessage)
			Msg(KillMessage)
		end

		self:GimpShip()
		self:ExplosionFunction()
	end

	--This is the function that calls the local function
	if time > 0 then
		timer.Create("EXPLODESHIP_"..self:EntIndex(), time, 1, function()
			if not IsValid(self) then return end

			--Stop Doom sounds
			--StopSelfDestructSounds(self)

			--Final little funny sound before kaboom
			self:EmitSound(table.Random(SC.SelfDestructFinalSounds), 350, 100)
			explode()
		end)
	else
		explode()
	end
end

function ENT:StopExplosion()
	timer.Destroy("EXPLODESHIP_"..self:EntIndex())
	timer.Destroy("ship_core_update_sd_sounds"..self:EntIndex())
	self.IsAlive = true --Just kidding!

	--Undo anti-wreck destruction protection
	for k,v in pairs(self.ConWeldTable) do
		v.SC_Immune = false
	end

	--Stop Sounds
	StopSelfDestructSounds(self)
end

--User selects Self-Destruct
function ENT:SelfDestruct(time)
	if not self.IsAlive then return end

	self:ExplodeShip(time, nil)

	--Spam self destruction sounds around the ship :D
	self.SelfDestructSounds = {}

	--Play on Core
	math.randomseed(CurTime())
	self.SelfDestructSounds[self:EntIndex()] = CreateSound(self, table.Random(SC.SelfDestructSounds))
	self.SelfDestructSounds[self:EntIndex()]:SetSoundLevel(400)
	self.SelfDestructSounds[self:EntIndex()]:Play()
	self.SelfDestructSounds[self:EntIndex()]:ChangeVolume(0.5, 0)

	local numSounds = math.Clamp(math.ceil((self.sigrad/39.3700787) / 15), 1, table.Count(self.ConWeldTable))
	for i = 2, numSounds do
		local part = nil
        local attempts = 0
		while attempts < 10 and (not IsValid(part) or self.SelfDestructSounds[part:EntIndex()]) do
			part = table.Random(self.ConWeldTable)
            attempts = attempts + 1
		end

		if self.SelfDestructSounds[part:EntIndex()] == nil then
			self.SelfDestructSounds[part:EntIndex()] = CreateSound(part, table.Random(SC.SelfDestructSounds))
			self.SelfDestructSounds[part:EntIndex()]:SetSoundLevel(400)
			self.SelfDestructSounds[part:EntIndex()]:Play()
			self.SelfDestructSounds[part:EntIndex()]:ChangeVolume(0.5, 0)
		end

		--In case this part goes missing
		part:CallOnRemove("SelfDestruct_Part"..part:EntIndex(), function()
			if IsValid(self) then
				if self.SelfDestructSounds[part:EntIndex()] ~= nil then
					self.SelfDestructSounds[part:EntIndex()]:Stop()
					table.remove(self.SelfDestructSounds, part:EntIndex())
				end
			end
		end)
	end

	--In case the ship is removed
	self:CallOnRemove("Core_SelfDestruct", StopSelfDestructSounds, self)
end

net.Receive("ship_core_start_selfdestruct", function(len, ply)
	if not IsValid(ply) then return end

	local core = net.ReadEntity()
	if not IsValid(core) then return end

	if CPPI then
		local owner = core:CPPIGetOwner()
		if ply ~= owner then return end
	end

	--Distance Check
	local dist = ply:GetPos():DistToSqr(core:GetPos())
	local maxRange = 1000
	maxRange = maxRange * maxRange
	if dist > maxRange then return end

	core:SelfDestruct(30)
end)

--User cancels Self-Destruct
net.Receive("ship_core_stop_selfdestruct", function(len, ply)
	if not IsValid(ply) then return end

	local core = net.ReadEntity()
	if not IsValid(core) then return end
	if not core.IsAlive then return end

	if CPPI then
		local owner = core:CPPIGetOwner()
		if ply ~= owner and not ply:IsAdmin() then return end
	end

	core:StopExplosion()
end)

function ENT:GimpShip()
	for _,i in pairs(self.ConWeldTable) do
		if IsValid(i) then
			if table.HasValue(SC.CoreDeath_KillClasses, i:GetClass()) then
				i:Remove()
			end
		end
	end
end

function ENT:PostExplode()
    -- Create a ship wreck for ourselves
		SC.CreateWreck(self)

	--Remove all managed ents
	for k,v in pairs(self.ConWeldTable) do
		if v ~= self then
			if IsValid(v) then
				v:Remove()
			end
		end
	end

	--Cleanup self
	self:Remove()
end

function ENT:InvalidateConstraints()
	self.ConstraintsChanged = true

	local sigrad = self:GetSigRad()
	local delay = 15 * math.Clamp(sigrad / 500, 0, 1)

	self:SetOctreeDelay(CurTime() + delay)
	SC.Msg("[SHIP CORE] - Ship updated, delaying by " .. math.floor(delay) .. " seconds.", 2)
end

function ENT:OnLinked(ent)
	ent:SetProtector(self)

    if not self.ConstraintsChanged and not self.DisableLinkUpdate then
        self:UpdateModifiers()
    end

    self.LinksChanged = true
end

function ENT:OnUnlinked(ent)
	ent:SetProtector(nil)

	if not self.ConstraintsChanged and not self.DisableLinkUpdate then
        self:UpdateModifiers()
    end

    self.LinksChanged = true
end

hook.Add("OnEntityConstrained", "CoreConstrainHook", function(ent1, ent2, type)
    if type ~= "weld" then --[[print("Not welded? We're not protecting it.")]] return end
    if not IsValid(ent1) or not IsValid(ent2) then --[[print("Not Valid? We, uh, can't protect it.")]] return end
    if ent1:GetProtector() == ent2:GetProtector() then --[[print("Same core? We're already protecting it.")]] return end
    if ent1:IsWorld() or ent2:IsWorld() then --[[print("You can't include world in your octree calculation, sorry.")]] return end
    if ent1 == ent2 then --[[print("You welded it to itself? Good for you.")]] return end

	if IsValid(ent1) and IsValid(ent1:GetProtector()) and ent1:GetProtector().IsCoreEnt then
		ent1:GetProtector():InvalidateConstraints()
	elseif IsValid(ent2) and IsValid(ent2:GetProtector()) and ent2:GetProtector().IsCoreEnt then
		ent2:GetProtector():InvalidateConstraints()
	end
end)

function ENT:OnUnconstrained(to)
	if not IsValid(to) or not IsValid(to:GetProtector()) or not to:GetProtector() == self then return end

	self.ConWeldTable[to:EntIndex()] = nil
    to:SetProtector(nil)

	self:InvalidateConstraints() --alert the core that it needs to update
end

hook.Add("EntityRemoved", "EntityRemoved", function(to)
	if not IsValid(to) or not IsValid(to:GetProtector()) or not to:GetProtector().IsCoreEnt then return end

	local self = to:GetProtector()
	self.ConWeldTable[to:EntIndex()] = nil
	to:SetProtector(nil)

	self:InvalidateConstraints() --alert the core that it needs to update
end)

function ENT:GetProtectedEntities()
    return self.ConWeldTable
end

--Stops the core from doing anything for a while
function ENT:CoreCooldown(seconds)
	if seconds <= 0.1 then
		self:SetOnCooldown(false)
		self:SetShieldAmount(0)
		self:SetArmorAmount(self:GetArmorMax())
		self:SetCooldownRemaining(0)
	else
		self:SetOnCooldown(true)
		self:SetShieldAmount(0)
		self:SetArmorAmount(0)
		self.CooldownEnd = CurTime() + seconds
	end
end

function ENT:CheckClass(Fitting)
	if not Fitting.Classes or #Fitting.Classes < 1 or table.HasValue(Fitting.Classes, self:GetShipClass()) then
		return true
	end

	return false
end

function ENT:CheckFitting(Fitting)
	if not Fitting then return false, false, false, false end

	local CoreFitting = self.Fitting
	local CoreSlots = self.Slots

    local NumSlots = Fitting.NumSlots or 1
	local HaveSlot = (Fitting.Slot == "None") or (CoreSlots[Fitting.Slot] and (CoreSlots[Fitting.Slot].Used + NumSlots) <= CoreSlots[Fitting.Slot].Total)
	local HasCPU = ((CoreFitting.CPU - CoreFitting.CPU_U) - Fitting.CPU) > 0
	local HasPG = ((CoreFitting.PG - CoreFitting.PG_U) - Fitting.PG) > 0
	return (HaveSlot and HasCPU and HasPG), HaveSlot, HasCPU, HasPG
end

function ENT:ConsumeFitting(Fitting)
	if not Fitting then return end

    if Fitting.Slot and Fitting.Slot ~= "None" then
        local NumSlots = Fitting.NumSlots or 1
		self.Slots[Fitting.Slot].Used = self.Slots[Fitting.Slot].Used + NumSlots
	end

	if Fitting.CPU then
		self.Fitting.CPU_U = self.Fitting.CPU_U + Fitting.CPU
	end

	if Fitting.PG then
		self.Fitting.PG_U = self.Fitting.PG_U + Fitting.PG
	end

	Fitting.CanRun = true
	Fitting.Status = "Online"
end

local MainSlotForClass = {
    Drone = "Tiny Weapon",
    Fighter = "Tiny Weapon",
    Frigate = "Small Weapon",
    Cruiser = "Medium Weapon",
    Battlecruiser = "Medium Weapon",
    Battleship = "Large Weapon",
    Dreadnaught = "X-Large Weapon",
    Titan = "X-Large Weapon"
}

function ENT:ApplyModifiers(Modifiers)
	local totals = table.Copy(Modifiers)

	--Use these functions if you add a core mod type, saves some space and prevents more copy paste mistakes! -Lt.Brandon
	--SC.CalcPercent(Start, Amounts, Mult, Clamp, min, max)
	--SC.CalcResist(Start, Amounts)

	for i,k in pairs(totals) do
		i = string.upper(i)
		local found = true

		--Resistance Bonuses
		if i == "SH_EM" then
			self.ShieldRes.EM = SC.CalcResist(self.BaseShieldRes.EM, k.Amounts)
		elseif i == "SH_KI" then
			self.ShieldRes.KIN = SC.CalcResist(self.BaseShieldRes.KIN, k.Amounts)
		elseif i == "SH_TH" then
			self.ShieldRes.THERM = SC.CalcResist(self.BaseShieldRes.THERM, k.Amounts)
		elseif i == "SH_EX" then
			self.ShieldRes.EXP = SC.CalcResist(self.BaseShieldRes.EXP, k.Amounts)
		elseif i == "AR_EM" then
			self.ArmorRes.EM = SC.CalcResist(self.BaseArmorRes.EM, k.Amounts)
		elseif i == "AR_KI" then
			self.ArmorRes.KIN = SC.CalcResist(self.BaseArmorRes.KIN, k.Amounts)
		elseif i == "AR_TH" then
			self.ArmorRes.THERM = SC.CalcResist(self.BaseArmorRes.THERM, k.Amounts)
		elseif i == "AR_EX" then
			self.ArmorRes.EXP = SC.CalcResist(self.BaseArmorRes.EXP, k.Amounts)
		elseif i == "HDR" then
			self.HullRes.EM = SC.CalcResist(self.BaseHullRes.EM, k.Amounts)
			self.HullRes.KIN = SC.CalcResist(self.BaseHullRes.KIN, k.Amounts)
			self.HullRes.THERM = SC.CalcResist(self.BaseHullRes.THERM, k.Amounts)
			self.HullRes.EXP = SC.CalcResist(self.BaseHullRes.EXP, k.Amounts)
		elseif i == "DCDR" then
			self.DirectDamageResistance = SC.CalcResist(0, k.Amounts)

		--Percent Bonuses
		elseif i == "CAPP" then
			self:SetCapMax(SC.CalcPercent(self:GetCapMax(), k.Amounts))
		elseif i == "SHIELDP" then
			self.Shield.Max = SC.CalcPercent(self.Shield.Max, k.Amounts)
			self.Shield.Amount = math.Clamp(self.Shield.Amount, 0, self.Shield.Max)
		elseif i == "ARMORP" then
			self.Armor.Max = SC.CalcPercent(self.Armor.Max, k.Amounts)
			self.Armor.Amount = math.Clamp(self.Armor.Max * self.Armor.Percent, 0, self.Armor.Max)
		elseif i == "HULLP" then
			self.Hull.Max = SC.CalcPercent(self.Hull.Max, k.Amounts)
			self.Hull.Amount = math.Clamp(self.Hull.Max * self.Hull.Percent, 0, self.Hull.Max)

		--Additive bonuses
		elseif i == "CAP" then
			self:SetCapMax(self:GetCapMax() + (k.Total * 2000))
		elseif i == "SHIELD" then
			self.Shield.Max = self.Shield.Max + (k.Total * 2000)
			self.Shield.Amount = math.Clamp(self.Shield.Amount, 0, self.Shield.Max)
		elseif i == "ARMOR" then
			self.Armor.Max = self.Armor.Max + (k.Total * 2000)
			self.Armor.Amount = math.Clamp(self.Armor.Max * self.Armor.Percent, 0, self.Armor.Max)
		elseif i == "HULL" then
			self.Hull.Max = self.Hull.Max + (k.Total * 2000)
			self.Hull.Amount = math.Clamp(self.Hull.Max * self.Hull.Percent, 0, self.Hull.Max)

        --Slot Bonuses
        elseif i == "EWS" then
            local Count = math.floor(k.Total / 30)
            local Slot = MainSlotForClass[self:GetShipClass()] or "HELP I BROKE IT"

            if self.Slots[Slot] then
                self.Slots[Slot].Total = self.Slots[Slot].Total + Count
            else
                self.Slots[Slot] = {
                    Used = 0,
                    Total = Count,
                }
            end

        elseif i == "EIS" then
            local Count = math.floor(k.Total / 30)
            local Slot = "Industrial"

            if self.Slots[Slot] then
                self.Slots[Slot].Total = self.Slots[Slot].Total + Count
            else
                self.Slots[Slot] = {
                    Used = 0,
                    Total = Count,
                }
            end
		else
			self.OtherBonuses[i] = k.Amounts
			found = false
		end

		if found then
			totals[i] = nil
		end
	end

    local ShieldRechargeMod = 1

	--These needed to be in a different loop so they didn't get overriden in the other one -Lt.Brandon
	for i,k in pairs(totals) do
        if i == "SHIELDR" then
			ShieldRechargeMod = 1 + SC.CalcPercent(0, k.Amounts, -0.25, true, -0.9, 0.9)
		end
	end

    local RechargeTime = ((self.Shield.Max/16000)^self.ShieldData.RechargeExponent)/self.Shield.RechargeRatio
    self:SetShieldRechargeTime(5 + (RechargeTime * ShieldRechargeMod), true)

	return totals
end

function ENT:UpdateModifiers(FakeVolume)
	local Volume = FakeVolume or self.OctreeVolume or 1
    local testval = (Volume * 0.001905) * 6

    local function SetNewShieldRecharge()
        self:SetShieldRechargeTime(5 + ((self.Shield.Max/10000)^self.ShieldData.RechargeExponent)/self.Shield.RechargeRatio)
    end

	-- SubClass
	local Subclasses = SC.GetSubClasses(self:GetShipClass())
	local SubclassData
	for i, k in pairs(Subclasses) do
		if Volume >= k.Volume then
			self:SetShipSubClass(k.Name)
			SubclassData = k
		end
	end

    -- Hull
	self.Hull.Max = math.Round(self.Hull.Ratio*testval)*self.HullData.HealthModifier
	if self.Hull.Max < 1000 then self.Hull.Max = 1000 end
	self.Hull.Amount = math.Clamp(self.Hull.Max * self.Hull.Percent, 0, self.Hull.Max)

    -- Armor
	self.Armor.Max = math.Round(self.Armor.Ratio*testval)*self.ArmorData.HealthModifier
	if self.Armor.Max < 3000 then self.Armor.Max = 3000 end
	self.Armor.Amount = math.Clamp(self.Armor.Max * self.Armor.Percent, 0, self.Armor.Max)

    -- Shields
	self.Shield.Max = math.Round(self.Shield.Ratio*testval)*self.ShieldData.HealthModifier
	if self.Shield.Max < 5000 then self.Shield.Max = 5000 end
	self.Shield.Amount = math.Clamp(self.Shield.Amount, 0, self.Shield.Max)

	SetNewShieldRecharge(self)

    -- Capacitor
	self:SetCapMax(50000 + math.Round((self.Cap.Ratio * testval))*self.Cap.Modifier)
	if self:GetCapMax() < 50000 then self:SetCapMax(50000) end

    -- Cargo Storage
    local ClassSize = 800 + (SC.GetShipClassTable(self:GetShipClass()).Size * 2400)
    self:GetStorageOfType("Cargo"):Resize(math.Round(ClassSize * self.HullData.CargoModifier))
    self:GetStorageOfType("Ammo"):Resize(math.Round(ClassSize * self.HullData.AmmoModifier))
    self:GetStorageOfType("Liquid"):Resize(math.Round(ClassSize * self.HullData.CargoModifier * 4))
    self:GetStorageOfType("Gas"):Resize(math.Round(ClassSize * self.HullData.CargoModifier * 8))

	-- Apply subclass slots
	for Slot, Count in pairs(SubclassData.Slots) do
		self.Slots[Slot] = {
			Used = 0,
			Total = Count,
		}
	end

	-- Apply bonus slots
	for Slot, Count in pairs(self.HullData.BonusSlots) do
		if self.Slots[Slot] then
			self.Slots[Slot].Total = self.Slots[Slot].Total + Count
		else
			self.Slots[Slot] = {
				Used = 0,
				Total = Count,
			}
		end
	end

	-- Calculate cpu and pg
	self.Fitting.CPU = math.floor(2400+((testval/330)*self.Fitting.CPU_R))
	self.Fitting.PG = math.floor(500+((testval/425)*self.Fitting.PG_R))
	self.Fitting.CPU_U = 0
	self.Fitting.PG_U = 0

	self.OtherBonuses = {}

	self.HullRes = table.Copy(self.BaseHullRes)
	self.ArmorRes = table.Copy(self.BaseArmorRes)
	self.ShieldRes = table.Copy(self.BaseShieldRes)

	-- Apply class bonuses
	self:ApplyModifiers(SubclassData.Bonuses)

	-- Find all the modules so we can turn them on
	local Modules = {}
	for i,k in pairs(self:GetLinks()) do
        if k.EnableModule then
			table.insert(Modules, k)
		end
	end

	-- Preferred slot order
	local SlotOrder = {
		["Primary Reactor"] = 1,
		["Auxiliary Reactor"] = 2,
		["None"] = 3,
		["Upgrade"] = 4,
		["Industrial"] = 5,
		["Super Weapon"] = 6,
		["X-Large Weapon"] = 7,
		["Large Weapon"] = 8,
		["Medium Weapon"] = 9,
		["Small Weapon"] = 10,
		["Tiny Weapon"] = 11
	}

	-- Sort the modules so they get turned on in an appropriate order
	table.sort(Modules, function(A, B)
		local FittingA = A:GetFitting()
		local FittingB = B:GetFitting()

        if FittingA.Slot ~= FittingB.Slot then
			return (SlotOrder[FittingA.Slot] or 1000) < (SlotOrder[FittingB.Slot] or 1000)
		elseif A.GetPassiveBonuses and B.GetPassiveBonuses then
			local BonusesA = A:GetPassiveBonuses()
			local BonusesB = B:GetPassiveBonuses()

			return table.Count(BonusesA) > table.Count(BonusesB)
		end

        return (FittingA.PG + FittingA.CPU) > (FittingB.PG + FittingB.CPU)
	end)

    local function TryEnableModules(ModulesToEnable)
	    local Bonuses = {}
        local FailedToOnline = {}
        for i,k in ipairs(ModulesToEnable) do
            if k:EnableModule() then
                if k.GetPassiveBonuses then
                    for Name, Bonus in pairs(k:GetPassiveBonuses()) do
                        if not Bonuses[Name] then Bonuses[Name] = {} end

                        table.insert(Bonuses[Name], Bonus)
                    end
                end
            else
                table.insert(FailedToOnline, k)
            end
        end

        local Totals = {}
        for Name, Tbl in pairs(Bonuses) do
            for i, Bonus in pairs(Tbl) do
                    if not Totals[Name] then Totals[Name] = {Count = 0, Total = 0, Amounts = {}} end
                    Totals[Name].Total = Totals[Name].Total + Bonus
                    Totals[Name].Count = Totals[Name].Count + 1
                    table.insert(Totals[Name].Amounts, Bonus)
            end
        end

        for i,k in pairs(Totals) do
            table.SortDesc(k.Amounts)
        end

        return FailedToOnline, Totals
    end

    -- Try to enable all of the modules
    local FailedMods, Totals = TryEnableModules(Modules)
	self:ApplyModifiers(Totals)

    -- Try one more time, just to catch any that needed bonuses from other mods
    FailedMods, Totals = TryEnableModules(FailedMods)
	self:ApplyModifiers(Totals)

    if table.Count(FailedMods) > 0 then
        SC.Error("ship_core::UpdateModifiers "..tostring(table.Count(FailedMods)).." mods failed to enable!", 3)
    end

	for i,k in ipairs(Modules) do
        if k.OnCoreUpdated then
            k:OnCoreUpdated()
        end
	end

    timer.Simple(1, function()
        if not IsValid(self) then return end
        SC.NWAccessors.SyncAccessors(self)
    end)

    self.DisableLinkUpdate = false
end

function ENT:CalculateOctree()
    if self.ConstraintsChanged and CurTime() > self:GetOctreeDelay() and not self:GetOctreeBuilding() then
       --We do NOT want more than one recalculation running at once!
       self:SetOctreeBuilding(true)
       self.ConstraintsChanged = false

        --Disable any old entities until we're done, they'll be enabled right away anyways
        for i,k in pairs(self:GetLinks()) do
			if k.DisableModule then
				k:DisableModule()
			end
        end

        --Prevent things relinking from triggering UpdateModifiers
        self.DisableLinkUpdate = true

        --Refresh the entity tables
		self.ConWeldTable = SC.GetWeldedEntities(self)
        self.Vehicles = {}

		local Bounds = {}
        local maxtable_X = {}
        local maxtable_Y = {}
        local maxtable_Z = {}
        local Invalid = {}
		for _,i in pairs(self.ConWeldTable) do
			if IsValid(i) and ((not IsValid(i:GetProtector())) or i:GetProtector() == self or i == self) then
				i:SetProtector(self)

				--fuck you and your invisible entities!
				if i:GetColor().a == 255 then
                    --These are used to calculate the actual health of a ship
                    local EntBounds = {}
					EntBounds["Min"] = i:OBBMins()
					EntBounds["Max"] = i:OBBMaxs()
                    EntBounds["Extents"] = Vector(math.abs(i:OBBMaxs().X) + math.abs(i:OBBMins().X), math.abs(i:OBBMaxs().Y) + math.abs(i:OBBMins().Y), math.abs(i:OBBMaxs().Z) + math.abs(i:OBBMins().Z)) * 0.5
                    local localang = self:WorldToLocalAngles(i:GetAngles())
                    EntBounds["Angle"] = localang
                    EntBounds["Axis"] = {localang:Forward(), localang:Right(), localang:Up()}
                    EntBounds["Radius"] = math.max(math.abs(i:OBBMaxs().X) + math.abs(i:OBBMins().X), math.abs(i:OBBMaxs().Y) + math.abs(i:OBBMins().Y), math.abs(i:OBBMaxs().Z) + math.abs(i:OBBMins().Z)) * 0.5
                    EntBounds["Center"] = self:WorldToLocal(i:LocalToWorld(i:OBBCenter()))
                    EntBounds["Position"] = self:WorldToLocal(i:GetPos())
                    table.insert(Bounds, EntBounds)

                    --These are used for calculating the sigrad and class
                    local min = self:WorldToLocal(i:LocalToWorld(i:OBBMins()))
                    local max = self:WorldToLocal(i:LocalToWorld(i:OBBMaxs()))

					table.insert(maxtable_X, min.x)
					table.insert(maxtable_Y, min.y)
					table.insert(maxtable_Z, min.z)
					table.insert(maxtable_X, max.x)
					table.insert(maxtable_Y, max.y)
					table.insert(maxtable_Z, max.z)
				end

                if i:IsVehicle() then
                    table.insert(self.Vehicles, i)
                end

                if i ~= self then
                    if i.IsLSEnt then
                        i:Link(self)
                    end
                end
			else
				SC.Error("WTF NuLL ENTITY IN WELD TABLE?!")
				table.insert(Invalid, _)
			end
        end

        for _, Bad in ipairs(Invalid) do
            self.ConWeldTable[Bad] = nil
        end

		--Sort all bounding boxes by size, largest first. This is to speed up octree calculations.
		table.sort(Bounds, function(a,b)
			local sizeA = a.Min:DistToSqr(a.Max)
			local sizeB = b.Min:DistToSqr(b.Max)

			return sizeA > sizeB
		end)

		table.SortDesc(maxtable_X)
		table.SortDesc(maxtable_Y)
		table.SortDesc(maxtable_Z)

		local MinVec = Vector(maxtable_X[#maxtable_X],maxtable_Y[#maxtable_Y],maxtable_Z[#maxtable_Z])
		local MaxVec = Vector(maxtable_X[1],maxtable_Y[1],maxtable_Z[1])

		self:SetBoxMin(MinVec)
		self:SetBoxMax(MaxVec)
		self:SetBoxCenter((MaxVec + MinVec) * 0.5)

        if SC.Debug and SC.DebugLevel <= 3 then
            debugoverlay.Axis(self:LocalToWorld(self:GetBoxCenter()), Angle(), 100, 10, true)
            debugoverlay.Axis(self:LocalToWorld(self:GetBoxMin()), Angle(), 100, 10, true)
            debugoverlay.Axis(self:LocalToWorld(self:GetBoxMax()), Angle(), 100, 10, true)
        end

        --Update the centers of all bounds
        for i,k in pairs(Bounds) do
            k["Center"] = k["Center"] + self:GetBoxCenter()

            if SC.Debug and SC.DebugLevel <= 3 then
                debugoverlay.SweptBox(self:LocalToWorld(k["Position"]), self:LocalToWorld(k["Position"]), k["Min"], k["Max"], self:LocalToWorldAngles(k["Angle"]), 10, Color(0, 255, 0))
            end
        end

		self.sigrad = math.max(math.abs(MaxVec.X) + math.abs(MinVec.X),
					math.abs(MaxVec.Y) + math.abs(MinVec.Y),
					math.abs(MaxVec.Z) + math.abs(MinVec.Z))

		local sigradm = self.sigrad/39.3700787
        self:SetSigRad(sigradm, true)

        if SC.Debug and SC.DebugLevel <= 3 then
            debugoverlay.Sphere(self:GetPos(), self.sigrad, 10, Color(0, 0, 255, 127))
        end

        local OctreeDepth = 1
		for i,k in pairs(SC.ShipClasses) do
			if sigradm > k.Size then
				self:SetShipClass(k.Name, true)
                self.EffectCounts = k.EffectCounts
				self.ExplosionFunction = SC.CoreExplosions[k.ExplosionEffect]
                OctreeDepth = k.OctreeDepth
			end
		end

		self:SetNodeRadius(math.Clamp(self.sigrad, 512, 16768), true)

        --This gets run when the octree is done calculating
        local function FinishCoreCalculation(octree)
            if not IsValid(self) then return end

            self.OctreeVolume = octree.Volume
            self:UpdateModifiers()
            self:SetOctreeBuilding(false)

			ErrorNoHalt("[Space Combat 2 - Ship Core] - Octree Calculation Finished after " .. (SysTime() - self.RebuildStartTime) .. " seconds. Total Volume: "..octree.Volume.."\n")
        end

        --Start the ship voxelization
		self.RebuildStartTime = SysTime()

        local octree
        if GetConVar("sc_FastCoreCalculations"):GetBool() then
            octree = SC.CoreOctree.Builder:New(self, Vector(self.sigrad, self.sigrad, self.sigrad), OctreeDepth, Bounds, 0.05, FinishCoreCalculation)
        else
            octree = SC.CoreOctree.Builder:New(self, self:GetBoxMax() - self:GetBoxMin(), OctreeDepth, Bounds, 0.05, FinishCoreCalculation)
        end
        octree:Rebuild()
	end
end

function ENT:AddManagedPlayer(ply)
	if not IsValid(ply) then return end

	self.ManagedPlayers[tostring(ply)] = ply
	ply.LSManager = self

	--Tell the gamemode that we're going to be manually setting stuff
	ply.HasGravityOverride = true
	ply.HasLifeSupportOverride = true

	--Restore Player Movement
	ply:SetGravity(1)
	GAMEMODE:SetPlayerSpeed(ply, 250, 500)

    self:SetManagedPlayerCount(table.Count(self:GetManagedPlayers()))
end

function ENT:RemoveManagedPlayer(ply)
	if not IsValid(ply) then return end

	self.ManagedPlayers[tostring(ply)] = nil
	ply.LSManager = nil

	--Return control to gamemode
	ply.HasGravityOverride = false
	ply.HasLifeSupportOverride = false

    self:SetManagedPlayerCount(table.Count(self:GetManagedPlayers()))
end

function ENT:GetManagedPlayers()
	return self.ManagedPlayers
end

function ENT:EnableLifeSupport()
    self:SetLifeSupportEnabled(true)
end

function ENT:DisableLifeSupport()
    self:SetLifeSupportEnabled(false)

    for k,v in pairs(self:GetManagedPlayers()) do
        self:RemoveManagedPlayer(v)
    end
end

function ENT:Think()
    -- If we haven't been setup yet, just return
    if not self.HullData or not self.ArmorData or not self.ShieldData then return end

	local Time = CurTime() - self.LastThink
	self.LastThink = CurTime()

	if self.timer + self.ReCalcTime <= CurTime() then
		self.timer = self.timer + self.ReCalcTime

		self:CalculateOctree()
	end

	if self.OneSecond <= CurTime() then
		self.OneSecond = self.OneSecond + 1
		self:Displays()
		self:OneSecond_Think() --Think for things doing stuff every second
	end

	if not self:GetOnCooldown() then
		if (self.ShieldRegenAllowed and self:GetShieldActive() and self:GetCapPercent() > 0.1) and self:GetShieldAmount() < self:GetShieldMax() then
			local Amount = self:CalculateRecharge(self.Shield.Max, self.Shield.Amount, self:GetShieldRechargeTime()) * Time

			local EnergyNeeded
			if self.ShieldData.Efficiency < 0 then
				EnergyNeeded = Amount * (1 - self.ShieldData.Efficiency)
			else
				EnergyNeeded = Amount * self.ShieldData.Efficiency
			end

			if self:HasResource("Energy", EnergyNeeded) then
				self:ConsumeResource("Energy", EnergyNeeded)
				self:SupplyShield(Amount)
			end
		end

		if self:GetArmorAmount() < self:GetArmorMax() and self.ArmorData.Regenerative then
			local Amount = self:CalculateRecharge(self.Armor.Max, self.Armor.Amount, 400) * Time
			self:SupplyArmor(Amount)
		end
	end
end

function ENT:OneSecond_Think()
	if self:GetIsStation() then
		if self:GetPos() ~= self.LastPos then
			self:CoreCooldown(30)
		end

		self.LastPos = self:GetPos()
	end

    local cooldown
	if self:GetOnCooldown() then
		cooldown = math.Round(self.CooldownEnd - CurTime())

		if cooldown <= 0 then
			self:SetOnCooldown(false)
			self:SetShieldAmount(0)
			self:SetArmorAmount(self:GetArmorMax())
            self:SetCooldownRemaining(0)
        else
            self:SetCooldownRemaining(cooldown)
		end
    end

    if self:GetLifeSupportEnabled() then
        for k,v in pairs(self.ManagedPlayers) do
            --Garbage Collect invalid entries
            if not IsValid(v) then
                self.ManagedPlayers[k] = nil
            end
        end

        -- Update managed player count
        self:SetManagedPlayerCount(table.Count(self.ManagedPlayers))

        if self:ConsumeResource("Energy", BaseEnergyCost + EnergyPerPlayer * self:GetManagedPlayerCount()) and self:GetManagedPlayerCount() > 0 then
            local HasOxygen = false
            if self:ConsumeResource("Oxygen", OxygenPerPlayer * self:GetManagedPlayerCount()) then
                self:SupplyResource("Carbon Dioxide", CO2PerPlayer * self:GetManagedPlayerCount())
                HasOxygen = true
            end

            for _,ply in pairs(self.ManagedPlayers) do
                ply.HasGravityOverride = true

                if HasOxygen then
                    ply.HasLifeSupportOverride = true

                    if GetConVarNumber("sc_core_canrefillsuit") ~= 0 then
                        -- TODO Implement
                    end
                else
                    ply.HasLifeSupportOverride = false
                end
            end
        else
            for _,ply in pairs(self.ManagedPlayers) do
                ply.HasLifeSupportOverride = false
                ply.HasGravityOverride = false
            end
        end
    end

	if self.DirectHitCounter > 0 and (self.LastDirectHit + self.DirectHitResetTime) <= CurTime() then
		self.DirectHitCounter = 0
	end

	for k, i in pairs(self:GetLinks(nil, true)) do
		if IsValid(i) then
			if i:GetPos():Distance(self:GetPos()) > self:GetNodeRadius() then
                i:Unlink()
				self:EmitSound("physics/metal/metal_computer_impact_bullet"..math.random(1,3)..".wav", 500)
				i:EmitSound("physics/metal/metal_computer_impact_bullet"..math.random(1,3)..".wav", 500)
			end
        else
            self:GetLinks(nil, true)[k] = nil
            self.LinksChanged = true
        end
	end

    --Resource Node deltas
    self:CalculateDeltas()

    --Sync tables
    local ShouldSyncStorage = false
    for i,k in pairs(self:GetDeltas()) do
        if math.abs(k.delta) >= math.max(50, self:GetMaxAmount(i) * 0.002) or math.abs(k.maxdelta) >= 1 then
            ShouldSyncStorage = true
            break
        end
    end

    if ShouldSyncStorage or (CurTime() - self.LastStorageSync) >= 15 then
		local ID = SC.NWAccessors.GetNWAccessorID(self, "Storage")
        SC.NWAccessors.SyncValue(self, ID)
        self.LastStorageSync = CurTime()
    end

    if self.LinksChanged then
		local ID = SC.NWAccessors.GetNWAccessorID(self, "Links")
        SC.NWAccessors.CreatePartialTable(self, "Links")
        SC.NWAccessors.SyncValue(self, ID)
        self.LinksChanged = false
    end

	--Check if we can free up damage effect slots
	DamageEffectCleanupCheck(self)

	--Reset shield hit counter
	self.ShieldHitsLastSecond = 0
end

function ENT:TriggerInput(Name, Value)
    if Name == "ShieldOn" then
        self:SetShieldActive(Value == 1)
    elseif Name == "LifeSupportOn" then
        if Value == 1 then
            self:EnableLifeSupport()
        else
            self:DisableLifeSupport()
        end
    end
end

function ENT:Displays()
    if WireLib then
        WireLib.TriggerOutput(self, "Shield", self.Shield.Amount)
        WireLib.TriggerOutput(self, "Armor", self.Armor.Amount)
        WireLib.TriggerOutput(self, "Hull", self.Hull.Amount)
        WireLib.TriggerOutput(self, "Capacitor", self:GetCapAmount())

        WireLib.TriggerOutput(self, "Max Shield", self.Shield.Max)
        WireLib.TriggerOutput(self, "Max Armor", self.Armor.Max)
        WireLib.TriggerOutput(self, "Max Hull", self.Hull.Max)
        WireLib.TriggerOutput(self, "Max Capacitor", self:GetCapMax())

        WireLib.TriggerOutput(self, "% Shield", math.Round(self.Shield.Percent*10000)/100)
        WireLib.TriggerOutput(self, "% Armor", math.Round(self.Armor.Percent*10000)/100)
        WireLib.TriggerOutput(self, "% Hull", math.Round(self.Hull.Percent*10000)/100)
        WireLib.TriggerOutput(self, "% Capacitor", math.Round(self:GetCapPercent()*10000)/100)
    end
end

--Hook into the Think loop so we don't have to manage players on a per-ent basis
local NextCheck = CurTime()
hook.Add("Think", "SC.ShipCoreLSCheck", function()
    if CurTime() > NextCheck then
        local Players = player.GetAll()
		for _,ply in pairs(Players) do
            local FoundCore
            -- First of all, check if we're sitting in a chair. If we are then chances are high we're flying a ship.
            if ply:InVehicle() then
                local Vehicle = ply:GetVehicle()
                local Protector = Vehicle:GetProtector()

                -- Is this vehicle cored? Does the core have life support enabled? If either of those is false, we don't care about it.
                if IsValid(Protector) and Protector.GetLifeSupportEnabled and Protector:GetLifeSupportEnabled() then
                    FoundCore = Protector
                end

            -- Check to see if the props above and below each player are managed by the same LS Core
            else
			    local TraceData = {
				    start = ply:GetPos(),
				    filter = Players,
				    mask = MASK_SOLID
			    }

			    TraceData.endpos = ply:GetPos() + ply:GetUp() * -LSCheckRange
			    local DownTrace = util.TraceLine(TraceData)

			    -- Check if we're standing on something first, if we are we're more likely to be in a ship
			    if IsValid(DownTrace.Entity) then
                    -- Now, if we're standing on something check if something is above us as well. If something is we're likely in a ship, but we'll check that next.
                    TraceData.endpos = ply:GetPos() + ply:GetUp() * (LSCheckRange + (LSCheckRange - DownTrace.HitPos:Distance(ply:GetPos()))) --Add in unused distance
			        local UpTrace = util.TraceLine(TraceData)

                    if IsValid(UpTrace.Entity) then
                        local UpEntity = UpTrace.Entity
                        local DownEntity = DownTrace.Entity

				        -- Do these entities have a core? If they don't its certainly not a ship
				        if UpEntity.GetProtector and DownEntity.GetProtector and IsValid(UpEntity:GetProtector()) and IsValid(DownEntity:GetProtector()) then
					        -- Finally, if these are the same core, and the core has a valid LS manager, then we're in a ship
                            if UpEntity:GetProtector() == DownEntity:GetProtector() then
                                if UpEntity:GetProtector().GetLifeSupportEnabled and UpEntity:GetProtector():GetLifeSupportEnabled() then
                                    FoundCore = UpEntity:GetProtector()
                                end
					        end
				        end
                    end
			    end
            end

			-- If we were previously managed by a core, and we didn't find the same one, then remove us from it.
			if IsValid(ply.LSManager) and ply.LSManager ~= FoundCore then
				ply.LSManager:RemoveManagedPlayer(ply)
			end

			-- If we found a new LS manager, then let it know about us
			if IsValid(FoundCore) and ply.LSManager ~= FoundCore then
				FoundCore:AddManagedPlayer(ply)
			end
		end

		NextCheck = CurTime() + 1
	end
end)

duplicator.RegisterEntityClass("ship_core", GAMEMODE.MakeEnt, "Data")