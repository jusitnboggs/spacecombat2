ENT.Type 			= "anim"
ENT.Base 			= "base_lsentity"
ENT.PrintName		= "Ship Core"
ENT.Author			= "Lt.Brandon"
ENT.Category 		= "Space Combat"

ENT.Spawnable 		= false
ENT.AdminSpawnable 	= false

ENT.ReCalcTime		= 2 --Seconds between each recalc
ENT.ShieldColor		= Color(255,255,255,255)
ENT.ShieldEnts		= {}

ENT.IsCoreEnt       = true

function ENT:SharedInit()
    self.Hull = {
		Max = 250,
		Amount = 250,
		Percent = 1,
		Ratio = 2.00,
        Modifier = 1.00
	}
	self.HullRes = {}

	self.Armor = {
		Max = 250,
		Amount = 250,
		Percent = 1,
		Ratio = 1.90,
        Modifier = 1.00
	}

	self.ArmorRes = {EM=0,KIN=0,EXP=0,THERM=0}

	self.Shield = {
		Max = 250,
		Amount = 0,
		Percent = 0,
		Ratio = 2.40,
		RechargeRatio = 2.40,
		LastRegen = 0,
        Modifier = 1.00,
        RegenTime = 1
	}

	self.ShieldRes = {EM=0,KIN=0,EXP=0,THERM=0}

	self.Cap = {
		Ratio = 1.50,
        Modifier = 1.00,
	}

    SC.MakeCoreAccessors(self, "Shield")
    SC.MakeCoreAccessors(self, "Armor")
    SC.MakeCoreAccessors(self, "Hull")
    SC.NWAccessors.CreateNWAccessor(self, "Node", "entity", NULL)
    SC.NWAccessors.CreateNWAccessor(self, "Links", "table", {})
    SC.NWAccessors.CreateNWAccessor(self, "SigRad", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "BoxMin", "vector", Vector())
    SC.NWAccessors.CreateNWAccessor(self, "BoxMax", "vector", Vector())
    SC.NWAccessors.CreateNWAccessor(self, "BoxCenter", "vector", Vector())
    SC.NWAccessors.CreateNWAccessor(self, "NodeRadius", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "ShipClass", "cachedstring", "Drone")
    SC.NWAccessors.CreateNWAccessor(self, "ShipSubClass", "cachedstring", "Drone")
    SC.NWAccessors.CreateNWAccessor(self, "ShipName", "string", "Ship Core")
    SC.NWAccessors.CreateNWAccessor(self, "OnCooldown", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "OctreeBuilding", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "OctreeDelay", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CooldownRemaining", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "ShieldRechargeTime", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "ShieldActive", "bool", true)
    SC.NWAccessors.CreateNWAccessor(self, "IsStation", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "LifeSupportEnabled", "bool", true)
    SC.NWAccessors.CreateNWAccessor(self, "ManagedPlayerCount", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "Targeter", "entity", NULL)

    local CoreStorageContainers = {}

    -- Create special storage types (Managed by Ship Core)
    CoreStorageContainers["Cargo"] = GAMEMODE:NewStorage("Cargo", 100, CONTAINERMODE_AUTOENLARGE)
    CoreStorageContainers["Ammo"] = GAMEMODE:NewStorage("Ammo", 100, CONTAINERMODE_AUTOENLARGE)
    CoreStorageContainers["Liquid"] = GAMEMODE:NewStorage("Liquid", 100, CONTAINERMODE_AUTOSPLIT)
    CoreStorageContainers["Gas"] = GAMEMODE:NewStorage("Gas", 100, CONTAINERMODE_AUTOSPLIT)

    for StorageType,_ in pairs(GAMEMODE:GetStorageTypes()) do
        if not CoreStorageContainers[StorageType] then
            CoreStorageContainers[StorageType] = GAMEMODE:NewStorage(StorageType, 0)
        end
	end

    CoreStorageContainers["Energy"]:AddResourceType("Energy", 0)

    SC.NWAccessors.CreateNWAccessor(self, "Storage", "custom", CoreStorageContainers, 1, nil, "SC.ShipStorageSync")
end
function ENT:GetCapAmount()
    return self:GetAmount("Energy")
end

function ENT:GetCapMax()
    return self:GetMaxAmount("Energy")
end

function ENT:GetCapPercent()
    return self:GetCapAmount() / self:GetCapMax()
end

function ENT:CalculateRecharge(C_max, C, Time)
	C = math.Max(C, 1) --This is in a math.Max so it doesn't get stuck with no regeneration at 0 C
	local tau 		= Time / 5 --Time constant
	local formula	= (math.sqrt(C/C_max) - C/C_max) * 2 * C_max / tau --Calculates the amount of recharge

	return math.min(formula, C_max - C)
end