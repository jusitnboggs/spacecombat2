include('shared.lua')

function ENT:Initialize()
    self:SharedInit()
	self.Name = "Ship Core"
	self.HitEffect = true
end

function ENT:Draw(Flags)
    self:DoNormalDraw(Flags)
end

function ENT:DoNormalDraw(Flags)
    self:DrawModel(Flags)
	self.RenderGroup = RENDERGROUP_TRANSLUCENT
	self:DrawModel()

	if self:BeingLookedAtByLocalPlayer() then
		AddWorldTip(self)
	end
end

local format = string.format

function ENT:GetWorldTipBodySize( WorldTip )
	local ShieldRecharge = sc_ds(self:CalculateRecharge(self:GetShieldMax(), self:GetShieldAmount(), self:GetShieldRechargeTime()))
	local ShieldRechargePeak = sc_ds(self:CalculateRecharge(self:GetShieldMax(), self:GetShieldMax() * 0.25, self:GetShieldRechargeTime()))

	local hull = self:GetHullAmount()
	local hull_max = self:GetHullMax()
	local hull_percent = hull / hull_max

	local armor = self:GetArmorAmount()
	local armor_max = self:GetArmorMax()
	local armor_percent = armor / armor_max

	local shield = self:GetShieldAmount()
	local shield_max = self:GetShieldMax()
	local shield_percent = shield / shield_max

	local cap = self:GetCapAmount()
	local cap_max = self:GetCapMax()
	local cap_percent = self:GetCapPercent()

	local name = self:GetShipName()

	local w_total, h_total = 0,0

	if self:GetOctreeBuilding() then
		local str = "Recalculation In Progress!\n"

		local w, h = surface.GetTextSize( str )
		w_total = w
		h_total = h + 4

	elseif self:GetOctreeDelay() > CurTime() then
		local str = "Recalculation Pending!\n"
			.."Calculation will begin in: "..math.Round( self:GetOctreeDelay() - CurTime() ).." seconds\n"

		local w, h = surface.GetTextSize( str )
		w_total = w
		h_total = h + 4
	end

	if self:GetOnCooldown() then
		local str = "Core has overheated due to excessive load!\n"
			.."Core integrity field disabled!\n"
			.."Cooldown Remaining: "..self:GetCooldownRemaining().." seconds\n"

		local w, h = surface.GetTextSize( str )
		w_total = w
		h_total = h + 4
	end


	local str = format(
[[- %s -
Hull: %s/%s (%i%%)
Armor: %s/%s (%i%%)
Shield: %s/%s (%i%%)
Current Shield Recharge: %s/s
Peak Shield Recharge %s/s


Capacitor: %s/%s (%i%%)


SigRad: %sm
Class: %s
Generator Class: %s

Node Radius: %s

Press [%s] to open menu]],
	name,
	sc_ds(hull), sc_ds(hull_max), hull_percent*100,
	sc_ds(armor), sc_ds(armor_max), armor_percent*100,
	sc_ds(shield), sc_ds(shield_max), shield_percent*100,
	ShieldRecharge, ShieldRechargePeak,
	sc_ds(cap), sc_ds(cap_max), cap_percent*100,
	sc_ds(self:GetSigRad()), (self:GetIsStation() and "Station" or self:GetShipSubClass()), self:GetShipClass(),
    sc_ds(self:GetNodeRadius()),
    string.upper(input.LookupBinding("+use")))

	local w,h = surface.GetTextSize( str )
	w_total = math.max(w_total,w)
	h_total = h_total + h + 8

	return w_total, h_total
end

local hull_color = Color(100,0,0,255)
local armor_color = Color(50,50,50,255)
local shield_color = Color(0,0,100,255)
local capacitor_color = Color(100,50,0,255)
local white = Color(255,255,255,255)
local black = Color(0,0,0,255)

function ENT:DrawWorldTipBody( WorldTip, pos )
	--Some local variables we'll need to draw our overlay
	local ShieldRecharge = sc_ds(self:CalculateRecharge(self:GetShieldMax(), self:GetShieldAmount(), self:GetShieldRechargeTime()))
	local ShieldRechargePeak = sc_ds(self:CalculateRecharge(self:GetShieldMax(), self:GetShieldMax() * 0.25, self:GetShieldRechargeTime()))

	local hull = self:GetHullAmount()
	local hull_max = self:GetHullMax()
	local hull_percent = hull / hull_max

	local armor = self:GetArmorAmount()
	local armor_max = self:GetArmorMax()
	local armor_percent = armor / armor_max

	local shield = self:GetShieldAmount()
	local shield_max = self:GetShieldMax()
	local shield_percent = shield / shield_max

	local cap = self:GetCapAmount()
	local cap_max = self:GetCapMax()
	local cap_percent = self:GetCapPercent()

	local name = self:GetShipName()

	local offset = pos.min.y + pos.edgesize
    local _, txt_h = surface.GetTextSize( "" )

    local warning_color = Color(180+math.sin(math.rad(RealTime()*180))*75,0,0,255)

	if self:GetOctreeBuilding() then
		local str = "Recalculation In Progress!\n"

		draw.DrawText( str, "GModWorldtip", pos.center.x, pos.min.y + pos.edgesize, warning_color, TEXT_ALIGN_CENTER )
		offset = offset + txt_h * 2 + 4

	elseif self:GetOctreeDelay() > CurTime() then
		local str = "Recalculation Pending!\n"
			.."Calculation will begin in: "..math.Round( self:GetOctreeDelay() - CurTime() ).." seconds\n"

		draw.DrawText( str, "GModWorldtip", pos.center.x, pos.min.y + pos.edgesize, warning_color, TEXT_ALIGN_CENTER )
		offset = offset + txt_h * 3 + 4
	end

	if self:GetOnCooldown() then
		local str = "Core has overheated due to excessive load!\n"
			.."Core integrity field disabled!\n"
			.."Cooldown Remaining: "..self:GetCooldownRemaining().." seconds\n"

		draw.DrawText( str, "GModWorldtip", pos.center.x, pos.min.y + pos.edgesize, warning_color, TEXT_ALIGN_CENTER )
		offset = offset + txt_h * 4 + 4
	end


	local str = format(
[[- %s -
Hull: %s/%s (%i%%)
Armor: %s/%s (%i%%)
Shield: %s/%s (%i%%)
Current Shield Recharge: %s/s
Peak Shield Recharge %s/s


Capacitor: %s/%s (%i%%)


SigRad: %sm
Class: %s
Generator Class: %s

Node Radius: %s

Press [%s] to open menu]],
	name,
	sc_ds(hull), sc_ds(hull_max), hull_percent*100,
	sc_ds(armor), sc_ds(armor_max), armor_percent*100,
	sc_ds(shield), sc_ds(shield_max), shield_percent*100,
	ShieldRecharge, ShieldRechargePeak,
	sc_ds(cap), sc_ds(cap_max), cap_percent*100,
	sc_ds(self:GetSigRad()), (self:GetIsStation() and "Station" or self:GetShipSubClass()), self:GetShipClass(),
    sc_ds(self:GetNodeRadius()),
    string.upper(input.LookupBinding("+use")))


	draw.DrawText( str, "GModWorldtip", pos.center.x, offset, white, TEXT_ALIGN_CENTER )

	local bar_w = pos.size.w - pos.edgesize * 2
	local bar_h = txt_h + 8

	offset = offset + txt_h * 6 + 4

	-- hull/armor/shield

	surface.SetDrawColor( black )
	local minx = pos.min.x + pos.edgesize
	local miny = offset
	surface.DrawLine( minx, miny, minx + bar_w, miny )
	surface.DrawLine( minx + bar_w, miny, minx + bar_w, miny + bar_h )
	surface.DrawLine( minx + bar_w, miny + bar_h, minx, miny + bar_h )
	surface.DrawLine( minx, miny + bar_h, minx, miny )

	local total = hull_max + armor_max + shield_max
	local hull_bar_percent = hull / total
	local armor_bar_percent = armor / total
	local shield_bar_percent = shield / total

	minx = minx + 1
	miny = miny + 1

	local _bar_w = math.min(hull_bar_percent,1) * bar_w
	surface.SetDrawColor( hull_color )
	surface.DrawRect( minx, miny, _bar_w, bar_h-1 )
	minx = minx + _bar_w

	_bar_w = math.min(armor_bar_percent,1) * bar_w
	surface.SetDrawColor( armor_color )
	surface.DrawRect( minx, miny, _bar_w, bar_h-1 )
	minx = minx + _bar_w

	_bar_w = math.min(shield_bar_percent,1) * bar_w
	surface.SetDrawColor( shield_color )
	surface.DrawRect( minx, miny, _bar_w, bar_h-1 )

	-- capacitor
	offset = offset + txt_h * 3
	surface.SetDrawColor( black )
	minx = pos.min.x + pos.edgesize
	miny = offset
	surface.DrawLine( minx, miny, minx + bar_w, miny )
	surface.DrawLine( minx + bar_w, miny, minx + bar_w, miny + bar_h )
	surface.DrawLine( minx + bar_w, miny + bar_h, minx, miny + bar_h )
	surface.DrawLine( minx, miny + bar_h, minx, miny )

	_bar_w = math.min(cap_percent,1) * (bar_w - 1)
	surface.SetDrawColor( capacitor_color )
	surface.DrawRect( minx+1, miny+1, _bar_w, bar_h-1 )
end

function ENT:DrawEntityOutline( size )
end

function ENT:SendUpdate()
	net.Start("Ship_Core_Update")
	net.WriteEntity(self)
	net.WriteTable({ShieldOn = self.ShieldOn, Name = self.Name, HitEffect = self.HitEffect})
	net.SendToServer()
end

net.Receive("Ship_Core_UMSG", function(len)
	local data =  net.ReadTable()
	local ent = data.Core
    local IsOwner = data.Owner == LocalPlayer()
	local frame1 = vgui.Create("DFrame")

	frame1:SetPos(100,100)
	frame1:SetSize(680,340 + (IsOwner and 80 or 0))
	frame1:SetTitle("Ship Info")
	frame1:MakePopup()

	local PropertySheet = vgui.Create( "DPropertySheet" )
	PropertySheet:SetParent( frame1 )
	PropertySheet:SetPos( 10, 30 )
	PropertySheet:SetSize( 660, 300 )

    if IsOwner then
	local SettingsPanel = vgui.Create("DPanel", frame1)
	SettingsPanel:SetPos(175, 335)
	SettingsPanel:SetSize(495, 80)

	local SettingsLabel = vgui.Create("DLabel", SettingsPanel)
	SettingsLabel:SetPos(5, 5)
	SettingsLabel:SetText("Ship Core Settings")
	SettingsLabel:SizeToContents()

	local HitEffects = vgui.Create('DCheckBoxLabel', SettingsPanel)
	HitEffects:SetPos(155, 20)
	HitEffects:SetText('Enable Shield Hit Effect?')
	HitEffects:SetValue(ent.HitEffect)
	HitEffects.OnChange = function()
		ent.HitEffect = HitEffects:GetChecked()
		ent:SendUpdate()
	end
	HitEffects:SizeToContents()

	local ShieldOn = vgui.Create('DCheckBoxLabel', SettingsPanel)
	ShieldOn:SetPos(155, 40)
	ShieldOn:SetText('Enable Shield?')
	ShieldOn:SetValue(ent:GetShieldActive())
	ShieldOn.OnChange = function()
		ent.ShieldOn = ShieldOn:GetChecked()
		ent:SendUpdate()
	end
	ShieldOn:SizeToContents()

	local DLabel1 = vgui.Create('DLabel', SettingsPanel)
	DLabel1:SetPos(10, 20)
	DLabel1:SetText('Core Name')
	DLabel1:SizeToContents()

	local core_Name = vgui.Create('DTextEntry', SettingsPanel)
	core_Name:SetSize(140, 20)
	core_Name:SetPos(10, 35)
	core_Name:SetText(ent:GetShipName("Ship Core"))
        ent.Name = ent:GetShipName("Ship Core")
	core_Name.OnEnter = function(Text)
                            ent.Name = tostring(Text:GetValue())
							ent:SendUpdate()
						end

	local SelfDestruct = vgui.Create("DButton", SettingsPanel)
	SelfDestruct:SetSize(140, 20)
	SelfDestruct:SetPos(325, 20)
	SelfDestruct:SetText("Self Destruct")
	SelfDestruct.DoClick = function()
		net.Start("ship_core_start_selfdestruct")
			net.WriteEntity(ent)
		net.SendToServer()
	end

	local CancelSelfDestruct = vgui.Create("DButton", SettingsPanel)
	CancelSelfDestruct:SetSize(140, 20)
	CancelSelfDestruct:SetPos(325, 45)
	CancelSelfDestruct:SetText("Cancel Self Destruct")
	CancelSelfDestruct.DoClick = function()
		net.Start("ship_core_stop_selfdestruct")
			net.WriteEntity(ent)
		net.SendToServer()
	end
    end

	--Health tab
	local HPPan = vgui.Create("DPanel")

	local StatPan = vgui.Create("DPanel", HPPan)
	StatPan:SetSize(300, 0)
	StatPan:Dock(RIGHT)

	local StatLabel = vgui.Create("DLabel", StatPan)
	StatLabel:SetPos(5,5)
	StatLabel:SetText("Other Statistics")
	StatLabel:SizeToContents()

	local StatsLabel = vgui.Create("DLabel", StatPan)
	StatsLabel:SetPos(30, 30)
	StatsLabel:SetText("Signature Radius: "..sc_ds(ent:GetSigRad()).." Meters"..
	"\n\nShield Peak Recharge: "..sc_ds(ent:CalculateRecharge(ent:GetShieldMax(), ent:GetShieldMax() * 0.25, ent:GetShieldRechargeTime()))..
	"\nShield Recharge Time: "..sc_ds(ent:GetShieldRechargeTime())..
	"\n\nEffective Hitpoints: "..sc_ds((ent:GetHullMax() / (1 - (data.HullRes.EM + data.HullRes.KIN + data.HullRes.THERM + data.HullRes.EXP)/4)) +
		(ent:GetShieldMax() / (1 - (data.ShieldRes.EM + data.ShieldRes.KIN + data.ShieldRes.THERM + data.ShieldRes.EXP)/4)) +
		(ent:GetArmorMax() / (1 - (data.ArmorRes.EM + data.ArmorRes.KIN + data.ArmorRes.THERM + data.ArmorRes.EXP)/4)))..
	"\n\nResource Node Radius: "..sc_ds(ent:GetNodeRadius())..
	"\n\nShip Class: "..(ent:GetIsStation() and "Station" or ent:GetShipClass())..
    "\nShip Sub-Class: "..ent:GetShipSubClass()..
    "\n\nGenerator Class: "..ent:GetShipClass())
	StatsLabel:SizeToContents()

	local HPCol = vgui.Create("DListView", HPPan)
	HPCol:Clear()
	HPCol:SetMultiSelect(false)
	HPCol:AddColumn("Type")
	HPCol:AddColumn("Amount")
	HPCol:AddColumn("Max Amount")
	HPCol:AddColumn("Percent")

	HPCol:AddLine("Hull", sc_ds(ent:GetHullAmount()), sc_ds(ent:GetHullMax()), (math.Round((ent:GetHullAmount() / ent:GetHullMax())*10000)/100).."%")
	HPCol:AddLine("Armor", sc_ds(ent:GetArmorAmount()), sc_ds(ent:GetArmorMax()), (math.Round((ent:GetArmorAmount() / ent:GetArmorMax())*10000)/100).."%")
	HPCol:AddLine("Shield", sc_ds(ent:GetShieldAmount()), sc_ds(ent:GetShieldMax()), (math.Round((ent:GetShieldAmount() / ent:GetShieldMax())*10000)/100).."%")
    HPCol:AddLine("Capacitor", sc_ds(ent:GetCapAmount()), sc_ds(ent:GetCapMax()), (math.Round((ent:GetCapAmount() / ent:GetCapMax())*10000)/100).."%")
    local CargoStorage = ent:GetStorageOfType("Cargo")
    local AmmoStorage = ent:GetStorageOfType("Ammo")
	HPCol:AddLine("Cargo Storage", sc_ds(CargoStorage:GetUsed()), sc_ds(CargoStorage:GetSize()), (math.Round((CargoStorage:GetUsed() / CargoStorage:GetSize())*10000)/100).."%")
	HPCol:AddLine("Ammo Storage", sc_ds(AmmoStorage:GetUsed()), sc_ds(AmmoStorage:GetSize()), (math.Round((AmmoStorage:GetUsed() / AmmoStorage:GetSize())*10000)/100).."%")
	HPCol:SetSize(0, 125)
	HPCol:Dock(TOP)

	local RESCol = vgui.Create("DListView", HPPan)
	RESCol:Clear()
	RESCol:SetMultiSelect(false)
	RESCol:AddColumn("Type")
	RESCol:AddColumn("Hull")
	RESCol:AddColumn("Armor")
	RESCol:AddColumn("Shield")

	RESCol:AddLine("EMP", (math.Round(data.HullRes.EM*10000)/100).."%", (math.Round(data.ArmorRes.EM*10000)/100).."%", (math.Round(data.ShieldRes.EM*10000)/100).."%")
	RESCol:AddLine("Explosive", (math.Round(data.HullRes.EXP*10000)/100).."%", (math.Round(data.ArmorRes.EXP*10000)/100).."%", (math.Round(data.ShieldRes.EXP*10000)/100).."%")
	RESCol:AddLine("Kinetic", (math.Round(data.HullRes.KIN*10000)/100).."%", (math.Round(data.ArmorRes.KIN*10000)/100).."%", (math.Round(data.ShieldRes.KIN*10000)/100).."%")
	RESCol:AddLine("Thermal", (math.Round(data.HullRes.THERM*10000)/100).."%", (math.Round(data.ArmorRes.THERM*10000)/100).."%", (math.Round(data.ShieldRes.THERM*10000)/100).."%")
	RESCol:Dock(FILL)

	local FITCol = vgui.Create("DListView")
	FITCol:Clear()
	FITCol:SetMultiSelect(false)
	FITCol:AddColumn("Type")
	FITCol:AddColumn("Amount")
	FITCol:AddColumn("Used")
	FITCol:AddColumn("Percent")

	for i,k in pairs(data.Slots) do
		if k.Total > 0 then
			FITCol:AddLine(i, k.Total, k.Used, (math.Round((k.Used/k.Total)*10000)/100).."%")
		end
	end

	FITCol:AddLine("CPU", sc_ds(data.Fitting.CPU), sc_ds(data.Fitting.CPU_U), (math.Round((data.Fitting.CPU_U/data.Fitting.CPU)*10000)/100).."%")
	FITCol:AddLine("Power Grid", sc_ds(data.Fitting.PG), sc_ds(data.Fitting.PG_U), (math.Round((data.Fitting.PG_U/data.Fitting.PG)*10000)/100).."%")

    local HOLDCol = vgui.Create("DPropertySheet")
    for StorageType, Storage in pairs(ent:GetStorage()) do
        local InventoryPanel = vgui.Create("InventoryPanel")
        InventoryPanel:SetSmallDisplayMode(true)
        InventoryPanel:LoadFromContainer(Storage)
        HOLDCol:AddSheet(StorageType, InventoryPanel, "icon16/heart.png", false, false, StorageType)
    end

	local LINKCol = vgui.Create("DListView")
	LINKCol:Clear()
	LINKCol:SetMultiSelect(false)
	LINKCol:AddColumn("Name")
	LINKCol:AddColumn("Distance")

	for i,k in pairs(data.Linked) do
        if IsValid(k) then
            if k.GetModuleName then
                LINKCol:AddLine(k:GetModuleName(), math.Round(ent:GetPos():Distance(k:GetPos())))
            else
                LINKCol:AddLine(k.PrintName, math.Round(ent:GetPos():Distance(k:GetPos())))
            end
        end
	end

	PropertySheet:AddSheet( "Health", HPPan, "icon16/heart.png", false, false, "Ship Health" )
	PropertySheet:AddSheet( "Fitting", FITCol, "icon16/wrench.png", false, false, "Ship Fitting" )
	PropertySheet:AddSheet( "Hold", HOLDCol, "icon16/box.png", false, false, "Ship Hold" )
	PropertySheet:AddSheet( "Linked", LINKCol, "icon16/server_connect.png", false, false, "Linked Entities" )

end)