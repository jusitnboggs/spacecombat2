--================================
-- DETAIL PROP CLIENTSIDE CODE
-- 		Author: Steeveeo
--================================

include('shared.lua')

CreateClientConVar("detailprop_enable", 1, true, false)
CreateClientConVar("detailprop_viewdistance", 750, true, false)
CreateClientConVar("detailprop_shadowstart", 0.5, true, false)
CreateClientConVar("detailprop_drawshadow", 1, true, false)
--CreateClientConVar("detailprop_fadestart", 0.75, true, false)

local DetailProps = {}

function ENT:Initialize()

	table.insert(DetailProps, self.Entity)

	self.SavedColor = Color(255,255,255,255)

end


--Only Draw within specified View Distance
function ENT:Draw()

	--Get Settings
	local doDraw = GetConVarNumber("detailprop_enable")
	local viewDist = GetConVarNumber("detailprop_viewdistance")
	local shadowStart = GetConVarNumber("detailprop_shadowstart")
	local drawShadows = GetConVarNumber("detailprop_drawshadow")
	--local fadeStart = GetConVarNumber("detailprop_fadestart")

	--Exit if disabled
	if doDraw == 0 then
		self:DrawShadow(false)
		return
	end

	--Get Camera Position
	local viewPos = Vector(0,0,0)
	if IsValid(LocalPlayer()) then --For some reason, LocalPlayer() sometimes goes nil so, here we are...
		local viewEnt = LocalPlayer():GetViewEntity()
		if viewEnt then
			viewPos = viewEnt:GetPos()
		else
			viewPos = LocalPlayer():GetPos()
		end
	end

	--Get Saved Color (so we can fade out without overriding stuff)
	--[[
	self.SavedColor = self:GetColor()
	local color = self.SavedColor
	local savedAlpha = color.a
	]]

	--Adjust Distance to match DistanceSquared formula
	viewDist = viewDist*viewDist

	--Distance from Camera to Target
	local dist = viewPos:DistToSqr(self:GetPos())

	--In range to render
	if dist < viewDist then

		--[[ Commented out until I can make the code work right
		--Fade prop out at distance, smoother transition
		if dist > fadeStart then
			local alpha = savedAlpha-(savedAlpha*((dist-fadeStart)/(viewDist-fadeStart)))
			alpha = math.Clamp(alpha,1,255)
			self:SetColor(Color(color.r, color.g, color.b, alpha))
			self:SetRenderMode( RENDERMODE_TRANSALPHA )

		end
		]]

		self:DrawModel()
	end

	--Turn shadows on or off (no idea why this only works
	--AFTER drawing the model..)
	if drawShadows == 0 or dist > (viewDist * shadowStart) then
		self:DrawShadow(false)
	else
		self:DrawShadow(true)
	end
end

--[[ Commented out until I can make it work right
function resetColor()
	for k,v in pairs(DetailProps) do
		v:SetColor(v.SavedColor)
	end
end
hook.Add("PostRender", "DetailProp - Reset Color", resetColor)
]]