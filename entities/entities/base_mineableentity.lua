AddCSLuaFile()

DEFINE_BASECLASS("base_scentity")

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName = "Space Combat 2 - Mineable"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "Rock go boom, make shiny."
ENT.Instructions = "...hit me?"

ENT.Spawnable = false
ENT.AdminOnly = false

local base = scripted_ents.Get("base_scentity")
hook.Add( "InitPostEntity", "base_mineableentity_post_entity_init", function()
	base = scripted_ents.Get("base_scentity")
end)

function ENT:SharedInit()
	base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Storage", "custom", {}, 1, nil, "SC.ShipStorageSync")
    SC.NWAccessors.CreateNWAccessor(self, "MiningLevel", "number", MININGLEVEL_COMMON)
end

if CLIENT then return end

function ENT:Initialize()
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_VPHYSICS)
    self:DrawShadow(false)
    self:SetRenderMode(RENDERMODE_TRANSALPHA)

    self.SB_Ignore = true
    self.Untouchable = true
    self.Unconstrainable = true
    self.PhysgunDisabled = true
    self.CanTool = function()
        return false
    end

    --Ownership crap
    if(NADMOD) then --If we're using NADMOD PP, then use its function
        NADMOD.SetOwnerWorld(self)
    elseif(CPPI) then --If we're using SPP, then use its function
        self:SetNWString("Owner", "World")
    else --Well fuck it, lets just use our own!
        self.Owner = game.GetWorld()
    end

    local phys = self:GetPhysicsObject()
    if phys:IsValid() then
        phys:Sleep()
        phys:EnableGravity(false)
        phys:EnableCollisions(true)
        phys:EnableMotion(false)
        phys:SetMass(50000)
    end

    self:SharedInit()
end

function ENT:CreateStorage(StorageType, Size, StartingResources)
    local NewStorage = GAMEMODE:NewStorage(StorageType, Size, CONTAINERMODE_AUTOENLARGE)

    -- FIXME: Update this when Supply/Consume resource are changed to take resource objects
    if StartingResources then
        for Name, Amount in pairs(StartingResources) do
            NewStorage:SupplyResource(Name, Amount)
        end
    end

    local NewStorageTable = {}
    NewStorageTable[StorageType] = NewStorage

    self:SetStorage(NewStorageTable)

    self.PrimaryStorage = NewStorage

    return NewStorage
end

function ENT:IsEmpty()
    if self.PrimaryStorage then
        return not (self.PrimaryStorage:GetUsed() > 0.1)
    end

    return false
end

function ENT:OnMined(TotalMinedVolume)
    local ByRarity = {}
    for Name, Resource in pairs(self.PrimaryStorage:GetStored()) do
        ByRarity[Resource:GetRarity()] = ByRarity[Resource:GetRarity()] or {}
        ByRarity[Resource:GetRarity()][Name] = Resource
    end

    local CurrentResourceVolume = 0
    local MinedResources = {}
    for Rarity, Resources in pairs(ByRarity) do
        for Name, Resource in pairs(Resources) do
            local TotalResourceVolume = Resource:GetSize() * Resource:GetAmount()
            local ResourceToAdd = GAMEMODE:NewResource(Name)

            if TotalResourceVolume + CurrentResourceVolume > TotalMinedVolume then
                local AvailableAmount = (TotalMinedVolume - CurrentResourceVolume) / Resource:GetSize()

                ResourceToAdd:SetMaxAmount(AvailableAmount)
                ResourceToAdd:SetAmount(AvailableAmount)
                self.PrimaryStorage:ConsumeResource(Name, AvailableAmount)
                table.insert(MinedResources, ResourceToAdd)

                break
            else
                ResourceToAdd:SetMaxAmount(Resource:GetAmount())
                ResourceToAdd:SetAmount(Resource:GetAmount())
                self.PrimaryStorage:ConsumeResource(Name, Resource:GetAmount())
                table.insert(MinedResources, ResourceToAdd)

                CurrentResourceVolume = CurrentResourceVolume + TotalResourceVolume
            end
        end
    end

    return MinedResources
end

function ENT:ApplyDamage(Damage, Attacker, Inflictor, HitData)
    -- TODO: Implement dropping resource items on damage
    return false
end