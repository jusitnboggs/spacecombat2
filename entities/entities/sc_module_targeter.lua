AddCSLuaFile()

DEFINE_BASECLASS("base_moduleentity")

ENT.PrintName = "Targeting Device"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Ordering weapons around since 3702!"
ENT.Instructions = "1. Find nearest target. 2. Put weight on fire button. 3. ??? 4. Salvage wreckage."

ENT.Spawnable = false
ENT.AdminOnly = false

local base = scripted_ents.Get("base_moduleentity")
hook.Add("InitPostEntity", "sc_targeter_post_entity_init", function()
	base = scripted_ents.Get("base_moduleentity")
end)

function ENT:SharedInit()
   base.SharedInit(self)

   SC.NWAccessors.CreateNWAccessor(self, "Target", "entity", NULL)
   SC.NWAccessors.CreateNWAccessor(self, "TargetOverride", "bool", false)

   self:SetModuleName("Targeting Device")
   self:SetCycleDuration(5)
end

function GetTargetPosition()

end

function GetPredictedTargetPosition(TimeToImpact)

end

if CLIENT then
	return
end

function ENT:OnModuleEnabled()
    if not self:IsProtected() then SC.Error("sc_module_targeter::OnModuleEnabled Core doesn't exist yet? WTF?", 5) return end

    self:GetProtector():SetTargeter(self)
end

function ENT:OnModuleDisabled()
    if not self:IsProtected() then SC.Error("sc_module_targeter::OnModuleDisabled Core doesn't exist? WTF?", 5) return end

    self:GetProtector():SetTargeter(NULL)
end

function ENT:OnCycleStarted()
    -- Called when the module starts a cycle
end

function ENT:OnStoppedCycling()
    -- Called when the entity stops cycling. This is different from OnCycledFinished, which calls at the end of every cycle.
    self:SetTarget(NULL)
end

function ENT:GetWirePorts()
    return {
            "On",
            "Mute",
            "Hide overlay",
            "Target Override [ENTITY]"
        },
        {
            "Cycle Percent",
            "On",
            "Target [ENTITY]"
        }
end

function ENT:SaveSCInfo()
    return {}
end

function ENT:LoadSCInfo(Info)

end

duplicator.RegisterEntityClass("sc_module_targeter", GAMEMODE.MakeEnt, "Data")
