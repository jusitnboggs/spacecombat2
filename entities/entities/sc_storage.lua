AddCSLuaFile()

DEFINE_BASECLASS("base_storageentity")

ENT.PrintName = "Storage Cache"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Quantum storage."
ENT.Instructions = "Throw items at face to insert, bash with hammer to retreive."

ENT.Spawnable = false
ENT.AdminOnly = false

if CLIENT then
	return
end

local base = scripted_ents.Get("base_storageentity")
hook.Add( "InitPostEntity", "sc_storage_post_entity_init", function()
	base = scripted_ents.Get("base_storageentity")
end)

function ENT:SaveSCInfo()
    return {StorageType = self:GetStorageType(), StorageMultipliers = table.Copy(self.StorageMultipliers)}
end

function ENT:LoadSCInfo(Info)
    -- Don't setup the storage if we're invalid
    if GAMEMODE:GetDefaultStorageModel(Info.StorageType) == nil then
        return
    end

    self:SetStorageType(Info.StorageType or "Gas", Info.StorageMultipliers)
end

local function MakeStorage(Ply, Data)
    -- Replace invalid storages with props
    if Data and Data.EntityMods then
        local SC2Data = Data.EntityMods.SC2DupeInfo
        if SC2Data then
            if GAMEMODE:GetDefaultStorageModel(SC2Data.StorageType) == nil then
                Data.Class = "prop_physics"
            end
        end

        Data.EntityMods.SC2DupeInfo = nil
    end

    return GAMEMODE.MakeEnt(Ply, Data)
end
duplicator.RegisterEntityClass("sc_storage", MakeStorage, "Data")

function ENT:SetStorageType(StorageType, Multipliers)
    if StorageType == nil or StorageType == "" then return end
    base.SetStorageType(self, StorageType)

    local ResourcesOfType = GAMEMODE:GetResourcesOfType(StorageType)
    local total = table.Count(ResourcesOfType)
    local HasMultipliers = Multipliers and true or false

    self.StorageMultipliers = table.Copy(Multipliers or {})

    -- If we have multipliers then total needs to be this table added up
    if HasMultipliers then
        total = 0
        for i,k in pairs(self.StorageMultipliers) do
            if ResourcesOfType[i] then
                total = total + math.max(k, 0)
            end
        end
    end

    local Storage = self:GetStorageOfType(self:GetStorageType())
    for resource, default in pairs(ResourcesOfType) do
        if default and default:GetType() == self:GetStorageType() then
            local mult = 1
            if HasMultipliers and self.StorageMultipliers[resource] then
                mult = self.StorageMultipliers[resource]
            end

            if mult > 0 then
                local max = math.floor((mult * Storage:GetSize() / total) / default:GetSize())
                Storage:AddResourceType(resource, max)
            end
        end
    end
end
