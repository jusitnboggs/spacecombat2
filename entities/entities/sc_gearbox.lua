AddCSLuaFile()

DEFINE_BASECLASS("base_scentity")

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName	= "Vehicle Gearbox"
ENT.Author	= "Lt.Brandon"

ENT.Spawnable	= true
ENT.AdminSpawnable = false
ENT.IsDriveLineComponent = true
ENT.IsDriveBeltComponent = false
ENT.IsMechanical = true

-- Gear Ratios
ENT.GearRatios = {}
ENT.GearRatios[-1] = -0.18
ENT.GearRatios[0] = 0.0
ENT.GearRatios[1] = 0.11
ENT.GearRatios[2] = 0.18
ENT.GearRatios[3] = 0.24
ENT.GearRatios[4] = 0.38
ENT.GearRatios[5] = 0.55
ENT.GearRatios[6] = 0.77
ENT.GearRatios[7] = 1.00

ENT.FinalDrive = 1.00
ENT.CurrentGear = 0

-- Outputs
ENT.OutputRPM = 0
ENT.OutputInertia = 0
ENT.LeftClutch = 1
ENT.RightClutch = 1
ENT.HasDualClutch = false
ENT.HasDualDiff = false
ENT.SteerRate = 0

-- Efficiency
ENT.InEfficiency = 0.0001
ENT.MaxTorque = 200

local Gearboxes
if GAMEMODE and GAMEMODE.Mechanical then
    Gearboxes = GAMEMODE.Mechanical.Gearboxes
end

local base = scripted_ents.Get("base_scentity")
hook.Remove("SC.Config.PostLoad", "sc_gearbox_init")
hook.Add("SC.Config.PostLoad", "sc_gearbox_init", function()
    base = scripted_ents.Get("base_scentity")

    local GM = GAMEMODE

    GM.Mechanical = GM.Mechanical or {}
    GM.Mechanical.Gearboxes = {}
    Gearboxes = GM.Mechanical.Gearboxes

    function GM.Mechanical.RegisterGearbox(ID, Data)
        -- TODO: Validate incoming data
        Gearboxes[ID] = Data
    end

    GM.Mechanical.RegisterGearbox("Default", {
        Name = "SC Gearbox",
        Description = "Default gearbox, for testing.",
        Model = "models/engines/i8med.mdl",
        MaxTorque = 200,
        InEfficiency = 0.0001,
        FinalDrive = 1.00,
        GearRatios = {
            [-1] = -0.18,
            [0] = 0.0,
            [1] = 0.11,
            [2] = 0.18,
            [3] = 0.24,
            [4] = 0.38,
            [5] = 0.55,
            [6] = 0.77,
            [7] = 1.00,
        }
    })

    hook.Run("SC.RegisterGearboxes")
    hook.Run("SC.PostRegisterGearboxes")
end)

function ENT:SharedInit()
    SC.NWAccessors.CreateNWAccessor(self, "GearboxType", "cachedstring", "Default", 1, function(self, Name, Value)
        if not CLIENT then return end
        if IsValid(self) then
            self:SetupGearbox(Value)
        end
    end)
end

function ENT:Initialize()
    base.Initialize(self)

    if CLIENT then return end

    self.Wheels = self.Wheels or {}
    self.WheelTorqueAxes = self.WheelTorqueAxes or {}
    self.DriveLineComponents = self.DriveLineComponents or {}
end

local OverlayTextFormat = [[
- %s -

Max Torque: %d lb-ft
Final Drive: %G
Gear Ratios: %s
]]

function ENT:UpdateOverlayText()
    local Gears = ""
    for Gear, Ratio in SortedPairs(self.GearRatios) do
        Gears = string.format("%s\n%s: %G", Gears, self:GetGearName(Gear), Ratio)
    end

    self:SetOverlayText(string.format(
        OverlayTextFormat,
        self.GearboxName,
        self.MaxTorque,
        self.FinalDrive,
        Gears
    ))
end

function ENT:GetWirePorts()
    local Inputs = {}
    local Outputs = {"Ratio", "Output RPM", "Output Inertia"}

    if self.HasMultipleGears then
        table.insert(Inputs, "Gear")
        table.insert(Inputs, "Gear Up")
        table.insert(Inputs, "Gear Down")
        table.insert(Outputs, "Current Gear")
    end

    if self.HasDualClutch then
        table.insert(Inputs, "Left Clutch")
        table.insert(Inputs, "Right Clutch")
    else
        table.insert(Inputs, "Clutch")
    end

    if self.HasDualDiff then
        table.insert(Inputs, "Steer Rate")
    end

    return Inputs, Outputs
end

function ENT:SetupGearbox(GearboxType, Settings)
    local GearboxData = Gearboxes[GearboxType]
    if not GearboxData then
        SC.Error(string.format("Invalid gearbox type %s, changing to default gearbox", tostring(GearboxType)), 5)
        GearboxData = Gearboxes["Default"]
        GearboxType = "Default"
    end

    self.GearboxName = GearboxData.Name
    self.MaxTorque = GearboxData.MaxTorque
    self.InEfficiency = GearboxData.InEfficiency
    self.FinalDrive = GearboxData.FinalDrive
    self.GearRatios = GearboxData.GearRatios
    self.HasMultipleGears = table.Count(GearboxData.GearRatios) > 1

    if Settings then
        self.HasDualClutch = Settings.HasDualClutch or false
        self.HasDualDiff = Settings.HasDualDiff or false
        self.FinalDrive = Settings.FinalDrive or GearboxData.FinalDrive

        for Gear, Ratio in pairs(Settings.GearRatios or {}) do
            if self.GearRatios[Gear] ~= nil then
                self.GearRatios[Gear] = tonumber(Ratio)
            end
        end
    end

    if SERVER then
        self:SetModel(GearboxData.Model)
        self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
        self:SetUseType(SIMPLE_USE)
        self:SetGearboxType(GearboxType)
        self:UpdateOverlayText()
        self:SetupWirePorts()
    end

    self.SetupFinished = true
end

function ENT:IsLinked(Linked)
    if not IsValid(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        return table.HasValue(self.DriveLineComponents, Linked)
    else
        return table.HasValue(self.Wheels, Linked)
    end
end

function ENT:Link(Linked)
    if not IsValid(Linked) then
        return false
    end

    if Linked == self then
        return false
    end

    if self:IsLinked(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        table.insert(self.DriveLineComponents, Linked)
    elseif not Linked.IsMechanical then
        local Axis = self:GetRight()
        local Phys = Linked:GetPhysicsObject()
        if IsValid(Phys) then
            Axis = Phys:WorldToLocalVector(Axis)
        end

        local Index = table.insert(self.Wheels, Linked)
        self.WheelTorqueAxes[Index] = Axis
    else
        return false
    end

    return true
end

function ENT:Unlink(Linked)
    if not self:IsLinked(Linked) then
        return false
    end

    if Linked.IsDriveLineComponent then
        table.RemoveByValue(self.DriveLineComponents, Linked)
    else
        local Index = table.RemoveByValue(self.Wheels, Linked)
        table.remove(self.WheelTorqueAxes, Index)
    end

    return true
end

function ENT:HandleClutchAndDiff(Component)
    local Clutch = self.LeftClutch
    local Direction = 1

    if self.HasDualClutch or self.HasDualDiff then
        local RightSide = (self:GetPos() - Component:GetPos()):DotProduct(self:GetRight()) > 0
        if self.HasDualClutch and RightSide then
            Clutch = self.RightClutch
        end

        if self.HasDualDiff then
            local Sign = self.SteerRate ~= 0 and self.SteerRate / math.abs(self.SteerRate) or 0
            if Sign ~= 0 then
                Clutch = math.Clamp(Clutch * math.abs(self.SteerRate), 0, 1)
                if RightSide then
                    Direction = Sign
                else
                    Direction = -Sign
                end
            end
        end
    end

    return Clutch, Direction
end

function ENT:OnEngineUpdate(UpdateTime, InputRPM, InputIntertia)
    -- Don't need to do anything in neutral
    if self.HasMultipleGears and self.CurrentGear == 0 then
        self:UpdateOutputs()
        return 0
    end

    -- Update output torque and RPM based on our current gear ratio
    local GearRatio = (self.GearRatios[self.CurrentGear] or 0) * self.FinalDrive
    self.OutputRPM = InputRPM * GearRatio
    self.OutputInertia = InputIntertia / math.abs(GearRatio)

    -- Update all of the components with their alloted torque
    local UsedTorque = 0
    local NumComponents = #self.DriveLineComponents
    for Index, Component in pairs(self.DriveLineComponents) do
        if Component.OnEngineUpdate then
            local Clutch, Direction = self:HandleClutchAndDiff(Component)
            if Clutch > 0.001 then
                UsedTorque = UsedTorque + (Component:OnEngineUpdate(UpdateTime, self.OutputRPM * Direction, self.OutputInertia * Clutch / NumComponents) or 0)
            end
        end
    end

    local Phys = self:GetPhysicsObject()
    local AngularVelocity = Phys:LocalToWorldVector(Phys:GetAngleVelocity())

    local UsedWheelTorque = 0
    local NumWheels = #self.Wheels
    for Index, Wheel in pairs(self.Wheels) do
        if IsValid(Wheel) then
            local WheelPhys = Wheel:GetPhysicsObject()
            local TorqueAxis = WheelPhys:LocalToWorldVector(self.WheelTorqueAxes[Index])
            local Clutch, Direction = self:HandleClutchAndDiff(Wheel)
            if Clutch > 0.001 then
                UsedWheelTorque = UsedWheelTorque + self:UpdateWheel(self.OutputRPM * Direction, self.OutputInertia * Clutch / NumWheels, AngularVelocity, TorqueAxis, WheelPhys)
            end
        end
    end

    self:UpdateOutputs()

    return (UsedWheelTorque + UsedTorque) * math.abs(GearRatio)
end

function ENT:CalculateWheelRPM(WheelPhys, TorqueAxis, AngularVelocity)
    local VelDiff = WheelPhys:LocalToWorldVector(WheelPhys:GetAngleVelocity()) - AngularVelocity
	local BaseRPM = VelDiff:Dot(TorqueAxis)

	return BaseRPM / -6
end

function ENT:UpdateWheel(OutputRPM, AvailableInertia, AngularVelocity, TorqueAxis, WheelPhys)
    if IsValid(WheelPhys) then
        local CurrentRPM = self:CalculateWheelRPM(WheelPhys, TorqueAxis, AngularVelocity)
        local WheelInertia = WheelPhys:GetInertia():Length() * 2.2046
        local RPMDiff = OutputRPM - CurrentRPM
        local NeededInertia = WheelInertia * RPMDiff
        local AppliedTorque = math.Clamp(NeededInertia, -AvailableInertia, AvailableInertia)

        WheelPhys:ApplyTorqueCenter(TorqueAxis * -AppliedTorque, 0, 0)

        return AppliedTorque
    else
        return 0
    end
end

function ENT:GetGearName(Gear)
    if Gear < 0 then
        return string.format("R%d", Gear)
    elseif Gear > 0 then
        return string.format("%d", Gear)
    else
        if self.HasMultipleGears then
            return "N"
        else
            return "1"
        end
    end
end

function ENT:ChangeGear(NewGear)
    if self.CurrentGear == NewGear or not self.GearRatios[NewGear] then return end
    self.CurrentGear = NewGear
    WireLib.TriggerOutput(self, "Ratio", self.GearRatios[self.CurrentGear] * self.FinalDrive)
    WireLib.TriggerOutput(self, "Current Gear", self.CurrentGear)
    self:EmitSound("buttons/lever7.wav",250,100)
end

function ENT:TriggerInput(Name, Value)
    if self.HasMultipleGears then
        if Name == "Gear Up" and Value == 1 then
            self:ChangeGear(self.CurrentGear + 1)
        elseif Name == "Gear Down" and Value == 1 then
            self:ChangeGear(self.CurrentGear - 1)
        elseif Name == "Gear" then
            self:ChangeGear(Value)
        end
    end

    if self.HasDualClutch then
        if Name == "Left Clutch" then
            self.LeftClutch = 1 - math.Clamp(Value, 0, 1)
        elseif Name == "Right Clutch" then
            self.RightClutch = 1 - math.Clamp(Value, 0, 100)
        end
    else
        if Name == "Clutch" then
            self.LeftClutch = 1 - math.Clamp(Value, 0, 1)
        end
    end

    if self.HasDualDiff then
        if Name == "Steer Rate" then
            self.SteerRate = math.Clamp(Value, -1, 1)
        end
    end
end

function ENT:UpdateOutputs()
    if WireLib then
        WireLib.TriggerOutput(self, "Output RPM", self.OutputRPM)
        WireLib.TriggerOutput(self, "Output Inertia", self.OutputInertia)
    end
end

function ENT:SaveSCInfo()
    local DriveLineComponentIDs = {}
    for _, Component in ipairs(self.DriveLineComponents) do
        if IsValid(Component) then
            table.insert(DriveLineComponentIDs, Component:EntIndex())
        end
    end

    local WheelIDs = {}
    for _, Component in ipairs(self.Wheels) do
        if IsValid(Component) then
            table.insert(WheelIDs, Component:EntIndex())
        end
    end

    return {
        GearboxType = self:GetGearboxType(),
        DriveLineComponents = DriveLineComponentIDs,
        Wheels = WheelIDs,
        GearRatios = table.Copy(self.GearRatios),
        FinalDrive = self.FinalDrive,
        HasDualDiff = self.HasDualDiff,
        HasDualClutch = self.HasDualClutch,
    }
end

function ENT:ApplySCDupeInfo(Info, GetEntByID)
    local Settings = {
        GearRatios = Info.GearRatios,
        FinalDrive = Info.FinalDrive,
        HasDualDiff = Info.HasDualDiff,
        HasDualClutch = Info.HasDualClutch
    }

    self:SetupGearbox(Info.GearboxType or "Default", Settings)

    if Info.DriveLineComponents then
        self.DriveLineComponents = {}
        for _, EntIndex in ipairs(Info.DriveLineComponents) do
            local Entity = GetEntByID(EntIndex)
            if IsValid(Entity) and Entity.IsDriveLineComponent then
                self:Link(Entity)
            end
        end
    end

    if Info.Wheels then
        self.Wheels = {}
        for _, EntIndex in ipairs(Info.Wheels) do
            local Entity = GetEntByID(EntIndex)
            if IsValid(Entity) then
                self:Link(Entity)
            end
        end
    end
end

duplicator.RegisterEntityClass("sc_gearbox", GAMEMODE.MakeEnt, "Data")