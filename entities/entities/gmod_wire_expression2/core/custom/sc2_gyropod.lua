e2function number entity:isSC2Gyropod()
	if IsValid(this) and this.IsSCGyropod then
		return 1
    end

    return 0
end

e2function number entity:getGyropodAcceleration()
	if IsValid(this) and this.IsSCGyropod then
		return this.Acceleration or 0
    end

    return 0
end

e2function number entity:getGyropodTurnAcceleration()
	if IsValid(this) and this.IsSCGyropod then
		return this.TurnAcceleration or 0
    end

    return 0
end

e2function number entity:getGyropodTurnSpeedLimit()
	if IsValid(this) and this.IsSCGyropod then
        local SpeedLimit = this.TurnSpeed
        if this.UserMaxTurnSpeed >= 0 then
            SpeedLimit = math.min(SpeedLimit, this.UserMaxTurnSpeed)
        end

		return SpeedLimit
    end

    return 0
end

e2function number entity:getGyropodSpeedLimit()
    if IsValid(this) and this.IsSCGyropod then
        local SpeedLimit = this.SpeedLimit
        if this.UserMaxSpeed >= 0 then
            SpeedLimit = math.min(SpeedLimit, this.UserMaxSpeed)
        end

		return SpeedLimit
    end

    return 0
end

e2function number entity:isGyropodRollLockEnabled()
	if IsValid(this) and this.IsSCGyropod then
		return this.RollLock and 1 or 0
    end

    return 0
end

e2function number entity:isGyropodLeveling()
	if IsValid(this) and this.IsSCGyropod then
		return this.Leveling and 1 or 0
    end

    return 0
end

e2function number entity:isGyropodAimingAtTarget()
	if IsValid(this) and this.IsSCGyropod then
		return this.AimAtTarget and 1 or 0
    end

    return 0
end

e2function number entity:isGyropodOn()
	if IsValid(this) and this.IsSCGyropod then
		return this.On and 1 or 0
    end

    return 0
end