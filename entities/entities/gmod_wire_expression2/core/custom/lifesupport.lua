E2Lib.RegisterExtension("Lifesupport", true)

local isOwner = E2Lib.isOwner

registerCallback("e2lib_replace_function", function(funcname, func, oldfunc)
	if funcname == "isOwner" then
		isOwner = func
	end
end)

local function IsRDDevice(ent)
	if ent.IsLSEnt then
		return true
	end

	return false
end

--=======================================
-- GENERAL RESOURCE FUNCTIONS
--=======================================

__e2setcost(1)
e2function number entity:isNode()
	if IsValid(this) then
		if this.IsNode then
			return 1
		end
	end

	return 0
end

__e2setcost(1)
e2function number entity:isLSDevice()
	if IsValid(this) then
		if this.IsLSEnt then
			return 1
		end
	end

	return 0
end

__e2setcost(2)
e2function number entity:lsCapacity(string res)
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		return this:GetMaxAmount(res)
	end

	return 0
end

__e2setcost(2)
e2function number entity:lsAmount(string res)
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		return this:GetAmount(res)
	end

	return 0
end

__e2setcost(2)
e2function number lsResSize(string res)
    local Resource = GAMEMODE:GetResourceFromName(res)
    if Resource then
        return Resource:GetSize()
    end

	return 0
end

__e2setcost(2)
e2function string lsResType(string res)
    local Resource = GAMEMODE:GetResourceFromName(res)
    if Resource then
        return Resource:GetType()
    end

	return "invalid"
end

e2function string entity:lsDeviceName()
	if IsValid(this) then
		if this.GeneratorInfo then
			return this.GeneratorInfo:GetName()
        elseif this.GetModuleName then
            return this:GetModuleName("FAIL")
		elseif this.PrintName then
			return this.PrintName
		else
			return ""
		end
	end

	return ""
end

e2function number entity:lsMultiplier()
	if IsValid(this) and this.IsLSEnt and this.GetMultiplier then
		return this:GetMultiplier()
	end

	return -1
end

e2function number entity:lsWantedMultiplier()
	if IsValid(this) and this.IsLSEnt and this.GetWantedMultiplier then
		return this:GetWantedMultiplier()
	end

	return -1
end

e2function number entity:lsIsOn()
	if IsValid(this) and this.IsLSEnt and this.IsOn then
		return this:IsOn() and 1 or 0
	end

	return -1
end

__e2setcost(5)
e2function array entity:lsResourceList()
	local temp = {}
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		local storages = this:GetStorage()
		local num = 1
		for i,k in pairs(storages) do
            for name, res in pairs(k:GetStored()) do
			    temp[num] = name
			    num = num + 1
            end
		end
	end

	return temp
end

__e2setcost(5)
e2function array entity:lsResourceList(string Type)
	local temp = {}
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		local storages = this:GetStorage()
        local storage = storages[Type]
        if storage then
            for name, res in pairs(storage:GetStored()) do
                table.insert(temp, name)
            end
        end
	end

	return temp
end

--Returns all of the entities which are part of the current res net.
--Only works on resource node entities!
__e2setcost(5)
e2function array entity:lsNetEntities()
	local temp = {}
	if IsValid(this) and isOwner(self,this) and this.IsNode then
		local res = this:GetLinks()
		local num = 1
		for i,k in pairs(res) do
			temp[num] = Entity(i)
			num = num + 1
		end
	end

	return temp
end

__e2setcost(5)
e2function number entity:lsNodeSize()
	if IsValid(this) then
		if this.IsNode then
			return this:GetNodeRadius()
		end
	end

	return -1
end

-- Drains a resource from a network.
e2function number entity:lsConsumeResource(string name, amount)
	-- Check permissions
	if not self.player:IsAdmin() then return -3 end --Not allowed
	if not IsValid(this) then return -1 end -- Not valid.
	if amount < 0 then amount = math.abs(amount) end

	if this.IsLSEnt and (IsValid(this:GetNode()) or this.IsNode) then
		if this:ConsumeResource(name,amount) then return 1 end -- nom nom nom
		return 0
	end
	return -2 -- We can't do that, Jim.
end

-- Links LS Ents
e2function number entity:lsLink(entity other)
	if not IsValid(this) then return 0 end
	if not IsValid(other) then return 0 end
	if this.IsNode and other.IsNode then return 0 end --Remove me when core->core links are either sane or not possible

	if not self.player:IsAdmin() then
		if not isOwner(self,this) then return 0 end
		if not isOwner(self,other) then return 0 end
	end

	--Determine which of these is the core
	if this.IsNode and other.IsLSEnt then
		if other:Link(this) then --Fuck e2's lack of boolean support
			return 1
		else
			return 0
		end
	elseif other.IsNode and this.IsLSEnt then
		if this:Link(other) then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

--Unlink an entity
e2function void entity:lsUnlink()
	if not IsValid(this) then return end

	if not self.player:IsAdmin() then
		if not isOwner(self,this) then return end
	end

	if this.IsLSEnt then
		this:Unlink()
	end
end

--Get linked to
e2function entity entity:lsGetNode()
	if not IsValid(this) then return NULL end
	if not this.IsLSEnt then return NULL end

	return this:GetNode()
end

--Get linked status
e2function number entity:lsIsLinked()
	if not IsValid(this) then return 0 end
	if not this.IsLSEnt then return 0 end

	if IsValid(this:GetNode()) then
		return 1
	else
		return 0
	end
end

--== End of General Resource Functions ==

--=======================================
-- ATMOSPHERE FUNCTIONS
--=======================================

--Environment Accessor Functions
e2function array getPlanets()
	local planets = {}
	for k,v in pairs(GAMEMODE.AtmosphereTable) do
		planets[k] = v:GetCelestial():getEntity()
	end

	return planets
end

e2function array getStars()
	local stars = {}
	for k,v in pairs(GAMEMODE.StarTable) do
		stars[k] = v:GetCelestial():getEntity()
	end

	return stars
end

--Get Environment from Ent
local function GetEnv(ent)
	if not IsValid(ent) then return nil end

	--Is an Environment
	if ent:GetClass() == "sc_planet" then
		return ent:GetCelestial():GetEnvironment()

	--Get Environment Reference
	else
		if ent.GetEnvironment then return ent:GetEnvironment() end

		return nil
	end
end

e2function string entity:lsName()
	if not IsValid(this) then return "Space" end

	local env = GetEnv(this)
	if env then
		return env:GetName()
	end

	return "Space"
end

e2function number entity:lsSize()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env and env:GetName() ~= "Space" then
		return env:GetRadius()
	else
		return math.huge
	end

	return -1
end

e2function number entity:lsTemperature()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetTemperature()
	end

	return -1
end

e2function number entity:lsGravity()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetGravity()
	end

	return -1
end

e2function number entity:lsPressure()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetPressure()
	end

	return -1
end

e2function number entity:lsO2Percent()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Oxygen")
		if res then
			return env:GetAmount("Oxygen") / env:GetMaxAmount("Oxygen")
		else
			return 0
		end
	end

	return -1
end

e2function number entity:lsCO2Percent()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Carbon Dioxide")
		if res then
			return env:GetAmount("Carbon Dioxide") / env:GetMaxAmount("Carbon Dioxide")
		else
			return 0
		end
	end

	return -1
end

e2function number entity:lsNPercent()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Nitrogen")
		if res then
			return env:GetAmount("Nitrogen") / env:GetMaxAmount("Nitrogen")
		else
			return 0
		end
	end

	return -1
end

e2function number entity:lsHPercent()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Hydrogen")
		if res then
			return env:GetAmount("Hydrogen") / env:GetMaxAmount("Hydrogen")
		else
			return 0
		end
	end

	return -1
end

e2function number entity:lsVolume()
	if not IsValid(this)then return -1 end

	local env = GetEnv(this)
	if env and env:GetName() ~= "Space" then
		return env:GetVolume()
	else
		return math.huge
	end

	return -1
end

e2function number entity:lsO2Amount()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetAmount("Oxygen")
	end

	return -1
end

e2function number entity:lsCO2Amount()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetAmount("Carbon Dioxide")
	end

	return -1
end

e2function number entity:lsNAmount()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetAmount("Nitrogen")
	end

	return -1
end

e2function number entity:lsHAmount()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetAmount("Hydrogen")
	end

	return -1
end

--Return all resources that currently exist within the atmosphere.
e2function array entity:lsEnvResources()
	if not IsValid(this) then return {} end

	local env = GetEnv(this)
	if env then
		local resources = env:GetResources()
		local resReturn = {}
		for k,v in pairs(resources) do
			table.insert(resReturn, v:GetName())
		end

		return resReturn
	end

	return {}
end

--Return the amount of the specified resource within the atmosphere.
e2function number entity:lsResAmount(string res)
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		return env:GetAmount(res)
	end

	return -1
end

e2function number entity:lsResPercent(string res)
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
	if env then
		local res1 = env:GetResource(res)
		if res1 then
			return env:GetAmount(res) / env:GetMaxAmount(res)
		else
			return 0
		end
	end

	return -1
end

e2function number entity:lsResTotal()
	if not IsValid(this) then return -1 end

	local env = GetEnv(this)
    if env then
        local Total = 0
        for _, Container in pairs(env:GetContainers()) do
            Total = Total + Container:GetSize()
        end
		return Total
	end

	return -1
end

--Get the atmosphere an ent is being managed by
e2function entity entity:lsGetEnvironment()
	if not IsValid(this) then return NULL end

	local env = GetEnv(this)
	if env then
		local celestial = env:GetCelestial()
		if celestial then
			return celestial:getEntity()
		end
	end

	return NULL
end

e2function vector lsGetSunDirection()
	return GAMEMODE:GetSunDir()
end

--Tell spacebuild to ignore this entity's gravity (for propcore stuff)
e2function void entity:sbSetGravityOverride(number)
	if not IsValid(this) then return end
    if not isOwner(self,this) or this:IsPlayer() then return end

	this.HasGravityOverride = (number ~= 0)
end

e2function number entity:sbGetGravityOverride()
	if not IsValid(this) then return 0 end

	if this.HasGravityOverride then return 1 end
	return 0
end

--The above, for drag
e2function void entity:sbSetDragOverride(number)
	if not IsValid(this) then return end
    if not isOwner(self,this) or this:IsPlayer() then return end

	this.HasDragOverride = (number ~= 0)
end

e2function number entity:sbGetDragOverride()
	if not IsValid(this) then return 0 end

	if this.HasDragOverride then return 1 end
	return 0
end

--=== End of Atmosphere Functions =======


--=======================================
-- SUIT INFO FUNCTIONS
--=======================================

__e2setcost(1)
e2function number entity:lsSuitLifesupport()
	if CAF then
		if this.SB4Suit then
			return (this.SB4Suit:GetIntake() / this.SB4Suit:GetMaxIntake())*100
		end
	end

	return 0
end

--=== End of Suit Info Functions ========


--=======================================
-- RESOURCE PUMP FUNCTIONS
--=======================================

local function isPump(ent)
	if ent == nil or not IsValid(ent) then return false end

	return ent:GetClass() == "sc_resource_pump" or ent:GetClass() == "sc_resource_pump_global"
end

__e2setcost(1)
e2function number entity:lsPumpDisconnectOutbound()
	if not isPump(this) then return 0 end
    if not isOwner(self,this) then return 0 end
	if not IsValid(this:GetOutboundPump()) then return 0 end

	this:Disconnect()

	return 1
end

__e2setcost(1)
e2function number entity:lsPumpDisconnectInbound()
	if not isPump(this) then return 0 end
    if not isOwner(self,this) then return 0 end
	if not IsValid(this:GetInboundPump()) then return 0 end

	this:GetInboundPump():Disconnect()

	return 1
end

__e2setcost(1)
e2function entity entity:lsPumpGetPlug()
	if not isPump(this) then return NULL end

	return this:GetPlug() or NULL
end

__e2setcost(1)
e2function number entity:lsPumpUndockPlug()
	if not isPump(this) then return 0 end
    if not isOwner(self,this) then return 0 end
	if not IsValid(this:GetPlug()) then return 0 end

	this:GetPlug():DisconnectFromSocket()
end

__e2setcost(1)
e2function entity entity:lsPumpGetInboundConnection()
	if not isPump(this) then return NULL end

	return this:GetInboundPump() or NULL
end

__e2setcost(1)
e2function entity entity:lsPumpGetOutboundConnection()
	if not isPump(this) then return NULL end

	return this:GetOutboundPump() or NULL
end

__e2setcost(1)
e2function number entity:lsPumpGetTransferSpeed()
	if not isPump(this) then return -1 end

	return this:GetTransferSpeed()
end

__e2setcost(1)
e2function number entity:lsPumpSetTransferSpeed(number speed)
	if not isPump(this) then return 0 end
	if not isOwner(self,this) then return 0 end

	this:SetDesiredTransferSpeed(speed)
	return 1
end

__e2setcost(1)
e2function number entity:lsPumpQueueTask(string resourceName, number amount)
    if amount ~= amount then return -3 end -- if its nan, fuck off
	if not isPump(this) then return -2 end
	if not IsValid(this:GetOutboundPump()) then return -1 end
    if not (this:GetClass() == "sc_resource_pump_global" and isOwner(self,this:GetOutboundPump()) or isOwner(self,this)) then return -4 end

	return this:AddTask(resourceName, amount) and 1 or 0
end

__e2setcost(1)
e2function number entity:lsPumpQueueTask(string resourceName, number amount, number continuous)
    if amount ~= amount then return -3 end -- if its nan, fuck off
	if not isPump(this) then return -2 end
	if not IsValid(this:GetOutboundPump()) then return -1 end
    if not (this:GetClass() == "sc_resource_pump_global" and isOwner(self,this:GetOutboundPump()) or isOwner(self,this)) then return -4 end

	return this:AddTask(resourceName, amount, continuous == 1) and 1 or 0
end

__e2setcost(1)
e2function number entity:lsPumpRemoveTask(number task)
	if not isPump(this) then return -1 end
    if not isOwner(self,this) then return -1 end

	this:RemoveTask(task)
	return 1
end

local function taskToE2Table(self, taskNum)
	local ret = {n={},ntypes={},s={},stypes={},size=0}

	if not IsValid(self) then return ret end
	if not self:GetClass() == "sc_resource_pump" and not self:GetClass() == "sc_resource_pump_global" then return ret end

	--Get Current Task
	local task = {}
	if taskNum == 0 then
		task = self:GetCurrentTask()
	else
		task = self:GetQueue()[taskNum]
	end

	if task == nil then return ret end

	--Convert to E2 table
	ret.s.Resource = task.Resource
	ret.stypes.Resource = "s"

	ret.s.Amount = task.Amount
	ret.stypes.Amount = "n"

	ret.s.Output = task.Output
	ret.stypes.Output = "n"

	ret.size = 3

	return ret
end

__e2setcost(1)
e2function table entity:lsPumpGetTask(number task)
	if not isPump(this) then return {n={},ntypes={},s={},stypes={},size=0} end

	return taskToE2Table(this, task)
end

__e2setcost(1)
e2function number entity:lsPumpGetQueueLength()
	if not isPump(this) then return -1 end

	return this:GetQueueLength()
end

__e2setcost(1)
e2function number entity:lsPumpGetTaskProgress()
	if not isPump(this) then return -1 end

	local task = this:GetCurrentTask()
	if task then
		return task.Progress
	end

	return 0
end

--Run On Task Completion Hook
__e2setcost(10)
e2function void runOnPumpTaskComplete(entity pump, number toggle)
	if not isPump(pump) then return end
    if not isOwner(self,pump) then return end

	--Toggle On
	if toggle then
		pump.E2Hooks[self.entity] = true
		self.data.lifesupport.pumpHook[pump] = true

	--Toggle Off
	else
		pump.E2Hooks[self.entity] = nil
		self.data.lifesupport.pumpHook[pump] = nil
	end
end

__e2setcost(1)
e2function number lsPumpCompleteClk()
	if self.entity.RunByRDPump ~= 1 then return 0 end
	return 1
end

__e2setcost(1)
e2function entity lsPumpCompleted()
	if self.entity.RunByRDPump ~= 1 then return NULL end

	return self.entity.CompletedPump
end

registerCallback("construct",function(self)
	self.data.lifesupport = {}
	self.data.lifesupport.pumpHook = {}
end)

registerCallback("destruct",function(self)
	for v,_ in pairs( self.data.lifesupport.pumpHook ) do
		if IsValid( v ) then
			v.E2Hooks[self.entity] = nil
		end
	end
	self.data.lifesupport.pumpHook = {}
end)

--=== End of Resource Pump Functions ====