AddCSLuaFile()

local base = scripted_ents.Get("base_lsentity")
hook.Add( "InitPostEntity", "base_resourcepump_post_entity_init", function()
	base = scripted_ents.Get("base_lsentity")
end)
local function RegisterPump()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Resource Pump")
    Generator:SetClass("sc_resource_pump")
    Generator:SetDescription("Moves resources to and from another network.")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/props_lab/tpplugholder_single.mdl")
	Generator:SetForceModel(true)

    GAMEMODE:RegisterGenerator(Generator)
end
hook.Add("InitPostEntity", "sc_resource_pump_toolinit", RegisterPump)
hook.Add("OnReloaded", "sc_resource_pump_reloaded", RegisterPump)

function ENT:SharedInit()
    SC.NWAccessors.CreateNWAccessor(self, "PumpName", "string", "#"..self:EntIndex())
	base.SharedInit(self)
end

--=================--
-- ConVar Settings --
--=================--

--Maximum Range before connections drop
if not ConVarExists("sc_resourcepump_maxrange") then
	CreateConVar("sc_resourcepump_maxrange", 1000, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

--Resources per second to transfer
if not ConVarExists("sc_resourcepump_speed") then
	CreateConVar("sc_resourcepump_speed", 1500, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

--Any ship with a SigRad smaller in meters than this will use the base transfer speed
if not ConVarExists("sc_resourcepump_minshipsize") then
	CreateConVar("sc_resourcepump_minshipsize", 20, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

--Portion that the transfer speed increases by (100-200-300, 50-100-150, etc.)
if not ConVarExists("sc_resourcepump_transferincrement") then
	CreateConVar("sc_resourcepump_transferincrement", 100, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

--The higher this number is, the lower the curve on the transfer multiplier
if not ConVarExists("sc_resourcepump_transfercurve") then
	CreateConVar("sc_resourcepump_transfercurve", 5, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end
--END CONVAR SETTINGS--

--Sounds
local Sounds = {}
Sounds.Connect = "buttons/button3.wav"
Sounds.Disconnect = "buttons/button18.wav"
Sounds.Error = "buttons/button11.wav"
Sounds.Failure = "buttons/blip2.wav"
Sounds.TaskAccept = "buttons/button9.wav"
Sounds.TaskRemove = "buttons/button16.wav"
Sounds.TaskDone = "buttons/bell1.wav"

for k,v in pairs(Sounds) do
	util.PrecacheSound(v)
end

ENT.Type = "anim"
ENT.Base = "base_lsentity"
ENT.PrintName = "Resource Pump"
ENT.Author = "Steeveeo"
ENT.Purpose = "Moves resources to and from another network."
ENT.Instructions = "Press Use to open the interface, select any in-range pumps, and queue up transfers."
ENT.Category = "Spacebuild 4"
ENT.Spawnable = false

--==============--
--  CLIENTSIDE  --
--==============--

if CLIENT then

--Queue Tab
local taskList
local resourceList
local resourceSlider
local addTaskButton
local delTaskButton
local taskProgress
local taskLabel

local dermaIsActive = false

--Control Vars
local DSelf
local resourcesAvailable = {}
local queue = {}
local taskPercent = 0

--Close Derma Panel
local function dermaClose()
	dermaIsActive = false
	timer.Destroy("sc_resourcepump_dermathink")
end

--Create Derma Panel
local function dermaCreate(data)
	--Defaults
	resourcesAvailable = {}
	queue = {}
	taskPercent = 0

	--Grab Data
	dermaIsActive = true
	DSelf = data.Pump
	resourcesAvailable = data.Resources

	local main = vgui.Create("DFrame")
	main:SetPos((ScrW() / 2) - 440, (ScrH() / 2) - 270)
	main:SetSize(680, 340)
	main:SetTitle("Resource Pump")
	main:MakePopup()
	main.OnClose = function()
		net.Start("sc_resourcepump_closemenu")
			net.WriteEntity(DSelf)
		net.SendToServer()

		dermaClose()
	end
	main:SetDeleteOnClose(true)

	local propertySheet = vgui.Create("DPropertySheet")
	propertySheet:SetParent(main)
	propertySheet:SetPos(10, 30)
	propertySheet:SetSize(660, 300)


	--=============--
	-- QUEUE PANEL --
	--=============--
	local queuePanel = vgui.Create("DPanel")

	--Queue List
	taskList = vgui.Create("DListView")
	taskList:Clear()
	taskList:SetMultiSelect(false)
	taskList:AddColumn("##"):SetWidth(25)
	taskList:AddColumn("Resource"):SetWidth(100)
	taskList:AddColumn("Amount"):SetWidth(90)
	taskList:SetSize(230, 0)
	taskList:Dock(LEFT)

	--Control Panel
	local taskControlPanel = vgui.Create("DPanel")
	local localStoreName = DSelf.LocalStorageNameOverride or "Local Storage"

	resourceList = vgui.Create("DListView", taskControlPanel)
	resourceList:Clear()
	resourceList:SetMultiSelect(false)
	resourceList:AddColumn("Resource")
	resourceList:AddColumn(localStoreName)
	resourceList:AddColumn("Remote Storage")
	resourceList:SetSize(0, 125)
	resourceList:Dock(TOP)
	resourceList.OnRowSelected = function(panel,row)
		resourceSlider:SetValue(0)

		if transferOut then
			resourceSlider:SetMax(resourcesAvailable[row].res.Local)
		else
			resourceSlider:SetMax(resourcesAvailable[row].res.Remote)
		end
	end

	resourceSlider = vgui.Create("Slider", taskControlPanel)
	resourceSlider:SetPos(10, 130)
	resourceSlider:SetWide(177)
	resourceSlider:SetMin(0)
	resourceSlider:SetMax(0)
	resourceSlider:SetValue(0)
	resourceSlider:SetDecimals(0)

	transferOut = true
	resourceSlider:SetValue(0)
	if resourceList:GetSelectedLine() then
		resourceSlider:SetMax(resourcesAvailable[resourceList:GetSelectedLine()].res.Local)
	else
		resourceSlider:SetMax(0)
	end

	continuousCheckBox = vgui.Create("DCheckBoxLabel", taskControlPanel)
	continuousCheckBox:SetPos(260, 140)
	continuousCheckBox:SetText("Continuous Transfer")
	continuousCheckBox:SetValue(0)
	function continuousCheckBox:OnChange(checked)
		if checked then
			resourceSlider:SetValue(0)
		end
	end

	addTaskButton = vgui.Create("DButton", taskControlPanel)
	addTaskButton:SetSize(140, 20)
	addTaskButton:SetPos(260, 170)
	addTaskButton:SetText("Add Task")
	addTaskButton.DoClick = function()
		net.Start("sc_resourcepump_addtask")
			net.WriteEntity(DSelf)
			net.WriteString(resourcesAvailable[resourceList:GetSelectedLine()].name)
			if continuousCheckBox:GetChecked() then
				net.WriteInt(-1, 32)
			else
				net.WriteInt(resourceSlider:GetValue(), 32)
			end
			net.WriteBool(transferOut)
		net.SendToServer()
	end
	addTaskButton:SetEnabled(false)

	delTaskButton = vgui.Create("DButton", taskControlPanel)
	delTaskButton:SetSize(140, 20)
	delTaskButton:SetPos(10, 170)
	delTaskButton:SetText("Remove Task")
	delTaskButton.DoClick = function()
		if taskList:GetSelectedLine() then
			net.Start("sc_resourcepump_removetask")
				net.WriteEntity(DSelf)
				net.WriteInt(taskList:GetSelectedLine() - 1, 16)
			net.SendToServer()
		end
	end
	delTaskButton:SetEnabled(false)

	taskLabel = vgui.Create("DLabel", taskControlPanel)
	taskLabel:SetText("Current Task: None")
	taskLabel:SetPos(10, 205)
	taskLabel:SizeToContents()

	taskProgress = vgui.Create("DProgress", taskControlPanel)
	taskProgress:SetSize(390, 40)
	taskProgress:SetPos(10, 220)
	taskProgress:SetFraction(0)

	local horizDiv = vgui.Create("DHorizontalDivider", queuePanel)
	horizDiv:Dock(FILL)
	horizDiv:SetLeft(taskList)
	horizDiv:SetRight(taskControlPanel)
	horizDiv:SetDividerWidth(4)
	horizDiv:SetLeftMin(230)
	horizDiv:SetRightMin(410)
	horizDiv:SetLeftWidth(230)


	--Add tabs to property sheet
	propertySheet:AddSheet("Transfer Queue", queuePanel, "icon16/lorry.png", false, false, "Queue")
end

--Update Derma Stuff
local function dermaThink()
	if not dermaIsActive then return end

	--Check if we have all the settings needed for a task
	addTaskButton:SetEnabled(false)
	if resourceList:GetSelectedLine() and (resourceSlider:GetValue() > 0 or continuousCheckBox:GetChecked()) then
		addTaskButton:SetEnabled(true)
	end

	--Remove
	delTaskButton:SetEnabled(false)
	if taskList:GetSelectedLine() then
		delTaskButton:SetEnabled(true)
	end
end

--Open Menu
net.Receive("sc_resourcepump_openmenu", function()
	local data = net.ReadTable()
	dermaCreate(data)

	timer.Create("sc_resourcepump_dermathink", 0.1, 0, dermaThink)
end)

--Update Local Status
net.Receive("sc_resourcepump_updatestatus", function()
	if not dermaIsActive then return end

	local pump = net.ReadEntity()

	if not IsValid(pump) then return end

	DSelf = pump
end)

--Update Available Resources
net.Receive("sc_resourcepump_updateresources", function()
	if not dermaIsActive then return end

	local self = net.ReadEntity()
	local res = net.ReadTable()

	if not IsValid(self) then return end

	--Numerically compile resource list
	resourcesAvailable = {}
	for name,resource in pairs(res) do
		table.insert(resourcesAvailable, {name=name, res=resource})
	end

	--Sort by amount available
	table.sort(resourcesAvailable, function(a,b)
		local thisAmt = a.res.Local + a.res.Remote
		local thatAmt = b.res.Local + b.res.Remote

		return thisAmt > thatAmt
	end)

	--Populate resource list
	resourceList:Clear()
	for k,v in pairs(resourcesAvailable) do
		local line = resourceList:AddLine(v.name, string.Comma(math.floor(v.res.Local)), string.Comma(math.floor(v.res.Remote)))
		line:SetSortValue(2,v.res.Local)
		line:SetSortValue(3,v.res.Remote)
	end
end)

--Update Queue
net.Receive("sc_resourcepump_updatequeue", function()
	if not dermaIsActive then return end

	local self = net.ReadEntity()
	local tasks = net.ReadTable()

	if not IsValid(self) then return end

	queue = tasks

	--Update the shit
	taskList:Clear()
	for k,v in pairs(queue) do
		if v.Amount > -1 then
			taskList:AddLine(k, v.Resource, string.Comma(v.Amount))
		else
			taskList:AddLine(k, v.Resource, "Continuous")
		end
	end

	--Reset progress bar
	taskProgress:SetFraction(0)
end)

--Update Task Progress
net.Receive("sc_resourcepump_updateprogress", function()
	if not dermaIsActive then return end

	local self = net.ReadEntity()
	local prog = net.ReadFloat()

	if not IsValid(self) then return end

	taskPercent = prog
	taskProgress:SetFraction(taskPercent)
end)

function ENT:Initialize()
	self.pulley = ClientsideModel("models/props_c17/pulleywheels_small01.mdl", 7)
	if not IsValid(self.pulley) then return end
	self.pulley:SetPos(self:LocalToWorld(Vector(3,0,9)))
	self.pulley:SetAngles(self:LocalToWorldAngles(Angle(0,90,0)))
	self.pulley:SetParent(self)
end

function ENT:OnRemove()
	if not IsValid(self.pulley) then return end
	self.pulley:Remove()
end
--END CLIENTSIDE--


--==============--
--  SERVERSIDE  --
--==============--

elseif SERVER then

	--SC Generator Tool Setup
	function ENT:SetupGenerator()
		-- Pretty much just a dummy so the tool doesn't error
	end

	--============--
	-- NETWORKING --
	--============--

	--Server to Client
	util.AddNetworkString("sc_resourcepump_openmenu")			--Send derma information
	util.AddNetworkString("sc_resourcepump_updatestatus")		--Update local status
	util.AddNetworkString("sc_resourcepump_updatequeue")		--Update displayed queue
	util.AddNetworkString("sc_resourcepump_updateresources")	--Update displayed queue
	util.AddNetworkString("sc_resourcepump_updateprogress")		--Update current task progress

	--Client to Server

	util.AddNetworkString("sc_resourcepump_addtask")			--Add a new task to queue
	util.AddNetworkString("sc_resourcepump_removetask")			--Remove job
	util.AddNetworkString("sc_resourcepump_closemenu")			--Tell server to stop sending updates on this

	--Client closed menu
	net.Receive("sc_resourcepump_closemenu", function()
		local pump = net.ReadEntity()

		if IsValid(pump) then
			pump.InUseMenu = false
			pump.UsingPly = nil

			--Kill updates
			timer.Destroy("sc_resourcepump_statusupdate" .. pump:EntIndex())
			timer.Destroy("sc_resourcepump_progressupdate" .. pump:EntIndex())
		end
	end)


	--Client sends new task
	net.Receive("sc_resourcepump_addtask", function(len, ply)
		local self = net.ReadEntity()
		local resourceName = net.ReadString()
		local resourceAmount = net.ReadInt(32)
		local output = net.ReadBool()

		if IsValid(self) and IsValid(ply) then
			self:AddTask(resourceName, resourceAmount, output)
		end
	end)

	--Client removes task
	net.Receive("sc_resourcepump_removetask", function(len,ply)
		local self = net.ReadEntity()
		local taskNum = net.ReadInt(16)

		if IsValid(self) and IsValid(ply) then
			self:RemoveTask(taskNum)
			self:EmitSound(Sounds.TaskRemove, 350, 100)
		end
	end)

	--END OF NETWORKING--

    function ENT:SpawnPlug()
        self.Plug = ents.Create("sc_resource_pump_plug")
		self.Plug:SetPos(self:GetPos() + self.Plug:GetOffset())
        self.Plug:SetAngles(self:GetAngles())
        self.Plug:SetBase(self)
		self.Plug:Spawn()
        self.Plug:ConnectToSocket(self, true)
		constraint.Elastic(self, self.Plug, 0, 0, Vector(11.5, 0, 9), Vector(12, 0, 0), 150, 50, 5, "cable/cable", 2, true)

        --Ownership crap
	    if NADMOD or CPPI then
			self.Plug:CPPISetOwnerless(false)
		    self.Plug:CPPISetOwner(self:CPPIGetOwner())
	    else
		    self.Plug.Owner = self.Owner
	    end
    end

	function ENT:Initialize()
		base.Initialize(self)
		self:SetUseType(SIMPLE_USE)

		self.OutboundPump = nil
		self.InboundPump = nil
		self.Connected = false
		self.Queue = {}
		self.Task = nil
		self.LowResourceWarning = false
		self.InUseMenu = false
		self.UsingPly = nil
		self.TransferSpeed = GetConVarNumber("sc_resourcepump_speed")
		self.TransferSpeedMax = GetConVarNumber("sc_resourcepump_speed")
		self.TransferSpeedDesired = GetConVarNumber("sc_resourcepump_speed")
		self.TransferSpeedCustom = false
		self.LinkChange = false
		self.IsPump = true

        -- We need to disable some of the autoparenting functionality for these entities
        self.SCAutoParenter = {
            ["DisableConstraintRemoval"] = true,
            ["IgnoreSpecialConstraints"] = true,
            ["IgnoredConstraints"] = {"Elastic"}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
            ["DisableWeighting"] = false,
            ["DisableWelding"] = false,
            ["DisableParenting"] = false
        }

		--For runOnPumpTaskComplete()
		self.E2Hooks = {}

		timer.Simple(1, function()
			self:SpawnPlug()
		end)
	end

	function ENT:OnRemove()
		base.OnRemove( self )

		--Clean up connections
		self:Disconnect(true)

		--kill plug
		if IsValid(self.Plug) then self.Plug:Remove() end

		--Kill updates
		timer.Destroy("sc_resourcepump_statusupdate" .. self:EntIndex())
		timer.Destroy("sc_resourcepump_progressupdate" .. self:EntIndex())
	end

	--Send Status Screen Derma Updates
	function ENT:SendStatusUpdate(ply)
		if not IsValid(self) or not IsValid(ply) then return end

		net.Start("sc_resourcepump_updatestatus")
			net.WriteEntity(self)
			net.WriteBool(self:IsLinked())
			net.WriteEntity(self:GetOutboundPump())
		net.Send(ply)
	end

    function ENT:GetNetworkResources(connected)
        local Types = {}
		for storagetype, storage in pairs(self:GetNode():GetStorage()) do
			for k,v in pairs(storage:GetStored()) do
				Types[v:GetName()] = true
			end
		end

        return Types
    end

    function ENT:GetNetworkResourceAmount(name, connected)
        return self:GetNode():GetAmount(name)
    end

	--Send available resource table
	--[[Table structure is as follows:
		Table:
			Resources: --List of all resources between the two networks
				Resources[name]:
					Local = Amount Locally stored
					Remote = Amount on other network
	--]]
	function ENT:SendResourceTable(ply)
		if not IsValid(self) or not IsValid(ply) then return end

		local Output = {}
		if self.Connected then
            -- Get the types stored in our network
            local Types = self:GetNetworkResources()

            -- Get the types of the remote pump
            table.Merge(Types, self:GetOutboundPump():GetNetworkResources(self))

			--Check what we have in storages
			for name,_ in pairs(Types) do
				local OurAmount = self:GetNetworkResourceAmount(name)
				local TheirAmount = self:GetOutboundPump():GetNetworkResourceAmount(name, self)

				Output[name] = Output[name] or {}
				Output[name].Local = OurAmount or 0
				Output[name].Remote = TheirAmount or 0
			end
		end

		net.Start("sc_resourcepump_updateresources")
			net.WriteEntity(self)
			net.WriteTable(Output)
		net.Send(ply)
	end

	--Send Queue Table to Client
	function ENT:SendQueueTable(ply)
		if not IsValid(self) or not IsValid(ply) then return end

		local out = {}

		--Compile for delivery
		if self.Task then table.insert(out, self.Task) end
		for i = 1, #self.Queue do
			table.insert(out, self.Queue[i])
		end

		net.Start("sc_resourcepump_updatequeue")
			net.WriteEntity(self)
			net.WriteTable(out)
		net.Send(ply)
	end

	--Send Current Progress to Client
	function ENT:SendProgressUpdate(ply)
		if not IsValid(self) or not IsValid(ply) then return end

		if self.Task then
			net.Start("sc_resourcepump_updateprogress")
				net.WriteEntity(self)
				net.WriteFloat(self.Task["Progress"])
			net.Send(ply)
		end
	end

    function ENT:GetMaxRange()
        return GetConVarNumber("sc_resourcepump_maxrange")
    end

	--Open Menu
	function ENT:Use(ply)
        if not IsValid(self.Plug) then
            self:SpawnPlug()
            return
        elseif self.Plug:GetPos():Distance(self:GetPos()) > 1000 then
            self.Plug:Remove()
            self:SpawnPlug()
            return
        end

		if not self.InUseMenu then
			self.InUseMenu = true
			self.UsingPly = ply

			--Send to Client
			local data = {
				Pump = self,
				IsLinked = self:IsLinked()
			}
			net.Start("sc_resourcepump_openmenu")
				net.WriteTable(data)
			net.Send(ply)

			--Schedule Updates
			timer.Create("sc_resourcepump_statusupdate" .. self:EntIndex(), 1, 0, function() self:SendStatusUpdate(ply) end)
			timer.Create("sc_resourcepump_progressupdate" .. self:EntIndex(), 0.25, 0, function() self:SendProgressUpdate(ply) end)

			--Send Resources
			if self.Connected then
				self:SendResourceTable(ply)
			end

			--Send Current Queue
			if self.Task or #self.Queue > 0 then
				self:SendQueueTable(ply)
			end
		end
	end


	--=============--
	-- CONNECTIONS --
	--=============--

	function ENT:ConnectToPump(pump)
		if not IsValid(pump) then return false end
		if not pump:GetClass() == "sc_resource_pump_global" or not pump:GetClass() == "sc_resource_pump" then return false end
		if pump:GetNode() == self:GetNode() then return false end

		--Link checking.
		if not self:IsLinked() or not pump:IsLinked() then
			self:EmitSound(Sounds.Error, 350, 100)
			return false
		end

		--Check if busy
		if #self:GetQueue() > 0 then
			self:EmitSound(Sounds.Error, 350, 100)
			return false
		end

		self:EmitSound(Sounds.Connect, 350, 100)
		self.OutboundPump = pump
		pump.InboundPump = self
		self.Connected = true
		self:TransferSpeedCalc()

		self:OnOutboundConnect(pump)
		pump:OnInboundConnect(self)

		return true
	end

	--Note: "both" argument disconnects the other side as well
	function ENT:Disconnect(both)
		if not self.Connected then return end

		if both then
			self:GetOutboundPump():Disconnect(false)
		end

		self:OnOutboundDisconnect(self.OutboundPump)
		self.OutboundPump:OnInboundDisconnect(self)

		--Remove connection pointers
		self:GetOutboundPump().InboundPump = nil
        self.OutboundPump = nil
		self.Task = nil
		self.Queue = {}

		self:TransferSpeedCalc()

		self.Connected = false

		--Disconnect plug
		self.Plug:DisconnectFromSocket()
	end

	function ENT:GetOutboundPump()
		return self.OutboundPump
	end

	function ENT:GetInboundPump()
		return self.InboundPump
	end

	function ENT:GetPlug()
		return self.Plug
	end

	--For connection event overriding. TODO: Make these into hooks.
	function ENT:OnInboundConnect(pump) end
	function ENT:OnOutboundConnect(pump) end
	function ENT:OnInboundDisconnect(pump) end
	function ENT:OnOutboundDisconnect(pump) end

	--END CONNECTIONS--


	--================--
	-- TRANSFER QUEUE --
	--================--

	--EX: ("Oxygen", 900) pushes 900 oxygen to the other pump.
	--EX: ("Oxygen", -1) pushes oxygen to the other pump continuously until the task is cancelled.
	function ENT:AddTask(resourceName, amount, continuous)
		if not GAMEMODE:GetResourceTypes()[resourceName] then
			self:EmitSound(Sounds.Error, 350, 100)
			return false
		end

		--E2/Starfall pump functions were causing errors when attempting impossible transfers, sanity checks needed.
		local resCheck = self:GetStoredAndEmpty(resourceName)
		if resCheck == nil or resCheck == 0 then
			self:EmitSound(Sounds.Error, 350, 100)
			return false
		end

		--Set transfer speed multiplier
		if not self.Task then
			self:TransferSpeedCalc()
		end

		--Create Task
		local task = {}
		task["Resource"] = resourceName
		task["Amount"] = math.floor(amount)

		table.insert(self.Queue, task)
		self:EmitSound(Sounds.TaskAccept, 350, 100)

		--Send to watcher
		if self.InUseMenu then
			self:SendQueueTable(self.UsingPly)
		end

		return true
	end

	--Pass 0 to this to stop current task
	function ENT:RemoveTask(taskNum)
		if taskNum < 0 or taskNum > #self.Queue then return end

		if taskNum == 0 then
			self.Task = nil
		else
			table.remove(self.Queue, taskNum)
		end

		if self.InUseMenu then
			self:SendQueueTable(self.UsingPly)
		end
	end

	function ENT:PullNextTask()
		if #self.Queue < 1 then return false end

		self.Task = table.remove(self.Queue, 1)
		self.Task["Progress"] = 0

		--Send to watcher
		if self.InUseMenu then
			self:SendQueueTable(self.UsingPly)
		end

		return true
	end

	function ENT:GetCurrentTask()
		return self.Task
	end

	function ENT:GetQueue()
		return self.Queue
	end

	function ENT:GetQueueLength()
		local num = 0

		if self:GetCurrentTask() then num = 1 end
		num = num + #self:GetQueue()

		return num
	end

	--END TRANSFER QUEUE--


	--================--
	-- PUMP FUNCTIONS --
	--================--

	function ENT:TransferSpeedCalc()
        local Pump = self:GetOutboundPump()
		if IsValid(Pump) and Pump:GetClass() == "sc_resource_pump" then
			if IsValid(Pump:GetProtector()) and IsValid(self:GetProtector()) then
				local localcoremult = 1
				local remotecoremult = 1
				local transferspeedbase = GetConVarNumber("sc_resourcepump_speed")
				local transferincrement = GetConVarNumber("sc_resourcepump_transferincrement")
				local transfercurve = GetConVarNumber("sc_resourcepump_transfercurve")

				if self:GetProtector():GetIsStation() then
					localcoremult = 10
				end
				if Pump:GetProtector():GetIsStation() then
					remotecoremult = 10
				end

				local rawmult = math.min((self:GetProtector():GetNodeRadius() * 0.0254) * localcoremult, (Pump:GetProtector():GetNodeRadius() * 0.0254) * remotecoremult) / GetConVarNumber("sc_resourcepump_minshipsize")

				if rawmult > 1 then
					rawmult = ((rawmult - 1) / transfercurve) + 1
				else
					rawmult = 1
				end

				self.TransferSpeedMax = math.max(transferspeedbase, math.floor((transferspeedbase * rawmult) / transferincrement) * transferincrement)
			else
				self.TransferSpeedMax = GetConVarNumber("sc_resourcepump_speed")
			end
		else
			if IsValid(self:GetProtector()) then
				local localcoremult = 1
				local transferspeedbase = GetConVarNumber("sc_resourcepump_speed")
				local transferincrement = GetConVarNumber("sc_resourcepump_transferincrement")
				local transfercurve = GetConVarNumber("sc_resourcepump_transfercurve")

				if self:GetProtector():GetIsStation() then
					localcoremult = 10
				end

				local rawmult = ((self:GetProtector():GetNodeRadius() * 0.0254) * localcoremult) / GetConVarNumber("sc_resourcepump_minshipsize")

				if rawmult > 1 then
					rawmult = ((rawmult - 1) / transfercurve) + 1
				end

				self.TransferSpeedMax = math.max(transferspeedbase, math.floor((transferspeedbase * rawmult) / transferincrement) * transferincrement)
			else
				self.TransferSpeedMax = GetConVarNumber("sc_resourcepump_speed")
			end
		end
	end

    function ENT:GetStoredAndEmpty(res, inbound) --Inbound is unused here for now, but its used for getting the right player on gstorage pumps
        return self:GetAmount(res) or 0, (self:GetMaxAmount(res) - self:GetAmount(res)) or 0
    end

	function ENT:Pump()
		self.LowResourceWarning = false

		if not self.Task then return end

		--Get our storage and their storage
		local res = self.Task["Resource"]
        local pump = self:GetOutboundPump()
		local amt = 0

		--Set Amount
		if self.Task["Amount"] > -1 then
			amt = (math.min(self.Task["Amount"], self:GetTransferSpeed())) * 0.25 --TODO: Convar for pump interval?
		else
			amt = self:GetTransferSpeed() * 0.25
		end

		if self:ConsumeResource(res, amt) then
            if not pump:SupplyResource(res, amt) then
                self:SupplyResource(res, amt)
                self.Task["Progress"] = 1
            end
        else
            self.Task["Progress"] = 1
        end

		if self.Task["Amount"] > -1 then
			self.Task["Progress"] = math.min(self.Task["Progress"] + (amt / self.Task["Amount"]), 1)
		end
	end

	--For E2 and other hooking
	function ENT:OnPumpTaskComplete()

		--Run standard hook
		hook.Run("LSPumpTaskFinish", self)

		--Run all hooked E2s
		for v,_ in pairs(self.E2Hooks) do
			if IsValid(v) then
				v.RunByRDPump = 1
				v.CompletedPump = self

				v:Execute()

				v.CompletedPump = nil
				v.RunByRDPump = 0
			else
				--Remove invalid hook
				self.E2Hooks[v] = nil
			end
		end
	end

	function ENT:SetDesiredTransferSpeed(speed)
		if speed > -1 then
			self.TransferSpeedDesired = speed
			self.TransferSpeedCustom = true
		else
			self.TransferSpeedCustom = false
		end
	end

	function ENT:GetTransferSpeed()
		return self.TransferSpeed
	end

	--END PUMP FUNCTIONS--


	function ENT:Think()
        local Pump = self:GetOutboundPump()

		if self.TransferSpeedCustom then
			self.TransferSpeed = math.min(self.TransferSpeedMax,self.TransferSpeedDesired)
		else
			self.TransferSpeed = self.TransferSpeedMax
		end
		if IsValid(Pump) then
			--Check range to other pump
			local dist = self:GetPos():DistToSqr(Pump:GetPos())
			local maxRange = math.max(self:GetMaxRange(), Pump:GetMaxRange())

			maxRange = maxRange * maxRange

			if dist > maxRange then
				self:EmitSound(Sounds.Failure, 350, 100)
				self:Disconnect()
			end

			--Get next Task
			if not self.Task then
				if #self.Queue > 0 then
					self:PullNextTask()
				end

			--Process current Task
			else
				self:Pump()

				--Check if task is finished
				if self.Task["Progress"] >= 1 then

					--If in menu, update resources to player
					if self.InUseMenu then
						self:SendResourceTable(self.UsingPly)
                    end

                     --If continuous, re-add task to end
                    if self.Task["Amount"] < 0 then
                        self:AddTask(self.Task["Resource"], -1, true)
                    end

					self:EmitSound(Sounds.TaskDone, 350, 100)
					self:RemoveTask(0)

					self:OnPumpTaskComplete()
				end
			end
		elseif self.Connected then
			--Pump somehow went invalid without calling OnRemove() (....?!)
			self:EmitSound(Sounds.Failure, 350, 100)
			self:Disconnect()
		end

		--Update transfer speed on core linking/unlinking
		if IsValid(self:GetProtector()) and not self.LinkChange then
			self:TransferSpeedCalc()
			self.LinkChange = true
		elseif not IsValid(self:GetProtector()) and self.LinkChange then
			self:TransferSpeedCalc()
			self.LinkChange = false
		end

		--self:UpdateOutputs()
		self:UpdateOverlay()
		self:NextThink(CurTime() + 0.25)
		return true
	end


	--TODO: Move me to the client after the networking pass!
	function ENT:UpdateOverlay()
		--Linked?
		local text = "Linked: "
		if self:IsLinked() then
			text = text .. "Yes\n"
		else
			text = text .. "No\n"
		end

		--Connected?
        local Pump = self:GetOutboundPump()
		text = text .. "Connected: "
		if IsValid(Pump) then
			text = text .. "Pump: " .. Pump:GetPumpName("#"..Pump:EntIndex()) .. "\n"
		else
			text = text .. "No\n"
		end

		--Transfer speed
		text = text .. "Transfer Speed: " .. string.Comma(self:GetTransferSpeed()) .. "\n"

		--Queue
		text = text .. "\n==[ Queue ]==\n"
		text = text .. "Current Task:\n"
		if self.Task then
			if self.Task["Amount"] > -1 then
				--Task Description
				text = text .. "Sending " .. string.Comma(self.Task["Amount"]) .. " units of " .. self.Task["Resource"] .. "\n"

				--TODO: Progress Bar
				text = text .. string.Comma(math.floor(self.Task["Progress"] * 1000) / 10) .. "%\n"
			else
				--Task Description
				text = text .. "Continously sending " .. self.Task["Resource"] .. "\n"
			end
			--Other things in queue
			text = text .. "Tasks Remaining: " .. #self.Queue + 1 .. "\n" -- +1 to include current task
		else
			text = text .. "Nothing Queued\n"
		end

		--Task Info
		text = text .. "\n==[ Status ] ==\n"
		if not self.Task then
			text = text .. "Waiting for Task..."
		elseif self.LowResourceWarning then
			text = text .. "WARN: Not enough resources!"
		else
			text = text .. "Running..."
		end

		self:SetOverlayText(text)
	end


	duplicator.RegisterEntityClass("sc_resource_pump", GAMEMODE.MakeEnt, "Data")
end

--END SERVERSIDE--