--[[
	Asteroid Scanner for McBuild's Mining

	Author: F�hrball+Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_lsentity"

ENT.PrintName = "Resource Scanner"
ENT.Author = "Fuhrball + Steeveeo"

local function RegisterScanner()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Resource Scanner")
    Generator:SetClass("mining_scanner")
    Generator:SetDescription("Scans asteroids, crystals, and wrecks to find out what they have in them.")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/cerus/modbridge/misc/accessories/acc_radar1.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end
hook.Add("InitPostEntity", "mining_scanner_toolinit", RegisterScanner)
hook.Add("OnReloaded", "mining_scanner_reloaded", RegisterScanner)

function ENT:SetupGenerator()
end

if SERVER then
    --Defined Variables
    ENT.EnergyUsage = 50
    ENT.UpdateRate = 0.5
    ENT.Active = false
    ENT.FeedingInfo = false
	ENT.ScanProgress = 0
    ENT.Ent = nil
    ENT.ScanningEnt = nil
    ENT.EntScanned = nil

    function ENT:Initialize()
	    self.BaseClass.Initialize(self)
        self.storage = {}

	    --Model and Physics Setup
        self:SetName( "Resource Scanner" )
        self:SetOverlayText("Inactive")

	    --Wire IO
	    if WireLib ~= nil then
		    self.Inputs = WireLib.CreateSpecialInputs(self, { "Scan", "Target Override"}, { "NORMAL", "ENTITY" })
		    self.Outputs = WireLib.CreateSpecialOutputs(self, { "Status", "Warnings", "Scan Progress", "Ores" }, {"STRING", "STRING", "NORMAL", "TABLE"} )
	    end
    end

    function ENT:TriggerInput(iname, value)
	    if (iname == "Scan") then
		    if value ~= 0 and not self.Active then
			    --Check if we have enough to start scanning
			    if self:GetAmount("Energy") > self.EnergyUsage then
				    self.Active = true

				    --Start scan if we can
				    self.EntScanned = nil
				    if IsValid(self.Ent) then
					    self:ScanRoid()
				    end
			    end

		    elseif value == 0 and self.Active then
			    self.Active = false
			    self.EntScanned = nil
			    self:StopScan()
		    end
	    end
	    if (iname == "Target Override") then
		    self.Ent = value
	    end
    end

    function ENT:ScanRoid()
	    self.ScanningEnt = self.Ent
	    --Scan loop
	    timer.Create("Mining_Scanner_ScanUpdate"..self:EntIndex(),0.25,0,function()
            if not IsValid(self) then return end

		    --Check if the Ent is still valid
		    if not IsValid(self.ScanningEnt) then
			    self:StopScan()
		    end

		    --Check if the ent changed
		    if self.ScanningEnt ~= self.Ent then
			    self:StopScan()
			    self:StartScan()
		    end

		    --Out of resources?
		    if not self:ResourceCheck() then
			    self:StopScan()
		    end

		    --Change scan rate based on range
		    local scanInc = 10
		    local Dist = self:GetPos():Distance(self.ScanningEnt:GetPos())
		    if Dist > 2000 then
			    scanInc = scanInc - (10*(Dist/5000))
			    scanInc = math.floor(math.Clamp(scanInc, 1, 10))
		    end

		    --Increment Scan, report to wire
		    self.ScanProgress = self.ScanProgress + scanInc
            Wire_TriggerOutput(self, "Scan Progress", self.ScanProgress)

		    --Scan complete?
		    if self.ScanProgress >= 100 then
			    self.EntScanned = self.ScanningEnt
			    self.FeedingInfo = true
				self:StopScan()
		    end
	    end)
    end

    function ENT:StopScan()
	    timer.Destroy("Mining_Scanner_ScanUpdate"..self:EntIndex())
	    self.ScanningEnt = nil
	    self.ScanProgress = 0
	    Wire_TriggerOutput(self, "Scan Progress", self.ScanProgress)
    end

    function ENT:ResourceCheck()
	    --Enough energy to keep going?
	    if self:GetAmount("Energy") < self.EnergyUsage then
		    return false
	    else
		    --Drain the Necessary Energy
		    self:ConsumeResource("Energy", self.EnergyUsage)
		    return true
	    end
    end

    function ENT:OnRemove()
	    self:StopScan()
    end

    local WIRETABLE = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
    function ENT:Think()
	    --Is self thing on?
	    if self.Active then
		    --Check if we have enough energy
		    if not self:ResourceCheck() then
                self.Active = false
                self:SetOverlayText("Scan Failed - Insufficient Energy")
			    Wire_TriggerOutput(self, "Status", "Scan Failed - Insufficient Energy")
			    Wire_TriggerOutput(self, "Warnings", "Scanner needs at least " .. self.EnergyUsage .. " energy to continue operating.")
		    else
			    --We found something?
			    if IsValid(self.EntScanned) then
				    local Dis = self:GetPos():Distance(self.EntScanned:GetPos())
				    --Is it a roid?
                    if self.EntScanned.OnMined then
                        self:SetOverlayText("Scan Success - Feeding Object Information")
					    Wire_TriggerOutput(self, "Status", "Scan Success - Feeding Object Information")
                        local Storage = self.EntScanned.PrimaryStorage
					    local Ores = Storage:GetStored()

					    --Report to Wire
					    if Dis <= 4500 then
						    local Out = table.Copy(WIRETABLE)
						    for i,k in pairs(Ores) do
							    Out["s"][i] = Storage:GetAmount(i)
							    Out["stypes"][i] = "n"
						    end

						    Wire_TriggerOutput(self, "Ores", Out)
						    Wire_TriggerOutput(self, "Warnings", "Scan is 100% Accurate")
					    elseif Dis <= 12000 then
						    local Out = table.Copy(WIRETABLE)
						    local DropChance = 5
						    for i,k in pairs(Ores) do
							    if math.random(1, 100) > DropChance then
								    Out["s"][i] = Storage:GetAmount(i) * math.random(0.8, 1.8)
								    Out["stypes"][i] = "n"
							    end
						    end

						    Wire_TriggerOutput(self, "Ores", Out)
						    Wire_TriggerOutput(self, "Warnings", "Interference Detected - Scan Not Accurate!")
					    else
                            Wire_TriggerOutput(self, "Ores", table.Copy(WIRETABLE))
                            self:SetOverlayText("Scan Failed - Object Lost")
						    Wire_TriggerOutput(self, "Status", "Scan Failed - Object Lost")
						    Wire_TriggerOutput(self, "Warnings", "Object Exceeded Maximum Detection Range")

							self.EntScanned = nil
							self.FeedingInfo = false
					    end
                    else
                        self:SetOverlayText("Scan Failed - Invalid Object")
					    Wire_TriggerOutput(self, "Status", "Scan Failed - Invalid Object")
				    end
                elseif self.ScanProgress > 0 then
                    self:SetOverlayText(Format("Scan Active - Acquiring Lock on Object: %s%%", self.ScanProgress))
				    Wire_TriggerOutput(self, "Status", "Scan Active - Acquiring Lock on Object")
				    Wire_TriggerOutput(self, "Warnings", "Standby...")
			    elseif self.FeedingInfo then
					Wire_TriggerOutput(self, "Ores", table.Copy(WIRETABLE))
					Wire_TriggerOutput(self, "Status", "Scan Failed - Object Lost")
                    Wire_TriggerOutput(self, "Warnings", "Object Integrity Compromised")
                    self:SetOverlayText("Scan Failed - Object Lost")

					self.EntScanned = nil
					self.FeedingInfo = false
				end
		    end
	    else
		    Wire_TriggerOutput(self, "Status", "Scan Idle - Targeter Inactive")
            Wire_TriggerOutput(self, "Warnings", "Scanner Inactive")
            self:SetOverlayText("Inactive")
		    Wire_TriggerOutput(self, "Scan Progress", self.ScanProgress)
            Wire_TriggerOutput(self, "Ores", table.Copy(WIRETABLE))

			self.FeedingInfo = false
			self.ScanProgress = 0
	    end
	    self:NextThink(CurTime() + self.UpdateRate)
	    return true
    end

	duplicator.RegisterEntityClass("mining_scanner", GAMEMODE.MakeEnt, "Data")
end