﻿AddCSLuaFile()

DEFINE_BASECLASS("base_gmodentity")
ENT.Type				= "anim"
ENT.PrintName			= "SC2 Entity"
ENT.Author				= "Lt.Brandon"
ENT.Category			= "Space Combat 2"
ENT.Contact				= "diaspora-communtiy.com"
ENT.Purpose				= "Rendering your ham"

ENT.Spawnable			= false
ENT.AdminSpawnable		= false
ENT.RenderGroup			= RENDERGROUP_BOTH

local DefaultFitting = {CPU = 0, PG = 0, Slot = "None", Status = "Offline", CanRun = false, Classes = {}}
function ENT:SetFitting(value, dontsync)
	if CLIENT then
		self.SC_Fitting = value or table.Copy(DefaultFitting)
		return
	end

	local accessor = self.SC.NWAccessors[SC.NWAccessors.GetNWAccessorID(self, "Fitting")]
	local same, new, removed = SC.NWAccessors.CompareTables(self.SC_Fitting or table.Copy(DefaultFitting), value)

	if same then return end

	self.SC_Fitting = value
	accessor.partial = {new = new, removed = removed}

	if not dontsync then
		SC.NWAccessors.SyncValue(self, accessor.id)
	end
end

function ENT:GetFitting()
	if self.SC_Fitting ~= nil then
		return table.Copy(self.SC_Fitting)
	end

	return table.Copy(DefaultFitting)
end

function ENT:SetProtector(value, dontsync)
	if CLIENT then
        self.SC_ProtectorEntity = value
        return
    end

	local accessor = self.SC.NWAccessors[SC.NWAccessors.GetNWAccessorID(self, "Protector")]
	if accessor and not dontsync then
		SC.NWAccessors.SyncValue(self, accessor.id)
	end
end

function ENT:GetProtector()
    return self.SC_ProtectorEntity
end

function ENT:SharedInit()
	SC.NWAccessors.CreateSpecialNWAccessor(self, "Protector", "entity", self["GetProtector"], self["SetProtector"], 1)
	SC.NWAccessors.CreateSpecialNWAccessor(self, "Fitting", "table", self["GetFitting"], self["SetFitting"], 1)
	self.SC_Fitting = table.Copy(DefaultFitting)
end

if CLIENT then
	function ENT:Draw(Flags)
	    self:DoNormalDraw(Flags)
	end

	function ENT:DoNormalDraw(Flags)
		self:DrawModel(Flags)

		if self:BeingLookedAtByLocalPlayer() and self:GetOverlayText() ~= "" then
			AddWorldTip( self, self:GetOverlayText() )
		end
	end

	function ENT:Initialize()
		self:SharedInit()
	end
else
	function ENT:Initialize()
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetUseType(SIMPLE_USE)

		local phys = self:GetPhysicsObject()
		if IsValid(phys) then
			phys:Wake()
			phys:EnableGravity(true)
			phys:EnableDrag(true)
			phys:EnableCollisions(true)
			phys:EnableMotion(false)
		end

		self:SharedInit()
	end

    function ENT:GetWirePorts()
    end

    function ENT:SetupWirePorts()
        if WireLib then
            local inputs, outputs = self:GetWirePorts()
            if inputs and #inputs > 0 then
                WireLib.CreateInputs( self, inputs )
            end

            if outputs and #outputs > 0 then
                WireLib.CreateOutputs( self, outputs )
            end
        end
    end

    function ENT:UpdateOutputs()
    end

	function ENT:OnRestore()
	    if WireLib and WireLib.Restored then WireLib.Restored( self ) end
	end

	function ENT:SaveSCInfo()
		return self.SCDupeInfo or {}
	end

    -- Called as soon as the entity is created
	function ENT:LoadSCInfo(Info)
		self.SCDupeInfo = Info or {}
    end

    -- Called when the entity is finished pasting
    function ENT:ApplySCDupeInfo(Info, GetEntByID)

	end

	function ENT:BuildDupeInfo()
		local Info = {}

		if WireLib then
			Info.Wire = WireLib.BuildDupeInfo( self )
		end

		Info.SC2 = self:SaveSCInfo()

		return Info
	end

	function ENT:ApplyDupeInfo( Player, Entity, Info, GetEntByID )
        self.Owner = Player

        local SC2DupeInfo
        if not Info.SC2DupeInfo then
            if Info.WireDupeInfo then
                SC2DupeInfo = Info.WireDupeInfo.sc_data or {}
            else
                SC2DupeInfo = {}
            end
        else
            SC2DupeInfo = Info.SC2DupeInfo
        end

        self:ApplySCDupeInfo(SC2DupeInfo, GetEntByID)

		if WireLib then WireLib.ApplyDupeInfo( Player, Entity, Info.WireDupeInfo, GetEntByID ) end
	end

	function ENT:PreEntityCopy()
        local DupeInfo = self:BuildDupeInfo()

        duplicator.ClearEntityModifier(self, "WireDupeInfo")
        if DupeInfo and DupeInfo.Wire then
			duplicator.StoreEntityModifier(self, "WireDupeInfo", DupeInfo.Wire)
		end

        duplicator.ClearEntityModifier(self, "SC2DupeInfo")
        if DupeInfo and DupeInfo.SC2 then
			duplicator.StoreEntityModifier(self, "SC2DupeInfo", DupeInfo.SC2)
		end
	end

	function ENT:PostEntityPaste( Player, Entity, CreatedEntities )
		if Entity.EntityMods then
			Entity:ApplyDupeInfo( Player, Entity, Entity.EntityMods, function(id) return CreatedEntities[id] end )
		end
	end
end