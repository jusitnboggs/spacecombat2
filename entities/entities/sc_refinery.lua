AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_lsentity"
ENT.PrintName = "Refinery"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "Melts down your soul and refines it into diamonds."
ENT.Instructions = "Throw rocks at door and have a rave."
ENT.Category = "Space Combat 2"
ENT.Spawnable = false
ENT.AdminSpawnable = false

hook.Add("InitPostEntity", "sc_refinery_toolinit", function()
	do
		local Generator = GAMEMODE:NewGeneratorInfo()
		Generator:SetName("Refinery")
		Generator:SetClass("sc_refinery")
		Generator:SetDescription("A giant, noisy rock tumbler that refines resources into more useful versions of what you already have.")
		Generator:SetCategory("Manufacturing")
		Generator:SetDefaultModel("models/slyfo/refinery_small.mdl")

		GAMEMODE:RegisterGenerator(Generator)
	end

	do
		local Generator = GAMEMODE:NewGeneratorInfo()
		Generator:SetName("Factory")
		Generator:SetClass("sc_refinery")
		Generator:SetDescription("A station sized industrial laser cutter complete with robot arms and tools for building everything you could ever want.")
		Generator:SetCategory("Manufacturing")
		Generator:SetDefaultModel("models/slyfo/refinery_large.mdl")

		GAMEMODE:RegisterGenerator(Generator)
	end

	do
		local Generator = GAMEMODE:NewGeneratorInfo()
		Generator:SetName("Reprocessor")
		Generator:SetClass("sc_refinery")
		Generator:SetDescription("The biggest car crusher you've ever seen, it even has a furnace to melt down parts into raw resources!")
		Generator:SetCategory("Manufacturing")
		Generator:SetDefaultModel("models/slyfo/crate_resource_large.mdl")

		GAMEMODE:RegisterGenerator(Generator)
	end
end)

function ENT:SetupGenerator(Generator)
    self:SetupType(Generator:GetName())
end

ENT.Types = {
			["Refinery"] = {
				PG = 16000,
				CPU = 10000,
				Slot = "Industrial",
                Status = "Offline", CanRun = false, Classes = {}
			},

			["Reprocessor"] = {
				PG = 26000,
				CPU = 15000,
				Slot = "Industrial",
                Status = "Offline", CanRun = false, Classes = {}
			},

			["Factory"] = {
				PG = 60000,
				CPU = 30000,
				Slot = "Industrial",
                Status = "Offline", CanRun = false, Classes = {}
			}
}

function ENT:SetupDataTables()
    SC.NWAccessors.CreateNWAccessor(self, "TimeRemaining", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "Active", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Producing", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Type", "cachedstring", "")
    SC.NWAccessors.CreateNWAccessor(self, "Recipe", "cachedstring", "")
end

if CLIENT then
    function ENT:GetOverlayText()
	    local type = self:GetType()
	    local recipe = self:GetRecipe()
	    local tbl = SC.Manufacturing.GetRecipe(recipe, type)
	    local fitting = self.Types[type]
        if not type or not recipe or not fitting then return "BROKEN SEND HELP" end
	    local hr = "--------------------"
	    local text = "SC2 "..self:GetType().."\n\nFitting\n"..hr.."\nCPU: "..sc_ds(fitting.CPU).."\nPG: "..sc_ds(fitting.PG)
	    .."\nSlot: "..fitting.Slot.."\nClasses:"

	    for i, k in pairs(fitting.Classes) do
		    text = text.." "..k
	    end

	    text = text.."\n\nProduction\n"..hr.."\nRecipe: "..recipe.."\nCapacitor: "..tbl.capacitor.."\nTime Per Item: "..tbl.time
	    .."\n\nStatus\n"..hr.."\nProduction: "

	    if self:GetProducing() then
		    text = text.."Online\nTime Remaining: "..self:GetTimeRemaining()
	    else
		    text = text.."Offline"
	    end

	    return text
    end

    function ENT:SendUpdate(active, recipe)
	    if not IsValid(self) or (active == nil) or (recipe == nil) then return end

	    net.Start("sc_refinery_ui")
	    net.WriteEntity(self)
	    net.WriteBit(active)
	    net.WriteString(recipe)
	    net.SendToServer()
    end

    local function UpdateText(panel, name, recipe)
	    if not IsValid(panel) or (recipe == nil) or (name == nil) then return end
	    local text = "Recipe: "..name.."\nCapacitor Required: "..sc_ds(recipe.capacitor)..
	    "\nTime Required: "..sc_ds(recipe.time).."\n\nItems Needed: "

	    for i,k in pairs(recipe.requires) do
		    text = text.."\n    "..sc_ds(k).." units of "..i
	    end

	    text = text.."\n\nItems Produced:"

	    for i,k in pairs(recipe.produces) do
		    text = text.."\n    "..sc_ds(k).." units of "..i
	    end

	    panel:SetText(text)
	    panel:SizeToContents()
	    panel:SetPos(30, 30)
    end

    net.Receive("sc_refinery_ui", function()
	    local self = net.ReadEntity()

	    if not IsValid(self) then return end

	    local recipes = SC.Manufacturing.GetRecipes(self:GetType())
	    local current = SC.Manufacturing.GetRecipe(self:GetRecipe(), self:GetType())
	    local currentname = self:GetRecipe()

	    local InfoPanel = vgui.Create("DFrame")
	    InfoPanel:SetPos(100,100)
	    InfoPanel:SetSize(680,420)
	    InfoPanel:SetTitle("Manufacturing Control Panel")
	    InfoPanel:MakePopup()

	    local StatPan = vgui.Create("DPanel", InfoPanel)
	    StatPan:SetSize(300, 0)
	    StatPan:Dock(RIGHT)

	    local RecipeInfoLabel = vgui.Create("DLabel", StatPan)
	    RecipeInfoLabel:SetPos(5,5)
	    RecipeInfoLabel:SetText("Recipe Information")
	    RecipeInfoLabel:SizeToContents()

	    local RecipeInfoLabel2 = vgui.Create("DLabel", StatPan)
	    RecipeInfoLabel2:SetPos(30, 30)
	    UpdateText(RecipeInfoLabel2, currentname, current)

	    local RecipeList = vgui.Create("DListView", InfoPanel)
	    RecipeList:Clear()
	    RecipeList:SetMultiSelect(false)
	    RecipeList:AddColumn("Recipe Name")
	    RecipeList:AddColumn("Time Required")
	    RecipeList:AddColumn("Capacitor Required")

	    local listLines = {}
	    for i,k in pairs(recipes) do
		    listLines[i] = RecipeList:AddLine(i, sc_ds(k.time), sc_ds(k.capacitor))
	    end
	    RecipeList:SelectItem(listLines[self:GetRecipe()])
	    RecipeList.OnRowSelected = function(panel, line)
		    local row = panel:GetLine(line)
		    local recipe = string.Trim(row:GetValue(1))

		    if not IsValid(self) or not SC.Manufacturing.IsValidRecipe(recipe, self:GetType()) then return end

		    current = SC.Manufacturing.GetRecipe(recipe, self:GetType())
		    currentname = recipe

		    UpdateText(RecipeInfoLabel2, currentname, current)
	    end

	    RecipeList:Dock(FILL)

	    local StartProd = vgui.Create("DButton", InfoPanel)
	    StartProd:SetText("Start Production")
	    StartProd.DoClick = function()
		    self:SendUpdate(true, currentname)
	    end

	    StartProd:Dock(BOTTOM)

	    local StopProd = vgui.Create("DButton", InfoPanel)
	    StopProd:SetText("Stop Production")
	    StopProd.DoClick = function()
		    self:SendUpdate(false, "")
	    end

	    StopProd:Dock(BOTTOM)
    end)
elseif SERVER then
    util.PrecacheSound( "k_lab.ambient_powergenerators" )
    util.PrecacheSound( "ambient/machines/thumper_startup1.wav" )

    function ENT:Initialize()
	    self.lastused = 0

	    self.BaseClass.Initialize(self)
    end

    function ENT:CanEnable()
        return IsValid(self:GetProtector()) and self:GetProtector():CheckClass(self:GetFitting()) and self:GetProtector():CheckFitting(self:GetFitting())
    end

    function ENT:EnableModule()
		if self:CanEnable() then
            local Fitting = self:GetFitting()
            self:GetProtector():ConsumeFitting(Fitting)
            self:SetFitting(Fitting)

            self:EmitSound("buttons/button16.wav")

            return true
		else
            self:EmitSound("buttons/button17.wav")
            self:DisableModule(true)
            return false
		end
    end

    function ENT:DisableModule(DisableModifierUpdate)
        local Core = self:GetProtector()
        local Fitting = self:GetFitting()
        if IsValid(Core) then
            local HasFitting, HasSlot, HasCPU, HasPG = Core:CheckFitting(Fitting)

            if not Core:CheckClass(Fitting) then
                Fitting.Status = "Offline - Wrong Class"
            elseif not HasFitting then
                local first = true
                Fitting.Status = "Offline - Not enough "

                if not HasSlot then
                    Fitting.Status = Fitting.Status.."Slots"
                    first = false
                end

                if not HasCPU then
                    if first then
                        Fitting.Status = Fitting.Status.."CPU"
                        first = false
                    else
                        Fitting.Status = Fitting.Status.."/CPU"
                    end
                end

                if not HasPG then
                    if first then
                        Fitting.Status = Fitting.Status.."PG"
                        first = false
                    else
                        Fitting.Status = Fitting.Status.."/PG"
                    end
                end
            else
                Fitting.Status = "Offline"
            end

            if not DisableModifierUpdate then
                Core:UpdateModifiers()
            end
        else
            Fitting.Status = "Offline"
        end

        self:SetFitting(Fitting)
    end

    function ENT:SetupType(type)
	    if (not SC.Manufacturing.IsValidType(type)) or (self.Types[type] == nil) then return end

	    local name, tbl = SC.Manufacturing.GetDefaultRecipe(type)

	    self:SetType(type)
	    self:SetRecipe(name)
	    self.Recipe = tbl
	    self:SetFitting(table.Copy(self.Types[type]))

	    if IsValid(self:GetNode()) then
		    self:GetNode():UpdateModifiers()
	    end

	    self:SetName(type)

        timer.Simple(0.1, function()
            if IsValid(self) then
                SC.NWAccessors.SyncAccessors(self)
            end
        end)
    end

    function ENT:SaveSCInfo()
	    return {type=self:GetType(), recipe=self:GetRecipe()}
    end

    function ENT:LoadSCInfo(Info)
	    if not Info.type then self:SetupType("Refinery") return end

	    self:SetupType(Info.type)

	    if not Info.recipe then return end

	    self:SetRecipe(Info.recipe)
	    self.Recipe = SC.Manufacturing.GetRecipe(Info.recipe, Info.type)
    end

    function ENT:TurnOn()
	    if not self:GetActive() then
		    self:EmitSound("k_lab.ambient_powergenerators")
		    self:EmitSound("ambient/machines/thumper_startup1.wav" )
		    self:SetActive(true)
	    end
    end

    function ENT:TurnOff()
	    if self:GetActive() then
		    self:StopSound("k_lab.ambient_powergenerators")
		    self:SetActive(false)
	    end
    end

    function ENT:SetOn(value)
	    if value then
		    if (CurTime() - self.lastused) > 2 then
			    self.lastused = CurTime()
			    self:TurnOn()
		    end
	    else
		    self:TurnOff()
	    end
    end

    function ENT:OnRemove()
	    self:TurnOff()
    end

    function ENT:Use(ply)
	    net.Start("sc_refinery_ui")
	    net.WriteEntity(self)
	    net.Send(ply)
    end

    net.Receive("sc_refinery_ui", function()
	    local self = net.ReadEntity()

	    if not IsValid(self) then return end

	    self:SetOn(net.ReadBit() == 1)

	    local recipe = net.ReadString()
	    if not SC.Manufacturing.IsValidRecipe(recipe, self:GetType()) then return end
	    self:SetRecipe(recipe)
	    self.Recipe = SC.Manufacturing.GetRecipe(recipe, self:GetType())
    end)

    function ENT:Process()
	    local recipe = self:GetRecipe()

	    if not SC.Manufacturing.IsValidRecipe(recipe, self:GetType()) then self:SetOn(false) return end

	    local tbl = SC.Manufacturing.GetRecipe(recipe, self:GetType())
	    if self:GetAmount("Energy") >= tbl.capacitor and self:HasResources(tbl.requires) then
		    self:ConsumeResources(tbl.requires)
            self:ConsumeResource("Energy", tbl.capacitor)
		    self:SetProducing(true)

		    timer.Create(self:EntIndex().."_process", tbl.time, 1, function()
			    if not IsValid(self) then return end
			    self:SupplyResources(tbl.produces)
			    self:SetProducing(false)
		    end)
	    end
    end

    function ENT:Think()
	    if self:GetActive() and not self:GetProducing() then
		    if IsValid(self:GetProtector()) and self:GetFitting().CanRun then
			    self:Process()
		    else
			    self:SetOn(false)
		    end
	    end

	    if self:GetProducing() then
		    self:SetTimeRemaining(timer.TimeLeft(self:EntIndex().."_process"))
	    end

	    self:NextThink( CurTime() + 1 )
	    return true
    end

    duplicator.RegisterEntityClass("sc_refinery", GAMEMODE.MakeEnt, "Data")
end