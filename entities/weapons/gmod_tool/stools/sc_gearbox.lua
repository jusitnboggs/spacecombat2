TOOL.Category		= "Mechanical Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Gearbox Spawner"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}

TOOL.ClientConVar["type"] = "Default"
TOOL.ClientConVar["type"] = "Default"

if ( CLIENT ) then
    language.Add( "Tool.sc_gearbox.name", "Space Combat 2 Gearbox Creation Tool" )
    language.Add( "Tool.sc_gearbox.desc", "Creates an Gearbox." )
    language.Add( "Tool.sc_gearbox.left", "Create Gearbox" )
    language.Add( "undone_sc_gearbox", "Undone Gearbox" )
end

cleanup.Register( "sc_gearbox" )

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

    local ply = self:GetOwner()
    local Gearbox = ents.Create("sc_gearbox")
    local Ang = ply:EyeAngles()
    Ang.yaw = Ang.yaw + 180 -- Rotate it 180 degrees in my favour
    Ang.roll = 0
    Ang.pitch = 0

    Gearbox:SetPos(trace.HitPos)

    -- Once the model has been set use it's OBB to move the turret out of the ground
    timer.Simple(0, function()
        if IsValid(Gearbox) then
            Gearbox:SetPos(trace.HitPos - trace.HitNormal * Gearbox:OBBMins().z)
        end
    end)

    Gearbox:SetAngles(Ang)
    Gearbox:Spawn()
    Gearbox:Activate()
    Gearbox:SetupGearbox(ply:GetInfo("sc_gearbox_type"))
    Gearbox.Owner = ply

    local PhysObj = Gearbox:GetPhysicsObject()
    if not IsValid(PhysObj) then
        ErrorNoHalt("[SC2] - ERROR: Gearbox spawned without a valid physics object! What?!\n")
        return
    end
    PhysObj:EnableMotion(false)

    undo.Create("sc_gearbox")
        undo.AddEntity( Gearbox )
        undo.SetPlayer( ply )
    undo.Finish()

    ply:AddCleanup( "sc_gearbox", Gearbox )

    return true
end

function TOOL.BuildCPanel(Panel)
	Panel:ClearControls()
	Panel:SetName("Space Combat 2 - Gearboxes")
	Panel:Help("Select an Gearbox Type")

    local Gearboxes = GAMEMODE.Mechanical.Gearboxes
	local SelectedGearbox = LocalPlayer():GetInfo("sc_gearbox_type")
	if not table.HasValue(Gearboxes, SelectedGearbox) then
		SelectedGearbox = next(Gearboxes)
		RunConsoleCommand("sc_gearbox_type", SelectedGearbox)
	end

	local GearboxTypeBox = vgui.Create("DCategoryTree", Panel)
	GearboxTypeBox:SetHeight(700)
    GearboxTypeBox:PopulateFromTableByValue(Gearboxes, "Category", "Name", "Hidden")
    Panel:AddPanel(GearboxTypeBox)

	GearboxTypeBox.OnNodeSelected = function(Parent, Node)
		RunConsoleCommand("sc_gearbox_type", Node.Data)
    end
end
