TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Repair Modules"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}
TOOL.ClientConVar["Type"] = "Booster"
TOOL.ClientConVar["Repair"] = "Booster"
TOOL.ClientConVar["Size"] = "Tiny"

if ( CLIENT ) then
    language.Add( "Tool.sc_repairs.name", "Repair Module Creation Tool" )
    language.Add( "Tool.sc_repairs.desc", "Spawns A Repair Module." )
    language.Add( "Tool.sc_repairs.left", "Create/Update Repair Module." )
	language.Add( "sboxlimit_sc_repair", "You've hit the Repair Module limit!" )
	language.Add( "undone_sc_repair", "Undone Repair Module" )
end

cleanup.Register( "sc_module_booster" )

function TOOL:LeftClick(trace)
    local type	= self:GetClientInfo("Type")
    local size = self:GetClientInfo("Size")
    local rep = self:GetClientInfo("Repair")

	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	if not IsValid(trace.Entity) then return false end

	local ply = self:GetOwner()

	if string.find(trace.Entity:GetClass(), "sc_module_booster") and trace.Entity.Owner == ply then
		local ent = trace.Entity
		ent:LoadSCInfo({type=type,size=size,rep=rep})
		return true --Stop it from going on to stuff below and making new ent
	end

	local Core = ents.Create("sc_module_booster")
    Core:Spawn()
    Core:Activate()
    local Ang = trace.HitNormal:Angle() + Angle(90,0,0)
	local Pos = trace.HitPos + trace.HitNormal * math.abs(Core:OBBMins().Z)
    Core:SetPos(Pos)
    Core:SetAngles(Ang)
    Core:LoadSCInfo({type=type,size=size,rep=rep})
    Core.Owner = ply

	local phys = Core:GetPhysicsObject()
	if (phys:IsValid() and trace.Entity:IsValid() ) then
		local weld = constraint.Weld(Core, trace.Entity, 0, trace.PhysicsBone, 0)
		local nocollide = constraint.NoCollide(Core, trace.Entity, 0, trace.PhysicsBone)
		phys:EnableMotion(false)
	end

	undo.Create("sc_module_booster")
		undo.AddEntity( Core )
		undo.SetPlayer( ply )
	undo.Finish()

	ply:AddCleanup( "sc_module_booster", Core )

	return true

end

local Data = {}
Data["Armor"] = {"Repair", "Nanites"}
Data["Shield"] = {"Booster", "Infuser"}
Data["Hull"] = {"Repair", "Nanites"}
Data["Capacitor"] = {"Booster"}

local Sizes = { "Tiny",
                "Small",
                "Medium",
                "Large",
                "X-Large" }

function TOOL.BuildCPanel(CPanel)
	CPanel:AddControl("Header", { Text = "#Tool.sc_repairs.name", Description = "#Tool.sc_repairs.desc" })

	CPanel.ClassBox = vgui.Create("DListView", CPanel)
	CPanel.ClassBox:SetHeight(100)
	CPanel.ClassBox:AddColumn("Repair Class")
	CPanel:AddItem(CPanel.ClassBox)

	for k, v in pairs( Data ) do
		CPanel.ClassBox:AddLine(k)
	end

	if #CPanel.ClassBox:GetSelected() < 1 then CPanel.ClassBox:SelectFirstItem() end

	local SelectedClass = "Armor"
	local SelectedType = "Repair"
	CPanel.ClassBox.OnClickLine = function(parent, line, isselected)
		RunConsoleCommand("sc_repairs_repair", line:GetValue(1))
		SelectedClass = line:GetValue(1)

		parent:ClearSelection()
		parent:SelectItem(line)

		if CPanel.TypeBox ~= nil then
			CPanel.TypeBox:Clear()

			for k, v in pairs( Data[SelectedClass] ) do
				CPanel.TypeBox:AddLine(v)
			end

			CPanel.TypeBox:SelectFirstItem()
			RunConsoleCommand("sc_repairs_type", CPanel.TypeBox:GetLine(1):GetValue(1))
			SelectedType = CPanel.TypeBox:GetLine(1):GetValue(1)
		end
	end

	CPanel.TypeBox = vgui.Create("DListView", CPanel)
	CPanel.TypeBox:SetHeight(100)
	CPanel.TypeBox:AddColumn("Repair Type")
	CPanel:AddItem(CPanel.TypeBox)

	for k, v in pairs( Data[SelectedClass] ) do
		CPanel.TypeBox:AddLine(v)
	end

	if #CPanel.TypeBox:GetSelected() < 1 then CPanel.TypeBox:SelectFirstItem() end

	CPanel.TypeBox.OnClickLine = function(parent, line, isselected)
		SelectedType = line:GetValue(1)
		RunConsoleCommand("sc_repairs_type", line:GetValue(1))

		parent:ClearSelection()
		parent:SelectItem(line)
	end

    local RealOptions = {}
	for k, v in pairs(Sizes) do
		RealOptions[v] = {sc_repairs_size = v}
	end

	local RepairList = CPanel:AddControl( "ListBox", { Label = "Repair Size", Height = "150", Options = RealOptions} )
end

