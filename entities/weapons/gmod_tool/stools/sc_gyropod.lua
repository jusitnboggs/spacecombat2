TOOL.Category = "Ship Tools"
TOOL.Name = "#Gyropod Spawner"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.Tab = "Space Combat 2"

TOOL.ClientConVar["model"] = "models/Spacebuild/Nova/drone2.mdl"
TOOL.ClientConVar["autoparent"] = 1
TOOL.ClientConVar["rolllock"] = 1
TOOL.ClientConVar["usekeysforturning"] = 0
TOOL.ClientConVar["deadzone"] = 5
TOOL.ClientConVar["turnmult_p"] = 1
TOOL.ClientConVar["turnmult_y"] = 1
TOOL.ClientConVar["turnmult_r"] = 1
TOOL.ClientConVar["accmult_x"] = 1
TOOL.ClientConVar["accmult_y"] = 0.5
TOOL.ClientConVar["accmult_z"] = 0.25
TOOL.ClientConVar["maxthrottleangle"] = 45
TOOL.ClientConVar["maxspeed"] = -1
TOOL.ClientConVar["maxturnspeed"] = -1

if CLIENT then
    language.Add("tool.sc_gyropod.name", "Space Combat 2 Gyropod")
	language.Add("Tool.sc_gyropod.desc", "Customize controls & sensitivity with inputs. READ THE INSTRUCTIONS!")
	language.Add("Tool.sc_gyropod.0", "Left-Click to spawn.  Right click the Gyro-Pod to start linking a vehicle.")
	language.Add("Tool.sc_gyropod.1", "Now Right click the vehicle that will control the Gyro-Pod via mouselook.  Reload to cancel.")
end

if SERVER then
	CreateConVar('sbox_maxsc_gyropod', 20)
end
cleanup.Register( "sc_gyropod" )

--Link an entity to the gyropod
function TOOL:RightClick(trace)
    if CLIENT then return end

	if (self:GetStage() == 0) and trace.Entity:GetClass() == "sc_gyropod" then
		self.Gyro = trace.Entity
		self:SetStage(1)
		return true
	elseif self:GetStage() == 1 and trace.Entity.GetPassenger then
		local owner = self:GetOwner()
		if self.Gyro:LinkPod(trace.Entity) then
			owner:PrintMessage(HUD_PRINTTALK,"Vehicle Linked to Gyro-Pod!")
		else
			owner:PrintMessage(HUD_PRINTTALK,"Link failed!")
		end
		self:SetStage(0)
		self.Gyro = nil
		return true
	else
		self:GetOwner():PrintMessage(HUD_PRINTTALK,"Left Click to Spawn.  To Link, Right Click the Gyro-Pod First, THEN the Vehicle!")
		return false
	end
end

local function ApplySettings(self, Gyropod)
    -- Apply Settings
    Gyropod.Deadzone = self:GetClientNumber("deadzone", 5)
    Gyropod.ShouldAutoParent = self:GetClientNumber("autoparent", 1) == 1
    Gyropod.UseKeysForTurning = self:GetClientNumber("usekeysforturning", 0) == 1
    Gyropod.RollLock = self:GetClientNumber("rolllock", 1) == 1
    Gyropod.AccelerationMult = Vector(math.Clamp(self:GetClientNumber("accmult_x", 1), 0, 1), math.Clamp(self:GetClientNumber("accmult_y", 1), 0, 1), math.Clamp(self:GetClientNumber("accmult_z", 1), 0, 1))
    Gyropod.TurnMult = Vector(math.Clamp(self:GetClientNumber("turnmult_p", 1), 0, 1), math.Clamp(self:GetClientNumber("turnmult_y", 1), 0, 1), math.Clamp(self:GetClientNumber("turnmult_r", 1), 0, 1))
    Gyropod.MaxThrottleAngle = self:GetClientNumber("maxthrottleangle", 45)
    Gyropod.UserMaxSpeed = self:GetClientNumber("maxspeed", -1)
    Gyropod.UserMaxTurnSpeed = self:GetClientNumber("maxturnspeed", -1)
end

--Spawn the gyropod
function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
    if CLIENT then return true end

	local ply = self:GetOwner()
    if IsValid(trace.Entity) and trace.Entity:GetClass() == "sc_gyropod" and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(ply)) then
        ApplySettings(self, trace.Entity)
		return true
    end

    if not self:GetSWEP():CheckLimit("sc_gyropod") then return false end

	local Ang = trace.HitNormal:Angle()
	Ang.pitch = Ang.pitch + 90
	local Model = self:GetClientInfo("model")
    local Gyropod = ents.Create("sc_gyropod")
    Gyropod:SetAngles(Ang)
    Gyropod:SetPos(trace.HitPos)
    Gyropod:SetModel(Model)
    Gyropod:Spawn()
    Gyropod:SetPlayer(ply)

    ApplySettings(self, Gyropod)

	local min = Gyropod:OBBMins()
    Gyropod:SetPos(trace.HitPos - trace.HitNormal * min.z)

	undo.Create("sc_gyropod")
		undo.AddEntity(Gyropod)
		undo.SetPlayer(ply)
    undo.Finish()

	ply:AddCleanup("sc_gyropod", Gyropod)
	return true
end

--Set the gyropods model, also clear the selected ent
function TOOL:Reload(trace)
	if self:GetStage() == 0 then
		if CLIENT and trace.Entity:IsValid() then return true end
		if not trace.Entity:IsValid() then return end
		local model = trace.Entity:GetModel()
		self:GetOwner():ConCommand("sc_gyro_model "..model);
        self.Model = model
    else
        self:SetStage(0)
		self.Gyro = nil
    end

	return true
end

local DefaultKeys = {}
DefaultKeys["Forward"] = KEY_W -- W
DefaultKeys["Back"]    = KEY_S -- S
DefaultKeys["Left"]    = KEY_A -- A
DefaultKeys["Right"]   = KEY_D -- D
DefaultKeys["Up"]      = KEY_SPACE -- Space
DefaultKeys["Down"]    = KEY_LCONTROL -- Ctrl
DefaultKeys["Look"]    = KEY_LALT -- Alt
DefaultKeys["Level"]   = KEY_R -- R
DefaultKeys["PitchUp"]   = KEY_PAD_8 -- Numpad 8
DefaultKeys["PitchDown"]   = KEY_PAD_5 -- Numpad 5
DefaultKeys["YawRight"]   = KEY_PAD_6 -- Numpad 6
DefaultKeys["YawLeft"]   = KEY_PAD_4 -- Numpad 4
DefaultKeys["RollRight"]   = KEY_PAD_9 -- Numpad 9
DefaultKeys["RollLeft"]   = KEY_PAD_7 -- Numpad 7

local KeyDisplayNames = {}
KeyDisplayNames["Forward"] = "Forward" -- W
KeyDisplayNames["Back"]    = "Back" -- S
KeyDisplayNames["Left"]    = "Left" -- A
KeyDisplayNames["Right"]   = "Right" -- D
KeyDisplayNames["Up"]      = "Up" -- Space
KeyDisplayNames["Down"]    = "Down" -- Ctrl
KeyDisplayNames["Look"]    = "Free Look" -- Alt
KeyDisplayNames["Level"]   = "Level Ship" -- R
KeyDisplayNames["PitchUp"]   = "Pitch Up" -- Numpad 8
KeyDisplayNames["PitchDown"]   = "Pitch Down" -- Numpad 5
KeyDisplayNames["YawRight"]   = "Yaw Right" -- Numpad 6
KeyDisplayNames["YawLeft"]   = "Yaw Left" -- Numpad 4
KeyDisplayNames["RollRight"]   = "Roll Right" -- Numpad 9
KeyDisplayNames["RollLeft"]   = "Roll Left" -- Numpad 7

local Keys = {}
Keys[1]    = "Forward"
Keys[2]    = "Back"
Keys[3]    = "Left"
Keys[4]    = "Right"
Keys[5]    = "Up"
Keys[6]    = "Down"
Keys[7]    = "Look"
Keys[8]    = "Level"
Keys[9]    = "PitchUp"
Keys[10]   = "PitchDown"
Keys[11]   = "YawRight"
Keys[12]   = "YawLeft"
Keys[13]   = "RollRight"
Keys[14]   = "RollLeft"

if SERVER then
    util.AddNetworkString("SC.Gyropod.UpdateKeys")
    net.Receive("SC.Gyropod.UpdateKeys", function(Length, Ply)
        if IsValid(Ply) then
            local NewKeys = net.ReadTable()

            if not Ply.GyropodKeys then
                Ply.GyropodKeys = table.Copy(DefaultKeys)
            end

            for i,k in pairs(NewKeys) do
                if Ply.GyropodKeys[i] ~= nil then
                    Ply.GyropodKeys[i] = k
                end
            end
        end
    end)

    hook.Add("PlayerInitialSpawn", "SetupGyropodDefaultKeys", function(Ply)
        if IsValid(Ply) and not Ply.GyropodKeys then
            Ply.GyropodKeys = table.Copy(DefaultKeys)
        end
    end)
else
    hook.Add("InitPostEntity", "SendGyropodKeysToServer", function()
        local SavedKeys = {}

        for i,k in pairs(DefaultKeys) do
            SavedKeys[i] = cookie.GetNumber("sc_gyropod_"..i, k)
        end

        net.Start("SC.Gyropod.UpdateKeys")
        net.WriteTable(SavedKeys)
        net.SendToServer()
    end)
end

function TOOL.BuildCPanel(CPanel)
    local Label = vgui.Create("DLabel")
    Label:SetText("Entity Settings")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These settings affect the spawned Gyropod entity. Each Gyropod can have different settings here. The settings are saved when the Gyropod is duplicated.\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Turn Speed Multipliers")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:NumSlider("Pitch","sc_gyropod_turnmult_p", 0, 1)
    CPanel:ControlHelp("How quickly you pitch up and down.")
    CPanel:NumSlider("Yaw", "sc_gyropod_turnmult_y", 0, 1)
    CPanel:ControlHelp("How quickly you turn left and right.")
    CPanel:NumSlider("Roll", "sc_gyropod_turnmult_r", 0, 1)
    CPanel:ControlHelp("How quickly you roll left and right.")
    CPanel:NumSlider("Max Turn Speed", "sc_gyropod_maxturnspeed", -1, 115)
    CPanel:ControlHelp("The maximum speed that the ship will turn at. This is also affected by your ship class, and you can never go faster than the class limit!\n\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Acceleration Multipliers")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:NumSlider("Forward", "sc_gyropod_accmult_x", 0, 1)
    CPanel:ControlHelp("How quickly the ship accelerates forward and back.")
    CPanel:NumSlider("Right", "sc_gyropod_accmult_y", 0, 1)
    CPanel:ControlHelp("How quickly the ship accelerates left and right.")
    CPanel:NumSlider("Up", "sc_gyropod_accmult_z", 0, 1)
    CPanel:ControlHelp("How quickly the ship accelerates up and down.\n\n")
    CPanel:NumSlider("Max Speed", "sc_gyropod_maxspeed", -1, 10000)
    CPanel:ControlHelp("The maximum speed that the ship will move at. This is also affected by your ship class, and you can never go faster than the class limit!\n\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Other Entity Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:CheckBox("Enable Auto Parent?", "sc_gyropod_autoparent")
    CPanel:ControlHelp("If this is enabled the Gyropod will automatically parent the ship to itself. Most users will want to leave this on, as only advanced users should need to control the parenting process themselves.")

    CPanel:NumSlider("Full Throttle Angle", "sc_gyropod_maxthrottleangle", 20, 90)
    CPanel:ControlHelp("The amount of distance you have to look away from the front of your ship to max out the turn throttle. Lowering this value will make it so you don't need to look around as much to turn at full speed, but raising it will give you more control over your turn speed.")

    CPanel:NumSlider("Deadzone", "sc_gyropod_deadzone", 0, 20)
    CPanel:ControlHelp("An area in the center of your screen where looking around won't turn the ship.\n\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Player Controls")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These controls will be used for flying any ship using a Gyropod to ensure a seamless flying experience even if the owner of the ship you're flying uses different controls than you do.\n")

    CPanel:CheckBox("Enable Roll Lock?", "sc_gyropod_rolllock")
    CPanel:ControlHelp("If this is enabled the ship will try to keep it's roll as close to 0 as possible. This can help prevent confusion about the direction the ship is facing while flying. This will also disable user control over roll!")

    CPanel:CheckBox("Enable Keyboard Turning?", "sc_gyropod_usekeysforturning")
    CPanel:ControlHelp("If this is enabled then you will be able to use the keyboard to control turning. If it is turned off you will turn the ship with the mouse instead. Rolling is always controlled by the keyboard, unless Roll Lock is enabled.\n\n")

    local function CreateKeyBinder(Key, Default)
        local BinderLabel = vgui.Create("DLabel")
        BinderLabel:SetText(KeyDisplayNames[Key] or "INVALID KEY")
        BinderLabel:SetFont("DermaDefaultBold")
        BinderLabel:SizeToContentsY(2)
        BinderLabel:SetContentAlignment(5)

        local Binder = vgui.Create("DBinder")
        Binder:SetSize(110, 60)
        Binder:SetSelectedNumber(cookie.GetNumber("sc_gyropod_"..Key, Default))

        function Binder:OnChange(NewKey)
            cookie.Set("sc_gyropod_"..Key, NewKey)

            net.Start("SC.Gyropod.UpdateKeys")
            net.WriteTable({[Key]=NewKey})
            net.SendToServer()
        end

        return Binder, BinderLabel
    end


    for i,k in ipairs(Keys) do
        local KeyBinder, BinderLabel = CreateKeyBinder(k, DefaultKeys[k])
        CPanel:AddItem(BinderLabel)
        CPanel:AddItem(KeyBinder)
    end
end