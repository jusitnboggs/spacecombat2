TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Ship Weapons"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left",
	"right",
	"reload"
}

TOOL.ClientConVar[ "launcher" ] = "Test Launcher"
TOOL.ClientConVar[ "recipe" ] = "Small Pulse Round"
TOOL.ClientConVar[ "turrettype" ] = "Dual Heavy"
TOOL.ClientConVar[ "firegroup" ] = 1
TOOL.ClientConVar[ "weaponcount" ] = 1
TOOL.ClientConVar[ "model" ] = "models/slyfo/howitzer.mdl"

if ( CLIENT ) then
    language.Add( "Tool.sc_weapon.name", "Space Combat 2 Weapon Creation Tool" )
    language.Add( "Tool.sc_weapon.desc", "Turns a prop into a weapon." )
    language.Add( "Tool.sc_weapon.left", "Create/Update weapon" )
    language.Add( "Tool.sc_weapon.right", "Create/Update turret" )
    language.Add( "Tool.sc_weapon.reload", "Copy Model from Entity, or Reset on World." )
    language.Add( "undone_sc_weapon", "Undone Weapon" )
    language.Add( "undone_sc_turret", "Undone Turret" )
end

cleanup.Register( "sc_weapon" )
cleanup.Register( "sc_turret" )

local DefaultKeys = {}
DefaultKeys["Primary"]      = MOUSE_LEFT
DefaultKeys["Secondary"]    = MOUSE_RIGHT
DefaultKeys["Tertiary"]     = KEY_F
DefaultKeys["Lock"]         = MOUSE_MIDDLE

local KeyDisplayNames = {}
KeyDisplayNames["Primary"]      = "Fire Primary" -- W
KeyDisplayNames["Secondary"]    = "Fire Secondary" -- S
KeyDisplayNames["Tertiary"]     = "Fire Tertiary" -- A
KeyDisplayNames["Lock"]         = "Lock Target" -- D

local Keys = {}
Keys[1] = "Primary"
Keys[2] = "Secondary"
Keys[3] = "Tertiary"
Keys[4] = "Lock"

if SERVER then
    util.AddNetworkString("SC.Weapon.UpdateKeys")
    net.Receive("SC.Weapon.UpdateKeys", function(Length, Ply)
        if IsValid(Ply) then
            local NewKeys = net.ReadTable()

            if not Ply.WeaponKeys then
                Ply.WeaponKeys = table.Copy(DefaultKeys)
            end

            for i,k in pairs(NewKeys) do
                if Ply.WeaponKeys[i] ~= nil then
                    Ply.WeaponKeys[i] = k
                end
            end
        end
    end)

    hook.Add("PlayerInitialSpawn", "SetupWeaponDefaultKeys", function(Ply)
        if IsValid(Ply) and not Ply.WeaponKeys then
            Ply.WeaponKeys = table.Copy(DefaultKeys)
        end
    end)
else
    hook.Add("InitPostEntity", "SendWeaponKeysToServer", function()
        local SavedKeys = {}

        for i,k in pairs(DefaultKeys) do
            SavedKeys[i] = cookie.GetNumber("sc_Weapon_"..i, k)
        end

        net.Start("SC.Weapon.UpdateKeys")
        net.WriteTable(SavedKeys)
        net.SendToServer()
    end)
end

local function GetPlayerOptions(ply)
	return ply:GetInfo("sc_weapon_launcher"), ply:GetInfo("sc_weapon_recipe")
end

function TOOL:Reload(trace)
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

	local ply = self:GetOwner()

	if IsValid(trace.Entity) then
		ply:ConCommand("sc_weapon_model "..trace.Entity:GetModel())
		return true
	else
		ply:ConCommand("sc_weapon_model models/slyfo/howitzer.mdl")
		return false
	end
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

    local ply = self:GetOwner()
    local LauncherType, Recipe = GetPlayerOptions(ply)

	--Update existing Weapon
	if trace.Entity:IsValid() and string.find(trace.Entity:GetClass(), "sc_module_weapon*") and trace.Entity.Owner == ply then
		trace.Entity:CreateLauncher(LauncherType, Recipe)
		ply:ChatPrint("[Space Combat 2] - Weapon was updated!")

		return true
	else
		local Mdl =	ply:GetInfo("sc_weapon_model")

		if not util.IsValidModel(Mdl) then
			ply:ChatPrint("[Space Combat 2] - Bad model selected, use your reload key on a prop to select a new one!")
			return false
        end

		local Weapon = ents.Create("sc_module_weapon")
		local Ang = ply:EyeAngles()
		Ang.yaw = Ang.yaw + 180 -- Rotate it 180 degrees in my favour
		Ang.roll = 0
		Ang.pitch = 0

		Weapon:SetModel(Mdl)
		Weapon:SetPos(trace.HitPos - trace.HitNormal * Weapon:OBBMins().z)
		Weapon:SetAngles(Ang)
		Weapon:Spawn()
		Weapon:Activate()

        Weapon.Owner = ply

        Weapon:CreateLauncher(LauncherType, Recipe)

		local PhysObj = Weapon:GetPhysicsObject()
		if not IsValid(PhysObj) then
			ErrorNoHalt("[SC2] - ERROR: Weapon spawned without a valid physics object! What?!\n")
			return
		end
		PhysObj:EnableMotion(false)

		undo.Create("sc_weapon")
			undo.AddEntity( Weapon )
			undo.SetPlayer( ply )
		undo.Finish()

		ply:AddCleanup( "sc_weapon", Weapon )

		return true
	end
end

function TOOL:RightClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

	local ply = self:GetOwner()

    if trace.Entity:IsValid() and trace.Entity:GetClass() == "sc_turret" and trace.Entity.Owner == ply then
        local LauncherType, Recipe = GetPlayerOptions(ply)
		local data = {launcher=LauncherType,recipe=Recipe,turret={type=ply:GetInfo("sc_weapon_turrettype"),count=ply:GetInfoNum("sc_weapon_weaponcount",1)}}
		if trace.Entity:ApplySettings(data) then
			ply:ChatPrint("[Space Combat 2] - Turret was updated!")
		else
			ply:ChatPrint("[Space Combat 2] - Cannot change model/class of Turret!")
		end

		return true
	else
	    local Weapon = MakeSCTurret(ply, trace)

	    undo.Create("sc_turret")
		    undo.AddEntity( Weapon )
		    undo.SetPlayer( ply )
	    undo.Finish()

	    ply:AddCleanup( "sc_turret", Weapon )
    end

	return true
end

if SERVER then
    function MakeSCTurret(pl, trace)
        local ply = pl
        local LauncherType, Recipe = GetPlayerOptions(ply)
		local data = {launcher=LauncherType,recipe=Recipe,turret={type=ply:GetInfo("sc_weapon_turrettype"),count=ply:GetInfoNum("sc_weapon_weaponcount",1)}}
        local Turret = ents.Create("sc_turret")

		Turret:SetPos(trace.HitPos)
		-- Once the model has been set use it's OBB to move the turret out of the ground
		timer.Simple(0, function()
			if IsValid(Turret) and not Turret.Invalid then
				Turret:SetPos(trace.HitPos - trace.HitNormal * Turret:OBBMins().z)
			end
		end)
		Turret:SetAngles(trace.HitNormal:Angle() + Angle(90,0,0))
		Turret:Spawn()
		Turret:Activate()

		Turret.Owner = pl

		Turret:ApplySettings(data)

		return Turret
	end
end

local function getLineByValue(DListView, str)
	for k,line in pairs(DListView:GetLines()) do
		if line:GetValue(1) == str then return line end
	end
	return nil
end

function TOOL.BuildCPanel(panel)
	CPanel = panel

	CPanel:ClearControls()
	CPanel:SetName("Space Combat 2 - Weapons")
	CPanel:Help("Select a Launcher Type")

    local SelectedLauncher = LocalPlayer():GetInfo("sc_weapon_launcher")
    if not GAMEMODE.Launchers.Types.LoadedTypes[SelectedLauncher] then
		SelectedLauncher = next(GAMEMODE.Launchers.Types.LoadedTypes)
		RunConsoleCommand("sc_weapon_launcher", SelectedLauncher)
    end

    local SelectedRecipe = LocalPlayer():GetInfo("sc_weapon_launcher")
    if not GAMEMODE.Launchers.Types.LoadedTypes[SelectedLauncher]:GetCompatibleProjectileRecipes()["NULL"][SelectedRecipe] then
		SelectedRecipe = next(GAMEMODE.Launchers.Types.LoadedTypes[SelectedLauncher]:GetCompatibleProjectileRecipes()["NULL"])
		RunConsoleCommand("sc_weapon_recipe", SelectedRecipe)
    end

	local SelectedTurretType = LocalPlayer():GetInfo("sc_weapon_turrettype")
	if not table.HasValue(SC.GetTurretList(),SelectedTurretType) then
		SelectedTurretType = SC.GetTurretList()[1]
		RunConsoleCommand("sc_weapon_turrettype", SelectedTurretType)
	end

	CPanel.LauncherBox = vgui.Create("DListView", CPanel)
	CPanel.LauncherBox:SetHeight(300)
	CPanel.LauncherBox:AddColumn("Launcher Type")
	CPanel:AddItem(CPanel.LauncherBox)

	for k, v in SortedPairs(GAMEMODE.Launchers.Types.LoadedTypes) do
		CPanel.LauncherBox:AddLine(k)
	end

	if #CPanel.LauncherBox:GetSelected() < 1 then CPanel.LauncherBox:SelectItem(getLineByValue(CPanel.LauncherBox,SelectedLauncher))end

	CPanel.LauncherBox.OnClickLine = function(parent, line, isselected)
		SelectedLauncher = line:GetValue(1)
		RunConsoleCommand("sc_weapon_launcher", line:GetValue(1))

		parent:ClearSelection()
		parent:SelectItem(line)

		if CPanel.RecipeBox ~= nil then
			CPanel.RecipeBox:Clear()

			for k, v in SortedPairs(GAMEMODE.Launchers.Types.LoadedTypes[SelectedLauncher]:GetCompatibleProjectileRecipes()["NULL"]) do
				CPanel.RecipeBox:AddLine(k)
			end

			CPanel.RecipeBox:SelectFirstItem()
			RunConsoleCommand("sc_weapon_recipe", CPanel.RecipeBox:GetLine(1):GetValue(1))
		end
	end

	CPanel.RecipeBox = vgui.Create("DListView", CPanel)
	CPanel.RecipeBox:SetHeight(300)
	CPanel.RecipeBox:AddColumn("Projectile Recipe")
	CPanel:AddItem(CPanel.RecipeBox)

	for k, v in SortedPairs(GAMEMODE.Launchers.Types.LoadedTypes[SelectedLauncher]:GetCompatibleProjectileRecipes()["NULL"]) do
		CPanel.RecipeBox:AddLine(k)
	end

	if #CPanel.RecipeBox:GetSelected() < 1 then CPanel.RecipeBox:SelectItem(getLineByValue(CPanel.RecipeBox,SelectedRecipe)) end

	CPanel.RecipeBox.OnClickLine = function(parent, line, isselected)
		RunConsoleCommand("sc_weapon_recipe", line:GetValue(1))

		parent:ClearSelection()
		parent:SelectItem(line)
	end

	CPanel.TurretTypeBox = vgui.Create("DListView", CPanel)
	CPanel.TurretTypeBox:SetHeight(300)
	CPanel.TurretTypeBox:AddColumn("Turret Type")
	CPanel:AddItem(CPanel.TurretTypeBox)

	for k, v in SortedPairs(SC.GetTurretList()) do
		CPanel.TurretTypeBox:AddLine(v)
	end

	if #CPanel.TurretTypeBox:GetSelected() < 1 then CPanel.TurretTypeBox:SelectItem(getLineByValue(CPanel.TurretTypeBox,SelectedTurretType)) end

	CPanel.TurretTypeBox.OnClickLine = function(parent, line, isselected)
		RunConsoleCommand("sc_weapon_turrettype", line:GetValue(1))
		RunConsoleCommand("sc_weapon_weaponcount", SC.GetTurretWeaponCount(line:GetValue(1)))
		parent:ClearSelection()
		parent:SelectItem(line)
	end

	CPanel:AddControl("Slider", {
			Label = "Turret Weapon Count",
			Type = "Int",
			Min = "1",
			Max = "20",
			Command = "sc_weapon_weaponcount"
        })

    local Label = vgui.Create("DLabel")
    Label:SetText("Player Controls")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These controls will be used for controlling any weapons or turrets linked to a seat or targeting computer to ensure a seamless experience even if the owner of the ship you're flying uses different controls than you do.\n")
    local function CreateKeyBinder(Key, Default)
        local BinderLabel = vgui.Create("DLabel")
        BinderLabel:SetText(KeyDisplayNames[Key] or "INVALID KEY")
        BinderLabel:SetFont("DermaDefaultBold")
        BinderLabel:SizeToContentsY(2)
        BinderLabel:SetContentAlignment(5)

        local Binder = vgui.Create("DBinder")
        Binder:SetSize(110, 60)
        Binder:SetSelectedNumber(cookie.GetNumber("sc_weapon_"..Key, Default))

        function Binder:OnChange(NewKey)
            cookie.Set("sc_weapon_"..Key, NewKey)

            net.Start("SC.Weapon.UpdateKeys")
            net.WriteTable({[Key]=NewKey})
            net.SendToServer()
        end

        return Binder, BinderLabel
    end

    for i,k in ipairs(Keys) do
        local KeyBinder, BinderLabel = CreateKeyBinder(k, DefaultKeys[k])
        CPanel:AddItem(BinderLabel)
        CPanel:AddItem(KeyBinder)
    end
end
