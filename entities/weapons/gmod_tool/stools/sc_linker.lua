TOOL.Category		= "Basic Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Entity Linker"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}
TOOL.Device         = nil

if CLIENT then
    language.Add( "Tool.sc_linker.name", "Entity Linking Tool" )
    language.Add( "Tool.sc_linker.desc", "Links a Space Combat device." )
    language.Add( "Tool.sc_linker.left", "Select Prop/Device/Core" )
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	if not trace.Entity:IsValid() then return false end

    if IsValid(self.Device) and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(self:GetOwner())) then
        if trace.Entity == self.Device then
            self:GetOwner():ChatPrint("[Space Combat 2] - Unselected Entity!")
            self.Device = nil
        elseif (self.Device.IsSCTurret or self.Device.IsSCWeapon) then
            if trace.Entity:IsVehicle() then
                if self.Device:LinkPod(trace.Entity) then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Linked Weapon to Pod!")
                    self.Device = nil
                else
                    self:GetOwner():ChatPrint("[Space Combat 2] - Weapon can't be linked to this Pod!")
                end
            elseif trace.Entity.IsSCTargeter then
                if self.Device:LinkTargeter(trace.Entity) then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Linked Weapon to Targeter!")
                    self.Device = nil
                else
                    self:GetOwner():ChatPrint("[Space Combat 2] - Weapon can't be linked to this Targeter!")
                end
            else
                self:GetOwner():ChatPrint("[Space Combat 2] - You can't link to that!")
            end
        elseif self.Device.IsLSEnt then
            if trace.Entity.IsNode then
                if self.Device.IsNode then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Cannot link a Ship Core to another Ship Core!")
                    self.Device = nil
                else
                    self:GetOwner():ChatPrint("[Space Combat 2] - Linked Device!")
                    self.Device:Link(trace.Entity)
                    self.Device = nil
                end
            end
        elseif trace.Entity.IsMechanical then
            if trace.Entity:IsLinked(self.Device) then
                if trace.Entity:Unlink(self.Device) then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Unlinked mechanical entity!")
                    self.Device = nil
                end
            else
                if trace.Entity:Link(self.Device) then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Linked mechanical entity!")
                    self.Device = nil
                else
                    self:GetOwner():ChatPrint("[Space Combat 2] - You can't link to that!")
                end
            end
        end
	else
		if not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(self:GetOwner()) then
			self:GetOwner():ChatPrint("[Space Combat 2] - Selected Entity!")
			self.Device = trace.Entity
		else
			self:GetOwner():ChatPrint("[Space Combat 2] - You can't link that!")
		end
	end
end

function TOOL.BuildCPanel(CPanel)
	CPanel:AddControl("Header", { Text = "#Tool.sc_linker.name", Description = "#Tool.sc_linker.desc" })
end