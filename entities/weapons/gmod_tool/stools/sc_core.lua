TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Ship Cores"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.ClientConVar["name"] = "Shippy McShipface"
TOOL.ClientConVar["shield"] = "Hybrid"
TOOL.ClientConVar["armor"] = "Standard"
TOOL.ClientConVar["hull"] = "Standard"
TOOL.ClientConVar["corer"] = 33
TOOL.ClientConVar["coreg"] = 54
TOOL.ClientConVar["coreb"] = 69
TOOL.ClientConVar["corea"] = 156

if CLIENT then
    language.Add("Tool.sc_core.name", "Ship Core Creation Tool")
    language.Add("Tool.sc_core.desc", "Turns a prop into a Ship Core.")
    language.Add("Tool.sc_core.0", "Primary: Create/Update Ship Core")
	language.Add("sboxlimit.sc_core", "You've hit the Ship Core limit!")
	language.Add("undone_sc_core", "Undone Ship Core")
end

if SERVER then
  CreateConVar('sbox_maxsc_core',4)
end

cleanup.Register("sc_core")

function TOOL:LeftClick(trace)
    local ply = self:GetOwner()

    if CLIENT then return true end
    if not trace.HitPos then return false end
    if trace.Entity:IsPlayer() then return false end
    if not IsValid(trace.Entity) then return false end
    if (NADMOD or CPPI) and trace.Entity:CPPIGetOwner() ~= ply then return false end

	--Core options
	local r	= self:GetClientNumber("corer", 0)
	local g	= self:GetClientNumber("coreg", 0)
	local b	= self:GetClientNumber("coreb", 0)
	local a	= self:GetClientNumber("corea", 0)

	COptions = {
		ShieldColor = Color(r, g, b, a),
		ShieldType = self:GetClientInfo("shield") or "Hybrid",
		ArmorType = self:GetClientInfo("armor") or "Standard",
		HullType = self:GetClientInfo("hull") or "Standard",
		ShipName = self:GetClientInfo("name") or "Shippy McShipface"
	}

	--Update existing core
	if string.find(trace.Entity:GetClass(), "ship_core") then
		trace.Entity:ApplySettings(COptions)
		ply:ChatPrint("[Space Combat 2] - Ship core was updated!")
		return false
	else

	local Ang = trace.Entity:GetAngles()
	local Pos =	trace.Entity:GetPos()
    local Mdl =	trace.Entity:GetModel()

	local Core = MakeSCCore(ply, Pos, Ang, Mdl, COptions)
	local PhysObj = Core:GetPhysicsObject()
	if not IsValid(PhysObj) then
		ErrorNoHalt("[SC2] - ERROR: Ship Core spawned without a valid physics object! What?!\n")
		return
	end
	PhysObj:EnableMotion(false)

	local function WeldEntities(core, ent)
		if not IsValid(ent) then return end

		local ConTable = constraint.GetTable(ent)

		for k, con in ipairs(ConTable) do
			for EntNum, Ent in pairs(con.Entity) do
				if con.Type == "Weld" then
					constraint.Weld(core, Ent)
				end
			end
		end
	end

	WeldEntities(Core, trace.Entity)

	trace.Entity:Remove()

	undo.Create("sc_core")
		undo.AddEntity(Core)
		undo.SetPlayer(ply)
	undo.Finish()

	ply:AddCleanup("sc_core", Core)

	return true
	end
end

if SERVER then
	function MakeSCCore(pl, Pos, Ang, Mdl, Setup)
		if not pl:CheckLimit("sc_core") then return nil end

		local Core = ents.Create("ship_core")

		Core:SetPos(Pos)
		Core:SetAngles(Ang)
		Core:SetModel(Mdl)
		Core:Spawn()
		Core:Activate()
		Core:ApplySettings(Setup)

		Core.Owner = pl

		return Core
	end
end

local function TableToText(Table)
	local Text = ""

	for i,k in pairs(Table) do
		Text = Text.."\n\t"..k.." "..i
	end

	return Text
end

function TOOL.BuildCPanel(CPanel)
	local HullHelpText =
[[Hull Health Modifier: %%%s
Cargo Storage Modifier: %%%s
Ammo Storage Modifier: %%%s

Extra Slots: %s

Resistances: %s]]

	local ArmorHelpText =
[[Armor Health Modifier: %%%s
Ship Mass Modifier: %%%s

Resistances: %s]]

	local ShieldHelpText =
[[Shield Health Modifier: %%%s
Shield Recharge Rate: %s
Shield Energy Efficiency: %%%s

Resistances: %s]]

	local ToolHelpText =
[[- SHIP CORE SYSTEM -

Guide: Steeveeo, Dubby

Ship Cores are designed with space
combat in mind. They provide EDD
(Equal Damage Distribution) so you
don't loose single parts of your ship
in the heat of combat.

- HOW TO -
1) Build your ship.
2) Choose a good, core looking prop.
3) Setup your core from the points you
   have available.
4) Click the prop with this tool, then
   weld it in. Wait a couple seconds for
   the stats to update.


- IMPORTANT NOTES -

1) CONTRAPTIONS WITHOUT SHIP CORES
   ARE EXTREMELY VULNERABLE TO DAMAGE!!!
2) This tool DOES NOT SAVE CONSTRAINTS,
   your ship will fall apart if you apply
   it to an existing part of the hull!!!
3) Direct shots to the core will cause
   DOUBLE damage.
]]

	CPanel:Help(ToolHelpText)
	CPanel:TextEntry("Ship Name", "sc_core_name")

	local HullHelp
	local HullTypes = CPanel:ComboBox("Hull Type", "sc_core_hull")
	function HullTypes:OnSelect(Index, Value)
		local Data = SC.HullTypes[Value]
		HullHelp:SetText(string.format(HullHelpText, Data.HealthModifier*100, Data.CargoModifier*100, Data.AmmoModifier*100, TableToText(Data.BonusSlots), TableToText(Data.Resistances)))
		RunConsoleCommand("sc_core_hull", Value)
	end

    local SelectedHullIndex = 0
    for i, k in pairs(SC.HullTypes or {}) do
        local Index = HullTypes:AddChoice(i)
        if GetConVarString("sc_core_hull") == i then
            SelectedHullIndex = Index
        end
	end

    HullHelp = CPanel:ControlHelp("Select a hull type!")

    if SelectedHullIndex ~= 0 then
        HullTypes:ChooseOptionID(SelectedHullIndex)
    end

	local ArmorHelp
	local ArmorTypes = CPanel:ComboBox("Armor Type", "sc_core_armor")
	function ArmorTypes:OnSelect(Index, Value)
		local Data = SC.ArmorTypes[Value]
		local Text = string.format(ArmorHelpText, Data.HealthModifier*100, Data.MassModifier*100, TableToText(Data.Resistances))

		if Data.Regenerative then
			Text = Text.."\n\nRegenerates health automatically!"
		end

		ArmorHelp:SetText(Text)

		RunConsoleCommand("sc_core_armor", Value)
	end

    local SelectedArmorIndex = 0
    for i, k in pairs(SC.ArmorTypes or {}) do
        local Index = ArmorTypes:AddChoice(i)
        if GetConVarString("sc_core_Armor") == i then
            SelectedArmorIndex = Index
        end
	end

    ArmorHelp = CPanel:ControlHelp("Select a Armor type!")

    if SelectedArmorIndex ~= 0 then
        ArmorTypes:ChooseOptionID(SelectedArmorIndex)
    end

	local ShieldHelp
	local ShieldTypes = CPanel:ComboBox("Shield Type", "sc_core_shield")
	function ShieldTypes:OnSelect(Index, Value)
		local Data = SC.ShieldTypes[Value]
		local Text = string.format(ShieldHelpText, Data.HealthModifier*100, Data.RechargeExponent, Data.Efficiency*100, TableToText(Data.Resistances))

		if Data.Efficiency < 0 then
			Text = Text.."\n\nThis special shield type will give you Energy when it resists damage!"
		end

		ShieldHelp:SetText(Text)

		RunConsoleCommand("sc_core_shield", Value)
	end

    local SelectedShieldIndex = 0
    for i, k in pairs(SC.ShieldTypes or {}) do
        local Index = ShieldTypes:AddChoice(i)
        if GetConVarString("sc_core_Shield") == i then
            SelectedShieldIndex = Index
        end
	end

    ShieldHelp = CPanel:ControlHelp("Select a Shield type!")

    if SelectedShieldIndex ~= 0 then
        ShieldTypes:ChooseOptionID(SelectedShieldIndex)
    end

	CPanel:AddControl("Color", {
	    Label = "Shield Color",
	    Red = "sc_core_corer",
	    Blue = "sc_core_coreb",
	    Green = "sc_core_coreg",
	    Alpha = "sc_core_corea",
	    ShowHSV = 1,
	    ShowRGB = 1,
	    Multiplier = 255 --You can change this to make the rgba values go up to any value
	})
end