TOOL.Category		= "Mechanical Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Engine Spawner"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}

TOOL.ClientConVar["type"] = "Default"

if ( CLIENT ) then
    language.Add( "Tool.sc_engine.name", "Space Combat 2 Engine Creation Tool" )
    language.Add( "Tool.sc_engine.desc", "Creates an Engine." )
    language.Add( "Tool.sc_engine.left", "Create Engine" )
    language.Add( "undone_sc_engine", "Undone Engine" )
end

cleanup.Register( "sc_engine" )

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

    local ply = self:GetOwner()
    local Engine = ents.Create("sc_engine")
    local Ang = ply:EyeAngles()
    Ang.yaw = Ang.yaw + 180 -- Rotate it 180 degrees in my favour
    Ang.roll = 0
    Ang.pitch = 0

    Engine:SetPos(trace.HitPos)

    -- Once the model has been set use it's OBB to move the turret out of the ground
    timer.Simple(0, function()
        if IsValid(Engine) then
            Engine:SetPos(trace.HitPos - trace.HitNormal * Engine:OBBMins().z)
        end
    end)

    Engine:SetAngles(Ang)
    Engine:Spawn()
    Engine:Activate()
    Engine:SetupEngine(ply:GetInfo("sc_engine_type"))
    Engine.Owner = ply

    local PhysObj = Engine:GetPhysicsObject()
    if not IsValid(PhysObj) then
        ErrorNoHalt("[SC2] - ERROR: Engine spawned without a valid physics object! What?!\n")
        return
    end
    PhysObj:EnableMotion(false)

    undo.Create("sc_engine")
        undo.AddEntity( Engine )
        undo.SetPlayer( ply )
    undo.Finish()

    ply:AddCleanup( "sc_engine", Engine )

    return true
end

function TOOL.BuildCPanel(panel)
	CPanel = panel

	CPanel:ClearControls()
	CPanel:SetName("Space Combat 2 - Engines")
	CPanel:Help("Select an Engine Type")

    local Engines = GAMEMODE.Mechanical.Engines
	local SelectedEngine = LocalPlayer():GetInfo("sc_engine_type")
	if not table.HasValue(Engines, SelectedEngine) then
		SelectedEngine = next(Engines)
		RunConsoleCommand("sc_engine_type", SelectedEngine)
	end

	CPanel.EngineTypeBox = vgui.Create("DCategoryTree", CPanel)
	CPanel.EngineTypeBox:SetHeight(700)
    CPanel.EngineTypeBox:PopulateFromTableByValue(Engines, "Category", "Name", "Hidden")
    CPanel:AddPanel(CPanel.EngineTypeBox)

	CPanel.EngineTypeBox.OnNodeSelected = function(Parent, Node)
		RunConsoleCommand("sc_engine_type", Node.Data)
	end
end
