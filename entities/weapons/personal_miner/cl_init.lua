include('shared.lua')

local BeamMat = Material("effects/mining_beam/mining_laser")
BeamMat:SetMaterialInt("$spriterendermode",0)
BeamMat:SetMaterialInt("$illumfactor",8)
BeamMat:SetMaterialFloat("$alpha",1)
BeamMat:SetMaterialInt("$nocull",1)
local FlareMat = Material("sprites/light_glow01")
FlareMat:SetMaterialInt("$spriterendermode",0)
FlareMat:SetMaterialInt("$illumfactor",8)
FlareMat:SetMaterialFloat("$alpha",0.8)
FlareMat:SetMaterialInt("$nocull",1)

SWEP.PrintName = "Personal Mining Device"
SWEP.DrawAmmo = false
SWEP.DrawCrosshair = true
SWEP.Slot = 3
SWEP.SlotPos = 3

local Soundpath_Active = Sound("sb3_mining/mining_laser/mining_laser_active.wav")
local Soundpath_Cutting = Sound("sb3_mining/mining_laser/mining_laser_cutting.wav")

local SpriteMultiplier = 0

local LASER = Material('effects/mining_laser')

function SWEP:PlaySound( hit )
	if hit then
		self.Sound_Active:ChangeVolume( 0,1 )
		self.Sound_Cutting:ChangeVolume( 1,1 )
	else
		self.Sound_Active:ChangeVolume( 1,1 )
		self.Sound_Cutting:ChangeVolume( 0,1 )
	end
end

function SWEP:Initialize()
    local ply = LocalPlayer()
    self.VM = ply:GetViewModel()
    if self.VM and IsValid(self.VM) then
        local attachmentIndex = self.VM:LookupAttachment("muzzle")
        if attachmentIndex == 0 then attachmentIndex = self.VM:LookupAttachment("1") end
	    self.Attach = attachmentIndex
    else
        self.Attach = 0
    end

	self.SizeMultiplier = 0

	self.BeamLength = 150
	self.BeamColor = Color(255,25,25,255)
	self.BeamWidth = 3
	self.BeamPulse = 0.5
	self.SpriteSize = 10
	self.SpritePulse = 2.5
end

function SWEP:ViewModelDrawn()
    local ply = LocalPlayer()
    self.VM = ply:GetViewModel()
    if self.VM then
        local attachmentIndex = self.VM:LookupAttachment("muzzle")
        if attachmentIndex == 0 then attachmentIndex = self.VM:LookupAttachment("1") end
	    self.Attach = attachmentIndex
    else
        self.Attach = 0
    end

    if not self.VM then return end

	if self.Weapon:GetNWBool("Charging") then
		local ang = self.Owner:GetAimVector()

		render.SetMaterial( FlareMat )
		local ChargePercent = self.SizeMultiplier / 1.5
		local Pulse = (self.SpritePulse * ChargePercent) * math.Rand(-1,1)
		local SpriteTotalPulse = (0.7 * (self.SpriteSize * ChargePercent)) + Pulse
		render.DrawSprite( self.VM:GetAttachment(self.Attach).Pos + (ang * 10), SpriteTotalPulse, SpriteTotalPulse, self.BeamColor)

		if self.SizeMultiplier < 1.5 then
			self.SizeMultiplier = self.SizeMultiplier + FrameTime()
		end
	else
		self.SizeMultiplier = 0
	end

	if(self.Weapon:GetNWBool("Active")) then
        --Draw the laser beam.
        local pos = self.Owner:GetShootPos()
		local ang = self.Owner:GetAimVector()
		local tracedata = {}
		tracedata.start = pos
		tracedata.endpos = pos+(ang*150)
		tracedata.filter = self.Owner
		local trace = util.TraceLine(tracedata)

		local target = trace.Entity

		local BPulse = self.BeamPulse * math.Rand(-1,1)
		render.SetMaterial( BeamMat )
		render.DrawBeam(self.VM:GetAttachment(self.Attach).Pos + (ang * 10), trace.HitPos, self.BeamWidth + BPulse, 0, 0, self.BeamColor)

		local SPulse = self.SpritePulse * math.Rand(-1,1)
		render.SetMaterial( FlareMat )
		render.DrawSprite( self.VM:GetAttachment(self.Attach).Pos + (ang * 10), self.SpriteSize + SPulse, self.SpriteSize + SPulse, self.BeamColor)

		SPulse = self.SpritePulse * math.Rand(-1,1)
		render.SetMaterial( FlareMat )
		render.DrawSprite( trace.HitPos, self.SpriteSize + SPulse, self.SpriteSize + SPulse, self.BeamColor)

		--If no sounds, then create sounds
		if not self.Sound_Active or not self.Sound_Cutting then
			self.Sound_Active = CreateSound(self.Weapon, Soundpath_Active)
			self.Sound_Cutting = CreateSound(self.Weapon, Soundpath_Cutting)
			self.Sound_Active:Play()
			self.Sound_Cutting:Play()
		end

		--Laser sound impact control
		self:PlaySound( trace.Hit )

		if trace.Hit then
			--Collision Effects
			if target:GetClass() == "mining_mineral" or target:GetClass() == "mining_mineral_parent" then
				--Sparkies
				local sparks = EffectData()
				sparks:SetOrigin(trace.HitPos)
				sparks:SetStart(trace.HitPos)
				sparks:SetNormal(trace.HitNormal)
				util.Effect("cball_bounce",sparks)
			else
				--Sparkies
				local sparks = EffectData()
				sparks:SetOrigin(trace.HitPos)
				sparks:SetStart(trace.HitPos)
				sparks:SetNormal(trace.HitNormal)
				util.Effect("MetalSpark",sparks)
				--Dust
				local dust = EffectData()
				dust:SetOrigin(trace.HitPos)
				dust:SetStart(trace.HitPos)
				util.Effect("WheelDust",dust)
			end
		end
	else
		--If not Active, mute sounds
		if(self.Sound_Active and self.Sound_Cutting) then
			self.Sound_Active:ChangeVolume( 0,1 )
			self.Sound_Cutting:ChangeVolume( 0,1 )
		end
	end
end
