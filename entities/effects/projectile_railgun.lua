/**********************************************
	Scalable Railgun Projectile Effect
	
	Author: Steeveeo
***********************************************/

local trailMat = Material("effects/ar2ground2")

function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.Scale		= data:GetScale()
	self.Velocity	= data:GetMagnitude()
	self.Time		= CurTime()
	self.LifeTime	= 0
	self.Model		= ClientsideModel("models/slyfo/how_explosive.mdl", 7)
	
	self.TrailLength = 1500 * self.Scale
	self.CurTrailLength = 0
	
	self.Emitter = ParticleEmitter(self.StartPos)
	
	self.Model:SetModelScale(self.Scale, 0)
end

function EFFECT:Think()
	self.EndPos = self.StartPos + (self.Forward * self.Velocity * (FrameTime()))
	
	--Grow trail (so it doesn't shoot out the back of the gun)
	self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)

	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit and tr.Entity ~= self.Model then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	self.Model:SetPos(self.StartPos)
	self.Model:SetAngles(self.Forward:Angle())
	self:SetRenderBoundsWS(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength))
	
	if self.Dead then
		self.Model:Remove()
		self.Emitter:Finish()
		return false
	end
	
	--Particles
	local velocity = (self.Forward + (VectorRand() * 0.15)):GetNormalized() + (self.Forward * (self.Velocity * math.Rand(-0.1, 0.75)))
	
	local p = self.Emitter:Add("effects/flares/light-rays_001", self.StartPos)
	p:SetDieTime(math.Rand(0.5, 1))
	p:SetVelocity(velocity)
	p:SetAirResistance(300)
	p:SetStartAlpha(255)
	p:SetEndAlpha(255)
	p:SetStartSize(math.random(15, 20) * self.Scale)
	p:SetEndSize(0)
	
	self.Time = CurTime()
	
	return true
end

function EFFECT:Render()
	render.SetMaterial(trailMat)
	render.DrawBeam(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength), 10 * self.Scale, 1, 0, Color(144, 196, 255, 255))
	
	return true
end
