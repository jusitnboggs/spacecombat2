--[[*********************************************
	Small Hull Explosion Effect w/ Debris
	
	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav"
	}

	self.Normal = data:GetNormal()
	self.Parent = data:GetEntity()
	
	if not IsValid(self.Parent) then return false end
	
	self.Pos = self.Parent:WorldToLocal(data:GetOrigin())
	self.Emitter = ParticleEmitter(self.Parent:LocalToWorld(self.Pos))
	
	--Debris
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(75, 350)
		local size = math.random(5, 15)
		
		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(4)
		p:SetVelocity(velocity)
		p:SetAirResistance(50)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(size)
		p:SetEndSize(size)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Fire
	for i=1, 10 do
		local velocity = (self.Normal + (VectorRand() * 0.65)):GetNormalized() * math.Rand(75, 500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.1, 1))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 150))
		p:SetEndSize(math.random(150, 200))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Flare
	for i=1, 5 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(300, 2000)
		
		local p = self.Emitter:Add("sprites/light_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.1, 0.2))
		p:SetStartAlpha(255)
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(1000)
		p:SetEndSize(1000)
		p:SetColor(255, 220, 200)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play(self.Sounds[math.random(1,#self.Sounds)], self.Parent:LocalToWorld(self.Pos))
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		--util.ScreenShake(self.Pos, 2, 0.1, 1, 2048)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
