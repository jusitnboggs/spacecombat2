--[[*********************************************
	Large Hull Explosion Effect w/ Shock
	
	Author: Steeveeo
**********************************************]]--

local sound_Explosions = {}

sound_Explosions[1] = {}
sound_Explosions[1].Near = Sound("shipsplosion/sc_explode_01_near.mp3")
sound_Explosions[1].Mid = Sound("shipsplosion/sc_explode_01_mid.mp3")
sound_Explosions[1].Far = Sound("shipsplosion/sc_explode_01_far.mp3")

sound_Explosions[2] = {}
sound_Explosions[2].Near = Sound("shipsplosion/sc_explode_02_near.mp3")
sound_Explosions[2].Mid = Sound("shipsplosion/sc_explode_02_mid.mp3")
sound_Explosions[2].Far = Sound("shipsplosion/sc_explode_02_far.mp3")

sound_Explosions[3] = {}
sound_Explosions[3].Near = Sound("shipsplosion/sc_explode_03_near.mp3")
sound_Explosions[3].Mid = Sound("shipsplosion/sc_explode_03_mid.mp3")
sound_Explosions[3].Far = Sound("shipsplosion/sc_explode_03_far.mp3")

sound_Explosions[4] = {}
sound_Explosions[4].Near = Sound("shipsplosion/sc_explode_04_near.mp3")
sound_Explosions[4].Mid = Sound("shipsplosion/sc_explode_04_mid.mp3")
sound_Explosions[4].Far = Sound("shipsplosion/sc_explode_04_far.mp3")

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	self.Parent = data:GetEntity()
	self.Pos = data:GetOrigin()
	self.Direction = VectorRand() --data:GetNormal()
	self.Lifetime = CurTime() + 2
	self.Duration = 0
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.Dist = LocalPlayer():GetPos():Distance(self.Pos)
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat()
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat()
	
	--Shockwave Settings
	self.Shock = {}
	self.Shock.Mat = Material("effects/shipsplosion/shockwave_001")
	self.Shock.Mat:SetInt("$overbrightfactor",1)
	self.Shock.Color = Color(128, 128, 144)
	self.Shock.CurSize = 0
	self.Shock.MaxSize = 4000
	self.Shock.StartAlpha = 255
	self.Shock.Alpha = self.Shock.StartAlpha
	self.Shock.FadeStart = 0.5
	self.Shock.FadeTime = 1.5
	self.Shock.Rotation = math.Rand(0, 360)
	self.Shock.RotationSpeed = 10
	self.Shock.RotationDrag = 2
	self.Shock.RotationMinSpeed = 0.75
	self.Shock.GrowExp = 0.65
	self.Shock.Passes = 1
	
	--Fire Main
	for i=1, 30 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Fire Highlights
	for i=1, 20 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(1, 1.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(5000)
		p:SetEndSize(1250)
		p:SetColor(255, 220, 200)
	end
	
	--Flare Overlay
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(1, 1.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(7500)
		p:SetEndSize(7500)
		p:SetColor(255, 220, 200)
	end
	
	--Play Sound
	math.randomseed(CurTime())
	local sound = sound_Explosions[math.random(1, #sound_Explosions)]
	if self.Dist < self.NearDist then
		LocalPlayer():EmitSound(sound.Near, 511, 50, 1)
	elseif self.Dist < self.MidDist then
		LocalPlayer():EmitSound(sound.Mid, 511, 50, 1)
	elseif self.Dist < self.FarDist then
		LocalPlayer():EmitSound(sound.Far, 511, 50, 1)
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(self.Pos, 3, 0.1, 0.5, 4096)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
  	if CurTime() < self.Lifetime then 
		local frameTime = FrameTime()
		
		-------------
		-- Shockwave
		-------------
		
		--Grow
		local growRate = math.abs(self.Shock.MaxSize - self.Shock.CurSize) * self.Shock.GrowExp
		self.Shock.CurSize = self.Shock.CurSize + (growRate * frameTime)
		
		--Spin
		self.Shock.RotationSpeed = math.Max(self.Shock.RotationSpeed - (self.Shock.RotationDrag * frameTime), self.Shock.RotationMinSpeed)
		self.Shock.Rotation = self.Shock.Rotation + (self.Shock.RotationSpeed * frameTime)
		
		--Fade Out
		if self.Duration > self.Shock.FadeStart then
			local alphaPerSec = self.Shock.StartAlpha / self.Shock.FadeTime
			local alphaStep = alphaPerSec * frameTime
			
			self.Shock.Alpha = math.Clamp(self.Shock.Alpha - alphaStep, 0, 255)
		end
		
		--Increment Effect Timer
		self.Duration = self.Duration + frameTime
		
		return true
	else
		return false	
	end
end

function EFFECT:Render( )
	--Shockwave
	if self.Shock.Alpha > 0 then
		render.SetMaterial(self.Shock.Mat)
		self.Shock.Color.a = self.Shock.Alpha
		
		for i = 1, self.Shock.Passes do
			render.DrawQuadEasy( self.Pos, self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, self.Shock.Rotation )
			render.DrawQuadEasy( self.Pos, -self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, -self.Shock.Rotation )
		end
	end			 
end
