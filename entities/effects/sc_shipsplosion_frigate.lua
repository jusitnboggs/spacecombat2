--[[*********************************************
	Frigate Main Death Explosion
	
	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "ambient/explosions/explode_2.wav" )
util.PrecacheSound( "ambient/explosions/explode_9.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_2.wav",
		"ambient/explosions/explode_9.wav"
	}
	
	--Set up Effect Settings
	self.Position = data:GetOrigin()
	self.Direction = VectorRand() --data:GetNormal()
	self.Lifetime = CurTime() + 5
	self.Duration = 0

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Shockwave Settings
	self.Shock = {}
	self.Shock.Mat = Material("effects/shipsplosion/shockwave_001")
	self.Shock.Mat:SetInt("$overbrightfactor",1)
	self.Shock.Color = Color(128, 128, 144)
	self.Shock.CurSize = 0
	self.Shock.MaxSize = 7500
	self.Shock.StartAlpha = 128
	self.Shock.Alpha = self.Shock.StartAlpha
	self.Shock.FadeStart = 2
	self.Shock.FadeTime = 3
	self.Shock.Rotation = math.Rand(0, 360)
	self.Shock.RotationSpeed = 10
	self.Shock.RotationDrag = 2
	self.Shock.RotationMinSpeed = 0.75
	self.Shock.GrowExp = 0.65
	self.Shock.Passes = 1
	
	--Fire
	for i=1, 150 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(300, 5000)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(3, 5))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(400, 600))
		p:SetEndSize(math.random(600, 750))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
	end
	
	--Fire Highlights
	for i=1, 50 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(300, 5000)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
		p:SetDieTime(math.Rand(3, 5))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(400, 600))
		p:SetEndSize(math.random(600, 750))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		p:SetColor(96, 96, 255)
	end
	
	--Debris
	for i=1, 75 do
		local velocity = VectorRand() * math.Rand(600, 2800)
		local size = math.random(25, 75)
		
		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(15, 25))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(size)
		p:SetEndSize(size)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Flare Small
	for i=1, 5 do
		local velocity = VectorRand() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(3, 5))
		p:SetStartAlpha(255)
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(2500)
		p:SetEndSize(7500)
		p:SetColor(220, 220, 255)
	end
	
	--Flare Big
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(7500)
		p:SetEndSize(25000)
		p:SetColor(255, 220, 200)
	end
	
	--Flash
	for i=1, 5 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.5, 0.75))
		p:SetStartAlpha(255)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(20000)
		p:SetEndSize(0)
		p:SetColor(220, 220, 255)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play( self.Sounds[math.random(1,#self.Sounds)], self.Pos, 511)
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(self.Pos, 5, 0.1, 2, 4096)
	end
	
	self.Emitter:Finish()
end


--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	
	if CurTime() < self.Lifetime then 
		local frameTime = FrameTime()
		
		-------------
		-- Shockwave
		-------------
		
		--Grow
		local growRate = math.abs(self.Shock.MaxSize - self.Shock.CurSize) * self.Shock.GrowExp
		self.Shock.CurSize = self.Shock.CurSize + (growRate * frameTime)
		
		--Spin
		self.Shock.RotationSpeed = math.Max(self.Shock.RotationSpeed - (self.Shock.RotationDrag * frameTime), self.Shock.RotationMinSpeed)
		self.Shock.Rotation = self.Shock.Rotation + (self.Shock.RotationSpeed * frameTime)
		
		--Fade Out
		if self.Duration > self.Shock.FadeStart then
			local alphaPerSec = self.Shock.StartAlpha / self.Shock.FadeTime
			local alphaStep = alphaPerSec * frameTime
			
			self.Shock.Alpha = math.Clamp(self.Shock.Alpha - alphaStep, 0, 255)
		end
		
		--Increment Effect Timer
		self.Duration = self.Duration + frameTime
		
		return true
	else
		return false	
	end

end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	
	--Shockwave
	if self.Shock.Alpha > 0 then
		render.SetMaterial(self.Shock.Mat)
		self.Shock.Color.a = self.Shock.Alpha
		
		for i = 1, self.Shock.Passes do
			render.DrawQuadEasy( self.Position, self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, self.Shock.Rotation )
			render.DrawQuadEasy( self.Position, -self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, -self.Shock.Rotation )
		end
	end
end

