 local Glow = GAMEMODE.MaterialFromVMT(
	"StaffGlow",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/light_glow01"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)

local Shaft = Material("effects/ar2ground2");

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.size		= data:GetScale()
	self.vel		= data:GetMagnitude()
	self.time		= CurTime()	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	self.EndPos = self.StartPos + (self.Forward * self.vel * (CurTime() - self.time))
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	
	self:SetRenderBoundsWS( self.StartPos, self.StartPos + (self.Forward * -100 * self.size) )
	
	if self.Dead then
		return false
	end
	
	self.time = CurTime()
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	render.SetMaterial(Shaft)
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -500 * self.size),200*self.size,1,0,Color(0,0,255,255))
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -500 * self.size),75*self.size,1,0,Color(100,100,255,255))
	render.SetMaterial(Glow)
	render.DrawSprite(self.StartPos,275*self.size,275*self.size,Color(0,0,255,255))
	render.DrawSprite(self.StartPos,275*self.size/2,275*self.size/2,Color(255,255,255,255)) 
end
