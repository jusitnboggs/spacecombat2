--[[*********************************************
	Capital Ship Main Death Explosion
	
	Author: Steeveeo
**********************************************]]--

local sound_MainExplode = Sound("shipsplosion/sc_explode_04_near.mp3")
local sound_SubExplode = Sound("ambient/explosions/explode_6.wav")
local sound_Thunder = Sound("shipsplosion/sc_preexplode_rumble.mp3")

function EFFECT:Init( data )
	
	--Set up Effect Settings
	self.Position = data:GetOrigin()
	self.Direction = VectorRand() --data:GetNormal()
	self.Lifetime = CurTime() + 8
	self.Duration = 0

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.Dist = LocalPlayer():GetPos():Distance(self.Pos) --Note: I know it seems inefficient to do this more than once, but this is a long sequence, and players tend to move when big things go boom
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat()
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat()
	
	--Shockwave Settings
	self.Shock = {}
	self.Shock.Mat = Material("effects/shipsplosion/shockwave_001")
	self.Shock.Mat:SetInt("$overbrightfactor",1)
	self.Shock.Color = Color(128, 128, 144)
	self.Shock.CurSize = 0
	self.Shock.MaxSize = 12500
	self.Shock.StartAlpha = 196
	self.Shock.Alpha = self.Shock.StartAlpha
	self.Shock.FadeStart = 4
	self.Shock.FadeTime = 4
	self.Shock.Rotation = math.Rand(0, 360)
	self.Shock.RotationSpeed = 10
	self.Shock.RotationDrag = 2
	self.Shock.RotationMinSpeed = 0.75
	self.Shock.GrowExp = 0.65
	self.Shock.Passes = 1
	
	--Fire
	for i=1, 250 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(600, 8500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(5, 8))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(600, 800))
		p:SetEndSize(math.random(800, 1000))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.05, 0.05))
	end
	
	--Fire Highlights
	for i=1, 100 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(600, 8500)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
		p:SetDieTime(math.Rand(5, 8))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(600, 800))
		p:SetEndSize(math.random(800, 1000))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.05, 0.05))
		p:SetColor(96, 96, 255)
	end
	
	--Debris
	for i=1, 150 do
		local velocity = VectorRand() * math.Rand(800, 5000)
		local size = math.random(50, 75)
		
		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(20, 40))
		p:SetVelocity(velocity)
		p:SetAirResistance(100)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(size)
		p:SetEndSize(size)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Flare Small
	for i=1, 10 do
		local velocity = VectorRand() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(5, 7))
		p:SetStartAlpha(255)
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(1350)
		p:SetEndSize(8000)
		p:SetColor(220, 220, 255)
	end
	
	--Flare Rays
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(5, 7))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(8000)
		p:SetEndSize(10000)
		p:SetColor(255, 220, 220)
	end
	
	--Flare Big
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(5000)
		p:SetEndSize(35000)
		p:SetColor(255, 220, 200)
	end
	
	--Flash
	for i=1, 5 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.5, 0.75))
		p:SetStartAlpha(255)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(20000)
		p:SetEndSize(0)
		p:SetColor(220, 220, 255)
	end
	
	--Play Sound
	local dist = LocalPlayer():GetPos():Distance(self.Pos)
	if dist < self.FarDist then
		LocalPlayer():EmitSound(sound_MainExplode, 511, 75, 1)
		LocalPlayer():EmitSound(sound_SubExplode, 511, math.Rand(100, 125), 1)
		LocalPlayer():EmitSound(sound_Thunder, 511, math.Rand(100, 150), 1)
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(LocalPlayer():GetPos(), 16, 0.1, 4, 4096)
	end
	
	self.Emitter:Finish()
end


--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	
	if CurTime() < self.Lifetime then 
		local frameTime = FrameTime()
		
		-------------
		-- Shockwave
		-------------
		
		--Grow
		local growRate = math.abs(self.Shock.MaxSize - self.Shock.CurSize) * self.Shock.GrowExp
		self.Shock.CurSize = self.Shock.CurSize + (growRate * frameTime)
		
		--Spin
		self.Shock.RotationSpeed = math.Max(self.Shock.RotationSpeed - (self.Shock.RotationDrag * frameTime), self.Shock.RotationMinSpeed)
		self.Shock.Rotation = self.Shock.Rotation + (self.Shock.RotationSpeed * frameTime)
		
		--Fade Out
		if self.Duration > self.Shock.FadeStart then
			local alphaPerSec = self.Shock.StartAlpha / self.Shock.FadeTime
			local alphaStep = alphaPerSec * frameTime
			
			self.Shock.Alpha = math.Clamp(self.Shock.Alpha - alphaStep, 0, 255)
		end
		
		--Increment Effect Timer
		self.Duration = self.Duration + frameTime
		
		return true
	else
		return false	
	end

end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	
	--Shockwave
	if self.Shock.Alpha > 0 then
		render.SetMaterial(self.Shock.Mat)
		self.Shock.Color.a = self.Shock.Alpha
		
		for i = 1, self.Shock.Passes do
			render.DrawQuadEasy( self.Position, self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, self.Shock.Rotation )
			render.DrawQuadEasy( self.Position, -self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, -self.Shock.Rotation )
		end
	end
end

