--[[*********************************************
	Big Bertha Hit Effect

	DOOOOOOOOM

	Author: Steeveeo
**********************************************]]--

local sound_MainBoom = Sound("ambient/explosions/explode_6.wav")
local matRefraction = Material("particle/warp4_warp_noz")

local function fireSpinThink(p)
	local roll = p:GetRollDelta()
	roll = roll - ((roll * 0.25) * FrameTime())

	p:SetRollDelta(roll)

	p:SetNextThink(CurTime())
end

function EFFECT:Init(data)
	--Particle Settings
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)

	self.RefractMin = 0
	self.RefractAmount = 0
	self.RefractMax = 6
	self.RefractPinchDuration = 1
	self.RefractEndDuration = 0.1

	self.LifeTime = 30
	self.Duration = 0

	self.NextLightningEmit = CurTime() + 8
	self.MinLightningDelay = 0.1
	self.MaxLightningDelay = 2

	self.RefractTotalDuration = self.RefractPinchDuration + self.RefractEndDuration

	--Flare Small
	for i = 1, 10 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.25, 2))
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(7500 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(200, 255, 200)
	end

	--Flare Long
	for i = 1, 15 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(6, 15))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.Rand(1000, 3500) * self.Scale)
		p:SetEndSize(15000 * self.Scale)
		p:SetColor(200, 255, 200)
	end

	--Big Flare Rays
	for i = 1, 10 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(6, 8))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(45000 * self.Scale)
		p:SetColor(0, 255, 0)
	end

	--Big Flare Core
	for i = 1, 10 do
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.Pos)
		p:SetDieTime(math.Rand(7, 9))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(35000 * self.Scale)
		p:SetColor(220, 255, 220)
	end

	--Plasma Cloud
	for i=1, 75 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(120, 1200) * self.Scale

		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Rand(20, 45))
		p:SetVelocity(velocity)
		p:SetAirResistance(15)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(1000, 1500) * self.Scale)
		p:SetEndSize(math.random(1500, 2000) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		p:SetColor(0, math.random(64, 255), 0)
	end

	--Plasma Fringe
	for i=1, 100 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(1200, 1500) * self.Scale

		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Rand(30, 35))
		p:SetVelocity(velocity)
		p:SetAirResistance(15)
		p:SetStartAlpha(64)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(1000, 1500) * self.Scale)
		p:SetEndSize(math.random(1500, 2000) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-2.5, 2.5))
		p:SetThinkFunction(fireSpinThink)
		p:SetNextThink(CurTime())
		p:SetColor(255, math.random(64, 96), 0)
	end

	--Fire Highlights
	for i=1, 85 do
		math.randomseed(i * FrameTime())
		local velocity = VectorRand() * math.Rand(120, 1500) * self.Scale

		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(30, 35))
		p:SetVelocity(velocity)
		p:SetAirResistance(15)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(2500, 3500) * self.Scale)
		p:SetEndSize(math.random(4500, 6000) * self.Scale)
		p:SetColor(0, 128, 0)
	end

	--Play Sound
	LocalPlayer():EmitSound(sound_MainBoom, 511, math.random(75, 100), 1)
	LocalPlayer():EmitSound(sound_MainBoom, 511, math.random(25, 50), 1)

	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(LocalPlayer():GetPos(), 20, 8, 1, 4096)
		util.ScreenShake(LocalPlayer():GetPos(), 8, 0.01, 8, 4096)
	end
end


--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think()
	self.Duration = self.Duration + FrameTime()

	--Lightning
	if CurTime() > self.NextLightningEmit then
		local roll = math.Rand(0, 360)
		local mat = "effects/shipsplosion/lightning_00" .. math.random(1, 4)
		local pos = self.Pos + VectorRand() * math.Rand(-4500, 4500) * self.Scale
		local size = math.Rand(250, 2500)

		for i = 1, math.random(3, 10) do
			timer.Simple(math.Rand(0.1, 0.5), function()
				if not IsValid(self) then return end

				local p = self.Emitter:Add(mat, pos)
				p:SetDieTime(math.Rand(0.05, 0.15))
				p:SetStartAlpha(255)
				p:SetEndAlpha(255)
				p:SetStartSize(size * self.Scale)
				p:SetEndSize(p:GetStartSize() + math.random(75, 125) * self.Scale)
				p:SetColor(200, 255, 200)
				p:SetRoll(roll)

				local p = self.Emitter:Add("sprites/light_ignorez", pos)
				p:SetDieTime(math.Rand(0.05, 0.15))
				p:SetStartAlpha(64)
				p:SetEndAlpha(0)
				p:SetStartSize((size * 10) * self.Scale)
				p:SetEndSize(p:GetStartSize() + math.random(75, 125) * self.Scale)
				p:SetColor(200, 255, 200)
			end)
		end

		self.NextLightningEmit = CurTime() + math.Rand(self.MinLightningDelay, self.MaxLightningDelay)
	end

	--Refract Map
	if self.Duration < self.RefractPinchDuration then
		self.RefractAmount = (self.RefractMax - self.RefractMin) * (self.Duration / self.RefractPinchDuration)
	else
		self.RefractAmount = math.max(self.RefractAmount - ((self.RefractMax / self.RefractEndDuration) * FrameTime()), 0)
	end

	--Kill
	if self.Duration > self.LifeTime then
		return false
	end

	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render()
	if self.Duration < self.RefractTotalDuration then
		local alpha = 255 - (255 * (self.Duration / self.RefractTotalDuration))

		matRefraction:SetMaterialFloat("$refractamount", self.RefractAmount)
		render.SetMaterial(matRefraction)
		render.UpdateRefractTexture()
		render.DrawSprite(self.Pos, 45000 * self.Scale, 65000 * self.Scale, Color(255, 255, 255, alpha))
	end
end

