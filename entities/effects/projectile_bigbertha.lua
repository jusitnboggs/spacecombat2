 --[[*******************************************
	Scalable Bertha Bolt
	
	Big green blob of doom.
	
	Author: Steeveeo
********************************************]]--

local Glow = Material("sprites/light_glow01")
local Shaft = Material("effects/ar2ground2")

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.Scale		= data:GetScale()
	self.Vel		= data:GetMagnitude()
	self.LastThink		= CurTime()
	
	self.Emitter = ParticleEmitter(self.StartPos)
	
	self.NextFlareEmit = CurTime()
	self.FlareEmitDelay = 0.1
	
	self.NextSparkEmit = CurTime()
	self.SparkEmitDelay = 0.1
	
	self.NextBlatEmit = CurTime()
	self.BlatEmitDelay = 0.1
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	self.EndPos = self.StartPos + (self.Forward * self.Vel * (CurTime() - self.LastThink))
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	
	self:SetRenderBoundsWS(self.StartPos, self.StartPos + (self.Forward * -100 * self.Scale))
	
	if self.Dead then
		self.Emitter:Finish()
		return false
	end
	
	--Core Flicker Flares
	if CurTime() > self.NextFlareEmit then
		for i = 1, 2 do
			local p = self.Emitter:Add("effects/flares/light-rays_001", self.StartPos)
			p:SetDieTime(math.Rand(0.15, 1.5))
			p:SetVelocity(self.Forward * self.Vel)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.Rand(125, 300) * self.Scale)
			p:SetColor(200, 255, 200)
		end
		
		self.NextFlareEmit = CurTime() + self.FlareEmitDelay
	end
	
	--Trailing Sparks
	if CurTime() > self.NextSparkEmit then
		for i = 1, 5 do
			local p = self.Emitter:Add("effects/shipsplosion/sparks_00" .. math.random(1,4), self.StartPos)
			p:SetDieTime(math.Rand(2, 6))
			p:SetVelocity(-self.Forward + (VectorRand() * 0.1))
			p:SetStartAlpha(64)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(75, 100) * self.Scale)
			p:SetEndSize(math.random(5, 15) * self.Scale)
			p:SetStartLength(0)
			p:SetEndLength(math.random(150, 300) * self.Scale)
			p:SetColor(0, math.random(128, 255), 0)
		end
		
		self.NextSparkEmit = CurTime() + self.SparkEmitDelay
	end
	
	--Trailing Particulates
	if CurTime() > self.NextBlatEmit then
		for i = 1, 5 do
			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.StartPos)
			p:SetDieTime(math.Rand(2, 4))
			p:SetVelocity(self.Forward * (self.Vel * math.Rand(0.25, 0.8)))
			p:SetStartAlpha(64)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(50, 75) * self.Scale)
			p:SetEndSize(math.random(5, 15) * self.Scale)
			p:SetColor(0, math.random(128, 255), 0)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
		end
		
		self.NextBlatEmit = CurTime() + self.BlatEmitDelay
	end
	
	self.LastThink = CurTime()
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	render.SetMaterial(Shaft)
	render.DrawBeam(self.StartPos, self.StartPos + (self.Forward * -150 * self.Scale), 75 * self.Scale, 1, 0, Color(0, 255, 0, 255))		--Edge
	render.DrawBeam(self.StartPos, self.StartPos + (self.Forward * -150 * self.Scale), 50 * self.Scale, 1, 0, Color(100, 255, 100, 255))	--Core
	
	render.SetMaterial(Glow)
	render.DrawSprite(self.StartPos, 275 * self.Scale, 275 * self.Scale, Color(0, 255, 0, 255))				--Edge
	render.DrawSprite(self.StartPos, 275 * self.Scale / 2, 275 * self.Scale / 2, Color(255, 255, 255, 255))	--Core
end
