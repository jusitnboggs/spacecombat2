--[[*********************************************
	Scalable, Colorable Pulse Beam Effect
			(Fighter Edition)
	
	Author: Steeveeo
**********************************************]]--

local bMats = {}

bMats.Glow23 = GAMEMODE.MaterialFromVMT(
"bluelaser",
[["UnlitTwoTexture"

{
	"$baseTexture"  "effects/bluelaser1"
	"$Texture2" "effects/bluelaser1_smoke"
	"$nocull" "1"	
	"$nodecal" "1"
	"$additive" "1"
	"$no_fullbright" 1
	"$model" "1"
	"$nofog" "1"


	"Proxies"
	{
		"TextureScroll"
		{
			"texturescrollvar" "$texture2transform"
			"texturescrollrate" 1
			"texturescrollangle" 0
		}

		"Sine"
		{
			"sineperiod"	.06
			"sinemin"	".3"
			"sinemax"	".5"
			"resultVar"	"$alpha"
		}
	}
}]]
)
bMats.Glow24 = Material("effects/blueflare1")
bMats.Glow24:SetMaterialInt("$selfillum", 10)
bMats.Glow24:SetMaterialInt("$illumfactor",8)
local lMats = {}
lMats.Glow13 = Material("effects/blueblacklargebeam")
lMats.Glow13:SetMaterialInt("$vertexalpha", 1)
lMats.Glow13:SetMaterialInt("$vertexcolor", 1)
lMats.Glow13:SetMaterialInt("$additive", 1)
lMats.Glow13:SetMaterialInt("$nocull", 1)
lMats.Glow13:SetMaterialInt("$selfillum", 10)
lMats.Glow13:SetMaterialInt("$illumfactor",8)
lMats.Glow23 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam02",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueblacklargebeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
		"$illumfactor" 8
	}]]
)
--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )

	self.StartPos 	= data:GetStart()
	self.EndPos		= data:GetOrigin()
	self.rad 		= 16
	self.Scale 		= 0.5 --data:GetScale() --No idea how to get Scale out of a bullet effect...
	self.ColLerp	= 255 --data:GetMagnitude() --Replace me with colorwheel!
	self.col 		= Color(0, 16, 255) --Color(Lerp(self.ColLerp, 0, 255), Lerp(self.ColLerp, 255, 0), Lerp(self.ColLerp, 0, 255), 255)
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	self.Scale = self.Scale - ((self.Scale * 15) * FrameTime())
	--self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
	timer.Simple(1, function() 
		self.Dead = true
	end)
	
	if self.Dead then
		return false
	end
	
	return true
end


--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local size = math.random(200,400) * self.Scale
   	local spriteStartOffset = (self.StartPos - self.EndPos):Angle():Forward() * (25 * self.Scale)
   	local spriteEndOffset = (self.StartPos - self.EndPos):Angle():Forward() * (-25 * self.Scale)
   	
	render.SetMaterial( bMats.Glow24 )
   	
   	render.DrawSprite(self.StartPos + spriteStartOffset, size*2 , size*2, self.col) 
   	render.DrawSprite(self.StartPos + spriteStartOffset, size, size , Color(255, 255, 255, 255))	
   	render.DrawSprite(self.EndPos + spriteEndOffset, size*2 , size*2, self.col) 
   	render.DrawSprite(self.EndPos + spriteEndOffset, size, size , Color(255, 255, 255, 255))	
	
	render.SetMaterial( bMats.Glow23 )	
   	render.DrawBeam( self.EndPos, self.StartPos, (100 + (15 * math.sin(CurTime() * (25 + math.Rand(1, 15))))) * self.Scale, 110, 0, Color( 0, 255, 0, 200 ) )
  
	render.SetMaterial( bMats.Glow24 )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.col )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.col )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.col )
   	render.DrawBeam( self.EndPos, self.StartPos, (50 + (25 * math.sin(CurTime() * 45))) * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, (50 + (25 * math.cos(CurTime() * 65))) * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, 50 * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
   	render.DrawBeam( self.EndPos, self.StartPos, 50 * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
				 
end
