--[[*********************************************
	Small Fighter Beam Impact Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	self.EmitterNorm = ParticleEmitter(self.Pos, true)
	
	--Energy Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	for i=1, 10 do
		ang:RotateAroundAxis(self.Normal, 36)
		local velocity = ang:Forward() * math.Rand(50, 75) + (self.Normal * 10)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Rand(0.3, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartLength(math.random(25, 45))
		p:SetEndLength(math.random(25, 45))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(25, 45))
		p:SetEndSize(math.random(25, 45))
		p:SetColor(math.random(144,196), math.random(144,196), 255)
	end
	
	--Normal Splash
	for i=1, 2 do
		local splash = self.EmitterNorm:Add("effects/shipsplosion/smoke_003add", self.Pos + self.Normal * 2)
		splash:SetDieTime(0.35)
		splash:SetStartSize(35)
		splash:SetEndSize(0)
		splash:SetStartAlpha(255)
		splash:SetEndAlpha(0)
		splash:SetColor(math.random(144,255), math.random(144,255), 255)
		splash:SetRoll(math.random(0, 360))
		splash:SetAngles(self.Normal:Angle())
	end
	
	self.Emitter:Finish()
	self.EmitterNorm:Finish()
end

function EFFECT:Think( )
  	return false
end

function EFFECT:Render( )
	return false
end
