/**********************************************
	Scalable Railgun Projectile Effect
	
	Author: Steeveeo
***********************************************/

local trailMat = Material("effects/ar2ground2")
local spriteFlareMat = Material("effects/flares/halo-flare_001")
local spriteCoreMat = Material("sprites/light_glow03")

function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.Scale		= data:GetScale()
	self.Velocity	= data:GetMagnitude()
	self.Time		= CurTime()
	self.LifeTime	= 0
	self.Model		= ClientsideModel("models/slyfo_2/pss_thrustprojectileclosed.mdl", 7)
	
	self.Trails = {}
	self.Trails[1] = Vector(-95, 0, 100)
	self.Trails[2] = Vector(-95, 0, -100)
	
	self.TrailLength = 3500 * self.Scale
	self.CurTrailLength = 0
	self.CurRoll = 0
	
	self.Emitter = ParticleEmitter(self.StartPos)
	
	local mat = Matrix()
	mat:Scale(Vector(22, 17, 17) * self.Scale)
	self.Model:EnableMatrix("RenderMultiply", mat)
end

function EFFECT:Think()
	self.EndPos = self.StartPos + (self.Forward * self.Velocity * (FrameTime()))
	
	--Grow trail (so it doesn't shoot out the back of the gun)
	self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)

	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit and tr.Entity ~= self.Model then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	self.Model:SetPos(self.StartPos)
	self.Model:SetAngles(self.Forward:Angle())
	self:SetRenderBoundsWS(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength))
	
	--Spiiiin
	self.CurRoll = self.CurRoll + (1080 * FrameTime())
	self.Model:SetAngles(self.Model:LocalToWorldAngles(Angle(0, 0, self.CurRoll)))
	
	if self.Dead then
		self.Model:Remove()
		self.Emitter:Finish()
		return false
	end
	
	--Spinny Particle Trail
	for i = 1, 2 do
		local trail = self.Trails[i]
		
		for ii = 1, 5 do
			local velocity = self.Forward * (self.Velocity * 0.25) + (VectorRand() * math.Rand(10, 20))
			
			local p = self.Emitter:Add("effects/flares/light-rays_001", self.Model:LocalToWorld(trail))
			p:SetDieTime(math.Rand(1.5, 2.5))
			p:SetVelocity(velocity)
			p:SetAirResistance(300)
			p:SetStartAlpha(255)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(15, 50) * self.Scale)
			p:SetEndSize(0)
			p:SetColor(200, 200, 255)
		end
	end
	
	--Fire
	for i = 1, 5 do
		local velocity = self.Forward * (self.Velocity * math.Rand(0, 0.75)) + (VectorRand() * math.Rand(300, 500))
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.StartPos - (self.Forward * 120))
		p:SetDieTime(math.Rand(0.25, 0.5))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(25, 100) * self.Scale)
		p:SetEndSize(0)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(-30)
		p:SetColor(255, math.random(96, 196), math.random(32, 96))
	end
	
	self.Time = CurTime()
	
	return true
end

function EFFECT:Render()
	render.SetMaterial(trailMat)
	render.DrawBeam(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength), 10 * self.Scale, 1, 0, Color(144, 196, 255, 255))
	
	render.SetMaterial(spriteFlareMat)
	render.DrawSprite(self.StartPos - (self.Forward * 120), self.Scale * 1000, self.Scale * 1000, Color(255, 240, 128))
	
	render.SetMaterial(spriteCoreMat)
	render.DrawSprite(self.StartPos - (self.Forward * 120), self.Scale * 350, self.Scale * 350, Color(255, 255, 255, 255))
	
	return true
end
