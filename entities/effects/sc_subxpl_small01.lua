--[[*********************************************
	Small Hull Explosion Effect
	
	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )
util.PrecacheSound( "ambient/explosions/explode_8.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_4.wav",
		"ambient/explosions/explode_8.wav"
	}

	self.Normal = data:GetNormal()
	self.Parent = data:GetEntity()
	
	if not IsValid(self.Parent) then return false end
	
	self.Pos = self.Parent:WorldToLocal(data:GetOrigin())
	self.Emitter = ParticleEmitter(self.Parent:LocalToWorld(self.Pos))
	
	--Fire Quick
	for i=1, 20 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.1, 0.5))
		p:SetVelocity(velocity)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(150, 175))
		p:SetEndSize(math.random(175, 200))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Flares Quick
	for i=1, 10 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.1, 0.5))
		p:SetVelocity(velocity)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(250, 275))
		p:SetEndSize(math.random(275, 300))
		p:SetColor(255, 220, 200)
	end
	
	--Fire Main
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(50, 500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.5, 2))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(50, 112))
		p:SetEndSize(math.random(125, 200))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Flare
	for i=1, 5 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(300, 2000)
		
		local p = self.Emitter:Add("sprites/light_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.1, 0.2))
		p:SetStartAlpha(255)
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetEndAlpha(0)
		p:SetStartSize(1000)
		p:SetEndSize(1000)
		p:SetColor(255, 220, 200)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play(self.Sounds[math.random(1,#self.Sounds)], self.Parent:LocalToWorld(self.Pos))
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		--util.ScreenShake(self.Pos, 2, 0.1, 1, 2048)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
