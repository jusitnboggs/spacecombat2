AddCSLuaFile()

local ACFEngines = {}
local ACFGearboxes = {}

hook.Add("SC.RegisterEngines", "SC.RegisterACFEngines", function()
    for ID, ACFData in pairs(ACFEngines) do
        if ACFData.iselec then
            local LowTorqueRPM, HighTorqueRPM, PeakTorqueRPM, RedlineRPM

            local LimitRPM = ACFData.limitrpm or 6000
            local ACFTorque = ACFData.torque or 345
            local IdleRPM = ACFData.idlerpm or 10

            if ACFData.peakminrpm == ACFData.peakmaxrpm then
                LowTorqueRPM = IdleRPM + math.max(50, IdleRPM * 0.1)
                HighTorqueRPM = LimitRPM - math.max(150, LimitRPM * 0.1)
            else
                LowTorqueRPM = ACFData.peakminrpm or 1400
                HighTorqueRPM = ACFData.peakmaxrpm or 4400
            end

            PeakTorqueRPM = LowTorqueRPM + ((HighTorqueRPM - LowTorqueRPM) * 0.5)
            RedlineRPM = HighTorqueRPM + ((LimitRPM - HighTorqueRPM) * 0.5)

            local Data = {
                Name = ACFData.name or "SC Engine",
                Description = ACFData.desc or "Default engine, for testing.",
                Model = ACFData.model or "models/engines/inline6m.mdl",
                Category = string.format("ACF/%s", ACFData.category or "Generic"),
                IgnitionSound = "",
                RunningSound = ACFData.sound or "vehicles/airboat/fan_motor_fullthrottle_loop1.wav",
                RedlineRPM = RedlineRPM,
                IdleRPM = IdleRPM,
                LimitRPM = LimitRPM,
                EngineDrag = 3.0,
                IdleTorque = ACFTorque * 0.464,
                LowTorque = ACFTorque * 0.96,
                LowTorqueRPM = LowTorqueRPM,
                PeakTorque = ACFTorque,
                PeakTorqueRPM = PeakTorqueRPM,
                HighTorque = ACFTorque * 0.97,
                HighTorqueRPM = HighTorqueRPM,
                RedlineTorque = ACFTorque * 0.9,
                FlywheelWeight = ACFData.flywheelmass or 2.6,
                FlywheelRadius = 8,
                ThrottleSpeed = 5,
            }

            GAMEMODE.Mechanical.RegisterEngine(ID, Data)
        else
            local LowTorqueRPM = ACFData.peakminrpm or 1400
            local HighTorqueRPM = ACFData.peakmaxrpm or 4400
            local PeakTorqueRPM = LowTorqueRPM + ((HighTorqueRPM - LowTorqueRPM) * 0.5)
            local LimitRPM = ACFData.limitrpm or 6000
            local RedlineRPM = HighTorqueRPM + ((LimitRPM - HighTorqueRPM) * 0.5)
            local ACFTorque = ACFData.torque or 345

            local Data = {
                Name = ACFData.name or "SC Engine",
                Description = ACFData.desc or "Default engine, for testing.",
                Model = ACFData.model or "models/engines/inline6m.mdl",
                Category = string.format("ACF/%s", ACFData.category or "Generic"),
                IgnitionSound = "",
                RunningSound = ACFData.sound or "vehicles/airboat/fan_motor_fullthrottle_loop1.wav",
                RedlineRPM = RedlineRPM,
                IdleRPM = ACFData.idlerpm or 700,
                LimitRPM = LimitRPM,
                EngineDrag = 3.0,
                IdleTorque = ACFTorque * 0.464,
                LowTorque = ACFTorque * 0.826,
                LowTorqueRPM = LowTorqueRPM,
                PeakTorque = ACFTorque,
                PeakTorqueRPM = PeakTorqueRPM,
                HighTorque = ACFTorque * 0.956,
                HighTorqueRPM = HighTorqueRPM,
                RedlineTorque = ACFTorque * 0.608,
                FlywheelWeight = ACFData.flywheelmass or 2.6,
                FlywheelRadius = 8,
                ThrottleSpeed = 5,
            }

            GAMEMODE.Mechanical.RegisterEngine(ID, Data)
        end
    end
end)

local DefaultGeartable = {
    [-1] = -0.18,
    [0] = 0.0,
    [1] = 0.11,
    [2] = 0.18,
    [3] = 0.24,
    [4] = 0.38,
    [5] = 0.55,
    [6] = 0.77,
    [7] = 1.00,
}

local function ConvertACFGeartable(NumGears, ACFGeartable)
    -- Converting this is going to screw over anyone that sets their
    -- gear manually with wire, but even if i just left the gear table
    -- alone it had problems anyways.
    local Gears
    if NumGears > 1 then
        if ACFGeartable then
            Gears = {[0] = 0}
            local NextReverseGear = -1
            local NextForwardGear = 1
            for Gear, Ratio in SortedPairs(ACFGeartable) do
                -- As far as I can tell ACF ignores gears below 0,
                -- but many gearboxes define gears below 0 for special functionality
                if Gear >= 0 then
                    if Ratio > 0 then
                        Gears[NextForwardGear] = Ratio
                        NextForwardGear = NextForwardGear + 1
                    elseif Ratio < 0 then
                        Gears[NextReverseGear] = Ratio
                        NextReverseGear = NextReverseGear - 1
                    end
                end
            end
        else
            Gears = table.Copy(DefaultGeartable)
        end
    else
        if ACFGeartable then
            Gears = {
                [0] = ACFGeartable[1]
            }
        else
            Gears = {
                [0] = 0.5
            }
        end
    end

    return Gears
end

local function IsValidACFGearbox(ACFGearbox)
    if not istable(ACFGearbox) then
        return false, false
    end

    -- ACF has a lot of duplicate gearboxes that we don't need so let's
    -- try to hide the ones that we can make with just a checkbox.
    local ShouldHide = false

    -- Double Clutch can be done with a checkbox in the spawner tool
    -- They still need to be valid for duplicator support though
    if ACFGearbox.doubleclutch then
        ShouldHide = true
    end

    -- Double Diff can be done with a checkbox in the spawner tool
    -- They still need to be valid for duplicator support though
    if ACFGearbox.doublediff then
        ShouldHide = true
    end

    return true, ShouldHide
end

hook.Add("SC.RegisterGearboxes", "SC.RegisterACFGearboxes", function()
    for ID, ACFData in pairs(ACFGearboxes) do
        local ValidGearbox, Hidden = IsValidACFGearbox(ACFData)
        if ValidGearbox then
            local NumGears = ACFData.gears or 1
            local Gears = ConvertACFGeartable(NumGears, ACFData.geartable)

            -- Convert special gears from ACF if they exist
            local FinalDrive
            if ACFData.geartable and ACFData.geartable[-1] then
                FinalDrive = ACFData.geartable[-1]
            else
                FinalDrive = 1.00
            end

            local Data = {
                Name = ACFData.name or "SC Gearbox",
                Description = ACFData.desc or "Default gearbox, for testing.",
                Category = string.format("ACF/%s", ACFData.category or "Generic"),
                Hidden = Hidden,
                Model = ACFData.model or "models/engines/inline6m.mdl",
                ShiftTime = ACFData.switch or 0.2,
                MaxTorque = ACFData.maxtq or 200,
                InEfficiency = 0.0001,
                FinalDrive = FinalDrive,
                GearRatios = Gears,
            }

            GAMEMODE.Mechanical.RegisterGearbox(ID, Data)
        end
    end
end)

function MakeACF_Engine(Owner, Pos, Angle, Id)
	local Engine = ents.Create("sc_engine")
    if not IsValid(Engine) then return false end

    local PostEntityPaste = Engine.PostEntityPaste
    Engine.PostEntityPaste = function(self, Player, Entity, CreatedEntities)
        if Entity.EntityMods then
            local DupeInfo = {
                EngineType = Id,
                DriveLineComponents = {}
            }

            local ACF2Links = Entity.EntityMods.GearLink
            if ACF2Links then
                for _, ID in pairs(ACF2Links.entities) do
                    table.insert(DupeInfo.DriveLineComponents, ID)
                end
            end

            local ACF3Links = Entity.EntityMods.ACFGearboxes
            if ACF3Links then
                for _, ID in pairs(ACF3Links) do
                    table.insert(DupeInfo.DriveLineComponents, ID)
                end
            end

            Entity.EntityMods.SC2DupeInfo = DupeInfo
            Entity.EntityMods.GearLink = nil
        end

        if PostEntityPaste then
            PostEntityPaste(self, Player, Entity, CreatedEntities)
        end
    end

	Engine:SetAngles(Angle)
    Engine:SetPos(Pos)
    Engine:Spawn()
    Engine:Activate()
	Engine:SetPlayer(Owner)
	Engine.Owner = Owner

	return Engine
end
duplicator.RegisterEntityClass("acf_engine", MakeACF_Engine, "Pos", "Angle", "Id")

function MakeACF_Gearbox(Owner, Pos, Angle, Id, Data1, Data2, Data3, Data4, Data5, Data6, Data7, Data8, Data9, Data10)
	local Gearbox = ents.Create("sc_gearbox")
    if not IsValid(Gearbox) then return false end

    local PostEntityPaste = Gearbox.PostEntityPaste
    Gearbox.PostEntityPaste = function(self, Player, Entity, CreatedEntities)
        if Entity.EntityMods then
            local ACFData = ACFGearboxes[Id] or {}
            local NumGears = ACFData.gears or 1
            local Gears = ConvertACFGeartable(NumGears, ACFData.geartable)

            local DupeInfo = {
                GearboxType = Id,
                FinalDrive = Data10 or 1.00,
                GearRatios = Gears,
                DriveLineComponents = {},
                Wheels = {},
                HasDualDiff = ACFData.dualdiff,
                HasDualClutch = ACFData.doubleclutch
            }

            local ACF2Links = Entity.EntityMods.WheelLink
            if ACF2Links then
                for _, ID in pairs(ACF2Links.entities) do
                    local Ent = CreatedEntities[ID]
                    if IsValid(Ent) then
                        if Ent.IsDriveLineComponent then
                            table.insert(DupeInfo.DriveLineComponents, ID)
                        else
                            table.insert(DupeInfo.Wheels, ID)
                        end
                    end
                end
            end

            local ACF3Wheels = Entity.EntityMods.ACFWheels
            if ACF3Wheels then
                for _, ID in pairs(ACF3Wheels) do
                    local Ent = CreatedEntities[ID]
                    if IsValid(Ent) then
                        table.insert(DupeInfo.Wheels, ID)
                    end
                end
            end

            local ACF3Gearboxes = Entity.EntityMods.ACFGearboxes
            if ACF3Gearboxes then
                for _, ID in pairs(ACF3Gearboxes) do
                    local Ent = CreatedEntities[ID]
                    if IsValid(Ent) then
                        table.insert(DupeInfo.DriveLineComponents, ID)
                    end
                end
            end

            Entity.EntityMods.SC2DupeInfo = DupeInfo
            Entity.EntityMods.WheelLink = nil
            Entity.EntityMods.ACFWheels = nil
            Entity.EntityMods.ACFGearboxes = nil
        end

        if PostEntityPaste then
            PostEntityPaste(self, Player, Entity, CreatedEntities)
        end
    end

	Gearbox:SetAngles(Angle)
    Gearbox:SetPos(Pos)
    Gearbox:Spawn()
    Gearbox:Activate()
	Gearbox:SetPlayer(Owner)
    Gearbox.Owner = Owner

	return Gearbox
end
duplicator.RegisterEntityClass("acf_gearbox", MakeACF_Gearbox, "Pos", "Angle", "Id", "Gear1", "Gear2", "Gear3", "Gear4", "Gear5", "Gear6", "Gear7", "Gear8", "Gear9", "Gear0")

function ACF_DefineEngine(id, data)
	data.id = id
	ACFEngines[id] = data
end

function ACF_DefineGearbox(id, data)
	data.id = id
	ACFGearboxes[id] = data
end

local engines = file.Find("acf/shared/engines/*.lua", "LUA")
for k, v in pairs(engines) do
	AddCSLuaFile("acf/shared/engines/"..v)
	include("acf/shared/engines/"..v)
end

local gearboxes = file.Find("acf/shared/gearboxes/*.lua", "LUA")
for k, v in pairs(gearboxes) do
	AddCSLuaFile("acf/shared/gearboxes/"..v)
	include("acf/shared/gearboxes/"..v)
end