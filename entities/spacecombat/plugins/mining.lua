--[[
		SPACE COMBAT 2 - MINING PLUGIN

	Controls the spawning and generation of mining
	features, such is asteroid belts and mineral
	fields.

	Generation is Feature based, wherein all mineables
	spawn based on set rules, either randomly generated
	or defined by config files (see below).
--]]

--[[ Currently Implemented Features and Settings

	ASTEROIDS

	-- Simple Cloud: cuboid random spawn from Pos, +- X/Y/ZRandom
	-- Can ignore atmospheres to spawn roids in planets

	[Simple Cloud Name]
	Type=Simple Cloud
	Pos=x,y,z
	MaxCount=##
	XRandom=####
	YRandom=####
	ZRandom=####
	Ignore Atmospheres=BOOL


	-- Asteroid Belt: Point-to-Point Line Variance
	-- Variance is max distance from the line (Pos1->Pos2)

	[Asteroid Belt Name]
	Type=Belt
	Pos1=startX,startY,startZ
	Pos2=endX,endY,endZ
	RoidCount=##
	Variance=####
	Ignore Atmospheres=BOOL


	-- Asteroid Ring: Tilted Axial Disk, Min and Max Radius
	-- A bit more involved;
			- Radius: maximum radius from centerpoint
			- InnerRadius: minimum distance from center before being able to spawn
			- Inclination: how much the ring is tilted
			- Azimuth: rotation of the tilt
			- Thickness: "vertical" variance off of the plane of the ring

	[Ring Name]
	Type=Ring
	Pos=x,y,z
	Radius=####
	InnerRadius=####
	Inclination=###
	Azimuth=###
	Thickness=###
	RoidCount=##
	Ignore Atmospheres=BOOL




	PN CRYSTALS


	-- Standard Crystal Field: Parent crystal spawns child crystals around it using conic-random tracing. It will attempt to spawn as close as possible before moving outwards.
	-- Unless Normal is defined, the spawner will trace directly down from Pos in order to spawn the node.
	-- MaxTraceAngle is how wide the conic spawn traces can spread (can be used to spawn minerals on ceilings).
	-- MaxDistance is how far away from the parent a crystal child can spawn.
	-- ScanHeight is how high up the conic tracing begins. Too low and you get "shadows" where no crystals spawn behind hills; too high and you lose grouping. Might take tuning to get right.
	--		NOTE: When spawning, a trace is cast to ScanHeight from parent to make sure cave crystals don't spawn inside the world.
	-- RespawnRate is how actively in seconds this field initially tries to respawn mined crystals. Overrides convar.
	-- FieldHealth is a numeric value on how much mining can be done before the field "dies" and starts to collapse.
	-- MinInitialSpawn is how many "child" crystals to spawn around the "parent" during inital spawning sequence.
	-- CrystalType is the numeric index of what color the crystals will be:
			-- 0: Green
			-- 1: Blue
			-- 2: Red
	-- In this instance, Ignore Atmospheres removes the edge-check that keeps minerals from spawning outside planets.

	[Field Name]
	Type=Crystal Field
	Pos=x,y,z
	Normal=x,y,z
	MaxCount=##
	MaxTraceAngle=###
	MaxDistance=####
	ScanHeight=####
	RespawnRate=###
	FieldHealth=######
	MinInitialSpawn=##
	CrystalType=#
	Ignore Atmospheres=BOOL



	--==PLANNED FEATURES==--


	PN CRYSTALS


	-- Sparce Field: Like the above, but doesn't try to spawn close to the parent crystal, instead opting to randomly disperse across the field

	[Sparce Field Name]
	Type=Sparce Crystal Field
	Pos=x,y,z
	Normal=x,y,z
	MaxCount=##
	MaxTraceAngle=###
	MaxDistance=####
	ScanHeight=####
	RespawnRate=###
	FieldHealth=######
	CrystalType=#
	Ignore Atmospheres=BOOL


	-- Fissure Field: A "crack" from which crystals spawn, basically an elongated circle with "tails" pointed in either direction.
	--		TODO: Figure out the math so we can add "curls" to the fissure.
	-- MaxDistance, in this case, is how far the "tails" can spread.
	-- MaxVariance is how much the field will spread laterally at its densest point (the center).

	[Fissure Name]
	Type=Crystal Fissure
	Pos=x,y,z
	Normal=x,y,z
	MaxCount=##
	MaxDistance=####
	ScanHeight=####
	MaxVariance=###
	RespawnRate=###
	FieldHealth=######
	CrystalType=#
	Ignore Atmospheres=BOOL



	MINERAL PATCHES
	NOTE: Nothing uses this idea yet. However, this is basically a way to make Drill Rigs less of just "dig wherever for stuff"
	and more of a hunt. Mining scanners should be changed to detect nearby resources (I have an idea for a Firefall-like "ping").


	-- Mineral Vein: Normally, drill rigs will dig up random stuff from the ground. If it's near to a mineral vein, though, it will start to pull from that as well, to a much better extent.
	-- NOTE: When defining manually, it would be a good idea to put these on the surface so the drills can actually get to them.
	-- NOTE: Currently, I only plan for resource veins to be of one type each. One can add more resources by spawning more patches.

	-- ResourceName is a named resource type. Must be a defined resource!
	-- ResourceAmount is how much of that resource can be mined before the field is fully mined out.
	-- MaxDistance defines how far this vein reaches. Of course, being closer to the center is better.
	-- DrillRate is the amount that can be pulled from the field at a time by a 1-multiplier drill rig. Use to make fields harder to mine.

	[Mineral Vein Name]
	Type=Mineral Vein
	Pos=x,y,z
	ResourceName=Name
	ResourceAmount=######
	MaxDistance=####
	DrillRate=####



	ORE DEPOSITS
	Mineable rocks. Like asteroids, but on planets. Main purpose is to be mined with personal miners.


	-- Ore Field: A collection of mineable rocks. Like crystals, uses traces to spawn on displacements. However, this spawner will offset from Feature origin and trace down, rather than conic.

	[Ore Field Name]
	Type=Rock Field
	Pos=x,y,z
	MaxCount=###
	XRandom=####
	YRandom=####


	-- Ore Belt: Like an asteroid belt, but for rocks on terrain.

	[Ore Belt Name]
	Type=Rock Belt
	Pos1=startX,startY,startZ
	Pos2=endX,endY,endZ
	MaxCount=###
	Variance=####
--]]



require("lip")

--ConVar Settings
local cvar_asteroidRespawn = CreateConVar("mining_asteroidRespawnEnable", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "Toggle respawning of asteroids for limited or unlimited resources.")
local cvar_asteroidRespawnDelay = CreateConVar("mining_asteroidRespawnDelay", 120, {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "How long in seconds that an asteroid feature will take to respawn one asteroid.")

local cvar_crystalFrenzyDelay = CreateConVar("mining_crystalFrenzyDelay", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "Delay between Crystal spawns while Feature is in \"Frenzy Mode.\" 0 means as fast as possible.")
local cvar_crystalFrenzyTimeout = CreateConVar("mining_crystalFrenzyTimeout", 3, {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "Maximum amount of time a Crystal Field in \"Frenzy Mode\" can fail to spawn before settling down.")
local cvar_crystalRespawnDelay = CreateConVar("mining_crystalRespawnDelay", 120, {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "Default delay between Crystal respawns.")

--Plugin Stuff
local GM = GM
GM.Mining = {}

--REMOVEME: For dev reloading, the feature tables seem to go a bit bunk, the following is just to not double up on stuff:
for k,v in pairs(ents.FindByClass("mining_mineral")) do v:Remove() end
for k,v in pairs(ents.FindByClass("mining_mineral_parent")) do v:Remove() end
for k,v in pairs(ents.FindByClass("mining_asteroid")) do v:Remove() end


--Initializers for Plugin
function GM.Mining.Enable()
	--If reinitializing, clean up first
	if GM.Mining.Enabled then GM.Mining.Disable() end

	ErrorNoHalt("[SC2 MINING] - Plugin Enabled.\n")

	----Create base folders
	file.CreateDir(SC.DataFolder.."/mining/")
	file.CreateDir(SC.DataFolder.."/mining/mapfeatures")
	file.CreateDir(SC.DataFolder.."/mining/asteroids")
	file.CreateDir(SC.DataFolder.."/mining/crystals")

	--Initialize Tables
	GM.Mining.Features = {}
	GM.Mining.Environments = {}

	GM.Mining.MineablesSource = file.Find("spacecombat/plugins/mining/mineables/*.lua", "LUA")
	GM.Mining.Mineables = {}
	GM.Mining.SpawnersSource = file.Find("spacecombat/plugins/mining/spawners/*.lua", "LUA")
	GM.Mining.Spawners = {}

	--Load Mineables
	for _,file in pairs(GM.Mining.MineablesSource) do
		include("spacecombat/plugins/mining/mineables/"..file)
	end

	--Initialize Mineables
	for _,mineable in pairs(GM.Mining.Mineables) do
		mineable:Setup()
	end

	--Load Spawners
	for _,file in pairs(GM.Mining.SpawnersSource) do
		include("spacecombat/plugins/mining/spawners/"..file)
	end

	--Initialize Spawners
	for _,spawner in pairs(GM.Mining.Spawners) do
		spawner:Setup()
	end

	GM.Mining.LoadFeatures()


	hook.Add("Think", "Mining Plugin Think", GM.Mining.Think)
	GM.Mining.Enabled = true
end
hook.Remove("SC.PostEnvironmentsCreated", "SC.LoadMining")
hook.Add("SC.PostEnvironmentsCreated", "SC.LoadMining", GM.Mining.Enable)

function GM.Mining.Disable()
	--Clean up existing stuff
	--** CALL EACH MINEABLE'S 'CLEANUP' FUNCTION HERE **--

	hook.Remove("Think", "Mining Plugin Think")
	GM.Mining.Enabled = false

	ErrorNoHalt("[SC2 MINING] - Plugin Disabled.\n")
end


--Supplemental Controller Registrations
function GM.Mining.RegisterMineable(name, mineable)
	GM.Mining.Mineables[name] = mineable

	ErrorNoHalt("[SC2 MINING] - Registered Mineable Type "..name..".\n")
end

function GM.Mining.UnregisterMineable(name) --I guess if you want to remove asteroids from the game for a session?
	GM.Mining.Mineables[name]:Cleanup()

	GM.Mining.Mineables[name] = nil

	ErrorNoHalt("[SC2 MINING] - Unregistered Mineable Type "..name..".\n")
end

function GM.Mining.RegisterSpawner(name, spawner)
	GM.Mining.Spawners[name] = spawner

	ErrorNoHalt("[SC2 MINING] - Registered Spawner Type "..name..".\n")
end

function GM.Mining.UnregisterSpawner(name)
	GM.Mining.Spawners[name] = nil

	--TODO: Cleanup all features using this spawner

	ErrorNoHalt("[SC2 MINING] - Unregistered Spawner Type "..name..".\n")
end


--Feature Loading
function GM.Mining.LoadFeatures()
	local mapName = game.GetMap()
	mapName = string.Replace(mapName, ".bsp", "")
	local fileName = SC.DataFolder.."/mining/mapfeatures/"..mapName..".txt"
	if file.Exists(fileName, "DATA") then
		ErrorNoHalt("[SC2 MINING] - Found Feature file for "..mapName..".\n")

		local INI = lip.load(fileName)

		--Get all features from file
		for node, data in pairs(INI) do
			local name = node

			if name == "Map Settings" then
				--TODO: Global settings, like auto-generation disables and such go here
			else
				local type = data.Type

				if GM.Mining.Spawners[type] then
					local feature = GM.Mining.Spawners[type]:LoadFromFile(name, data)

					if feature then
						GM.Mining.Spawners[type]:Initialize(feature)
						GM.Mining.Features[name] = feature
					else
						ErrorNoHalt("[SC2 MINING] - ERROR: Could not load Feature "..name.."! Feature culled!\n")
					end
				else
					ErrorNoHalt("[SC2 MINING] - WARNING: Could not find Spawner type "..type.." for Feature "..name.."!\n")
				end
			end
		end
	end
end


-------------------
-- PLUGIN THINK
-------------------
-- The main activity of this plugin goes here
local nextCleanupPass = CurTime()
function GM.Mining.Think()
	--Cleanup Feature tables
	if CurTime() > nextCleanupPass then
		for _,feature in pairs(GM.Mining.Features) do
			for i,ent in ipairs(feature.Children) do
				if not IsValid(ent) then
					table.remove(feature.Children, i)
				end
			end
		end

		nextCleanupPass = CurTime() + 1
	end

	--Tick all features
	for name,feature in pairs(GM.Mining.Features) do
		if CurTime() > feature.NextThink then
			local spawner = GM.Mining.Spawners[feature.Spawner]

			if spawner then
				spawner:Think(feature)
			else
				ErrorNoHalt("[SC2 MINING] - ERROR: Feature "..name.." doesn't have a valid Spawner Type! Culling!\n")
				GM.Mining.Features[name] = nil
			end
		end
	end
end