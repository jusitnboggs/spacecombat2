--[[
Space Combat Global Storage and Markets - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

require( "mysql" )

--Convars
if not ConVarExists("sc_market_storage_tax") then
	CreateConVar("sc_market_storage_tax",10,{FCVAR_ARCHIVE, FCVAR_NOTIFY})
end

if not ConVarExists("sc_market_spawn_tax") then
	CreateConVar("sc_market_spawn_tax",0,{FCVAR_ARCHIVE, FCVAR_NOTIFY})
end

SC.Market = {}

--Meta tables
local metaply = FindMetaTable("Player")
local metaent = FindMetaTable("Entity")

--Player meta stuff
function metaply:GetGlobalStorage()
	if not IsValid(self) or not self:IsPlayer() then return {} end

	if not self.SC then
		self.SC = {}
	end

    if not self.SC.globalstorage then
        self:SetupGlobalStorage({})
    end

	return self.SC.globalstorage
end

function metaply:SetupGlobalStorage(items)
	if not items then items = {} end
	if not self.SC then self.SC = {} end

	self.SC.globalstorage = items

	if not self.SC.globalstorage["Tritanium"] then
		self.SC.globalstorage["Tritanium"] = 100000
	end
end

function metaply:AddItemsToStorage(items)
	if not items then return end

	local storage = self:GetGlobalStorage()

	for i,k in pairs(items) do
		if not storage[i] then
			storage[i] = k
		else
			storage[i] = storage[i] + k
		end
	end
end

function metaply:TakeItemsFromStorage(items)
	if not items then return end

	local storage = self:GetGlobalStorage()

	for i,k in pairs(items) do
		if storage[i] then
			storage[i] = math.max(storage[i] - k, 0)

			if storage[i] <= 0 then
				storage[i] = nil
			end
		end
	end
end

--Entity tax function
local function EntTax(ply, ent)
	if GetConVarNumber("sc_market_spawn_tax") ~= 1 then return end
	if not IsValid(ent) or not IsValid(ent:GetPhysicsObject()) then return end

	local items = ply:GetGlobalStorage()
	local Volume = ent:GetPhysicsObject():GetVolume()/1000

	if items["Tritanium"] and items["Tritanium"] >= Volume then
		items["Tritanium"] = items["Tritanium"] - math.floor(Volume)
		return true
	else
		return false
	end
end

--hooks for spawning of things and causing taxes :D
if(cleanup) then
	local backupcleanupAdd = cleanup.Add
	function cleanup.Add(ply, enttype, ent)
		if IsValid(ent) and ply:IsPlayer() and GetConVarNumber("sc_market_spawn_tax") == 1 then
			if EntTax(ply, ent) == false then return false end
		end
		backupcleanupAdd(ply, enttype, ent)
	end
end

if(metaply.AddCount) then
	local backupAddCount = metaply.AddCount
	function metaply:AddCount(enttype, ent)
		if GetConVarNumber("sc_market_spawn_tax") == 1 and EntTax(self, ent) == false then return false end
		backupAddCount(self, enttype, ent)
	end
end

hook.Add("PlayerSpawnedSENT", "SENTSpawnTax", EntTax)

-- When the database connects, run a test.
function SC.Market.SQLConnected()
	local SQL = SC.MYSQL

	mysql.query( SQL, "CREATE TABLE IF NOT EXISTS sc_storage ( uid BIGINT NOT NULL, item CHAR(32) NOT NULL, amount INTEGER NOT NULL, CONSTRAINT uc_StorageID UNIQUE (uid, item) );" )
	mysql.query( SQL, "CREATE TABLE IF NOT EXISTS sc_market ( uid BIGINT NOT NULL, item CHAR(32) NOT NULL, amount INTEGER NOT NULL, wanted CHAR(32) NOT NULL, price INTEGER NOT NULL );" )
	mysql.query( SQL, "CREATE TABLE IF NOT EXISTS dupe_market ( uid BIGINT NOT NULL, dupeid INTEGER NOT NULL, item CHAR(32) NOT NULL, item2 CHAR(32) NOT NULL, amount INTEGER NOT NULL );" )

	--Replace our previous function to use mysql
	function metaply:SetupGlobalStorage(items, existing)
		if not items then items = {} end
		if not self.SC then self.SC = {} end

		self.SC.globalstorage = items

        if not existing then
		    if not self.SC.globalstorage["Tritanium"] then
			    self.SC.globalstorage["Tritanium"] = 100000
		    end

			self:UpdateGlobalStorageSQL()
        end
	end

    function metaply:UpdateGlobalStorageSQL(Transaction)
        local items = self:GetGlobalStorage()
        local uid = self:SteamID64()

        if items and table.Count(items) > 0 then
            ExecuteTransaction = (Transaction ~= nil)
            Transaction = Transaction or SQL:createTransaction()

            for i,k in pairs(items) do
                Transaction:addQuery(SQL:query("INSERT INTO sc_storage (uid, item, amount) VALUES ( "..uid..", '"..SQL:escape(i).."', "..k..") ON DUPLICATE KEY UPDATE amount="..k..";"))
            end

            if ExecuteTransaction then
                Transaction:start()
            end
        end
    end

    --Update global storage
    timer.Create("SC_GLOBALSTORAGE_UPDATE", 300, 0, function()
        ErrorNoHalt("[Space Combat 2 - Global Storage] - Starting MySQL Database update.\n")

        local Transaction = SQL:createTransaction()
        for i,k in pairs(player.GetAll()) do
            k:UpdateGlobalStorageSQL(Transaction)
        end

        function Transaction:onSuccess()
            ErrorNoHalt("[Space Combat 2 - Global Storage] - Successfully updated MySQL Database.\n")
        end

        function Transaction:onError(Error)
            ErrorNoHalt("[Space Combat 2 - Global Storage] - MySQL Database update failed!\n")
            ErrorNoHalt(Error.."\n")
        end

        Transaction:start()
    end)

	--Load global storage from sql on spawn
	hook.Add( "PlayerInitialSpawn", "sc_globalload", function(ply)
		ply.SC = {}
		ply.SC.globalstorage = {}

		mysql.asyncquery(SQL, "SELECT item, amount FROM sc_storage WHERE uid="..ply:SteamID64(), function(success, tbl)
			if not tbl or not tbl[1] or not tbl[1].amount then
				ErrorNoHalt("[Space Combat 2 - Global Storage] - Setting up storage for new player.\n")
				ply:SetupGlobalStorage()
			else
				local actual = {}

				for i,k in pairs(tbl) do
					actual[k.item] = k.amount
				end

				ply:SetupGlobalStorage(actual, true)
			end
		end)
	end)

	hook.Add( "PlayerDisconnected", "sc_globalsave", function(ply)
		ply:UpdateGlobalStorageSQL()
	end)
end

local function addmysql()
	if SC.MYSQLFuncs then
		table.insert(SC.MYSQLFuncs, SC.Market.SQLConnected)
	else
		timer.Create("mining.mysql", 0.1, 1, addmysql)
	end
end
timer.Create("mining.mysql", 0.1, 1, addmysql)
