--[[
Space Combat Networked Accessors - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

--Global Tables
if not SC.NWAccessors then
    SC.NWAccessors = {}
    SC.NWAccessors.Accessors = {}
    SC.NWAccessors.GlobalAccessors = {}
    SC.NWAccessors.GlobalSendQueue = {}
    SC.NWAccessors.SendQueue = {}
    SC.NWAccessors.CustomSyncFunctions = {}
end

if SERVER then
    util.AddNetworkString("SC.NWAccessorSync")
    util.AddNetworkString("SC.GlobalNWAccessorSync")
    util.AddNetworkString("SC.NWAccessors.RequestEntitySync")
    util.AddNetworkString("SC.NWAccessors.RequestGlobalSync")
    util.AddNetworkString("SC.NWAccessors.RequestFullGlobalSync")

    local NetworkUpdateRate = 15

    local UpdConvar = CreateConVar("sc_networkupdaterate", "15", {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "How many network updates can we send per frame?")
    cvars.AddChangeCallback("sc_networkupdaterate", function()
        NetworkUpdateRate = UpdConvar:GetInt()
    end)

    hook.Add("Think", "SC.NWAccessors.Think", function()
        local Count = 0
        local GlobalsSent = {}
        for GlobalName, Data in pairs(SC.NWAccessors.GlobalSendQueue) do
            if Count >= NetworkUpdateRate then break end
            Count = Count + 1

            SC.NWAccessors.DoSyncGlobalValue(GlobalName, Data.Recipients, Data.Force)
            table.insert(GlobalsSent, GlobalName)
        end

        for _, GlobalName in ipairs(GlobalsSent) do
            SC.NWAccessors.GlobalSendQueue[GlobalName] = nil
        end

        local Sent = {}
        for obj, tosend in pairs(SC.NWAccessors.SendQueue) do
            for id, tbl in pairs(tosend) do
                if Count >= NetworkUpdateRate then break end
                Count = Count + 1

                SC.NWAccessors.DoSyncValue(obj, id, tbl.recipients, tbl.force)
                table.insert(Sent, {obj, id})
            end
        end

        for _, Pair in ipairs(Sent) do
            local obj, id = Pair[1], Pair[2]
            SC.NWAccessors.SendQueue[obj][id] = nil

            if table.Count(SC.NWAccessors.SendQueue[obj]) == 0 then
                SC.NWAccessors.SendQueue[obj] = nil
            end
        end
    end)
end

--Locals
local NWTypes = {}
NWTypes["number"] = 0
NWTypes["string"] = 1
NWTypes["vector"] = 2
NWTypes["color"] = 3
NWTypes["table"] = 4
NWTypes["bool"] = 5
NWTypes["entity"] = 6
NWTypes["uint"] = 7
NWTypes["int"] = 8
NWTypes["double"] = 9
NWTypes["matrix"] = 10
NWTypes["angle"] = 11
NWTypes["normal"] = 12
NWTypes["cachedstring"] = 13
NWTypes["custom"] = 14

function SC.NWAccessors.CompareTables(a, b)
    --for all the sanity
    local a = a or {}
    local b = b or {}
    local newvalues = {}
    local removedkeys = {}
    local ret = true

    for i,k in pairs(b) do
        if type(a[i]) == "table" and type(b[i]) == "table" then
            local ret2, newvals, remkeys = SC.NWAccessors.CompareTables(a[i], b[i])
            if not ret2 then
                ret = false
                newvalues[i] = newvals
                removedkeys[i] = remkeys
            end
        elseif a[i] == nil or a[i] ~= k then
            ret = false
            newvalues[i] = k
        end
    end

    for i,k in pairs(a) do
        if b[i] == nil then
            ret = false
            table.insert(removedkeys, i)
        end
    end

    return ret, newvalues, removedkeys
end
local CompareTables = SC.NWAccessors.CompareTables

--Merges a table with another one, optionally removing keys when mode is set to true
local function MergePartialTable(old, new, mode)
    if mode then
        for i,k in pairs(new) do
            if type(k) == "table" then
                MergePartialTable(old[i], k, true)
            else
                old[k] = nil
            end
        end
    else
        for i,k in pairs(new) do
            if type(k) == "table" and old[i] ~= nil and type(old[i]) == "table" then
                MergePartialTable(old[i], k)
            else
                old[i] = k
            end
        end
    end
end

--Functions
function SC.NWAccessors.GetNWAccessorID(self, name)
    if not self or not self.SC or not self.SC.NWAccessors then return end
    for i,k in ipairs(self.SC.NWAccessors) do
        if k.name == name then return k.id end
    end
end

function SC.NWAccessors.RegisterCustomSyncFunction(name, func)
    if not name or not func then return end

    if SC.NWAccessors.CustomSyncFunctions[name] ~= nil then
        SC.Print(string.format("Warning: Replacing existing custom sync function %s with new version! \n\n %s\n", name, debug.traceback()), 4)
    end

    SC.NWAccessors.CustomSyncFunctions[name] = func
end

function SC.NWAccessors.CreateNWAccessor(self, name, atype, default, priority, onset, onsync)
    if not NWTypes[string.lower(atype)] or not IsValid(self) then return end
    if string.lower(atype) == "custom" and not onsync then SC.Error("Must add onsync parameter for Custom accessors!", 5) return end

    self.SC = self.SC or {}
    self.SC.NWAccessors = self.SC.NWAccessors or {}

    for i,k in ipairs(self.SC.NWAccessors) do
        if k.name == name then return end
    end

    atype = NWTypes[string.lower(atype)]
    priority = math.Clamp(priority or 1,1,5)

    local AccessorTable = {name = name, type=atype, default=default, value=default, priority=priority, onset=onset, onsync=onsync}
    local AccessorID = table.insert(self.SC.NWAccessors, AccessorTable)
    AccessorTable.id = AccessorID

    SC.NWAccessors.Accessors[tostring(self)] = SC.NWAccessors.Accessors[tostring(self)] or {}
    SC.NWAccessors.Accessors[tostring(self)][AccessorID] = {object = self, name = name, accessor = AccessorTable}

    local IsTable = atype == 4
    self["Set"..name] = function(self, value, dontsync)
        local SameType
        if atype == NWTypes["entity"] then
            SameType = isentity(value)
        else
            SameType = type(value) == type(AccessorTable.value)
        end

        if not SameType then
            SC.Error("Setting variable "..name.." failed, bad type", 5)
			return
        end

        local Same = AccessorTable.value == value
        local new, removed
        if atype == NWTypes["table"] then
            Same, new, removed = CompareTables(AccessorTable.value, value)
        end

        if Same then
			SC.Error("Setting Variable "..name.." failed, value is the same", 0)
			return
		end

        SC.Error("Trying to set variable "..name.." to value "..tostring(value))
        AccessorTable.value = value

        if IsTable then
            AccessorTable.partial = {new = new, removed = removed}
        end

        if onset then
            onset(self, name, value)
        end

        if not dontsync then
            SC.Error("Sending Sync Request")
            SC.NWAccessors.SyncValue(tostring(self), AccessorID)
        end
    end

    self["Get"..name] = function(self, default, useref)
        if AccessorTable.value ~= nil then
            SC.Error("Getting "..name.." value of "..tostring(AccessorTable.value), -1)
            if AccessorTable.type == 4 and not useref then
                return table.Copy(AccessorTable.value)
            else
                return AccessorTable.value
            end
        end

        if AccessorTable.type == 4 then
            return table.Copy(default ~= nil and default or AccessorTable.default)
        else
            return default ~= nil and default or AccessorTable.default
        end
    end
end

function SC.NWAccessors.CreateSpecialNWAccessor(self, name, atype, get, set, priority, onset, onsync)
    if not NWTypes[string.lower(atype)] or not IsValid(self) then return end
    if string.lower(atype) == "custom" and not onsync then SC.Error("Must add onsync parameter for Custom accessors!", 5) return end

    self.SC = self.SC or {}
    self.SC.NWAccessors = self.SC.NWAccessors or {}

    for i,k in ipairs(self.SC.NWAccessors) do
        if k.name == name then return end
    end

    atype = NWTypes[string.lower(atype)]
    priority = math.Clamp(priority or 1,1,5)

    local AccessorTable = {name = name, type=atype, special=true, get=get, set=set, priority=priority, onset=onset, onsync=onsync}
    local AccessorID = table.insert(self.SC.NWAccessors, AccessorTable)
    AccessorTable.id = AccessorID

    SC.NWAccessors.Accessors[tostring(self)] = SC.NWAccessors.Accessors[tostring(self)] or {}
    SC.NWAccessors.Accessors[tostring(self)][AccessorID] = {object = self, name = name, accessor = AccessorTable}
end

function SC.NWAccessors.RegisterGlobalNWAccessor(Name, Type, Default, Priority)
    if not NWTypes[string.lower(Type)] or not Name then return end

    local ExistingAccessor = SC.NWAccessors.GlobalAccessors[Name]
    local TypeID = NWTypes[string.lower(Type)]
    local OnChangedCallbacks
    local Value

    if ExistingAccessor then
        if TypeID ~= ExistingAccessor.Type then
            SC.Error("Tried to override existing global accessor with different type!", 5)
            return
        end

        OnChangedCallbacks = ExistingAccessor.OnChangedCallbacks or {}
        Value = ExistingAccessor.Value
    end

    SC.NWAccessors.GlobalAccessors[Name] = {
        OnChangedCallbacks = OnChangedCallbacks,
        Value = Value or Default,
        Default = Default,
        Type = TypeID,
        Priority = math.Clamp(Priority or 1,1,5)
    }
end

function SC.NWAccessors.AddGlobalOnChangedCallback(Name, CallbackName, Callback)
    local Accessor = SC.NWAccessors.GlobalAccessors[Name]
    if not Accessor then
        SC.NWAccessors.GlobalAccessors[Name] = {
            OnChangedCallbacks = {}
        }
        Accessor = SC.NWAccessors.GlobalAccessors[Name]
    end

    Accessor.OnChangedCallbacks[CallbackName] = Callback
end

function SC.NWAccessors.RemoveGlobalOnChangedCallback(Name, CallbackName)
    local Accessor = SC.NWAccessors.GlobalAccessors[Name]
    if not Accessor then
        return
    end

    Accessor.OnChangedCallbacks[CallbackName] = nil
end

function SC.NWAccessors.GetGlobalValue(Name, Default, GetRef)
    local Accessor = SC.NWAccessors.GlobalAccessors[Name]
    if not Accessor then
        return Default
    end

    if Accessor.Value == nil then
        return (Default ~= nil) and Default or Accessor.Default
    end

    if Accessor.Type == NWTypes["table"] and not GetRef then
        return table.Copy(Accessor.Value)
    end

    return Accessor.Value
end

function SC.NWAccessors.SetGlobalValue(Name, Value, SkipSync)
    if Name then
        local Accessor = SC.NWAccessors.GlobalAccessors[Name]
        if not Accessor then
            SC.Error(string.format("Tried to set value of unregistered global %s", Name), 5)
            return
        end

        local SameType
        if Accessor.Type == NWTypes["entity"] then
            SameType = isentity(Value)
        else
            SameType = type(Value) == type(Accessor.Value)
        end

        if not SameType then
            print(type(Value) ~= type(Accessor.Value), type(Value), type(Accessor.Value))
            SC.Error("Setting global variable "..Name.." failed, bad type", 5)
			return
        end

        local Same = Accessor.Value == Value
        if Accessor.Type == NWTypes["table"] then
            Same = CompareTables(Accessor.Value, Value)
        end

        if Same then
			SC.Error("Setting Variable "..name.." failed, value is the same", 0)
			return
		end

        Accessor.Value = Value

        if Accessor.OnChangedCallbacks then
            for CallbackName, Callback in pairs(Accessor.OnChangedCallbacks) do
                Callback(Name, Value)
            end
        end

        if not SkipSync then
            SC.NWAccessors.SyncGlobalValue(Name)
        end
    end
end

function SC.NWAccessors.CreatePartialTable(self, name)
    local accessor
    for i,k in ipairs(self.SC.NWAccessors) do
        if k.name == name then accessor = k break end
    end

    if not accessor or not accessor.type == 4 then return end

    local same, new, removed = CompareTables(accessor.old or {}, accessor.value)

    accessor.old = table.Copy(accessor.value)

    if not same then accessor.partial = {new = new, removed = removed} end
end

function SC.NWAccessors.SyncAccessors(self, recipients, force)
    if CLIENT or not IsValid(self) or not self.SC or not self.SC.NWAccessors then return end
    local sync = {{},{},{},{},{}}
    for i,k in pairs(self.SC.NWAccessors) do
        sync[k.priority][i] = k
    end

    for I = 1, 5 do
        local tbl = sync[I]
        for i,k in pairs(tbl) do
            SC.NWAccessors.SyncValue(self, i, recipients, force)
        end
    end
end

local function NumberInRange(Value, Min, Max)
    if Value < Min or Value > Max then
        return false
    end

    return true
end

function SC.NWAccessors.WriteSyncPacket(Type, Value, Accessor, Force, Object)
    if Type == 0 then
        net.WriteFloat(Value)
    elseif Type == 1 then
        net.WriteString(Value)
    elseif Type == 2 then
        net.WriteFloat(Value.X)
        net.WriteFloat(Value.Y)
        net.WriteFloat(Value.Z)
    elseif Type == 3 then
        net.WriteColor(Value)
    elseif Type == 4 then
        net.WriteBool(not Force and Accessor.partial ~= nil)
        if not Force and Accessor.partial then
            net.WriteTable(Accessor.partial)
        else
            -- Force an update of the partial table if it exists
            if Accessor.partial then
                Accessor.old = table.Copy(Value)
                Accessor.partial = nil
            end

            net.WriteTable(Value)
        end
    elseif Type == 5 then
        net.WriteBool(Value)
    elseif Type == 6 then
        net.WriteEntity(Value)
    elseif Type == 7 then
        local Bits = 3
        if NumberInRange(Value, 0, 15) then
            Bits = 0
        elseif NumberInRange(Value, 0, 255) then
            Bits = 1
        elseif NumberInRange(Value, 0, 65535) then
            Bits = 2
        end

        net.WriteUInt(Bits, 4)
        net.WriteUInt(Value, Bits)
    elseif Type == 8 then
        local Bits = 3
        if NumberInRange(Value, -8, 7) then
            Bits = 0
        elseif NumberInRange(Value, -128, 127) then
            Bits = 1
        elseif NumberInRange(Value, -32768, 32767) then
            Bits = 2
        end

        net.WriteUInt(Bits, 4)
        net.WriteInt(Value, Bits)
    elseif Type == 9 then
        net.WriteDouble(Value)
    elseif Type == 10 then
        net.WriteMatrix(Value)
    elseif Type == 11 then
        net.WriteAngle(Value)
    elseif Type == 12 then
        net.WriteNormal(Value)
    elseif Type == 13 then
        SC.WriteNetworkedString(Value)
    elseif Type == 14 then
        net.WriteUInt(util.NetworkStringToID(Accessor.onsync), 16)
        SC.NWAccessors.CustomSyncFunctions[Accessor.onsync](Object, true, Accessor)
    else
        SC.Error("Sending bad network accessor!", 5)
    end
end

function SC.NWAccessors.DoSyncValue(obj, id, recipients, force)
    if CLIENT then return end
    if not id or not obj then SC.Error("NWAccessors.SyncValue: SyncValue requires a valid object and id", 5) return end

    obj = tostring(obj)

    if not SC.NWAccessors.Accessors[obj] or not SC.NWAccessors.Accessors[obj][id] then SC.Error("SyncValue got invalid accessor object or id! "..obj.." "..tostring(id), 5) return end

    local tbl = SC.NWAccessors.Accessors[obj][id]
    if not IsValid(tbl.object) then SC.Error("Accessor object was invalid!", 5) return end

    local accessor = tbl.accessor
    local Type = accessor.type
    local Value = accessor.value
    local Special = accessor.special

    if Special then
        Value = accessor.get(tbl.object)
        SC.Error("accessor is special\n", 1)
    end

    if Type == nil or Value == nil then return end

    net.Start("SC.NWAccessorSync")
    net.WriteUInt(Type, 4)
    net.WriteUInt(id, 8)
    net.WriteEntity(tbl.object)
    net.WriteBool(Special)

    SC.NWAccessors.WriteSyncPacket(Type, Value, accessor, force, tbl.object)

    if recipients then
        net.Send(recipients)
    else
        net.Broadcast()
    end
end

function SC.NWAccessors.DoSyncGlobalValue(GlobalName, Recipients, Force)
    if CLIENT then return end
    if not GlobalName then SC.Error("NWAccessors.DoSyncGlobalValue: SyncValue requires an id", 5) return end

    local Accessor = SC.NWAccessors.GlobalAccessors[GlobalName]
    if not Accessor then SC.Error("Accessor object was invalid!", 5) return end

    local Type = Accessor.Type
    local Value = Accessor.Value

    if Type == nil or Value == nil then return end

    net.Start("SC.GlobalNWAccessorSync")
    net.WriteUInt(Type, 4)
    SC.WriteNetworkedString(GlobalName)

    SC.NWAccessors.WriteSyncPacket(Type, Value, Accessor, Force)

    if Recipients then
        net.Send(Recipients)
    else
        net.Broadcast()
    end
end

function SC.NWAccessors.SyncValue(obj, id, recipients, force)
    if not SERVER then return end

    if not id or not obj then SC.Error("NWAccessors.SyncValue: SyncValue requires a valid object and id", 5) return end

    obj = tostring(obj)

    if not SC.NWAccessors.Accessors[obj] or not SC.NWAccessors.Accessors[obj][id] then SC.Error("SyncValue got invalid accessor object or id! "..tostring(obj).." "..tostring(id), 5) return end

    if SC.NWAccessors.SendQueue[obj] and SC.NWAccessors.SendQueue[obj][id] and SC.NWAccessors.SendQueue[obj][id]["recipients"] then
        local merged

        if type(SC.NWAccessors.SendQueue[obj][id]["recipients"]) == "table" then
            merged = table.Copy(SC.NWAccessors.SendQueue[obj][id]["recipients"])
        else
            merged = {[1]=SC.NWAccessors.SendQueue[obj][id]["recipients"]}
        end

        for t1i, t1v in pairs(type(recipients) == "table" and recipients or {[1]=recipients}) do
            local found = false
            for t2i, t2v in pairs(merged) do
                if t2v == t1v then
                    found = true
                    break
                end
            end

            if not found then
                table.insert(merged, t1v)
            end
        end

        SC.NWAccessors.SendQueue[obj][id]["recipients"] = merged
    else
        SC.NWAccessors.SendQueue[obj] = SC.NWAccessors.SendQueue[obj] or {}
        SC.NWAccessors.SendQueue[obj][id] = {
            recipients = recipients,
            force = force
        }
    end
end

function SC.NWAccessors.SyncGlobalValue(GlobalName, Recipients, Force)
    if not SERVER then return end

    if not GlobalName then SC.Error("NWAccessors.SyncValue: SyncValue requires a valid object and id", 5) return end

    if SC.NWAccessors.GlobalSendQueue[GlobalName] then
        local OldRecipients = SC.NWAccessors.GlobalSendQueue[GlobalName]["Recipients"]
        if OldRecipients then
            local merged

            if type(OldRecipients) == "table" then
                merged = table.Copy(OldRecipients)
            else
                merged = {[1]=OldRecipients}
            end

            for t1i, t1v in pairs(type(Recipients) == "table" and Recipients or {[1]=Recipients}) do
                local found = false
                for t2i, t2v in pairs(merged) do
                    if t2v == t1v then
                        found = true
                        break
                    end
                end

                if not found then
                    table.insert(merged, t1v)
                end
            end

            SC.NWAccessors.GlobalSendQueue[GlobalName]["Recipients"] = merged
        end
    else
        SC.NWAccessors.GlobalSendQueue[GlobalName] = {
            Force = Force,
            Recipients = Recipients
        }
    end
end

if SERVER then
    function SC.NWAccessors.SyncAllAccessors(recipients, force, GlobalOnly)
        if not GlobalOnly then
            local sync = {}
            local invalid = {}
            for obj,tbl in pairs(SC.NWAccessors.Accessors) do
                for i,k in pairs(tbl) do
                    if not IsValid(k.object) then
                        SC.Error("NWAccessors.SyncAllAccessors: Accessor "..i.." was invalid, removing.", 5)
                        table.insert(invalid, {obj, i})
                    else
                        sync[k.accessor.priority] = sync[k.accessor.priority] or {}
                        table.insert(sync[k.accessor.priority], k)
                    end
                end
            end

            for _, Pair in ipairs(invalid) do
                SC.NWAccessors.Accessors[Pair[1]][Pair[2]] = nil
            end

            for priority,tab in ipairs(sync) do
                for i,k in pairs(tab) do
                    SC.NWAccessors.SyncValue(k.object, k.accessor.id, recipients, force)
                end
            end
        end

        for GlobalName, Data in SortedPairsByMemberValue(SC.NWAccessors.GlobalAccessors, "Priority") do
            SC.NWAccessors.SyncGlobalValue(GlobalName, recipients, force)
        end
    end

    function SC.NWAccessors.RequestEntitySync(Len, Ply)
        local Ent = net.ReadEntity()
        SC.Error("SC.NWAccessors.RequestEntitySync: Sending entity data for entity "..tostring(Ent).." back to player "..tostring(Ply), 3)
        if IsValid(Ply) and IsValid(Ent) then
            SC.NWAccessors.SyncAccessors(Ent, Ply, true)
        end
    end
    net.Receive("SC.NWAccessors.RequestEntitySync", SC.NWAccessors.RequestEntitySync)

    function SC.NWAccessors.RequestGlobalSync(Len, Ply)
        local GlobalName = SC.ReadNetworkedString()
        if IsValid(Ply) and GlobalName then
            SC.NWAccessors.SyncGlobalValue(GlobalName, Ply, true)
        end
    end
    net.Receive("SC.NWAccessors.RequestGlobalSync", SC.NWAccessors.RequestGlobalSync)

    function SC.NWAccessors.RequestFullGlobalSync(Len, Ply)
        if IsValid(Ply) then
            SC.NWAccessors.SyncAllAccessors(Ply, true, true)
        end
    end
    net.Receive("SC.NWAccessors.RequestFullGlobalSync", SC.NWAccessors.RequestFullGlobalSync)
else
    function SC.NWAccessors.ClientRequestGlobalSync(GlobalName)
        net.Start("SC.NWAccessors.RequestGlobalSync")
        SC.WriteNetworkedString(GlobalName)
        net.SendToServer()
    end

    function SC.NWAccessors.ClientRequestFullGlobalSync()
        net.Start("SC.NWAccessors.RequestFullGlobalSync")
        net.SendToServer()
    end
    hook.Remove("InitPostEntity", "SC.NWAccessors.ClientRequestFullGlobalSync")
    hook.Add("InitPostEntity", "SC.NWAccessors.ClientRequestFullGlobalSync", SC.NWAccessors.ClientRequestFullGlobalSync)
    hook.Remove("OnReloaded", "SC.NWAccessors.ClientRequestFullGlobalSyncReloaded")
    hook.Add("OnReloaded", "SC.NWAccessors.ClientRequestFullGlobalSyncReloaded", SC.NWAccessors.ClientRequestFullGlobalSync)
end

function SC.NWAccessors.RegisterAllCustomSyncFunctions()
    hook.Run("SC.RegisterCustomSyncFunction")
end
hook.Remove("InitPostEntity", "SC.NWAccessors.RegisterCustomSyncFunction")
hook.Add("InitPostEntity", "SC.NWAccessors.RegisterCustomSyncFunction", SC.NWAccessors.RegisterAllCustomSyncFunctions)
hook.Remove("OnReloaded", "SC.NWAccessors.RegisterCustomSyncFunctionReloaded")
hook.Add("OnReloaded", "SC.NWAccessors.RegisterCustomSyncFunctionReloaded", SC.NWAccessors.RegisterAllCustomSyncFunctions)

if CLIENT then
    function SC.NWAccessors.UpdateAccessor(Ent, ID, Special, Partial, Value)
        --If our entity was invalid it usually means it was either removed or wasn't created yet
        if not IsValid(Ent) or not Ent.SC or not Ent.SC.NWAccessors then return end

        local Accessor = Ent.SC.NWAccessors[ID]
        local Name = Accessor.name

        if not Accessor then
            SC.Error("Accessor "..Name.." for Entity "..tostring(Ent).." wasn't on the client!")
        end

        if Special then
            if Partial then
                Value = Accessor.get(Ent)

                MergePartialTable(Value, Partial.new)
                MergePartialTable(Value, Partial.removed, true)

                Accessor.set(Ent, Value)
            else
                Accessor.set(Ent, Value)
            end

            return
        end

        if Partial then
            Accessor.value = Accessor.value or {}

            MergePartialTable(Accessor.value, Partial.new)
            MergePartialTable(Accessor.value, Partial.removed, true)
        else
            Accessor.value = Value
        end

        if Accessor.onset then
            Accessor.onset(Ent, Accessor.name, Accessor.value)
        end
    end

    function SC.NWAccessors.ReceiveSync(NetLength)
        local Type = net.ReadUInt(4)
        local ID = net.ReadUInt(8)
        local Ent = net.ReadEntity()
        local Special = net.ReadBool()
        local Partial, Value

        --Get the actual value of the data
        if Type == 0 then
            Value = net.ReadFloat()
        elseif Type == 1 then
            Value = net.ReadString()
        elseif Type == 2 then
            Value = Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
        elseif Type == 3 then
            Value = net.ReadColor()
        elseif Type == 4 then
            local IsPartial = net.ReadBool()

            if IsPartial then
                Partial = net.ReadTable()
            else
                Value = net.ReadTable()
            end
        elseif Type == 5 then
            Value = net.ReadBool()
        elseif Type == 6 then
            Value = net.ReadEntity()
        elseif Type == 7 then
            local Bits = net.ReadUInt(4)
            Value = net.ReadUInt((Bits + 1) * 4)
        elseif Type == 8 then
            local Bits = net.ReadUInt(4)
            Value = net.ReadInt((Bits + 1) * 4)
        elseif Type == 9 then
            Value = net.ReadDouble()
        elseif Type == 10 then
            Value = net.ReadMatrix()
        elseif Type == 11 then
            Value = net.ReadAngle()
        elseif Type == 12 then
            Value = net.ReadNormal()
        elseif Type == 13 then
            Value = SC.ReadNetworkedString(nil, function(StringID, NewString)
                SC.NWAccessors.UpdateAccessor(Ent, ID, Special, Partial, NewString)
            end)

            -- If the string wasn't valid then skip updating the accessor until it is
            if Value == nil then
                return
            end
        elseif Type == 14 then
            local Accessor
            local CustomSyncFunc = util.NetworkIDToString(net.ReadUInt(16))
            if IsValid(Ent) and Ent.SC and Ent.SC.NWAccessors then
                Accessor = Ent.SC.NWAccessors[ID]
            else
                SC.Error("Accessor data does not exist, unable to call custom sync function with complete data, attempting cleanup! "..CustomSyncFunc.." "..tostring(Ent), 5)
                local Bytes, Bits = net.BytesLeft(true)
                while Bits > 0 do
                    net.ReadBit()
                    Bytes, Bits = net.BytesLeft(true)
                end
                return
            end

            if SC.NWAccessors.CustomSyncFunctions[CustomSyncFunc] == nil then
                SC.Error("Custom sync function does not exist, attempting to clean up invalid data! "..CustomSyncFunc.." "..tostring(Ent), 5)
                local Bytes, Bits = net.BytesLeft(true)
                while Bits > 0 do
                    net.ReadBit()
                    Bytes, Bits = net.BytesLeft(true)
                end
                return
            end

            Value = SC.NWAccessors.CustomSyncFunctions[CustomSyncFunc](Ent, false, Accessor, function()
                if Value == nil then
                    if Accessor and Accessor.onset then
                        Accessor.onset(Ent, Accessor.name, Accessor.value)
                    end
                end
            end)

            -- Some accessors handle updating the accessor themselves and return nil
            if Value == nil then
                return
            end
        end

        SC.NWAccessors.UpdateAccessor(Ent, ID, Special, Partial, Value)
    end
    net.Receive("SC.NWAccessorSync", SC.NWAccessors.ReceiveSync)

    function SC.NWAccessors.ReceiveGlobalSync(NetLength)
        local Type = net.ReadUInt(4)
        local GlobalName = SC.ReadNetworkedString(nil, function(StringID, NewString)
            SC.NWAccessors.ClientRequestGlobalSync(NewString)
        end)
        local Value

        --Get the actual value of the data
        if Type == 0 then
            Value = net.ReadFloat()
        elseif Type == 1 then
            Value = net.ReadString()
        elseif Type == 2 then
            Value = Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
        elseif Type == 3 then
            Value = net.ReadColor()
        elseif Type == 4 then
            local IsPartial = net.ReadBool()

            if IsPartial then
                Partial = net.ReadTable()
            else
                Value = net.ReadTable()
            end
        elseif Type == 5 then
            Value = net.ReadBool()
        elseif Type == 6 then
            Value = net.ReadEntity()
        elseif Type == 7 then
            local Bits = net.ReadUInt(4)
            Value = net.ReadUInt((Bits + 1) * 4)
        elseif Type == 8 then
            local Bits = net.ReadUInt(4)
            Value = net.ReadInt((Bits + 1) * 4)
        elseif Type == 9 then
            Value = net.ReadDouble()
        elseif Type == 10 then
            Value = net.ReadMatrix()
        elseif Type == 11 then
            Value = net.ReadAngle()
        elseif Type == 12 then
            Value = net.ReadNormal()
        elseif Type == 13 then
            Value = SC.ReadNetworkedString(nil, function(StringID, NewString)
                if GlobalName then
                    SC.NWAccessors.ClientRequestGlobalSync(GlobalName)
                end
            end)

            -- If the string wasn't valid then skip updating the accessor until it is
            if Value == nil then
                return
            end
        else
            SC.Error(string.format("Global Accessor Error, type %d not implemented", Type), 5)
        end

        if GlobalName then
            local Accessor = SC.NWAccessors.GlobalAccessors[GlobalName]
            if not Accessor then
                SC.NWAccessors.GlobalAccessors[GlobalName] = {}
                Accessor = SC.NWAccessors.GlobalAccessors[GlobalName]
            end

            if Partial then
                Accessor.Value = Accessor.Value or {}

                MergePartialTable(Accessor.Value, Partial.new)
                MergePartialTable(Accessor.Value, Partial.removed, true)
            else
                Accessor.Value = Value
            end

            if Accessor.OnChangedCallbacks then
                for CallbackName, Callback in pairs(Accessor.OnChangedCallbacks) do
                    Callback(GlobalName, Value)
                end
            end
        end
    end
    net.Receive("SC.GlobalNWAccessorSync", SC.NWAccessors.ReceiveGlobalSync)

    local EntityBlacklist = {
        "gmod_",
        "prop_",
        "physgun_",
        "info_",
        "func_",
        "path_",
        "logic_",
        "ai_",
        "env_",
        "sc_planet",
        "phys_",
        "point_",
        "trigger_",
        "class C_"
    }

    hook.Add("NetworkEntityCreated", "SC.NWAccessors.NetworkEntityCreate", function(Ent)
        -- Check against the blacklist before requesting the data
        -- Network overhead is a bigger issue than CPU overhead 99% of the time in space combat
        for i, k in ipairs(EntityBlacklist) do
            if string.sub(Ent:GetClass(), 1, k:len()) == k then
                return
            end
        end

        net.Start("SC.NWAccessors.RequestEntitySync")
        net.WriteEntity(Ent)
        net.SendToServer()

        SC.Error("SC.NWAccessors.NetworkEntityCreate: Requesting entity data from server for "..tostring(Ent).." with class of "..tostring(Ent:GetClass()), 3)
    end)
end