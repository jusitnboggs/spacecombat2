--[[
Space Combat Core - Created by Steeveeo/Lt.Brandon/Others

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

if SERVER then
    --Core Octree

    --TODO: Replace this entire octree section with a class, copy pasting so much code is wasteful
    --Global Tables
    SC.CoreOctree = {}
    SC.CoreOctree.Builder = {}
    SC.CoreOctree.Octant = {}

    --Octant Functions
    function SC.CoreOctree.Octant:New(Origin, Axis, Size, IsRoot, Parent)
        if not Origin or not Axis or not Size or ((not IsRoot) and (not Parent)) then
            ErrorNoHalt("[Space Combat 2 - Octree Builder] Unable to create octant, invalid parameters\n")
            return false
        end

        local obj = {}
	    setmetatable(obj,self)
        obj.Parent = Parent
        obj.Axis = Axis
        obj.Size = Size
		obj.Radius = math.max(Size.x, Size.y, Size.z) * 0.5
        obj.Position = Origin
        obj.Children = nil
        obj.IsRoot = IsRoot
        obj.IsBlocked = false

        if IsRoot then
            obj.Root = obj
            obj.OctantCount = 1
        else
            obj.Root = Parent.Root
            obj.Root.OctantCount = obj.Root.OctantCount + 1
        end

        return obj
    end

    function SC.CoreOctree.Octant:IsLeaf()
        return self.Children ~= nil
    end

    function SC.CoreOctree.Octant:IsPointInOctant(Point)
        local Center = self.Position
        local Size = self.Size
        local Min = Center - Vector(Size, Size, Size)
        local Max = Center + Vector(Size, Size, Size)

        return Point:WithinAABox(Min, Max)
    end

    function SC.CoreOctree.Octant:GetPointOctant(Point)
        if self:IsLeaf() and self:IsPointInOctant(Point) then
            return self
        elseif self.Children then
            local Octants = self.Children
            for I = 1, 8 do
                local SubOctant = Octants[I]
                if SubOctant:IsPointInOctant(Point) then
                    return SubOctant:GetPointOctant(Point)
                end
            end
        else
            ErrorNoHalt("[Space Combat 2 - Octree Builder] - Unable to find the octant which held the point!\n")
            return false
        end
    end

    local function SphereInSphere(Position1, Position2, Radius1, Radius2)
        if Radius1 > Radius2 + 0.0001 then
			return false
		end

		return (Position1 - Position2):LengthSqr() <= math.pow(Radius2 + 0.0001 - Radius1, 2)
    end

    local function SpheresIntersect(Position1, Position2, Radius1, Radius2)
        return (Position1 - Position2):LengthSqr() <= math.pow(math.max(0, Radius2 + Radius1 + 0.0001), 2)
    end

    -- The below function AABBOBBIntersect is based on
    -- work provided by Geometric Tools LLC under the Boost Software License.
    -- As required by the license, below are the copyright statement and license.
    -- Geometric Tools LLC, Redmond WA 98052
    -- Copyright (c) 1998-2015
    -- Distributed under the Boost Software License, Version 1.0.
    -- http://www.boost.org/LICENSE_1_0.txt
    -- http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt

    local function AABBOBBIntersect(Pos, Extents, OBB)
        -- Get the centered form of the aligned box.  The axes are implicitly
        -- A0[1] = (1,0,0), A0[2] = (0,1,0), and A0[3] = (0,0,1).
        local C0, E0 = Pos, Extents * 0.5 --I think this wants half extents?

        -- Convenience variables.
        local C1 = OBB.Center * 0.5
        local A1 = OBB.Axis
        local E1 = OBB.Extents * 0.5 --I think this wants half extents?

        local cutoff = 0.999
        local existsParallelPair = false

        -- Compute the difference of box centers.
        local D = C1 - C0

        local dot01 = {{}, {}, {}}      -- dot01[i][j] = Dot(A0[i],A1[j]) = A1[j][i]
        local absDot01 = {{}, {}, {}}   -- |dot01[i][j]|
        local r0, r1, r                 -- interval radii and distance between centers
        local r01                       -- r0 + r1

        -- Test for separation on the axis C0 + t*A0.X
        for i = 1, 3 do
            dot01[1][i] = A1[i].X
            absDot01[1][i] = math.abs(A1[i].X)
            if absDot01[1][i] >= cutoff then
                existsParallelPair = true
            end
        end

        r = math.abs(D.X)
        r1 = E1.X * absDot01[1][1] + E1.Y * absDot01[1][2] + E1.Z * absDot01[1][3]
        r01 = E0.X + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0.Y
        for i = 1, 3 do
            dot01[2][i] = A1[i].Y
            absDot01[2][i] = math.abs(A1[i].Y)
            if absDot01[2][i] >= cutoff then
                existsParallelPair = true
            end
        end

        r = math.abs(D.Y)
        r1 = E1.X * absDot01[2][1] + E1.Y * absDot01[2][2] + E1.Z * absDot01[2][3]
        r01 = E0.Y + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3].
        for i = 1, 3 do
            dot01[3][i] = A1[i].Z
            absDot01[3][i] = math.abs(A1[i].Z)
            if absDot01[3][i] >= cutoff then
                existsParallelPair = true
            end
        end

        r = math.abs(D.Z)
        r1 = E1.X * absDot01[3][1] + E1.Y * absDot01[3][2] + E1.Z * absDot01[3][3]
        r01 = E0.Z + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[1].
        r = math.abs(D:Dot(A1[1]))
        r0 = E0.X * absDot01[1][1] + E0.Y * absDot01[2][1] + E0.Z * absDot01[3][1]
        r01 = r0 + E1.X
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[2].
        r = math.abs(D:Dot(A1[2]))
        r0 = E0.X * absDot01[1][2] + E0.Y * absDot01[2][2] + E0.Z * absDot01[3][2]
        r01 = r0 + E1.Y
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[3].
        r = math.abs(D:Dot(A1[3]))
        r0 = E0.X * absDot01[1][3] + E0.Y * absDot01[2][3] + E0.Z * absDot01[3][3]
        r01 = r0 + E1.Z
        if r > r01 then
            return false
        end

        -- At least one pair of box axes was parallel, so the separation is
        -- effectively in 2D.  The edge-edge axes do not need to be tested.
        if existsParallelPair then
            return true
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[1].
        r = math.abs(D.Z * dot01[2][1] - D.Y * dot01[3][1])
        r0 = E0.Y * absDot01[3][1] + E0.Z * absDot01[2][1]
        r1 = E1.Y * absDot01[1][3] + E1.Z * absDot01[1][2]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[2].
        r = math.abs(D.Z * dot01[2][2] - D.Y * dot01[3][2])
        r0 = E0.Y * absDot01[3][2] + E0.Z * absDot01[2][2]
        r1 = E1.X * absDot01[1][3] + E1.Z * absDot01[1][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[3].
        r = math.abs(D.Z * dot01[2][3] - D.Y * dot01[3][3])
        r0 = E0.Y * absDot01[3][3] + E0.Z * absDot01[2][3]
        r1 = E1.X * absDot01[1][2] + E1.Y * absDot01[1][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[1].
        r = math.abs(D.X * dot01[3][1] - D.Z * dot01[1][1])
        r0 = E0.X * absDot01[3][1] + E0.Z * absDot01[1][1]
        r1 = E1.Y * absDot01[2][3] + E1.Z * absDot01[2][2]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[2].
        r = math.abs(D.X * dot01[3][2] - D.Z * dot01[1][2])
        r0 = E0.X * absDot01[3][2] + E0.Z * absDot01[1][2]
        r1 = E1.X * absDot01[2][3] + E1.Z * absDot01[2][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[3].
        r = math.abs(D.X * dot01[3][3] - D.Z * dot01[1][3])
        r0 = E0.X * absDot01[3][3] + E0.Z * absDot01[1][3]
        r1 = E1.X * absDot01[2][2] + E1.Y * absDot01[2][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[1].
        r = math.abs(D.Y * dot01[1][1] - D.X * dot01[2][1])
        r0 = E0.X * absDot01[2][1] + E0.Y * absDot01[1][1]
        r1 = E1.Y * absDot01[3][3] + E1.Z * absDot01[3][2]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[2].
        r = math.abs(D.Y * dot01[1][2] - D.X * dot01[2][2])
        r0 = E0.X * absDot01[2][2] + E0.Y * absDot01[1][2]
        r1 = E1.X * absDot01[3][3] + E1.Z * absDot01[3][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[3].
        r = math.abs(D.Y * dot01[1][3] - D.X * dot01[2][3])
        r0 = E0.X * absDot01[2][3] + E0.Y * absDot01[1][3]
        r1 = E1.X * absDot01[3][2] + E1.Y * absDot01[3][1]
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        return true
    end

    local function OBBContainsPoint(OBB, Point)
        local Difference = Point - OBB.Center * 0.5
        local HalfExtents = (OBB.Extents * 0.5) + Vector(0.0001, 0.0001, 0.0001)
        return (math.abs(Difference:Dot(OBB.Axis[1])) <= HalfExtents.X) and (math.abs(Difference:Dot(OBB.Axis[2])) <= HalfExtents.Y) and (math.abs(Difference:Dot(OBB.Axis[3])) <= HalfExtents.Z)
    end

    local Corners = {Vector(0.5,0.5,0.5),
                    Vector(-0.5,0.5,0.5),
                    Vector(-0.5,-0.5,0.5),
                    Vector(-0.5,-0.5,-0.5),
                    Vector(0.5,-0.5,-0.5),
                    Vector(0.5,0.5,-0.5),
                    Vector(0.5,-0.5,0.5),
                    Vector(-0.5,0.5,-0.5)}

    local function AABBIsInsideOBB(Pos, Extents, OBB)
        for i,k in ipairs(Corners) do
            if not OBBContainsPoint(OBB, Pos + (k * Extents)) then
                return false
            end
        end

        return true
    end

	--Check if anything is inside the Octant. Returns if there's anything in the octant and
	--if the bounding box found completely overlaps the octant itself.
	--	Returns: boolean hasContents, boolean objectEnvelops
    function SC.CoreOctree.Octant:Check(Bounds)
        local HasContent, IsInside = false, false

		for i, k in pairs(Bounds) do
            local Intersects = SpheresIntersect(self.Position, k.Center * 0.5, self.Radius, k.Radius)
            if Intersects then
                if AABBOBBIntersect(self.Position, self.Size, k) then
                    HasContent = true
                    IsInside = AABBIsInsideOBB(self.Position, self.Size, k)

                    if IsInside then
                        return true, IsInside
                    end
                end
            end
        end

        return HasContent, IsInside
    end

    function SC.CoreOctree.Octant:Subdivide()
        local Origin = self.Position
        local NewScanVolume = self.Size * 0.5
        local Dist = NewScanVolume * 0.5

        local Children = {}
        Children[1] = SC.CoreOctree.Octant:New(Origin + Vector(Dist.x, Dist.y, Dist.z), self.Axis, NewScanVolume, false, self)
        Children[2] = SC.CoreOctree.Octant:New(Origin + Vector(Dist.x, -Dist.y, Dist.z), self.Axis, NewScanVolume, false, self)
        Children[3] = SC.CoreOctree.Octant:New(Origin + Vector(-Dist.x, Dist.y, Dist.z), self.Axis, NewScanVolume, false, self)
        Children[4] = SC.CoreOctree.Octant:New(Origin + Vector(-Dist.x, -Dist.y, Dist.z), self.Axis, NewScanVolume, false, self)
        Children[5] = SC.CoreOctree.Octant:New(Origin + Vector(Dist.x, Dist.y, -Dist.z), self.Axis, NewScanVolume, false, self)
        Children[6] = SC.CoreOctree.Octant:New(Origin + Vector(Dist.x, -Dist.y, -Dist.z), self.Axis, NewScanVolume, false, self)
        Children[7] = SC.CoreOctree.Octant:New(Origin + Vector(-Dist.x, Dist.y, -Dist.z), self.Axis, NewScanVolume, false, self)
        Children[8] = SC.CoreOctree.Octant:New(Origin + Vector(-Dist.x, -Dist.y, -Dist.z), self.Axis, NewScanVolume, false, self)

        self.Children = Children
    end
    SC.CoreOctree.Octant.__index = SC.CoreOctree.Octant

    --Builder Functions
    function SC.CoreOctree.Builder:New(Core, Size, MaxDepth, Bounds, MaxTime, FinishedCallback)
        local obj = {}
	    setmetatable(obj,self)
        obj.Core = Core
        obj.Axis = {Core:GetForward(), Core:GetRight(), Core:GetUp()}
        obj.Size = Size * 0.5
        obj.MaxDepth = MaxDepth
        obj.MaxTime = MaxTime
        obj.Bounds = Bounds
        obj.RootOctant = SC.CoreOctree.Octant:New(Core:GetBoxCenter(), obj.Axis, obj.Size, true)
        obj.Finished = false
        obj.CurDepth = 0
        obj.CurParent = obj.RootOctant
        obj.CurOctant = obj.RootOctant
        obj.OctantIndex = {}
        obj.OctantIndex[0] = 1
        obj.FinishedCallback = FinishedCallback
        obj.StartTime = -1
        obj.StopTime = -1
        obj.Volume = 0

        if SC.Debug and SC.DebugLevel <= 3 then
            local Min = -obj.Size
            local Max = obj.Size

            debugoverlay.SweptBox(Core:LocalToWorld(Core:GetBoxCenter()), Core:LocalToWorld(Core:GetBoxCenter()), Min, Max, Core:GetAngles(), 10, Color(255,0,255,255))
        end

        return obj
    end

    function SC.CoreOctree.Builder:Rebuild(running)
        if not running then self.StartTime = SysTime() end
        local Finished = self.Finished
        local FinishTime = SysTime() + self.MaxTime
        local Time = SysTime()

        while Time < FinishTime and not Finished and IsValid(self.Core) do
            Time = SysTime()

            --Check if this octant has content
			local HasContent, IsEnveloped = self.CurOctant:Check(self.Bounds)

            --If we have content in the current octant, figure out if its blocked or needs to subdivide
            if HasContent then
                --We are at max depth or are completely blocked, pop back up a level
                if IsEnveloped or self.CurDepth >= self.MaxDepth then
                    self.CurOctant.IsBlocked = true
                    self.Volume = self.Volume + (self.CurOctant.Size.x * self.CurOctant.Size.y * self.CurOctant.Size.z)

                    if SC.Debug and SC.DebugLevel <= 3 then
                        local Center = self.CurOctant.Position
                        local Size = self.CurOctant.Size
                        local Min = Center - Vector(Size, Size, Size)
                        local Max = Center + Vector(Size, Size, Size)
                        local Col = Color(255,255,255)
                        if self.CurDepth < self.MaxDepth then
                            Col = Color(255, 0, 0)
                        end
                        debugoverlay.SweptBox(self.Core:LocalToWorld(Center - self.Core:GetBoxCenter()), self.Core:LocalToWorld(Center - self.Core:GetBoxCenter()), Min, Max, self.Core:GetAngles(), 10, Col)
                    end
                --Else subdivide and iterate downwards
                else
                    self.CurOctant:Subdivide()
                    self.CurDepth = self.CurDepth + 1

                    self.OctantIndex[self.CurDepth] = 0
                    self.CurParent = self.CurOctant
                end
            end

            --If the current octant is a leaf or is blocked, go back up the tree and look for the next octant
            if not HasContent or self.CurOctant.IsBlocked then
                --Iterate back up the Octree
                while self.OctantIndex[self.CurDepth] >= 8 do
                    self.CurDepth = self.CurDepth - 1
                    self.CurOctant = self.CurParent
                    self.CurParent = self.CurOctant.Parent

                    if self.CurDepth < 1 then
                        break
                    end
                end

                if self.CurDepth < 1 then
                    Finished = true
                end
            end

            --Next Octant
            if not Finished then
                self.OctantIndex[self.CurDepth] = self.OctantIndex[self.CurDepth] + 1
                self.CurOctant = self.CurParent.Children[self.OctantIndex[self.CurDepth]]
            end
        end

        if not Finished then
            if IsValid(self.Core) then
                ErrorNoHalt("[Space Combat 2 - Octree Builder] - Built "..self.RootOctant.OctantCount.." Octants\n")
                timer.Simple(0.1, function()
                    if self then
                        self:Rebuild(true)
                    end
                end)
            else
                ErrorNoHalt("[Space Combat 2 - Octree Builder] - Failed, invalid entity")
                self.Finished = true
                self.Failed = true
                self.StopTime = SysTime()

                if self.FinishedCallback then
                    self:FinishedCallback(self)
                end
            end
        else
            self.StopTime = SysTime()
            ErrorNoHalt("[Space Combat 2 - Octree Builder] - Finished Building. Generated "..self.RootOctant.OctantCount.." Octants.\n")
            self.Finished = true

            if self.FinishedCallback then
                self:FinishedCallback(self)
            end
        end
    end
    SC.CoreOctree.Builder.__index = SC.CoreOctree.Builder

    --Global Table
    SC.CoreExplosions = {}

    --Local functions
    --Spawn an explosion somewhere on the hull of the ship
    local function HullExplosion(self, level)
	    if not IsValid(self) then return end

	    local trace = SC.GetRandomHullTrace(self)

	    if trace.Hit and IsValid(trace.Entity) and trace.Entity:GetProtector() == self then
		    --Setup Effect
		    local effect = {}
		    effect.Origin = trace.HitPos
		    effect.Normal = trace.HitNormal
		    effect.Entity = trace.Entity

		    --Small Explosion
		    if not level or level == 1 then
			    SC.CreateEffect("sc_subxpl_small0" .. math.random(1,3), effect)
		    --Medium Explosion
		    elseif level == 2 then
			    SC.CreateEffect("sc_subxpl_medium0" .. math.random(1,3), effect)
		    --Large Explosion
		    elseif level == 3 then
			    SC.CreateEffect("sc_subxpl_large0" .. math.random(1,3), effect)
		    end
	    end
    end

    --Global Functions
    --Fighter Death Sequence
    function SC.CoreExplosions.FighterDeath(self)
	    --Play a "Critial Hit"
	    local effect = {}
	    effect.Origin = self:GetPos()
	    effect.Entity = self

	    SC.CreateEffect("sc_subxpl_fightercrit", effect)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 1)

	    --Knock Around
	    local parent = self
	    if IsValid(self:GetParent()) then parent = self:GetParent() end
	    local physObj = parent:GetPhysicsObject()
	    if IsValid(physObj) then
		    physObj:AddAngleVelocity(VectorRand() * math.Rand(-90, 90))
	    end

	    --Drift for a bit, then pop
	    timer.Simple(2, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_fighter", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Frigate Death Sequence
    function SC.CoreExplosions.FrigateDeath(self)
	    local duration = 3

	    --Initial Boom
	    HullExplosion(self, 1)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 50)

	    --Schedule increasing amounts of hullbooms until death
	    local seconds = 0
	    while seconds < duration do
		    local delay = ((duration - seconds) * 0.30) + math.Rand(-0.2, 0.2)

		    --Small Explosions at first
		    if seconds < (duration * 0.85) then
			    timer.Simple(seconds + delay, function()
				    HullExplosion(self, 1)
			    end)
		    --Medium Explosions near end
		    else
			    timer.Simple(seconds + delay, function()
				    HullExplosion(self, 2)
			    end)
		    end

		    seconds = seconds + delay
	    end

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_frigate", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Capital Death Sequence
    function SC.CoreExplosions.CapitalDeath(self)
	    local duration = 20

	    --Initial Booms
		for i = 0, 2 do
			timer.Simple(0.75 * i, function()
				HullExplosion(self, 3)
			end)
		end

		--Main Death Sequence Effect and Sounds
		local effect = {}
		effect.Origin = self:GetBoxCenter()
		effect.Entity = self

		SC.CreateEffect("sc_subxpl_capitalcrit", effect)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 150)

	    --Intersperse some small to medium explosions for texture
	    local seconds = 4
	    while seconds < duration - 5 do
		    local delay = math.Rand(0.15, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, math.random(1,2))
		    end)

		    seconds = seconds + delay
	    end

		--End the sequence with some larger explosions
	    seconds = duration - 5
	    while seconds < duration - 1 do
		    local delay = math.Rand(0.15, 0.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, math.random(2,3))
		    end)

		    seconds = seconds + delay
	    end

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_capital", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Titan Death Sequence
    function SC.CoreExplosions.TitanDeath(self)
	    local duration = 30

	    --Initial Booms to get the party started
		for i = 0, 2 do
			timer.Simple(0.75 * i, function()
				HullExplosion(self, 3)
			end)
		end

		--Main Big Critical Boom and Sound Cues
		local effect = {}
		effect.Origin = self:GetBoxCenter()
		effect.Entity = self

		SC.CreateEffect("sc_subxpl_titancrit", effect)

		--Set Drag
		SC.SetDrag(self.ConWeldTable, 500)

		--Queue some big Hull Explosions for main death sequence
	    local seconds = 3
	    while seconds < 6 do
		    local delay = math.Rand(0.75, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 3)
		    end)

		    seconds = seconds + delay
	    end

		--Queue some big Hull Explosions at the end for maximum boom
	    seconds = duration - 5
	    while seconds < duration - 1.5 do
		    local delay = math.Rand(0.15, 0.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 3)
		    end)

		    seconds = seconds + delay
	    end

		--Intersperse some medium explosions for texture
	    seconds = 7.5
	    while seconds < duration - 4 do
		    local delay = math.Rand(0.35, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 2)
		    end)

		    seconds = seconds + delay
	    end

		--Queue Explosion Effect (Negative Delay to match sound effect)
		timer.Simple(duration - 2.75, function()
			if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_titan", effect)
		end)

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end
end

--Saves a ton of space in the core file
function SC.MakeCoreAccessors(ENT, name)
    if not ENT or not name then return end

    if SERVER then
        ENT["Consume"..name] = function(self, amount, dontsync)
            self["Set"..name.."Amount"](self, math.Clamp(self[name]["Amount"] - amount, 0, self[name]["Max"], dontsync))
	        self[name]["Percent"] = self[name]["Amount"]/self[name]["Max"]
        end

        ENT["Supply"..name] = function(self, amount, dontsync)
            self["Set"..name.."Amount"](self, math.Clamp(self[name]["Amount"] + amount, 0, self[name]["Max"], dontsync))
	        self[name]["Percent"] = self[name]["Amount"]/self[name]["Max"]
        end
    end

    ENT["Get"..name.."Max"] = function(self)
        return self[name]["Max"]
    end

    local maxid
    ENT["Set"..name.."Max"] = function(self, value, dontsync)
        self[name]["Max"] = value

        if not dontsync then
            --ErrorNoHalt("Sending Sync Request")
            SC.NWAccessors.SyncValue(self, maxid)
        end
    end
    SC.NWAccessors.CreateSpecialNWAccessor(ENT, name.."Max", "number", ENT["Get"..name.."Max"], ENT["Set"..name.."Max"], 1)
    maxid = SC.NWAccessors.GetNWAccessorID(ENT, name.."Max")

    ENT["Get"..name.."Percent"] = function(self)
        return self[name]["Percent"]
    end

    ENT["Get"..name.."Amount"] = function(self)
        return self[name]["Amount"]
    end

    local amountid
    ENT["Set"..name.."Amount"] = function(self, value, dontsync)
        value = math.Clamp(value,0,self[name]["Max"])

	    self[name]["Amount"] = value
	    self[name]["Percent"] = value/self[name]["Max"]

        if not dontsync then
            --ErrorNoHalt("Sending Sync Request")
            SC.NWAccessors.SyncValue(self, amountid)
        end

	    return self[name]["Percent"]
    end
    SC.NWAccessors.CreateSpecialNWAccessor(ENT, name.."Amount", "number", ENT["Get"..name.."Amount"], ENT["Set"..name.."Amount"], 2)
    amountid = SC.NWAccessors.GetNWAccessorID(ENT, name.."Amount")
end

--Picks a point on the exterior bounding box and traces inwards to find a
--point on the hull, useful for things like death explosions, damage fires,
--and other such things.
--	Returns tracedata.
local maxHullTraceAttempts = 50	--Used to prevent infinite loops, but may cause inaccurate tracing in rare cases.
function SC.GetRandomHullTrace(self)
	if not IsValid(self) then return nil end
	if self:GetClass() ~= "ship_core" then
		if IsValid(self:GetProtector()) then
			self = self:GetProtector()
		else
			return nil
		end
	end

	local boxSize = (self:GetBoxMax() - self:GetBoxMin()):Length() + 10

	--Pick a random point along the bounding box of the ship
	math.randomseed(SysTime())
	local vec = VectorRand()
	vec = vec * boxSize
	vec.x = math.Clamp(vec.x, self:GetBoxMin().x, self:GetBoxMax().x)
	vec.y = math.Clamp(vec.y, self:GetBoxMin().y, self:GetBoxMax().y)
	vec.z = math.Clamp(vec.z, self:GetBoxMin().z, self:GetBoxMax().z)
	vec = vec + self:GetBoxCenter()
	vec = self:LocalToWorld(vec)

	local trace
	local attempts = 0
	while attempts < maxHullTraceAttempts and (trace == nil or not IsValid(trace.Entity) or not IsValid(trace.Entity:GetProtector()) or trace.Entity:GetProtector() ~= self) do
		attempts = attempts + 1

		--Aim at a random part of the ship
		local part
		while not IsValid(part) do
			part = table.Random(self.ConWeldTable)
		end

		--Trace from point to part
		local dir = (part:GetPos() - vec):GetNormalized()
		local tData = {
			start = vec,
			endpos = dir * 30000,
			ignoreworld = true,
			filter = function(e)
                if not IsValid(e) then return false end

				if e:GetProtector() == self then
					return true
				end

				return false
			end
		}
		trace = util.TraceLine(tData)
	end

	return trace
end

--Set Drag on a number of entities
function SC.SetDrag(parts, coef)
	if not parts then return end

	for k,v in pairs(parts) do
		if IsValid(v) then
			local physobj = v:GetPhysicsObject()
			if IsValid(physobj) then
				v.SB_Ignore = true
				physobj:EnableDrag(true)
				physobj:SetDragCoefficient(coef)
			end
		end
	end
end

function SC.GetWeldedEntities( ent, ResultTable )
	local ResultTable = ResultTable or {}

	if not IsValid(ent) then return end
	if IsValid( ResultTable[ ent ] ) then return end

	ResultTable[ ent ] = ent

	local ConTable = constraint.GetTable( ent )

	for k, con in ipairs( ConTable ) do

		for EntNum, Ent in pairs( con.Entity ) do
			if con.Type == "Weld" then
				SC.GetWeldedEntities(Ent.Entity, ResultTable)
			end
		end

	end

	return ResultTable
end

function SC.CalcPercent(Start, Amounts, Mult, Clamp, min, max)
    local Mult = Mult or 1
    local ret = 0

    for i, k in pairs(Amounts) do
        ret = ret + ((k/100 * Mult) * (1/i^1.45))
    end

    if Clamp then ret = math.Clamp(ret, min, max) end

    if Start == 0 then
        return ret
    end

    return Start + (Start * ret)
end

local MaxResist = 90
function SC.CalcResist(Start, Amounts)
    local ret = Start*100
    for i,k in pairs(Amounts) do
        ret = ret + (k * (0.5/i^1.2))
    end

    return math.Clamp(ret, -MaxResist, MaxResist)/100
end