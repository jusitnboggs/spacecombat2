--[[
		SPACE COMBAT 2 - Simple Asteroid Cloud

	A cuboid region in which asteroids can spawn.
--]]

--[[ EXAMPLE FEATURE DEFINITION:

[Feature Readable Name]
Type=Simple Cloud			--Feature Type
Pos=X,Y,Z					--Center point of feature
MaxCount=##					--Maximum number of roids that can spawn here
XRandom=####				--How far from center that asteroids can spawn on the X axis
YRandom=####				-- """ Y axis
ZRandom=####				-- """ Z axis
Ignore Atmospheres=Bool		-- If 0, roids that would spawn in an atmosphere are culled

]]--

local GM = GAMEMODE
local SPAWNER = {}

local AsteroidMineable = GM.Mining.Mineables.Asteroids
local cvar_asteroidRespawn = GetConVar("mining_asteroidRespawnEnable")
local cvar_asteroidRespawnDelay = GetConVar("mining_asteroidRespawnDelay")


function SPAWNER:Setup()
	--Stub
end

function SPAWNER:Initialize(feature)
	for i=1, feature.MaxCount do
		self:Spawn(feature)
	end
end

function SPAWNER:LoadFromFile(name, data)
	local feature = {}

	ErrorNoHalt("[SC2 MINING] - Adding a Simple Cloud Feature.\n")

	--Check for validity
	local didError = false
	if not data["Pos"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Pos argument!\n") didError = true end
	if not data["MaxCount"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing MaxCount argument!\n") didError = true end
	if not data["XRandom"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing XRandom argument!\n") didError = true end
	if not data["YRandom"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing YRandom argument!\n") didError = true end
	if not data["ZRandom"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing ZRandom argument!\n") didError = true end
	if not data["Ignore Atmospheres"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Ignore Atmospheres argument!\n") didError = true end

	--If all went well, process and add
	if not didError then
		feature.Name = name
		feature.Spawner = data["Type"]
		feature.Pos = SC.StringToVector(data["Pos"])
		feature.MaxCount = tonumber(data["MaxCount"])
		feature.XRandom = tonumber(data["XRandom"])
		feature.YRandom = tonumber(data["YRandom"])
		feature.ZRandom = tonumber(data["ZRandom"])
		feature.IgnoreAtmospheres = (tonumber(data["Ignore Atmospheres"]) ~= 0)

		feature.Children = {}
		feature.NextThink = CurTime()

		return feature
	end

	return nil
end

function SPAWNER:Cleanup(feature)
	for k,v in pairs(feature.Children) do
		if IsValid(v) then
			v:Remove()
		end
	end
end

function SPAWNER:Spawn(feature)
	local attempts = 0
	local pos = Vector()
	while pos == vector_origin or (not feature.IgnoreAtmospheres and GM:GetAtmosphereAtPoint(pos) ~= GM:GetSpace()) or not util.IsInWorld(pos) do
		pos = Vector(math.Rand(-feature.XRandom, feature.XRandom),
					 math.Rand(-feature.YRandom, feature.YRandom),
					 math.Rand(-feature.ZRandom, feature.ZRandom))
		pos = pos + feature.Pos

		attempts = attempts + 1
		if attempts > 25 then
			return false
		end
	end

	local roid = AsteroidMineable:Spawn(pos)
	if not roid then return false end

	--One final check to see if this roid is stuck inside something
	local tr = {}
	tr.start = pos
	tr.endpos = pos
	tr.maxs = roid:OBBMaxs()
	tr.mins = roid:OBBMins()
	tr.filter = roid
	tr.ignoreworld = false
	if util.TraceHull(tr).Hit then
		roid:Remove()
		return false
	end

	table.insert(feature.Children, roid)
	return true
end

function SPAWNER:Think(feature)
	if #feature.Children < feature.MaxCount then
		self:Spawn(feature)
	end

	feature.NextThink = CurTime() + cvar_asteroidRespawnDelay:GetFloat()
end


GM.Mining.RegisterSpawner("Simple Cloud", SPAWNER)