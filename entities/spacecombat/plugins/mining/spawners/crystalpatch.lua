--[[
		SPACE COMBAT 2 - Crystal Field

	Standard conic-spawning crystal patch.
--]]

--[[ EXAMPLE Feature DEFINITION:

[Feature Readable Name]
Type=Crystal Field			--Feature Type
Pos=X,Y,Z					--Location of Parent crystal. Traces straight down to actually spawn unless Normal is defined.
Normal=#,#,#				--Up direction of Parent crystal. Defaults to hitnormal of down-trace from Pos.
MaxCount=###				--Maximum number of child crystals.
ScanHeight=###				--Vertex of scan cone.
RespawnRate=###				--How often to spawn a new child crystal.
MinInitialSpawn=##			--Minimum number of crystals to spawn during initial frenzy.
FieldHealth=#######			--How much can be mined from this field before the Parent stops spawning new children.
CrystalMaterialColor=Vector(4, 0.3, 0)    -- Color of the material on the crystal
CrystalEnvColor=Vector(0.5, 0.12, 0.12)   -- Color of the reflections in the material
CrystalParticleColor=Color(255, 0, 0, 65) -- Color of the particles around the crystal
Ignore Atmospheres=Bool		--If true, doesn't attempt to clamp away from edge of planet. Required for cuboid atmospheres.

]]--

local GM = GAMEMODE
local SPAWNER = {}

local CrystalMineable = GM.Mining.Mineables.Crystals
local cvar_crystalFrenzyDelay = GetConVar("mining_crystalFrenzyDelay")
local cvar_crystalFrenzyTimeout = GetConVar("mining_crystalFrenzyTimeout")
local cvar_crystalRespawnDelay = GetConVar("mining_crystalRespawnDelay")


function SPAWNER:Setup()
	--Stub
end

function SPAWNER:Initialize(Feature)
	self:SpawnParent(Feature)
	Feature.NextThink = CurTime() + 10
end

function SPAWNER:LoadFromFile(Name, Data)
	local Feature = {}

	ErrorNoHalt("[SC2 MINING] - Adding a Crystal Field Feature.\n")

	--Check for Validity
	local DidError = false
	if not Data["Pos"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..Name.." missing Pos argument!\n") DidError = true end
	if not Data["MaxCount"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..Name.." missing MaxCount argument!\n") DidError = true end
	if not Data["Ignore Atmospheres"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..Name.." missing Ignore Atmospheres argument!\n") DidError = true end

	--Process and Add
	if not DidError then
		Feature.Name = Name
		Feature.Spawner = Data["Type"]
		Feature.Pos = SC.StringToVector(Data["Pos"])
		Feature.Normal = SC.StringToVector(Data["Normal"] or "0,0,0")
		Feature.MaxCount = tonumber(Data["MaxCount"])
		Feature.MaxTraceAngle = tonumber(Data["MaxTraceAngle"]) or 90
		Feature.MaxDistance = tonumber(Data["MaxDistance"]) or -1
		Feature.ScanHeight = tonumber(Data["ScanHeight"]) or 500
		Feature.RespawnRate = tonumber(Data["RespawnRate"]) or -1
		Feature.MinInitialSpawn = (tonumber(Data["MinInitialSpawn"])) or (Data["MaxCount"] * 0.25)
		Feature.FieldHealth = tonumber(Data["FieldHealth"]) or -1
        Feature.CrystalMaterialColor = Data["CrystalMaterialColor"] or Vector(4, 0.3, 0)
        Feature.CrystalEnvColor = Data["CrystalEnvColor"] or Vector(0.5, 0.12, 0.12)
        Feature.CrystalParticleColor = Data["CrystalParticleColor"] or Color(255, 0, 255, 65)
		Feature.IgnoreAtmospheres = (tonumber(Data["Ignore Atmospheres"]) ~= 0)
		Feature.IgnoreDisplacement = (tonumber(Data["Ignore Displacement"]) ~= 0)

		Feature.Children = {}
		Feature.NextThink = CurTime()
		Feature.PreviousFailures = 0
		Feature.FrenzyLastSpawn = 1e30

		return Feature
    end

	return nil
end

function SPAWNER:Cleanup(Feature)
	for k,v in pairs(Feature.Children) do
		if IsValid(v) then
			v:Remove()
		end
	end
end

function SPAWNER:SpawnParent(Feature)
	local pos = Feature.Pos
	local normal = Feature.Normal

	--Trace downwards if no "normal" given
	if not normal or normal == vector_origin then
		local traceData = {
			start = pos,
			endpos = pos + Vector(0, 0, -10000),
			mask = MASK_NPCWORLDSTATIC
		}
		local trace = util.TraceLine(traceData)

		if trace.Hit then
			pos = trace.HitPos
			normal = trace.HitNormal
		end
	end

	local ang = normal:Angle() + Angle(90, 0, 0)
	local Parent = CrystalMineable:Spawn(pos, ang, true, Feature)

	if Parent and IsValid(Parent) then
		Feature.Planet = GM:GetAtmosphereAtPoint(pos)
		Feature.Parent = Parent

		--Determine scan origin
		local traceData = {
			start = pos + Parent:GetUp() * 100,
			endpos = pos + (Parent:GetUp() * Feature.ScanHeight),
			mask = MASK_NPCWORLDSTATIC
		}
		local trace = util.TraceLine(traceData)

		Feature.ScanOrigin = trace.HitPos + (trace.HitNormal * 100)
		Feature.ScanHeight = trace.HitPos:Distance(Feature.Pos)

		Feature.FrenzyMode = true
		Feature.FrenzyEnd = math.Rand(Feature.MinInitialSpawn / Feature.MaxCount, 1)

		Feature.ParentSpawned = true
	else
		SC.Error("[SC2 MINING] - ERROR: Could not spawn Parent crystal for Crystal Field Feature "..Feature.Name.."!")
	end
end

function SPAWNER:Spawn(Feature)
	local attempts = 0
	local maxAttempts = 10
	local maxDist = Feature.MaxDistance
	local Parent = Feature.Parent
	while attempts < maxAttempts do
		attempts = attempts + 1

		--Pick Direction
		local maxTraceAng = Feature.MaxTraceAngle * (Feature.PreviousFailures / 20)
		local dir = Parent:GetUp() * -1
		local ang = dir:Angle()

		math.randomseed(SysTime())
		ang:RotateAroundAxis(Parent:GetRight(), math.Rand(-maxTraceAng, maxTraceAng))
		ang:RotateAroundAxis(Parent:GetUp(), math.Rand(0, 360))

		dir = ang:Forward()

		--Trace check
		local startPos = Feature.ScanOrigin
		local traceData = {
			start = startPos,
			endpos = startPos + (dir * 30000),
			mask = MASK_NPCWORLDSTATIC
		}
		local trace = util.TraceLine(traceData)

		--Check if we're within the bounds of the planet
		local canSpawn = false
		local planet = Feature.Planet

		if not planet or Feature.IgnoreAtmospheres or planet:GetType() == "cube" then
			canSpawn = true
		else
			local planetEnt = planet:GetCelestial():getEntity()
			local planetRadius = planet:GetRadius() - (planet:GetRadius() * 0.1)
			local distancePlanet = trace.HitPos:DistToSqr(planetEnt:GetPos())
			local distanceParent = trace.HitPos:DistToSqr(Parent:GetPos())
			if distancePlanet < (planetRadius * planetRadius) and (maxDist == -1 or distanceParent < (maxDist * maxDist)) then
				canSpawn = true
			end
		end

		if canSpawn then
			--Check if this is spawnable ground
			if Feature.IgnoreDisplacement or string.find(trace.HitTexture, "displacement") ~= nil then
				local pos = trace.HitPos
				local ang = trace.HitNormal:Angle() + Angle(90, 0, 0)
				local child = CrystalMineable:Spawn(pos, ang, false, Feature)

				if child then
					table.insert(Feature.Children, child)

					Feature.PreviousFailures = 0
					return true
				end
			end
		end
	end

	Feature.PreviousFailures = Feature.PreviousFailures + 1
	return false
end

function SPAWNER:Think(Feature)
	local numChildren = #Feature.Children

	--Check to die
	if not Feature.Parent or not IsValid(Feature.Parent) then
		--Did it break or did it just not exist yet?
		if Feature.ParentSpawned then
			--Any children to eat?
			if numChildren <= 0 then
				--Kill Feature
				GM.Mining.Features[Feature.Name] = nil

				return

			--Start eating children
			else
				local max = math.random(1, math.max(numChildren * 0.05, 3))
				for i=1, max do
					local child = table.Random(Feature.Children)
					if IsValid(child) then
						child:Decay(math.Rand(3,8))
					end
				end
			end
		end

		Feature.NextThink = CurTime() + 1
		return
	end

	--Spawn new stuff
	if numChildren < Feature.MaxCount then
		local success = self:Spawn(Feature)

		--Frenzy Mode Calculations
		if Feature.FrenzyMode then
			if success then
				Feature.FrenzyLastSpawn = CurTime()
			end

			--Check if we're done
			local percent = numChildren / Feature.MaxCount
			if percent >= Feature.FrenzyEnd or CurTime() > Feature.FrenzyLastSpawn + cvar_crystalFrenzyTimeout:GetFloat() then
				Feature.FrenzyMode = false
				Feature.NextThink = CurTime() + cvar_crystalRespawnDelay:GetFloat()
			else
				--Continue frenzy!
				Feature.NextThink = CurTime() + cvar_crystalFrenzyDelay:GetFloat()
			end
		else
			Feature.NextThink = CurTime() + cvar_crystalRespawnDelay:GetFloat()
		end
	else
		Feature.NextThink = CurTime() + cvar_crystalRespawnDelay:GetFloat()
	end
end


GM.Mining.RegisterSpawner("Crystal Field", SPAWNER)