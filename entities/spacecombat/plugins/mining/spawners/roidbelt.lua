--[[
		SPACE COMBAT 2 - Asteroid Belt

	A varied line from which asteroids can spawn.
--]]

--[[ EXAMPLE FEATURE DEFINITION:

[Feature Readable Name]
Type=Belt					--Feature Type
Pos1=X,Y,Z					--Start point of feature
Pos2=X,Y,Z					--End point of feature
MaxCount=##					--Maximum number of roids that can spawn here
Variance=####				--Maximum distance from line that roids can spawn
Ignore Atmospheres=Bool		-- If 0, roids that would spawn in an atmosphere are culled

]]--

local GM = GAMEMODE
local SPAWNER = {}

local AsteroidMineable = GM.Mining.Mineables.Asteroids
local cvar_asteroidRespawn = GetConVar("mining_asteroidRespawnEnable")
local cvar_asteroidRespawnDelay = GetConVar("mining_asteroidRespawnDelay")


function SPAWNER:Setup()
	--Stub
end

function SPAWNER:Initialize(feature)
	for i=1, feature.MaxCount do
		self:Spawn(feature)
	end
end

function SPAWNER:LoadFromFile(name, data)
	local feature = {}

	ErrorNoHalt("[SC2 MINING] - Adding a Line Belt Feature.\n")

	--Check for Validity
	local didError = false
	if not data["Pos1"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Pos1 argument!\n") didError = true end
	if not data["Pos2"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Pos2 argument!\n") didError = true end
	if not data["MaxCount"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing MaxCount argument!\n") didError = true end
	if not data["Variance"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Variance argument!\n") didError = true end
	if not data["Ignore Atmospheres"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Ignore Atmospheres argument!\n") didError = true end

	--Process and Add
	if not didError then
		feature.Name = name
		feature.Spawner = data["Type"]
		feature.Pos1 = SC.StringToVector(data["Pos1"])
		feature.Pos2 = SC.StringToVector(data["Pos2"])
		feature.MaxCount = tonumber(data["MaxCount"])
		feature.Variance = tonumber(data["Variance"])
		feature.IgnoreAtmospheres = (tonumber(data["Ignore Atmospheres"]) ~= 0)

		feature.Children = {}
		feature.NextThink = CurTime()

		return feature
	end

	return nil
end

function SPAWNER:Cleanup(feature)
	for k,v in pairs(feature.Children) do
		if IsValid(v) then
			v:Remove()
		end
	end
end

function SPAWNER:Spawn(feature)
	local attempts = 0
	local pos = Vector()
	while pos == vector_origin or (not feature.IgnoreAtmospheres and GM:GetAtmosphereAtPoint(pos) ~= GM:GetSpace()) or not util.IsInWorld(pos) do
		local variance = feature.Variance
		local pos1 = feature.Pos1
		local pos2 = feature.Pos2

		--Get position on line
		local segMult = math.Rand(0, 100) * 0.01
		local segment = (pos2 - pos1) * segMult

		pos = (pos1 + segment) + Vector(math.Rand(-variance, variance), math.Rand(-variance, variance), math.Rand(-variance, variance))

		attempts = attempts + 1
		if attempts > 25 then
			return false
		end
	end

	local roid = AsteroidMineable:Spawn(pos)
	if not roid then return false end

	--One final check to see if this roid is stuck inside something
	local tr = {}
	tr.start = pos
	tr.endpos = pos
	tr.maxs = roid:OBBMaxs()
	tr.mins = roid:OBBMins()
	tr.filter = roid
	tr.ignoreworld = false
	if util.TraceHull(tr).Hit then
		roid:Remove()
		return false
	end

	table.insert(feature.Children, roid)
	return true
end

function SPAWNER:Think(feature)
	if #feature.Children < feature.MaxCount then
		self:Spawn(feature)
	end

	feature.NextThink = CurTime() + cvar_asteroidRespawnDelay:GetFloat()
end


GM.Mining.RegisterSpawner("Belt", SPAWNER)