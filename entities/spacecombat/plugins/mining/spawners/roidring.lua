--[[
		SPACE COMBAT 2 - Simple Asteroid Cloud

	A cuboid region in which asteroids can spawn.
--]]

--[[ EXAMPLE FEATURE DEFINITION:

[Feature Readable Name]
Type=Ring					--Feature Type
Pos=X,Y,Z					--Center point of feature
Radius=####					--Maximum width of ring
InnerRadius=####			--Radius in which no asteroids will spawn
Inclination=##				--"Pitch" of the ring
Azimuth=##					--"Yaw" of the ring
Thickness=###				--How far roids can spawn from the plane of the ring
MaxCount=##					--Maximum number of roids that can spawn here
Ignore Atmospheres=Bool		-- If 0, roids that would spawn in an atmosphere are culled

]]--

local GM = GAMEMODE
local SPAWNER = {}

local AsteroidMineable = GM.Mining.Mineables.Asteroids
local cvar_asteroidRespawn = GetConVar("mining_asteroidRespawnEnable")
local cvar_asteroidRespawnDelay = GetConVar("mining_asteroidRespawnDelay")


function SPAWNER:Setup()
	--Stub
end

function SPAWNER:Initialize(feature)
	for i=1, feature.MaxCount do
		self:Spawn(feature)
	end
end

function SPAWNER:LoadFromFile(name, data)
	local feature = {}

	ErrorNoHalt("[SC2 MINING] - Adding a Planetary Ring Feature.\n")

	--Check for Validity
	local didError = false
	if not data["Pos"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Pos argument!\n") didError = true end
	if not data["MaxCount"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing MaxCount argument!\n") didError = true end
	if not data["Radius"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Radius argument!\n") didError = true end
	if not data["InnerRadius"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing InnerRadius argument!\n") didError = true end
	if not data["Inclination"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Inclination argument!\n") didError = true end
	if not data["Azimuth"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Azimuth argument!\n") didError = true end
	if not data["Thickness"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Thickness argument!\n") didError = true end
	if not data["Ignore Atmospheres"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..data["Name"].." missing Ignore Atmospheres argument!\n") didError = true end

	--Process and Add
	if not didError then
		feature.Name = name
		feature.Spawner = data["Type"]
		feature.Pos = SC.StringToVector(data["Pos"])
		feature.Radius = tonumber(data["Radius"])
		feature.InnerRadius = tonumber(data["InnerRadius"])
		feature.Inclination = tonumber(data["Inclination"])
		feature.Azimuth = tonumber(data["Azimuth"])
		feature.Thickness = tonumber(data["Thickness"])
		feature.MaxCount = tonumber(data["MaxCount"])
		feature.IgnoreAtmospheres = (tonumber(data["Ignore Atmospheres"]) ~= 0)

		--Determine Feature "Up" direction
		feature.Up = Vector(0, 0, 1)
		feature.Forward = Vector(1, 0, 0)
		feature.Up:Rotate(Angle(feature.Inclination, feature.Azimuth, 0))
		feature.Forward:Rotate(Angle(feature.Inclination, feature.Azimuth, 0))

		feature.Children = {}
		feature.NextThink = CurTime()

		return feature
	end

	return nil
end

function SPAWNER:Cleanup(feature)
	for k,v in pairs(feature.Children) do
		if IsValid(v) then
			v:Remove()
		end
	end
end

function SPAWNER:Spawn(feature)
	local attempts = 0
	local pos = Vector()
	while pos == vector_origin or (not feature.IgnoreAtmospheres and GM:GetAtmosphereAtPoint(pos) ~= GM:GetSpace()) or not util.IsInWorld(pos) do
		local outerRadius = feature.Radius
		local innerRadius = feature.InnerRadius
		local thickness = feature.Thickness

		--Feature Angles/Directions
		local up = feature.Up
		local forward = feature.Forward

		--Spin forward angle
		local forwardAng = forward:Angle()
		forwardAng:RotateAroundAxis(up, math.Rand(0, 360))

		--Set Distance
		local distance = math.Rand(innerRadius, outerRadius)

		pos = feature.Pos + (forwardAng:Forward() * distance) + (up * (thickness * math.Rand(-1, 1)))

		attempts = attempts + 1
		if attempts > 25 then
			return false
		end
	end

	local roid = AsteroidMineable:Spawn(pos)
	if not roid then return false end

	--One final check to see if this roid is stuck inside something
	local tr = {}
	tr.start = pos
	tr.endpos = pos
	tr.maxs = roid:OBBMaxs()
	tr.mins = roid:OBBMins()
	tr.filter = roid
	tr.ignoreworld = false
	if util.TraceHull(tr).Hit then
		roid:Remove()
		return false
	end

	table.insert(feature.Children, roid)
	return true
end

function SPAWNER:Think(feature)
	if #feature.Children < feature.MaxCount then
		self:Spawn(feature)
	end

	feature.NextThink = CurTime() + cvar_asteroidRespawnDelay:GetFloat()
end


GM.Mining.RegisterSpawner("Ring", SPAWNER)