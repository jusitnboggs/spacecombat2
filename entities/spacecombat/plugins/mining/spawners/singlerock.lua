--[[
		SPACE COMBAT 2 - Single Terrestrial Rock

	A big fat rock, always defined where it is. Useful
	for spawn planet resources.
--]]

--[[ EXAMPLE FEATURE DEFINITION:

[Feature Readable Name]
Type=Rock Single				--Feature Type
Pos=X,Y,Z						--Center point of feature
Normal=X,Y,Z					--Up direction of this rock. Don't define to auto-trace to ground.
Yaw=###							--Relative Yaw angle of this rock. Don't define to randomize.
Rock Type=Rock Name				--Defined Rock name to use. Don't define to randomize.
ResourceOverride=Name:Amount	--Manually specify what this feature has stored. Can be a comma-separated list.
								--	If not defined, will pull from given Rock definition.

]]--

local GM = GAMEMODE
local SPAWNER = {}

local RockMineable = GM.Mining.Mineables.Rocks


function SPAWNER:Setup()
	--Stub
end

function SPAWNER:Initialize(feature)
	self:Spawn(feature)
end

function SPAWNER:LoadFromFile(name, data)
	local feature = {}

	ErrorNoHalt("[SC2 MINING] - Adding a Single Rock Feature.\n")

	--Check for validity
	local didError = false
	if not data["Pos"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..name.." missing Pos argument!\n") didError = true end

	--If all went well, process and add
	if not didError then
		feature.Name = name
		feature.Spawner = data["Type"]
		feature.Pos = SC.StringToVector(data["Pos"])
		feature.Normal = SC.StringToVector(data["Normal"] or "0,0,0")
		feature.Yaw = tonumber(data["Yaw"])
		feature.RockType = RockMineable:GetTypeByName(data["Rock Type"])

		--Process resource override
		local resourceTable
		if data["ResourceOverride"] then
			local resourceOverride = SC.StringToTable(data["ResourceOverride"])
            resourceTable = {}

			for _,resourceStr in ipairs(resourceOverride) do
				local resourceData = string.Explode(":", resourceStr)
				local resource = resourceData[1]
				local amount = tonumber(resourceData[2]) or 0

				if not GM:GetResourceTypes()[resource] then
					ErrorNoHalt("[SC2 MINING] - WARNING: Invalid resource "..resource.." in ResourceOverride table for entry "..feature.Name.."!\n")
				else
					resourceTable[resource] = amount
				end
			end
		end

		feature.ResourceOverride = resourceTable

		feature.Children = {}
		feature.NextThink = CurTime()

		return feature
	end

	return nil
end

function SPAWNER:Cleanup(feature)
	for k,v in pairs(feature.Children) do
		if IsValid(v) then
			v:Remove()
		end
	end
end

function SPAWNER:Spawn(feature)
    local TotalResources
    if feature.ResourceOverride then
        TotalResources = 0
        for i,k in pairs(feature.ResourceOverride) do
            TotalResources = TotalResources + k
        end
    end

	local rock = RockMineable:Spawn(feature.Pos, feature.RockType, feature.Normal, feature.Yaw, feature.ResourceOverride, TotalResources)
	if not rock then return false end

	table.insert(feature.Children, rock)
	return true
end

function SPAWNER:Think(feature)
	--Rocks don't think, they sit there
end


GM.Mining.RegisterSpawner("Rock Single", SPAWNER)