--[[
		SPACE COMBAT 2 - CRYSTAL DEFINITIONS

	Loads in the crystal definition files from
	data/spacecombat2/mining/crystals.
--]]

--[[ EXAMPLE CRYSTAL DEFINITION:

[Crystal Name]
Model=path/to/model.mdl				--Model to use for crystal.
PossibleElements=Polonium Nitrate	--Resources that can be included when spawning.
MinOre=#####						--Minimum amount of mineable material possible.
MaxOre=######						--Maximum amount that this crystal can have spawned.
IsParent=Bool						--IsParent=1 marks this to be used as a Parent node, will not spawn as a child.
Size=##								--Footprint radius of this crystal, used to hulltrace around to see if it will collide with other crystals.

]]--


local GM = GAMEMODE
local MINEABLE = {}
MINEABLE.Types = {}
MINEABLE.ParentTypes = {}

require("lip")

function MINEABLE:Setup()
	self:LoadFromData()
end

function MINEABLE:Cleanup()
	ErrorNoHalt("BYE! D:\n")
end

function MINEABLE:LoadFromData()
	local crysCount = 0
	local crysDefinitions = file.Find(SC.DataFolder.."/mining/crystals/*.txt", "DATA")

	for _,file in ipairs(crysDefinitions) do
		local INI = lip.load(SC.DataFolder.."/mining/crystals/"..file)

		--Parse Crystal Info
		for node, data in pairs(INI) do
			ErrorNoHalt("[SC2 MINING] - Loading Crystal Type "..node..".\n")

			local didError = false

			--Check for broken configs
			if not util.IsValidModel(data["Model"]) then ErrorNoHalt("[SC2 MINING] - ERROR: Model for entry "..node.." is invalid!\n") didError = true end
			if not data["PossibleElements"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing PossibleElements table!\n") didError = true end
			if not data["MinOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MinOre argument!\n") didError = true end
			if not data["MaxOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MaxOre argument!\n") didError = true end
			if not data["Size"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing Size argument!\n") didError = true end

			--Parse Element Table
			local elementData = SC.StringToTable(data.PossibleElements)
			local elements = {}
			for _,element in ipairs(elementData) do
				if not GM:GetResourceTypes()[element] then
					ErrorNoHalt("[SC2 MINING] - WARNING: Invalid resource "..element.." in Elements table for entry "..node.."!\n")
				else
					table.insert(elements, element)
				end
			end

			if #elements <= 0 then ErrorNoHalt("[SC2 MINING] - ERROR: No valid resources defined for entry "..node.."!\n") didError = true end


			--Process and Add
			if not didError then
				data.Name = node
				data.PossibleElements = elements
				data.MinOre = tonumber(data.MinOre)
				data.MaxOre = tonumber(data.MaxOre)
                data.MiningLevel = tonumber(data.MiningLevel) or MININGLEVEL_COMMON

				if data.IsParent then
					table.insert(self.ParentTypes, data)
				else
					table.insert(self.Types, data)
				end
				crysCount = crysCount + 1
			end
		end
	end

	ErrorNoHalt("[SC2 MINING] - Cached "..crysCount.." crystal types.\n\n")
end

function MINEABLE:GetTypeByName(name)
	if not name then return nil end

	for i,v in ipairs(self.Types) do
		if v.Name == name then
			return i
		end
	end

	ErrorNoHalt("[SC2 MINING] - WARNING: Attempted to get Crystal type from non-indexed name "..name.."!\n")
	return nil
end

--Generate Random Resources based on Crystal Type
function MINEABLE:GenerateResources(crystalData)
	if not crystalData then
		SC.Error("[SC2 MINING] - ERROR: Attempted to generate crystal resources using invalid data table!")
		return {}
	end

	local resources = {}
    local resourceAmount = math.random(crystalData.MinOre, crystalData.MaxOre)
    local totalAmount = resourceAmount
	local resourceNum = #crystalData.PossibleElements

	for i = 1, resourceNum do
		local resource = table.Random(crystalData.PossibleElements)
		local amount = math.random(0, resourceAmount)

		--If last loop, then just plop the rest in
		if i == resourceNum then
			amount = resourceAmount
		end

		resources[resource] = (resources[resource] or 0) + amount
		resourceAmount = resourceAmount - amount
	end

	return totalAmount, resources
end

--Spawn the Deathy Crystal
function MINEABLE:Spawn(pos, ang, isParent, Feature, resources, resourceAmount)
	math.randomseed(SysTime())

	--Retreive Crystal info
	local crystalData
	if isParent then
		crystalData = table.Random(self.ParentTypes)
	else
		crystalData = table.Random(self.Types)
	end

	local crystal
	if isParent then
		crystal = ents.Create("mining_mineral_parent")
	else
		crystal = ents.Create("mining_mineral")
	end

    crystal:SetModel(crystalData.Model)

	--Check for collisions on children
	if not isParent then
		--Hull trace to see if there's any nearby crystals in the way
		local halfSize = crystalData.Size * 0.75
		local hullTraceData = {
			start = pos - (ang:Up() * halfSize),
			endpos = pos + (ang:Up() * halfSize),
			mins = Vector(-halfSize, -halfSize, -halfSize),
			maxs = Vector(halfSize, halfSize, halfSize),
			ignoreworld = true,
			filter = player.GetAll()
		}
		local hullTrace = util.TraceHull(hullTraceData)

		--Just clean up and escape
		if hullTrace.Hit then
			crystal:Remove()
			return nil
		end
    end

	crystal:SetPos(pos)

	--Randomize Yaw
	crystal:SetAngles(ang)
	crystal:SetAngles(crystal:LocalToWorldAngles(Angle(0, math.Rand(0,360), 0)))

	--Randomize height slightly
	local height = math.Clamp(crystal:OBBMaxs().z, 5, 20)
	crystal:SetPos(crystal:LocalToWorld(Vector(0, 0, math.Rand(height * -0.25, height))))

    crystal:Spawn()

    crystal:SetMiningLevel(crystalData.MiningLevel)

	if not resources then
		resourceAmount, resources = self:GenerateResources(crystalData)
    end

	crystal:CreateStorage("Cargo", resourceAmount, resources)

    timer.Simple(0.1, function()
        crystal:SetMatColor(Feature.CrystalMaterialColor)
        crystal:SetEnvColor(Feature.CrystalEnvColor)
        crystal:SetParticleColor(Feature.CrystalParticleColor)
    end)

    crystal.Feature = Feature

	return crystal
end

GM.Mining.RegisterMineable("Crystals", MINEABLE)