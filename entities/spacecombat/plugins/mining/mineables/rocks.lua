--[[
		SPACE COMBAT 2 - ROCK DEFINITIONS

	Loads in the mineable rock definition files
	from data/spacecombat2/mining/rocks.
--]]

--[[ EXAMPLE ROCK DEFINITION:

[Rock Readable Name]
Model=path/to/model.mdl							--Model to use for rock
PossibleElements=List,Of,Possible,Resources		--List of pre-defined resources that this rock can have*
MinOre=######									--Min amount of mineable ore
MaxOre=######									--Max amount of mineable ore
Chance=###										--For weighted-random spawning; larger numbers = larger chance to spawn
SkinType=#										--Which skin to use for rock, based on Model
Color=Color(###,###,###)						--Color override for rock

*NOTE: Resources can be defined with random weighting and resource multipliers. Use Name:Weight:Mult to specify.
	   Weight and Mult fields are optional and will default to 100 and 1, respectively. No, you cannot specify
	   Mult without Weight, deal with it.

Example:
PossibleElements=Veldspar:100:1,Uranium:50:0.1

]]--


local GM = GAMEMODE
local MINEABLE = {}
MINEABLE.Types = {}

require("lip")

function MINEABLE:Setup()
	self:LoadFromData()
	self:BuildRockGeneratorTable()
end

function MINEABLE:Cleanup()
	ErrorNoHalt("BYE! D:\n")
end

function MINEABLE:LoadFromData()
	--Load in Rock Definitions
	local rockCount = 0
	local rockDefinitions = file.Find(SC.DataFolder.."/mining/rocks/*.txt", "DATA")

	for _,file in ipairs(rockDefinitions) do
		local INI = lip.load(SC.DataFolder.."/mining/rocks/"..file)

		--Parse Rock Info
		for node, data in pairs(INI) do
			ErrorNoHalt("[SC2 MINING] - Loading Terrestrial Rock Type "..node..".\n")

			local didError = false

			--Check for broken configs
			if not util.IsValidModel(data["Model"]) then ErrorNoHalt("[SC2 MINING] - ERROR: Model for entry "..node.." is invalid!\n") didError = true end
			if not data["PossibleElements"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing PossibleElements table!\n") didError = true end
			if not data["MinOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MinOre argument!\n") didError = true end
			if not data["MaxOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MaxOre argument!\n") didError = true end

			--Recoverable errors
			if (tonumber(data.Chance) or 0) <= 0 then
				ErrorNoHalt("[SC2 MINING] - WARNING: Chance in entry "..node.." is invalid, defaulting to 100.\n")
				data.Chance = 100
			end

			--Parse Element Table
			local elementList = SC.StringToTable(data.PossibleElements)
			local elements = {}
			local chances = {}
			local resourceMults = {}

			for _,elementData in ipairs(elementList) do
				local data = string.Explode(":", elementData)
				local element = data[1]
				local chance = data[2] or 100
				local amountModifier = data[3] or 1

				if not GM:GetResourceTypes()[element] then
					ErrorNoHalt("[SC2 MINING] - WARNING: Invalid resource "..element.." in Elements table for entry "..node.."!\n")
				else
					table.insert(elements, element)
					table.insert(chances, chance)
					resourceMults[element] = amountModifier
				end
			end

			if #elements <= 0 then ErrorNoHalt("[SC2 MINING] - ERROR: No valid resources defined for entry "..node.."!\n") didError = true end


			--Process and Add
			local resourceChanceTable, resourceRandomRange = self:BuildRockResourceTable(elements, chances)

			if not didError then
				data.Name = node
				data.PossibleElements = elements
				data.ResourceChanceTable = resourceChanceTable
				data.ResourceRandomRange = resourceRandomRange
				data.ResourceGenerationMults = resourceMults
				data.MinOre = tonumber(data.MinOre)
				data.MaxOre = tonumber(data.MaxOre)
				data.Chance = tonumber(data.Chance) or 100
                data.SkinType = tonumber(data.SkinType) or 0
                data.MiningLevel = tonumber(data.MiningLevel) or MININGLEVEL_COMMON

				table.insert(MINEABLE.Types, data)
				rockCount = rockCount + 1
			end
		end
	end

	ErrorNoHalt("[SC2 MINING] - Cached "..rockCount.." terrestrial rock types.\n\n")
end

function MINEABLE:GetTypeByName(name)
	if not name then return nil end

	for i,v in ipairs(self.Types) do
		if v.Name == name then
			return i
		end
	end

	ErrorNoHalt("[SC2 MINING] - WARNING: Attempted to get Rock type from non-indexed name "..name.."!\n")
	return nil
end

--Compile "roulette wheel" for rock type randomization
function MINEABLE:BuildRockGeneratorTable()
	self.RockRandomTable = {}
	self.RockRandomRange = 0

	local curPos = 0
	for i,rockType in ipairs(self.Types) do
		--Grab the Chance and add it into the range
		self.RockRandomRange = self.RockRandomRange + rockType.Chance

		--Save Range to table
		self.RockRandomTable[i] = {}
		self.RockRandomTable[i].Min = curPos
		self.RockRandomTable[i].Max = self.RockRandomRange

		--Move range cursor to front
		curPos = self.RockRandomRange + 1
	end
end

--The above, for individual rock resources
function MINEABLE:BuildRockResourceTable(resources, chances)
	local randomResourceTable = {}
	local randomResourceRange = 0

	local curPos = 0
	for i,resource in ipairs(resources) do
		--Grab the Chance and add it into the range
		randomResourceRange = randomResourceRange + chances[i]

		--Save Range to table
		randomResourceTable[i] = {}
		randomResourceTable[i].Min = curPos
		randomResourceTable[i].Max = randomResourceRange

		--Move range cursor to front
		curPos = randomResourceRange + 1
	end

	return randomResourceTable, randomResourceRange
end

--Spin the wheel!
function MINEABLE:GetRandomRockType()
	math.randomseed(SysTime())
	local spin = math.random(0, self.RockRandomRange)

	--Landed on red?
	for index,data in ipairs(self.RockRandomTable) do
		if spin <= data.Max then
			if spin >= data.Min then
				return index
			end
		end
	end
end

--Resource Generation Roulette!
function MINEABLE:GetRandomResource(rockData)
	math.randomseed(SysTime())
	local spin = math.random(0, rockData.ResourceRandomRange)

	--Landed on red?
	for index,data in ipairs(rockData.ResourceChanceTable) do
		if spin <= data.Max then
			if spin >= data.Min then
				return rockData.PossibleElements[index]
			end
		end
	end
end

--Generate Random Resources based on Rock Type
function MINEABLE:GenerateResources(rockData)
	if not rockData then
		ErrorNoHalt("[SC2 MINING] - ERROR: Attempted to generate resources using invalid Asteroid data!")
		return {}
	end

	local resources = {}

    local resourceAmount = math.random(rockData.MinOre, rockData.MaxOre)
    local totalAmount = resourceAmount

	while resourceAmount > 0 do
		local resource = self:GetRandomResource(rockData)
		local amount = math.floor(math.random(1, resourceAmount) * rockData.ResourceGenerationMults[resource])

		if amount > resourceAmount * 0.1 then
			resources[resource] = (resources[resource] or 0) + amount
			resourceAmount = resourceAmount - amount
		end
	end

	return totalAmount, resources
end

--Spawn the Rock
function MINEABLE:Spawn(pos, rockType, normal, yaw, resources, resourceAmount)
	if not rockType or not self.Types[rockType] then
		rockType = self:GetRandomRockType()
	end

	local rockData = self.Types[rockType]

	--Trace downwards if no "normal" given
	if not normal or normal == vector_origin then
		local traceData = {
			start = pos,
			endpos = pos + Vector(0, 0, -10000),
			mask = MASK_NPCWORLDSTATIC
		}
		local trace = util.TraceLine(traceData)

		if trace.Hit then
			pos = trace.HitPos
			normal = trace.HitNormal
		else
			normal = AngleRand():Forward()
		end
	end

	local rock = ents.Create("mining_rock")

	rock:SetModel(rockData.Model)
	rock:SetSkin(rockData.SkinType or 0)
	rock:SetColor(rockData.Color or Color(255, 255, 255, 255))
    rock:SetMaterial(rockData.Material or "")
    rock:SetPos(pos)
	rock:SetAngles(normal:Angle() + Angle(90, 0, 0))
	rock:SetAngles(rock:LocalToWorldAngles(Angle(0, yaw or math.Rand(0, 360), 0)))
	rock:Spawn()

	rock:SetMiningLevel(rockData.MiningLevel)

	if not resources or not resourceAmount then
		resourceAmount, resources = self:GenerateResources(rockData)
	end

	rock:CreateStorage("Cargo", resourceAmount, resources)

	--Translate to not be entirely in the floor
	local height = rock:OBBMaxs().z
	rock:SetPos(rock:LocalToWorld(Vector(0, 0, height * math.Rand(0.75, 0.95))))

	return rock
end

GM.Mining.RegisterMineable("Rocks", MINEABLE)