--[[
		SPACE COMBAT 2 - ASTEROID DEFINITIONS

	Loads in the asteroid definition files from
	data/spacecombat2/mining/asteroids.
--]]

--[[ EXAMPLE ASTEROID DEFINITION:

[Asteroid Readable Name]
Model=path/to/model.mdl							--Model to use for roid
PossibleElements=List,Of,Possible,Resources		--List of pre-defined resources that this roid can have*
MinOre=######									--Min amount of mineable ore
MaxOre=######									--Max amount of mineable ore
Chance=###										--For weighted-random spawning; larger numbers = larger chance to spawn
SkinType=#										--Which skin to use for roid, based on Model
Color=Color(###,###,###)						--Color override for roid

*NOTE: Resources can be defined with random weighting and resource multipliers. Use Name:Weight:Mult to specify.
	   Weight and Mult fields are optional and will default to 100 and 1, respectively. No, you cannot specify
	   Mult without Weight, deal with it.

Example:
PossibleElements=Veldspar:100:1,Uranium:50:0.1

]]--


local GM = GAMEMODE
local MINEABLE = {}
MINEABLE.Types = {}

require("lip")

function MINEABLE:Setup()
	self:LoadFromData()
	self:BuildAsteroidGeneratorTable()
end

function MINEABLE:Cleanup()
	ErrorNoHalt("BYE! D:\n")
end

function MINEABLE:LoadFromData()
	--Load in Asteroid Definitions
	local roidCount = 0
	local roidDefinitions = file.Find(SC.DataFolder.."/mining/asteroids/*.txt", "DATA")

	for _,file in ipairs(roidDefinitions) do
		local INI = lip.load(SC.DataFolder.."/mining/asteroids/"..file)

		--Parse Asteroid Info
		for node, data in pairs(INI) do
			ErrorNoHalt("[SC2 MINING] - Loading Asteroid Type "..node..".\n")

			local didError = false

			--Check for broken configs
			if not util.IsValidModel(data["Model"]) then ErrorNoHalt("[SC2 MINING] - ERROR: Model for entry "..node.." is invalid!\n") didError = true end
			if not data["PossibleElements"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing PossibleElements table!\n") didError = true end
			if not data["MinOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MinOre argument!\n") didError = true end
			if not data["MaxOre"] then ErrorNoHalt("[SC2 MINING] - ERROR: Entry "..node.." missing MaxOre argument!\n") didError = true end

			--Recoverable errors
			if (tonumber(data.Chance) or 0) <= 0 then
				ErrorNoHalt("[SC2 MINING] - WARNING: Chance in entry "..node.." is invalid, defaulting to 100.\n")
				data.Chance = 100
			end

			--Parse Element Table
			local elementList = SC.StringToTable(data.PossibleElements)
			local elements = {}
			local chances = {}
			local resourceMults = {}

			for _,elementData in ipairs(elementList) do
				local explodedData = string.Explode(":", elementData)
				local element = explodedData[1]
				local chance = explodedData[2] or 100
				local amountModifier = explodedData[3] or 1

				if not GM:GetResourceTypes()[element] then
					ErrorNoHalt("[SC2 MINING] - WARNING: Invalid resource "..element.." in Elements table for entry "..node.."!\n")
				else
					table.insert(elements, element)
					table.insert(chances, chance)
					resourceMults[element] = amountModifier
				end
			end

			if #elements <= 0 then ErrorNoHalt("[SC2 MINING] - ERROR: No valid resources defined for entry "..node.."!\n") didError = true end


			--Process and Add
			local resourceChanceTable, resourceRandomRange = self:BuildAsteroidResourceTable(elements, chances)

			if not didError then
				data.Name = node
				data.PossibleElements = elements
				data.ResourceChanceTable = resourceChanceTable
				data.ResourceRandomRange = resourceRandomRange
				data.ResourceGenerationMults = resourceMults
				data.MinOre = tonumber(data.MinOre)
				data.MaxOre = tonumber(data.MaxOre)
				data.Chance = tonumber(data.Chance) or 100
                data.SkinType = tonumber(data.SkinType) or 0
                data.MiningLevel = tonumber(data.MiningLevel) or MININGLEVEL_COMMON

				table.insert(MINEABLE.Types, data)
				roidCount = roidCount + 1
			end
		end
	end

	ErrorNoHalt("[SC2 MINING] - Cached "..roidCount.." asteroid types.\n\n")
end

function MINEABLE:GetTypeByName(name)
	if not name then return nil end

	for i,v in ipairs(self.Types) do
		if v.Name == name then
			return i
		end
	end

	ErrorNoHalt("[SC2 MINING] - WARNING: Attempted to get Asteroid type from non-indexed name "..name.."!\n")
	return nil
end

--Compile "roulette wheel" for asteroid type randomization
function MINEABLE:BuildAsteroidGeneratorTable()
	self.AsteroidRandomTable = {}
	self.AsteroidRandomRange = 0

	local curPos = 0
	for i,roidType in ipairs(self.Types) do
		--Grab the Chance and add it into the range
		self.AsteroidRandomRange = self.AsteroidRandomRange + roidType.Chance

		--Save Range to table
		self.AsteroidRandomTable[i] = {}
		self.AsteroidRandomTable[i].Min = curPos
		self.AsteroidRandomTable[i].Max = self.AsteroidRandomRange

		--Move range cursor to front
		curPos = self.AsteroidRandomRange + 1
	end
end

--The above, for individual asteroid resources
function MINEABLE:BuildAsteroidResourceTable(resources, chances)
	local randomResourceTable = {}
	local randomResourceRange = 0

	local curPos = 0
	for i,resource in ipairs(resources) do
		--Grab the Chance and add it into the range
		randomResourceRange = randomResourceRange + chances[i]

		--Save Range to table
		randomResourceTable[i] = {}
		randomResourceTable[i].Min = curPos
		randomResourceTable[i].Max = randomResourceRange

		--Move range cursor to front
		curPos = randomResourceRange + 1
	end

	return randomResourceTable, randomResourceRange
end

--Spin the wheel!
function MINEABLE:GetRandomAsteroidType()
	math.randomseed(SysTime())
	local spin = math.random(0, self.AsteroidRandomRange)

	--Landed on red?
	for index,data in ipairs(self.AsteroidRandomTable) do
		if spin <= data.Max then
			if spin >= data.Min then
				return index
			end
		end
	end
end

--Resource Generation Roulette!
function MINEABLE:GetRandomResource(roidData)
	math.randomseed(SysTime())
	local spin = math.random(0, roidData.ResourceRandomRange)

	--Landed on red?
	for index,data in ipairs(roidData.ResourceChanceTable) do
		if spin <= data.Max then
			if spin >= data.Min then
				return roidData.PossibleElements[index]
			end
		end
    end

    SC.Error("[SC2 Mining] - Tried to generate bad asteroid resource", 5)
end

--Generate Random Resources based on Asteroid Type
function MINEABLE:GenerateResources(roidData)
	if not roidData then
		ErrorNoHalt("[SC2 MINING] - ERROR: Attempted to generate resources using invalid Asteroid data!")
		return {}
    end

    local ChunksToGenerate = {}
    for I = 1, 1000 do
        local Resource = self:GetRandomResource(roidData)
        ChunksToGenerate[Resource] = (ChunksToGenerate[Resource] or 0) + 1
    end

    local Resources = {}
    local WantedAmount = math.random(roidData.MinOre, roidData.MaxOre)
    local AmountPerChunk = WantedAmount / 1000
    local TotalAmount = 0
    for Name, Amount in pairs(ChunksToGenerate) do
        local AddedAmount = math.floor(Amount * AmountPerChunk * roidData.ResourceGenerationMults[Name])
        Resources[Name] = AddedAmount
        TotalAmount = TotalAmount + (AddedAmount * GM:GetResourceFromName(Name):GetSize())
	end

	return TotalAmount, Resources
end

--Spawn the Roid
function MINEABLE:Spawn(pos, roidType, resources, resourceAmount)
	if not roidType or not self.Types[roidType] then
		roidType = self:GetRandomAsteroidType()
	end

	local roidData = self.Types[roidType]

	local roid = ents.Create("mining_asteroid")

	roid:SetModel(roidData.Model)
	roid:SetSkin(roidData.SkinType or 0)
	roid:SetColor(roidData.Color or Color(255, 255, 255, 255))
    roid:SetMaterial(roidData.Material or "")
	roid:SetPos(pos)
	roid:SetAngles(AngleRand())
    roid:Spawn()

    roid:SetMiningLevel(roidData.MiningLevel)

	if not resources then
		resourceAmount, resources = self:GenerateResources(roidData)
	end

	roid:CreateStorage("Cargo", resourceAmount, resources)

	return roid
end

GM.Mining.RegisterMineable("Asteroids", MINEABLE)