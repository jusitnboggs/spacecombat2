local function AddDir(dir) -- recursively adds everything in a directory to be downloaded by client, this is here because sometimes this loads too fast
	local files, directories = file.Find(dir.."/*", "GAME")
	for _, fdir in pairs(directories) do
		if fdir ~= ".svn" then -- don't spam people with useless .svn folders
			AddDir(dir .. "/" .. fdir)
		end
	end

	for k,v in pairs(files) do
		resource.AddFile(dir.."/"..v)
	end

	return true
end

resource.AddWorkshop("237728488")
resource.AddWorkshop("238102719")
resource.AddWorkshop("247808687")
resource.AddWorkshop("397979594") -- Content Pack 3

resource.AddWorkshop("284266415") -- Sci-fi Props Megapack
resource.AddWorkshop("175520745") -- SB_Omen_V2
resource.AddWorkshop("605147065") -- sb_gooniverse_v3e