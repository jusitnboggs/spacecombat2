--[[
Space Combat NPCs - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

if SERVER then
	SC.NPC = {}
	SC.NPC.Active = {} --List of spawned npcs
	SC.NPC.Types = {} --Various functions

	util.AddNetworkString("trader_used")

	--Add an NPC type
	function SC.NPC.AddType(name, model, class, onused)
		SC.NPC.Types[name] = {Model=model, Class=class, OnUsed=onused}
	end

	--NPC Think Timer
	timer.Create("SC.NPC.Think", 0.1, 0, function()
		for i,k in pairs(SC.NPC.Active) do
            if IsValid(k) then
			    k.Bubble:Think()
            else
                SC.NPC.Active[i] = nil
            end
		end
	end)

	--Spawn an NPC
	function SC.NPC.CreateNPC(Identifier, Type, Location, Angles)
        if not SC.NPC.Types[Type] then return end

        -- Get NPC information
        local Info = SC.NPC.Types[Type]

		-- Create the NPC
		local NewNPC = ents.Create(Info.Class)
        NewNPC:SetModel(Info.Model)
		NewNPC:SetPos(Location)
		NewNPC:SetAngles(Angles)
		NewNPC:SetKeyValue("spawnflags", "16386") --Stops NPC from moving out of the way when a player gets close.
		NewNPC:Spawn()
        NewNPC:UseNoBehavior()
        NewNPC:CapabilitiesClear() -- Make the NPC unable to do anything
		NewNPC.OnUsed = Info.OnUsed
        NewNPC.Type = Type
		NewNPC.Identifier = Identifier
        NewNPC.SC_Immune = true
	    NewNPC.Untouchable = true
	    NewNPC.Unconstrainable = true
	    NewNPC.PhysgunDisabled = true

        NewNPC.CanProperty = function()
            return false
        end

		NewNPC.CanTool = function( ply, trace, mode )
		    return false
	    end

		--Ownership crap
	    if NADMOD then --If we're using NADMOD PP, then use its function
		    NADMOD.SetOwnerWorld(NewNPC)
	    elseif CPPI then --If we're using SPP, then use its function
		    NewNPC:SetNWString("Owner", "World")
	    else --Well fuck it, lets just use our own!
		    NewNPC.Owner = game.GetWorld()
	    end

        --Put a nice chat bubble over their head
		local ent = ents.Create("prop_physics")
		ent:SetModel("models/extras/info_speech.mdl")
		ent:SetMoveType(MOVETYPE_NONE)
		ent:SetNotSolid(1)
		ent:AddEffects(EF_ITEM_BLINK)
		ent:AddEffects(EF_NOSHADOW)
		ent:SetPos(NewNPC:GetPos() + Vector(0,0,75))
		ent:SetAngles(Angle(0,0,0))
		NewNPC.Bubble = ent

		--Replace the think for the spinny bit
		ent.Think = function()
			ent:SetAngles(ent:GetAngles() + Angle(0,6,0))
		end

		--Add the NPC to the active NPCs table
		SC.NPC.Active[Identifier] = NewNPC
	end

    -- Removes an NPC, can be given an entity or an npc identifier
	function SC.NPC.RemoveNPC(npc)
        local ent = type(npc) == "string" and SC.NPC.Active[npc] or npc
		if IsValid(ent) then
            SC.NPC.Active[ent.Identifier] = nil
            SC.NPC.SaveNPCs()
			ent.Bubble:Remove()
			ent:Remove()
		end
	end

    -- Cleans up an NPC and respawns it if it gets killed
    hook.Add("OnNPCKilled", "SC.NPCKilled", function(npc, attacker, inflictor)
		if npc and npc.Identifier and SC.NPC.Active[npc.Identifier] then
            npc.Bubble:Remove()
            SC.NPC.Active[npc.Identifier] = nil
			SC.NPC.LoadNPCs()
		end
	end)

	--Save all the NPCs!
	function SC.NPC.SaveNPCs()
		local Folder = SC.DataFolder.."/npcs/"
		file.CreateDir(Folder)

		local data = {}
		local File = Folder..game.GetMap()..".txt"

		for i,k in pairs(SC.NPC.Active) do
			data[i] = {Type=k.Type, Location=k:GetPos(), Angles=k:GetAngles()}
		end

		file.Write(File, util.TableToJSON(data))
	end

	--Load all the NPCs!
	function SC.NPC.LoadNPCs()
		local data = {}
		local File = SC.DataFolder.."/npcs/"..game.GetMap()..".txt"

		if file.Exists(File, "DATA") then
			data = util.JSONToTable(file.Read(File, "DATA"))
		end

		for i,k in pairs(data) do
            if not IsValid(SC.NPC.Active[i]) then
			    SC.NPC.CreateNPC(i, k.Type, k.Location, k.Angles)
            end
		end
	end
    hook.Add("InitPostEntity", "SC.NPC.LoadNPCs", SC.NPC.LoadNPCs)

	--Did someone use one of the NPCs?
	function SC.NPC.KeyPress( Player, Key )
		if Key == IN_USE then
			local TTable = {}
			TTable.start = Player:GetShootPos()
			TTable.endpos = TTable.start + Player:GetAimVector() * 100
			TTable.filter = Player
			TTable.mask = MASK_OPAQUE_AND_NPCS

			local Tr = util.TraceLine(TTable)

			if Tr.Entity and Tr.Entity:IsValid() and Tr.Entity:IsNPC() and Tr.Entity.OnUsed then
				Tr.Entity:OnUsed(Player)
			end
		end
	end
	hook.Add("KeyPress", "SCNPCTrace", SC.NPC.KeyPress)

	--Make it so the NPCs can't die if protected
	function SC.NPC.ScaleNPCDamage( NPC, HitGroup, DmgInfo )
		if NPC.SC_Immune then
			DmgInfo:ScaleDamage(0)
		end

		return DmgInfo
	end
	hook.Add("ScaleNPCDamage", "SCDMGScale", SC.NPC.ScaleNPCDamage)

    -- On Used Functions
    -- Trader Definition
	SC.NPC.AddType("trader", "models/Humans/Group03/male_07.mdl", "npc_citizen", function(self, ply)
		local data = { Globalstorage = ply:GetGlobalStorage() }

		net.Start("trader_used")
			net.WriteTable(data)
		net.Send(ply)
	end)

    hook.Add("RegisterSC2Commands", "RegisterNPCCommands", function()
        local Admin = SC.Administration

		-- Register any permissions we need for these commands
		Admin.RegisterPermission("AdministrateNPCs", "The ability to create, modify, and remove NPCs.")

		-- Register commands
		Admin.RegisterCommand("GetNPCTypes", "Gets a list of NPC types that can be spawned using AddNPCSpawn.",
			-- Permissions
			{
				"AdministrateNPCs"
			},

			-- Arguments
			{},

			-- Callback
			function(Executor, Arguments)
				for i,k in pairs(SC.NPC.Types) do
					Executor:ChatPrint(i)
				end
			end)

		Admin.RegisterCommand("GetNPCList", "Gets a list of NPCs spawned on the map.",
			-- Permissions
			{
				"AdministrateNPCs"
			},

			-- Arguments
			{},

			-- Callback
			function(Executor, Arguments)
				if table.Count(SC.NPC.Active) > 0 then
					Executor:ChatPrint("- NPC List -")
					for k,v in pairs(SC.NPC.Active) do
						Executor:ChatPrint(k .. ": " .. v.Type)
					end
				else
					Executor:ChatPrint("[Space Combat 2 - NPCs] - No NPC's have been added to this map!")
				end
			end)

		Admin.RegisterCommand("AddNPCSpawn", "Adds a new NPC spawn to the map.",
			-- Permissions
			{
				"AdministrateNPCs"
			},

			-- Arguments
			{
				{
					Name = "Name",
					Type = "string"
				},
				{
					Name = "Type",
					Type = "string"
				}
			},

			-- Callback
			function(Executor, Arguments)
				local Name = Arguments.Name
				local Type = Arguments.Type

				if SC.NPC.Types[Type] then
					SC.NPC.CreateNPC(Name, Type, Executor:GetPos() + Vector(0,0,15), Executor:GetAngles())
					SC.NPC.SaveNPCs()

					Executor:ChatPrint("[Space Combat 2 - NPCs] - NPC spawn has been added!")
				else
					Executor:ChatPrint("[Space Combat 2 - NPCs] - NPC type does not exist!")
				end
			end)

		Admin.RegisterCommand("GetGlobalPumpList", "Gets a list of pumps spawned on the map.",
			-- Permissions
			{
				"AdministrateNPCs"
			},

			-- Arguments
			{
				{
					Name = "Name",
					Type = "string"
				}
			},

			-- Callback
			function(Executor, Arguments)
				local Name = Arguments.Name
		        if IsValid(SC.NPC.Active[Name]) then
			        SC.NPC.RemoveNPC(Name)

			        Executor:ChatPrint("[Space Combat 2 - NPCs] - NPC spawn has been removed!")
		        end
			end)
	end)
elseif CLIENT then
	net.Receive("trader_used", function()
		local data = net.ReadTable()
		local gs_frame
		local DPanel4
		local DPanel3
		local DPanel2
		local DPanel1
		local resources
		local retreive
		local send
		local resourceslider
		local global
		local hold
		local DLabel2
		local DLabel1

		gs_frame = vgui.Create('DFrame')
		gs_frame:SetSize(400, 510)
		gs_frame:SetPos(ScrW() / 8, ScrH() / 8)
		gs_frame:SetTitle('Trader')
		gs_frame:MakePopup()

		DPanel2 = vgui.Create('DPanel')
		DPanel2:SetParent(gs_frame)
		DPanel2:SetSize(390, 75)
		DPanel2:SetPos(5, 430)

		DPanel1 = vgui.Create('DPanel')
		DPanel1:SetParent(gs_frame)
		DPanel1:SetSize(390, 400)
		DPanel1:SetPos(5, 25)

		todupemarket = vgui.Create('DButton')
		todupemarket:SetParent(gs_frame)
		todupemarket:SetSize(380, 30)
		todupemarket:SetPos(10, 470)
		todupemarket:SetText('To the Duplication Market!')
		todupemarket.DoClick = function() end

		tomarket = vgui.Create('DButton')
		tomarket:SetParent(gs_frame)
		tomarket:SetSize(380, 30)
		tomarket:SetPos(10, 435)
		tomarket:SetText('To the Resource Market!')
		tomarket.DoClick = function() end

		global = vgui.Create('DListView')
		global:SetParent(gs_frame)
		global:SetSize(380, 390)
		global:SetPos(10, 30)
		global:Clear()
		global:AddColumn("Resource in Global Storage")
		global:AddColumn("Amount")

		for i,k in pairs(data.Globalstorage) do
			local line = global:AddLine(i, string.Comma(math.floor(k * 100) / 100))
			line:SetSortValue(2, k)
		end

		global:SortByColumn(2, true)
	end)
end