
AddCSLuaFile()
include("player_sandbox.lua")
DEFINE_BASECLASS( "player_sandbox" )

local PLAYER = {}

PLAYER.DuckSpeed			= 0.1		-- How fast to go from not ducking, to ducking
PLAYER.UnDuckSpeed			= 0.1		-- How fast to go from ducking, to not ducking

--
-- Creates a Taunt Camera
--
PLAYER.TauntCam = TauntCamera()

--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400


PLAYER.BaseResistances = {

}

-- A list of resource percentages that needs to be met for the player to breath in an atmosphere
PLAYER.BreathableAtmosphere = {
    Oxygen = 0.05
}

PLAYER.CanBreatheUnderWater = false

PLAYER.MaxBreath = 15

-- The temperature that the player's body will try to maintain
PLAYER.IdealTemperature = 310

-- The maximum temperature of the player's body before they take damage
PLAYER.MaxTemperature = 325

-- The minimum temperature of the player's body before they take damage
PLAYER.MinTemperature = 295

-- The highest environment temperature before life support starts to kick in
PLAYER.MaxSafeEnvironmentTemperature = 315

-- The lowest environment temperature before life support starts to kick in
PLAYER.MinSafeEnvironmentTemperature = 270

-- The highest atmosphere pressure the player can withstand
PLAYER.MaxPressure = 2

-- The lowest atmosphere pressure the player can withstand
PLAYER.MinPressure = 0.2

PLAYER.EnergyStorage = 10000

PLAYER.CargoStorage = 3500

PLAYER.AmmoStorage = 2000

PLAYER.AirStorage = 1500

PLAYER.CoolantStorage = 1500

PLAYER.CoolantName = "Water"

PLAYER.CoolantRegenRate = 1

PLAYER.CoolantConsumeRate = 10

PLAYER.HeatingEnergyConsumeRate = 15

PLAYER.AirName = "Oxygen"

PLAYER.AirRegenRate = 1

PLAYER.AirConsumeRate = 10

hook.Remove("SC.RegisterCustomSyncFunction", "SC.Player.RegisterCustomSyncFunction")
hook.Add("SC.RegisterCustomSyncFunction", "SC.Player.RegisterCustomSyncFunction", function()
    SC.NWAccessors.RegisterCustomSyncFunction("SC.PlayerStorageSync", function(LSEntity, Sending, Accessor, OnFinishedUpdating)
        if Sending then
            if not Accessor or not Accessor.value then
                error("invalid accessor")
            end

            Accessor.value:WriteCreationPacket()
        else
            local NewContainer = GAMEMODE.class.getClass("MultiTypeResourceContainer"):new()
            NewContainer:ReadCreationPacket()
            Accessor.value = NewContainer
            OnFinishedUpdating()
        end
    end)
end)

--
-- Set up the network table accessors
--
function PLAYER:SetupDataTables()
    -- Data indexes below 8 should be reserved for using below
    -- unless we run out of them...
    self.Player:NetworkVar("Float", 0, "Temperature")
    self.Player:NetworkVar("Float", 1, "Breath")
    self.Player:NetworkVar("Bool", 0, "EnvironmentHostile")
end

function PLAYER:CreateResourceContainers()
    local Container = GAMEMODE.class.getClass("MultiTypeResourceContainer"):new(
        {"Cargo", "Ammo", "Liquid", "Gas", "Energy"},
        {CONTAINERMODE_AUTOENLARGE, CONTAINERMODE_AUTOENLARGE, CONTAINERMODE_DEFAULT, CONTAINERMODE_DEFAULT, CONTAINERMODE_DEFAULT}
    )

    Container:SetContainerSize("Cargo", self.CargoStorage)
    Container:SetContainerSize("Ammo", self.AmmoStorage)
    Container:SetContainerSize("Liquid", self.CoolantStorage)
    Container:SetContainerSize("Gas", self.AirStorage)
    Container:SetContainerSize("Energy", self.EnergyStorage)

    Container:AddResourceType("Energy")
    Container:AddResourceType(self.AirName)
    Container:AddResourceType(self.CoolantName)

    return Container
end

function PLAYER:Init()
    -- Setup player inventory
    local Container = self:CreateResourceContainers()
    SC.NWAccessors.CreateNWAccessor(self.Player, "Storage", "custom", Container, 1, nil, "SC.PlayerStorageSync")

    -- Setup player tick
    local TickTimerName = Format("SC.PlayerTick.%s", self.Player:SteamID64())
    timer.Create(TickTimerName, 1, 0, function()
        if IsValid(self.Player) then
            self:Tick()
        else
            timer.Remove(TickTimerName)
        end
    end)
end

function PLAYER:Tick()
    if SERVER then
        local ID = SC.NWAccessors.GetNWAccessorID(self.Player, "Storage")
        SC.NWAccessors.SyncValue(self.Player, ID)
    end
end

function PLAYER:GetResistance(Resistance)
    return self.BaseResistances[Resistance] or 0
end

function PLAYER:HandleEnvironmentUpdate(Environment)
    if self.Player.HasLifeSupportOverride then
        self.Player:SetEnvironmentHostile(false)
        return
    end

    local EnvironmentIsHostile = false

    -- Handle temperature increases and decreases
    local Temperature = Environment:GetTemperature()
    local TemperatureDifference = 0
    if Temperature > self.MaxSafeEnvironmentTemperature then
        EnvironmentIsHostile = true
        TemperatureDifference = Temperature - self.MaxSafeEnvironmentTemperature
    elseif Temperature < self.MinSafeEnvironmentTemperature then
        EnvironmentIsHostile = true
        TemperatureDifference = Temperature - self.MinSafeEnvironmentTemperature
    end

    -- Update our body temperature
    local BodyTemperature = self.Player:GetTemperature()
    local ThermalResistance = self:GetResistance("THERM")
    TemperatureDifference = TemperatureDifference * (1 - ThermalResistance)
    local AbsTemperatureDifference = math.abs(TemperatureDifference)
    if AbsTemperatureDifference > 0 then
        local SuitAbsorbed = false
        local Storage = self.Player:GetStorage()
        local ConsumeMultiplier = math.Max(math.Round(AbsTemperatureDifference / 50), 1)
        if TemperatureDifference > 0 then
            if Storage:ConsumeResource(self.CoolantName, self.CoolantConsumeRate * ConsumeMultiplier) then
                SuitAbsorbed = true
            end
        elseif TemperatureDifference < 0 then
            if Storage:ConsumeResource("Energy", self.HeatingEnergyConsumeRate * ConsumeMultiplier) then
                SuitAbsorbed = true
            end
        end

        if not SuitAbsorbed then
            BodyTemperature = Lerp(0.1, BodyTemperature, Temperature)
        end
    else
        BodyTemperature = Lerp(0.2, BodyTemperature, self.IdealTemperature)
    end

    self.Player:SetTemperature(BodyTemperature)

    if BodyTemperature > self.MaxTemperature then
        local Damage = DamageInfo()
        Damage:SetDamage(math.max(math.abs(TemperatureDifference) * 0.05, 2))
        Damage:SetDamageType(DMG_SLOWBURN)
        Damage:SetAttacker(self.Player)
        Damage:SetInflictor(self.Player)

        self.Player:TakeDamageInfo(Damage)
    elseif BodyTemperature < self.MinTemperature then
        local Damage = DamageInfo()
        Damage:SetDamage(math.max(math.abs(TemperatureDifference) * 0.05, 2))
        Damage:SetDamageType(DMG_PARALYZE)
        Damage:SetAttacker(self.Player)
        Damage:SetInflictor(self.Player)

        self.Player:TakeDamageInfo(Damage)
    end

    -- Check if we can breathe
    local CanBreathe = true
    if not self.CanBreatheUnderWater and self.Player:WaterLevel() >= 3 then
        CanBreathe = false
        EnvironmentIsHostile = true
    else
        for Name, Percent in pairs(self.BreathableAtmosphere) do
            if Environment:GetAmount(Name) / Environment:GetMaxAmount(Name) < Percent then
                CanBreathe = false
                EnvironmentIsHostile = true
            end
        end
    end

    -- Check if the pressure is bad
    local Pressure = Environment:GetPressure()
    if Pressure > self.MaxPressure then
        self:ApplyDamage({Pressure = 5 * (1 + (Pressure - self.MaxPressure))})
        EnvironmentIsHostile = true
    elseif Pressure < self.MinPressure then
        CanBreathe = false
        EnvironmentIsHostile = true
    end

    -- Drown the player if they can't breath
    local CurrentBreath = self.Player:GetBreath()
    if CanBreathe then
        if self.Player:GetBreath() < self.MaxBreath then
            self.Player:SetBreath(math.min(CurrentBreath + 1, self.MaxBreath))
        else
            local Storage = self.Player:GetStorage()
            Storage:SupplyResource(self.AirName, self.AirRegenRate)
        end
    else
        local Storage = self.Player:GetStorage()
        if not Storage:ConsumeResource(self.AirName, self.AirConsumeRate) then
            self.Player:SetBreath(math.max(CurrentBreath - 2, 0))

            if self.Player:GetBreath() <= 0 then
                local Damage = DamageInfo()
                Damage:SetDamage(20)
                Damage:SetDamageType(DMG_DROWN)
                Damage:SetAttacker(self.Player)
                Damage:SetInflictor(self.Player)

                self.Player:TakeDamageInfo(Damage)
            end
        end
    end

    self.Player:SetEnvironmentHostile(EnvironmentIsHostile)
end

function PLAYER:ApplyDamage(Damage, Attacker, Inflictor, HitData)
    local TotalDamage = 0
    for Type, Damage in pairs(Damage) do
        TotalDamage = TotalDamage + (Damage * (1 - self:GetResistance(Type)))
    end

    -- TODO: Convert this to TakeDamageInfo
    self.Player:TakeDamage(TotalDamage, Attacker, Inflictor)
end

function PLAYER:Loadout()
	self.Player:RemoveAllAmmo()

	self.Player:Give( "gmod_tool" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "personal_miner" )

	self.Player:SwitchToDefaultWeapon()
end


--
-- Called when the player spawns
--
function PLAYER:Spawn()
    BaseClass.Spawn( self )

    self.Player:SetTemperature(self.IdealTemperature)
    self.Player:SetBreath(self.MaxBreath)

    local Storage = self:CreateResourceContainers()
    Storage:SetAmount(self.AirName, 200)
    Storage:SetAmount(self.CoolantName, 200)
    Storage:SetAmount("Energy", 1500)

    self.Player:SetStorage(Storage)

    self.Player.player_class = self
end

player_manager.RegisterClass( "player_spacecombat", PLAYER, "player_sandbox" )
