local GM = GM

local SpaceBotEnabled = false
local SpaceBotReady = false
local SpaceBotFirstLogin = true
local SpaceBotServer = "https://127.0.0.1:9080"
local SpaceBotSession = "INVALID"
local SpaceBotEndpoints = {}

hook.Remove("SC.Config.Register", "SC.LoadSpacebotConfig")
hook.Add("SC.Config.Register", "SC.LoadSpacebotConfig", function()
    local Config = GM.Config

    Config:Register({
        File = "chat",
        Section = "SpaceBot",
        Key = "EnableSpaceBot",
        Default = false
    })

    Config:Register({
        File = "chat",
        Section = "SpaceBot",
        Key = "ServerAddress",
        Default = "https://127.0.0.1:9080"
    })

    Config:Register({
        File = "chat",
        Section = "SpaceBot",
        Key = "ServerKey",
        Default = "TestKey"
    })
end)


local PollSpaceBotChat
local SpaceBotLoggingIn = false
local function DoSpaceBotLogin()
    -- Prevent multiple login attempts
    if SpaceBotLoggingIn or SpaceBotReady then
        SC.Error("Spacebot is already logging in", 3)
        return
    end

    local Config = GM.Config
    SpaceBotLoggingIn = true

    -- Cleanup any currently running Poll timers just in case we fail to login
    timer.Remove("SC.SpaceBot.Poll")

    SC.Error("Spacebot is logging in", 5)

    -- Login to the spacebot server and get our session key
    local RequestCreated = HTTP(
        {
            url = SpaceBotEndpoints["serverlogin"],

            method = "POST",

            -- Headers
            headers = {
                ["Content-Type"] = "application/json"
            },

            parameters = {
                ["key"] = Config:Get("chat", "SpaceBot", "ServerKey", "TestKey")
            },

            -- Success
            success = function(Status, Content, Headers)
                if Status == 200 then
                    local ContentAsTable = util.JSONToTable(Content)
                    if ContentAsTable and ContentAsTable.Success and ContentAsTable.SessionKey then
                        SpaceBotReady = true
                        SpaceBotSession = ContentAsTable.SessionKey
                        timer.Create("SC.SpaceBot.Poll", 1, 0, PollSpaceBotChat)

                        if SpaceBotFirstLogin then
                            SpaceBotFirstLogin = false
                            local StartString = "Server is now running on %s with (%d/%d) players"
                            StartString = string.format(StartString, game.GetMap(), player.GetCount(), game.MaxPlayers())
                            hook.Run("BroadcastSpacebotEvent", "ServerStarted", StartString)
                        end
                    else
                        SpaceBotReady = false
                        SC.Error("Space Bot Invalid Session Key Received", 5)
                    end
                elseif Status == 403 then
                    SpaceBotReady = false
                    SC.Error("Space Bot Invalid Server Key", 5)
                else
                    SpaceBotReady = false
                    SC.Error("Space Bot Server Error", 5)
                end

                SpaceBotLoggingIn = false

                -- if the connection failed try again in 30 seconds
                if not SpaceBotReady then
                    timer.Simple(30, DoSpaceBotLogin)
                end
            end,

            -- Failure
            failed = function()
                SpaceBotReady = false
                SpaceBotLoggingIn = false
                SC.Error("Space Bot Connection Failed", 5)

                -- if the connection failed try again in 30 seconds
                if not SpaceBotReady then
                    timer.Simple(30, DoSpaceBotLogin)
                end
            end
        }
    )

    -- If we couldn't make a request then just try again in 5 seconds
    -- Sometimes this code is called too soon after the game starts
    -- and HTTP returns nil
    if not RequestCreated then
        SpaceBotLoggingIn = false
        timer.Simple(5, DoSpaceBotLogin)
    end
end

PollSpaceBotChat = function()
    if SpaceBotReady then
        http.Fetch(string.format("%s?session=%s", SpaceBotEndpoints["chatqueue"], SpaceBotSession),

        -- Success
        function(Content, Length, Headers, Status)
            if Status == 204 then
                -- There was nothing...
            elseif Status == 200 then
                local ContentAsTable = util.JSONToTable(Content)
                local Players = player.GetAll()
                if ContentAsTable and ContentAsTable.Messages then
                    for _, MessageData in pairs(ContentAsTable.Messages) do
                        local ToSend = string.format("[%s] %s", MessageData.Source, MessageData.Message)
                        ErrorNoHalt(ToSend.."\n")

                        for __, Player in pairs(Players) do
                            Player:ChatPrint({ToSend}, "All")
                        end
                    end
                end
            elseif Status == 401 or Status == 400 then
                SpaceBotReady = false
                SC.Error("Space Bot Invalid Session, Attempting to login again", 5)
                DoSpaceBotLogin()
            end
        end)
    end
end

hook.Remove("SC.Config.PostLoad", "SC.SpaceBot.Config.PostLoad")
hook.Add("SC.Config.PostLoad", "SC.SpaceBot.Config.PostLoad", function()
    local Config = GM.Config

    SpaceBotEnabled = Config:Get("chat", "SpaceBot", "EnableSpaceBot", false)
    if SpaceBotEnabled then
        SpaceBotServer = Config:Get("chat", "SpaceBot", "ServerAddress", "https://127.0.0.1:9080")

        -- Populate the endpoints table
        SpaceBotEndpoints = {
            ["chat"] = string.format("%s/chat", SpaceBotServer),
            ["event"] = string.format("%s/event", SpaceBotServer),
            ["announce"] = string.format("%s/announce", SpaceBotServer),
            ["serverlogin"] = string.format("%s/serverlogin", SpaceBotServer),
            ["chatqueue"] = string.format("%s/chatqueue", SpaceBotServer),
        }

        DoSpaceBotLogin()
    else
        SpaceBotReady = false
    end
end)

hook.Remove("SC.PlayerSentMessage", "SC.SpaceBotChatRelay")
hook.Add("SC.PlayerSentMessage", "SC.SpaceBotChatRelay", function(Player, Message, IsTeamMessage)
    if SpaceBotEnabled and not IsTeamMessage then
        if not SpaceBotReady then
            DoSpaceBotLogin()
            return
        end

        http.Post(SpaceBotEndpoints["chat"],
            {
                ["session"] = SpaceBotSession,
                ["message"] = Message,
                ["user"] = Player:Nick()
            },

            -- Success
            function(Content, Length, Headers, Status)
                if Status == 401 then
                    SpaceBotReady = false
                    SC.Error("Space Bot Invalid Session, Attempting to login again", 5)
                    DoSpaceBotLogin()
                end
            end,

            -- Failure
            function(Error)
                SC.Error("Space Bot got a connection error: "..tostring(Error), 5)
            end,

            -- Headers
            {
                ["Content-Type"] = "application/json"
            }
        )
    end
end)

function GM:BroadcastSpacebotEvent(EventName, Message)
    if SpaceBotEnabled then
        if not SpaceBotReady then
            DoSpaceBotLogin()
            return
        end

        local ConvertedMessage
        if type(Message) == "table" then
            ConvertedMessage = ""
            for _, Data in pairs(Message) do
                local DataType = type(Data)
                if DataType == "string" then
                    ConvertedMessage = ConvertedMessage..Data
                elseif DataType == "Player" then
                    ConvertedMessage = ConvertedMessage..Player:Nick()
                end
            end
        else
            ConvertedMessage = Message
        end

        http.Post(SpaceBotEndpoints["event"],
            {
                ["session"] = SpaceBotSession,
                ["message"] = ConvertedMessage
            },

            -- Success
            function(Content, Length, Headers, Status)
                if Status == 401 then
                    SpaceBotReady = false
                    SC.Error("Space Bot Invalid Session, Attempting to login again", 5)
                    DoSpaceBotLogin()
                end
            end,

            -- Failure
            function(Error)
                SC.Error("Space Bot got a connection error: "..tostring(Error), 5)
            end,

            -- Headers
            {
                ["Content-Type"] = "application/json"
            }
        )
    end
end

hook.Remove("BroadcastEvent", "SC.Spacebot.BroadcastEvent")
hook.Add("BroadcastEvent", "SC.Spacebot.BroadcastEvent", function(EventName, Message)
    GM:BroadcastSpacebotEvent(EventName, Message)
end)