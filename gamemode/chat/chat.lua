AddCSLuaFile("cl_chat.lua")

local GM = GM

include("spacebot.lua")

local function DoGrammar(Say)
    local FilterWords = {
            i = "I",
            u = "you",
            cuz = "because",
            im = "I'm",
            ur = "your",
            omg = "oh my god",
            omfg = "oh my fucking god",
            wtf = "what the fuck",
            tbh = "to be honest",
            wont = "won't",
            dont = "don't",
            cant = "can't",
            theyre = "they're",
            hax = "hacks",
            teh = "the",
            bbl = "be back later",
            brb = "be right back",
            bsod = "blue screen of death",
            hai = "yes",	--Get out, you weeb bastard. -Steeveeo, king of weebs --NEVER -Lt.Brandon
            bbiab = "be back in a bit",
            afaicr = "as far as I can recall",
            asap = "as soon as possible",
            atm = "at the moment",
            --leet = "leet", --dafaq is this sheet - Steeveeo
            cya = "see ya",
            ffs = "for fucks sake",
            gtg = "got to go",
            g2g = "got to go",
            haxor = "hacker",
            ty = "thank you",
            np = "no problem",
            wtg = "way to go",
            wth = "what the hell",
            zomg = "omg", --"oh my god",
            nub = "I am a noob", --"noob", --Lul
            wat = "what",

            --Steeveeo's Additions
            iirc = "if I recall correctly",
            btw = "by the way",

            -- Ninjr's lulzy additions.
            soon = "soon&trade;",

            -- Kanzuke is helping.
            eventually = "eventually&trade;"
        }

    Say = " "..Say.." "

    for K, V in pairs(FilterWords) do
        Say = string.Replace(Say, " "..K.." ", " "..V.." ")
    end

    Say = string.Trim(Say)

    local UpperText = string.upper(string.sub(Say, 0, 1))

    Say = UpperText..""..string.sub(Say, 2, string.len(Say))

    if not string.find(string.sub(Say, string.len(Say)), "%p") then
        Say = Say.."."
    end

    return Say
end

--rpchat
local meta = FindMetaTable("Player")
local FuncPrintMessage = meta.PrintMessage

function meta:ChatPrint(msg,tab)
	self:PrintMessage(HUD_PRINTTALK,msg,tab)
end

function meta:PrintMessage(typ, msg, tab)
	if type(msg) ~= "table" and typ ~= HUD_PRINTNOTIFY and typ ~= HUD_PRINTTALK then
		FuncPrintMessage(self, typ, msg)
		return
	end

	if not tab then tab = "All" end
	if type(msg) ~= "table" then msg = {msg} end

	net.Start("AddToChatBox")
		net.WriteString(tab)
		net.WriteTable(msg)
	net.Send(self)
end

function DoSay(ply, text, isTeam)
	if IsValid(ply) then
		local Say = DoGrammar(text)
		hook.Run("PostPlayerSay", ply, Say, isTeam)

		if isTeam then
            local Faction = ply:Team()
            local FactionName = team.GetName(Faction)
			for i,k in pairs(player.GetAll()) do
				if k:Team() == Faction then
					k:ChatPrint({ply, " ["..FactionName.."]: "..Say}, "Faction")
				end
			end
		else
			ErrorNoHalt(ply:Name()..": "..Say.."\n")

            for i,k in pairs(player.GetAll()) do k:ChatPrint({ply, ": "..Say}, "All") end
        end

        hook.Run("SC.PlayerSentMessage", ply, Say, isTeam)
	end
end

net.Receive("PlayerSay", function(len, ply)
	local str = net.ReadString()
	local tab = net.ReadString()
	local isTeam = (net.ReadBit() == 1) or (tab == "Faction")
	local say = hook.Run("PlayerSay", ply, str, isTeam, tab)

	if string.len(say) > 0 then
		DoSay(ply, say, isTeam)
	end
end)

local HoverHeads = {}
concommand.Add("start_chatting",function(ply, cmd, args)
    if not HoverHeads[ply] then
        local ent = ents.Create("prop_physics")
        HoverHeads[ply] = ent
        ent:SetModel("models/extras/info_speech.mdl")
        ent:SetMoveType(MOVETYPE_NONE)
        ent:SetNotSolid(1)
        ent:AddEffects( EF_ITEM_BLINK )
        ent:AddEffects( EF_NOSHADOW )
        ent:SetParent(ply)
        ent:SetPos(ply:GetPos() + Vector(0,0,100))
        ent:SetAngles(Angle(0,0,0))
	end
end
)

concommand.Add("stop_chatting", function(ply, cmd, args)
    if HoverHeads[ply] and HoverHeads[ply]:IsValid() then
        HoverHeads[ply]:Remove()
        HoverHeads[ply] = nil
    end
end
)

hook.Add("Tick","SpinMeRightRound", function()
    for k,v in pairs(HoverHeads) do
        if (v:IsValid()) then
            v:SetAngles(v:GetAngles() + Angle(0,3,0))
        end
    end
end)

function GM:BroadcastEvent(EventName, Message)
    for i,k in pairs(player.GetAll()) do
        k:ChatPrint(Message, "Notices")
    end
end