AddCSLuaFile()

if not CLIENT then return end

local PANEL = {}
local GM = GM
local Factions = GM.Factions

surface.CreateFont("FactionSelectFont", {
    font = "Consola Mono",
    size = SC2ScreenScale(12),
    weight = 1000,
    shadow = true,
    outline = true
})

-- Panel based blur function by Chessnut from NutScript
local blurmat = Material("pp/blurscreen")
local function blur(panel, layers, density, alpha)
    -- Its a scientifically proven fact that blur improves a script
    local x, y = panel:LocalToScreen(0, 0)
    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetMaterial(blurmat)

    for i = 1, 6 do
        blurmat:SetFloat("$blur", (i / layers) * density)
        blurmat:Recompute()

        render.UpdateScreenEffectTexture()
        surface.DrawTexturedRect(-x, -y, ScrW(), ScrH())
    end
end

local function MakeButton(Text, Dock, Parent, DoClick)
    local Button = vgui.Create("DButton", Parent)
    Button:SetText(Text)
    Button:SetFont("DermaLarge")
    Button:SizeToContents()
    Button.DoClick = DoClick
    Button:DockMargin(5, 5, 5, 5)
    Button:Dock(Dock)
    Button:InvalidateLayout(true)
    Parent:InvalidateLayout(true)

    return Button
end

PANEL.TopBar = nil
PANEL.BottomBar = nil
PANEL.CenterWindow = nil
PANEL.CurrentChild = nil

function PANEL:ClearCenterWindow()
    if IsValid(self.CurrentChild) then
        self.CurrentChild:Remove()
    end
end

function PANEL:OnPlayerCreationCompleted(Player, Name, Faction, Info, Success)
    self:Remove()
    gui.EnableScreenClicker(false)
end

function PANEL:OnFactionSelected(Faction)
    -- Clear center window
    self:ClearCenterWindow()

    -- Hook in and wait for creation to complete
    hook.Remove("SC.OnPlayerCreationCompleted", "SC.CleanupCreationScreen")
    hook.Add("SC.OnPlayerCreationCompleted", "SC.CleanupCreationScreen", function(...)
        hook.Remove("SC.OnPlayerCreationCompleted", "SC.CleanupCreationScreen")
        if IsValid(self) then
            self:OnPlayerCreationCompleted(...)
        end
    end)

    -- Request character creation for faction
    GM.RequestPlayerCreation("", Faction.Name, {})
end

function PANEL:Init()
    self:SetSize(ScrW(), ScrH())
    self:Center()
    self:SetBackgroundColor(Color(0, 0, 0, 10))
    gui.EnableScreenClicker(true)

    local OldPaint = self.Paint
    self.Paint = function(self, w, h)
        blur(self, 7, 15, 150)
        OldPaint(self, w, h)
    end

    TopBar = vgui.Create("DPanel", self)
    TopBar:SetHeight(100)
    TopBar:SetAlpha(200)
    TopBar:SetContentAlignment(5)
    TopBar:Dock(TOP)

    -- Top Bar Setup
    do
        local ServerText = vgui.Create("DLabel", TopBar)
        ServerText:SetText("Welcome to the "..GetHostName().." Server!")
        ServerText:SetFont("DermaLarge")
        ServerText:SizeToContents()
        ServerText:SetWidth(ScrW())
        ServerText:SetContentAlignment(5)
        ServerText:DockMargin(0, 10, 0, 10)
        ServerText:Dock(TOP)

        MakeButton("Server Information", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local DHTML = vgui.Create("DHTML", CenterWindow)
            DHTML:Dock(FILL)
            DHTML:OpenURL("http://diaspora-community.com/discussion/8/server-motd")

            self.CurrentChild = DHTML
        end)

        --[[
        MakeButton("Map Information", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local NotImplementedText = vgui.Create("DLabel", CenterWindow)
            NotImplementedText:SetText("Coming Soon!")
            NotImplementedText:SetFont("DermaLarge")
            NotImplementedText:SizeToContents()
            NotImplementedText:SetWidth(ScrW())
            NotImplementedText:SetContentAlignment(5)
            NotImplementedText:DockMargin(0, 10, 0, 10)
            NotImplementedText:Dock(TOP)
            self.CurrentChild = NotImplementedText
        end)
        ]]

        MakeButton("Help and Tutorials", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local DHTML = vgui.Create("DHTML", CenterWindow)
            DHTML:Dock(FILL)
            DHTML:OpenURL("http://diaspora-community.com/categories/help-and-tutorials")

            self.CurrentChild = DHTML
        end)

        MakeButton("Issue Tracker", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local DHTML = vgui.Create("DHTML", CenterWindow)
            DHTML:Dock(FILL)
            DHTML:OpenURL("https://gitlab.com/TeamDiaspora/spacecombat2/issues")

            self.CurrentChild = DHTML
        end)

        --[[
        MakeButton("Character Stats", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local NotImplementedText = vgui.Create("DLabel", CenterWindow)
            NotImplementedText:SetText("Coming Soon!")
            NotImplementedText:SetFont("DermaLarge")
            NotImplementedText:SizeToContents()
            NotImplementedText:SetWidth(ScrW())
            NotImplementedText:SetContentAlignment(5)
            NotImplementedText:DockMargin(0, 10, 0, 10)
            NotImplementedText:Dock(TOP)
            self.CurrentChild = NotImplementedText
        end)

        MakeButton("Progression", LEFT, TopBar, function(Button)
            self:ClearCenterWindow()

            local NotImplementedText = vgui.Create("DLabel", CenterWindow)
            NotImplementedText:SetText("Coming Soon!")
            NotImplementedText:SetFont("DermaLarge")
            NotImplementedText:SizeToContents()
            NotImplementedText:SetWidth(ScrW())
            NotImplementedText:SetContentAlignment(5)
            NotImplementedText:DockMargin(0, 10, 0, 10)
            NotImplementedText:Dock(TOP)
            self.CurrentChild = NotImplementedText
        end)
        ]]
    end

    CenterWindow = vgui.Create("DPanel", self)
    CenterWindow:SetAlpha(200)
    CenterWindow:Dock(FILL)
    CenterWindow:DockMargin(40, 40, 40, 40)

    self:ClearCenterWindow()
    local DHTML = vgui.Create("DHTML", CenterWindow)
    DHTML:Dock(FILL)
    DHTML:OpenURL("http://diaspora-community.com/discussion/8/server-motd")
    self.CurrentChild = DHTML

    BottomBar = vgui.Create("DPanel", self)
    BottomBar:SetHeight(150)
    BottomBar:SetAlpha(200)
    BottomBar:Dock(BOTTOM)

    -- Bottom Bar Setup
    do
        local function MakeSpawnIcon(Model, DoClick)
            local Button = vgui.Create("DModelPanel", BottomBar)
            Button.DoClick = DoClick
            Button:SetSize(128, 128)
            Button:SetModel(Model)
            Button:SetFOV(20)
            Button:SetAnimated(true)
            Button:SetLookAt(Vector(0, 0, 68))
            Button:DockMargin(5, 5, 5, 5)
            Button:Dock(LEFT)

            local Sequence = Button:GetEntity():LookupSequence("idle")
            Button:GetEntity():SetSequence(Sequence)
        end

        --MakeSpawnIcon("models/gman_high.mdl", function() print("HELO") end)
        --MakeSpawnIcon("models/gman_high.mdl", function() print("HELO") end)
        --MakeSpawnIcon("models/gman_high.mdl", function() print("HELO") end)
        --MakeSpawnIcon("models/gman_high.mdl", function() print("HELO") end)
        --MakeSpawnIcon("models/gman_high.mdl", function() print("HELO") end)

        local New = MakeButton("Select Faction", FILL, BottomBar, function(Button)
            -- Clear center window
            self:ClearCenterWindow()

            -- Hide the button
            Button:SetDisabled(true)

            -- Start character creation
            local FactionList = vgui.Create("DPanel", CenterWindow)
            FactionList:DockMargin(10, 10, 10, 10)
            FactionList:Dock(FILL)
            FactionList:InvalidateLayout(true)

            local SelectFactionText = vgui.Create("DLabel", FactionList)
                SelectFactionText:SetText("Please select a faction:")
                SelectFactionText:SetFont("DermaLarge")
                SelectFactionText:SizeToContents()
                SelectFactionText:SetContentAlignment(1)
                SelectFactionText:DockMargin(20, 10, 10, 10)
                SelectFactionText:Dock(TOP)

            -- THE HAX
            timer.Simple(0, function()
                for _, Faction in ipairs(Factions) do
                    -- Faction image
                    local DImage = vgui.Create("DImageButton", FactionList)
                    DImage:SetImage(Faction.Image)
                    DImage:SetKeepAspect(true)
                    DImage:SetWide((FactionList:GetSize() / 5) - 10)
                    DImage:DockMargin(5, 10, 5, 10)
                    DImage:Dock(LEFT)

                    -- Faction color fade
                    local FactionColorFade = vgui.Create("DImageButton", DImage)
                    FactionColorFade:SetImage("gui/gradient_up")
                    FactionColorFade:SetColor(Color(Faction.Color.r / 4, Faction.Color.g / 4, Faction.Color.b / 4, 255))
                    FactionColorFade:Dock(FILL)

                    FactionColorFade.DoClick = function()
                        self:OnFactionSelected(Faction)
                    end

                    -- Faction Description
                    local FactionDescriptionScrollbox = vgui.Create("DScrollPanel", FactionColorFade)
                    FactionDescriptionScrollbox:SetSize(350, 300)
                    FactionDescriptionScrollbox:Dock(BOTTOM)

                    local OldPaint = FactionDescriptionScrollbox.Paint
                    FactionDescriptionScrollbox.Paint = function(self, w, h)
                        blur(self, 5, 5, 150)
                        OldPaint(self, w, h)
                    end

                    local Markup = markup.Parse("<font=FactionSelectFont>"..Faction.Description.."</font>", 320)
                    local Text = ""

                    for i,k in pairs(Markup.blocks) do
                        Text = Text..k.text
                    end

                    local FactionDescription = vgui.Create("DTextEntry", FactionDescriptionScrollbox)
                    FactionDescription:SetWide(320)
                    FactionDescription:SetTall(Markup:GetHeight() > 300 and Markup:GetHeight() or 300)
                    FactionDescription:SetWrap(true)
                    FactionDescription:SetText(Text)
                    FactionDescription:SetFont("FactionSelectFont")
                    FactionDescription:SetContentAlignment(1)
                    FactionDescription:SetMultiline(true)
                    FactionDescription:SetEditable(false)
                    FactionDescription:Dock(TOP)

                    FactionDescriptionScrollbox:AddItem(FactionDescription)
                end
            end)
            -- end of hax

            self.CurrentChild = FactionList
        end)
        New:SetSize(128, 128)
        New:SetEnabled(false)

        timer.Simple(10, function()
            if IsValid(New) then
                New:SetEnabled(true)
            end
        end)
    end
end

vgui.Register("SC.CharacterMenu", PANEL, "DPanel")