--[[
	Space Combat Inventory Control Panel
	Author: Steve "Steeveeo" Green

	For use in any player-accessible inventory, like
	own pockets and global storage.
]]--

local PANEL = {}

PANEL.ItemList = {}
PANEL.CurSelectedItem = nil

PANEL.Capacity = 400.0
PANEL.CapacityUsed = 0.0
PANEL.Mass = 0.0
PANEL.InfoLabel = nil

PANEL.ResourcePanel = nil

PANEL.DropItemButton = nil
PANEL.DestroyItemButton = nil

PANEL.UseSmallDisplayMode = false

--[[---------------------------------------------------------
   Initialize Inventory Panel
-----------------------------------------------------------]]
function PANEL:Init()
	--Inventory Information
	self.InfoLabel = vgui.Create("DLabel", self)

	local info = "Capacity: " .. self.CapacityUsed .. "L / " .. self.Capacity .. "L\n"
	info = info .. "Mass: " .. self.Mass .. " kg"

	self.InfoLabel:SetText(info)
	self.InfoLabel:SizeToContents()
	self.InfoLabel:Dock(TOP)
	self.InfoLabel:DockMargin(0, 5, 0, 0)

	--Resource List Scrollpane
	self.ResourcePanel = vgui.Create("DScrollPanel", self)
	self.ResourcePanel:Dock(FILL)
	self.ResourcePanel:DockMargin(0, 5, 0, 2)
	self.ResourcePanel:SetBackgroundColor(Color(255, 255, 255, 255))
	self.ResourcePanel:SetPaintBackground(true)
	self.ResourcePanel:InvalidateLayout()
end


--[[---------------------------------------------------------
   Change Settings
-----------------------------------------------------------]]
function PANEL:SetSmallDisplayMode(Enabled)
    self.UseSmallDisplayMode = Enabled or false
end

function PANEL:LoadFromContainer(Container)
    self.ResourceContainer = Container

    local Mass = 0
    for ResourceName, Resource in pairs(Container:GetStored()) do
		local InventoryItem = vgui.Create("InventoryItem", self.ResourcePanel)
		InventoryItem:Dock(TOP)
        InventoryItem:DockMargin(2, 1, 2, 1)
        InventoryItem:SetSmallDisplayMode(self.UseSmallDisplayMode)
		InventoryItem:LoadFromResource(Resource)
        InventoryItem.InventoryPanel = self
        Mass = Mass + Resource:GetAmount()
		table.insert(self.ItemList, InventoryItem)
    end

    self.CapacityUsed = self.ResourceContainer:GetUsed()
    self.Capacity = self.ResourceContainer:GetSize()
    self.Mass = Mass

    local info = "Capacity: " .. self.CapacityUsed .. "L / " .. self.Capacity .. "L\n"
	info = info .. "Mass: " .. self.Mass .. " kg"

	self.InfoLabel:SetText(info)
end


--[[---------------------------------------------------------
   InventoryItem Selection
-----------------------------------------------------------]]
function PANEL:SetSelected(item)
	--Deselect if another item is already selected
	if self.CurSelectedItem and self.CurSelectedItem ~= item then
		self.CurSelectedItem:Deselect()
	end

	--Select this one (since we do the selection event on InventoryItem, we only need to update the tracker)
	self.CurSelectedItem = item
end



vgui.Register("InventoryPanel", PANEL, "Panel")