AddCSLuaFile()

if not CLIENT then return end

local PANEL = {}

local function RecursivePopulate(Parent, Table)
    for Category, Data in SortedPairs(Table) do
        if istable(Data) then
            local Child = Parent:AddNode(Category, "icon16/folder.png")
            RecursivePopulate(Child, Data)
        else
            local Child = Parent:AddNode(Category, "icon16/page.png")
            Child.Data = Data
        end
    end
end

function PANEL:PopulateFromTable(Table)
    RecursivePopulate(self, Table)
end

function PANEL:PopulateFromTableByValue(Table, CategroyValueName, ItemValueName, IgnoreValueName)
    local CategoryTable = {}
    for Index, Subtable in SortedPairs(Table) do
        if istable(Subtable) and isstring(Subtable[CategroyValueName]) and isstring(Subtable[ItemValueName]) then
            if isstring(IgnoreValueName) then
                if Subtable[IgnoreValueName] then
                    continue
                end
            end

            local Subcategories = string.Explode("/", Subtable[CategroyValueName])
            local LastCategory
            for _, Subcategory in ipairs(Subcategories) do
                if not LastCategory then
                    CategoryTable[Subcategory] = CategoryTable[Subcategory] or {}
                    LastCategory = CategoryTable[Subcategory]
                else
                    LastCategory[Subcategory] = LastCategory[Subcategory] or {}
                    LastCategory = LastCategory[Subcategory]
                end
            end

            LastCategory[Subtable[ItemValueName]] = Index
        end
    end

    self:PopulateFromTable(CategoryTable)
end

vgui.Register("DCategoryTree", PANEL, "DTree")