AddCSLuaFile()

local GM = GM
local PlayerMeta = FindMetaTable("Player")
local Admin = SC.Administration or {}

-- Tables
Admin.Permissions = Admin.Permissions or {}
Admin.UserPermissions = Admin.UserPermissions or {}
Admin.Commands = Admin.Commands or {}
Admin.Aliases = Admin.Aliases or {}

-- Define new Player functions to get user permissions
function PlayerMeta:HasPermission(Permission)
    -- Force enable all permissions for the listen server host
    if SERVER and self:IsListenServerHost() then
        return true
    end

    local ID64 = self:SteamID64()
    return Admin.UserPermissions[ID64] and Admin.UserPermissions[ID64][Permission]
end

function PlayerMeta:HasPermissions(Permissions)
    if not Permissions then return true end
    if table.Count(Permissions) < 1 then return true end

    for i,k in pairs(Permissions) do
        if not self:HasPermission(k) then return false end
    end

    return true
end

function PlayerMeta:GetPermissions()
    local ID64 = self:SteamID64()
    return Admin.UserPermissions[ID64]
end

-- Functions to control commands and permissions
function Admin.RegisterPermission(Name, Description)
    Admin.Permissions[Name] = Description or Name
end

function Admin.IsValidPermission(Name)
    return Admin.Permissions[Name] ~= nil
end

function Admin.RegisterCommand(Name, Description, Permissions, Arguments, Callback)
    Admin.Commands[Name] = {
        Description = Description,
        Permissions = Permissions,
        Arguments = Arguments,
        Callback = Callback
    }
end

function Admin.RegisterCommandAlias(Name, Alias)
    Admin.Aliases[Alias] = Name
end

function Admin.GetCommandPermissions(Name)
    return Admin.Commands[Name] and Admin.Commands[Name].Permissions or {}
end

function Admin.GetCommandDescription(Name)
    return Admin.Commands[Name] and Admin.Commands[Name].Description or "Invalid Command"
end

function Admin.GetCommandArguments(Name)
    return Admin.Commands[Name] and Admin.Commands[Name].Arguments or {}
end

function Admin.GetCommandCallback(Name)
    return Admin.Commands[Name] and Admin.Commands[Name].Callback or function() end
end

function Admin.ParseArguments(ToParse, Command)
    local ArgumentTypes = Admin.GetCommandArguments(Command)
    local ParsedArguments = {}
    local RemainingString = ToParse

    -- Loop over the function's expected arguments to parse the string
    for _, Argument in ipairs(ArgumentTypes) do
        local Name = Argument.Name
        local Type = Argument.Type
        local CurrentString = ""
        local LastCharacter = ''
        local IsOptional = Type:sub(1, 9) == "optional_"
        local InQuotes = false
        local HitSpace = false

        for c in RemainingString:gmatch(".") do
            -- If we hit a non-escaped quote then exclude it from the string and continue building the string
            if c == '"' and LastCharacter ~= '\\' then
                InQuotes = not InQuotes
            -- If we hit a space and we're not inside of a set of quotes then stop the loop
            elseif c == ' ' and not InQuotes then
                HitSpace = true
                break
            -- Otherwise just add the character to the string for later
            else
                CurrentString = CurrentString..c
            end

            LastCharacter = c
        end

        -- If we're missing data for a required argument then throw an error out
        if #CurrentString == 0 then
            if not IsOptional then
                return nil, "Missing non-optional argument "..Name.."!"
            end
        else
            -- Convert the string into a useful format
            if Type == "optional_string" or Type == "string" then
                ParsedArguments[Name] = CurrentString
            elseif Type == "optional_bool" or Type == "bool" then
                ParsedArguments[Name] = tobool(CurrentString)
            elseif Type == "optional_vector" or Type == "angle" then
                ParsedArguments[Name] = Angle(CurrentString)
            elseif Type == "optional_vector" or Type == "vector" then
                ParsedArguments[Name] = Vector(CurrentString)
            elseif Type == "optional_entity" or Type == "entity" then
                ParsedArguments[Name] = Entity(tonumber(CurrentString) or 0)
            elseif Type == "optional_player" or Type == "player" then
                ParsedArguments[Name] = FindPlayerByName(CurrentString)
            elseif Type == "optional_number" or Type == "number" then
                ParsedArguments[Name] = tonumber(CurrentString)
            else
                error("Invalid argument type detected in command \""..Command.."\" for argument \""..Name.."\"\n")
            end

            RemainingString = RemainingString:sub(#CurrentString + (HitSpace and 1 or 0) + 1)
        end
    end

    return ParsedArguments
end

SC.Administration = SC.Administration or Admin

include("cl_init.lua")
include("commands/commands.lua")