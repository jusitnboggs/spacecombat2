AddCSLuaFile()

if not CLIENT then return end

local Admin = SC.Administration

-- Hooks
hook.Add("InitPostEntity", "SC.Admin.ClientInit", function()
    hook.Run("RegisterSC2Commands")
end)

hook.Add("ChatAutoComplete", "SC.Admin.ChatAutoComplete", function(Autocomplete, Text)
    for i, k in pairs(Admin.Commands) do
        table.insert(Autocomplete, "!"..i)
        table.insert(Autocomplete, "/"..i)
    end
end)

-- Networking
net.Receive("SC.Admin.PermissionBroadcast", function()
    local SteamID = net.ReadString()
    Admin.UserPermissions[SteamID] = net.ReadTable()
end)

net.Receive("SC.Admin.RequestPermissions", function()
    Admin.UserPermissions = net.ReadTable()
end)

-- Client UI
local UIPanel
function Admin.OpenUI()

end

function Admin.CloseUI()

end