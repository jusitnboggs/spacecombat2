AddCSLuaFile()

local Admin = SC.Administration
local PlayerMeta = FindMetaTable("Player")

function PlayerMeta:IsAdmin()
    return self:HasPermission("Admin")
end

function PlayerMeta:IsSuperAdmin()
    return self:HasPermission("SuperAdmin")
end

hook.Add("RegisterSC2Commands", "RegisterAdministrationCommands", function()
    -- Register any permissions we need for these commands
    Admin.RegisterPermission("KickPlayer", "The ability to kick a player from the server.")
    Admin.RegisterPermission("BanPlayer", "The ability to ban a player from the server.")
    Admin.RegisterPermission("SuperAdmin", "This permission will make other addons recognize the player as a 'super admin'")
    Admin.RegisterPermission("Admin", "This permission will make other addons recognize the player as a 'admin'")
    Admin.RegisterPermission("SetPermissions", "This permission will allow the player to set other user's permissions.")

    -- Register commands
    Admin.RegisterCommand("Kick", "Kick someone from the server",
        -- Permissions
        {
            "KickPlayer"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Reason",
                Type = "optional_string"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target
            local Reason = Arguments.Reason or "You were kicked from the server."

            if IsValid(Target) then
                Target:Kick(Reason)
            end
        end)

    Admin.RegisterCommand("Ban", "Ban someone from the server",
        -- Permissions
        {
            "BanPlayer"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Reason",
                Type = "optional_string"
            },
            {
                Name = "Time",
                Type = "optional_number"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target
            local Time = Arguments.Time or -1
            local Reason = Arguments.Reason or "You were banned from the server."

            if IsValid(Target) then
                Target:Ban(Time, false)
                Target:Kick(Reason)
            end
        end)

    Admin.RegisterCommand("AddPermission", "Gives someone permission to do something.",
        -- Permissions
        {
            "SetPermissions"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Permission",
                Type = "string"
            },
        },

        -- Callback
        function(Executor, Arguments)
            if Arguments.Permission == "*" then
                Executor:ChatPrint("Not implemented yet!")
            end

            if not Admin.IsValidPermission(Arguments.Permission) then
                Executor:ChatPrint("Invalid permission!")
                return
            end

            Arguments.Target:AddPermission(Arguments.Permission)
            Executor:ChatPrint("Permissions updated!")
        end)

    Admin.RegisterCommand("RemovePermission", "Removes someone's permission to do something.",
        -- Permissions
        {
            "SetPermissions"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Permission",
                Type = "string"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target
            if Arguments.Permission == "*" then
                Executor:ChatPrint("Removing all permissions from user:")
                for Permission, _ in pairs(Target:GetPermissions()) do
                    Executor:ChatPrint("  - "..Permission)
                end

                Arguments.Target:ResetPermissions()
            end

            if not Admin.IsValidPermission(Arguments.Permission) then
                Executor:ChatPrint("Invalid permission!")
            end

            Arguments.Target:RemovePermission(Arguments.Permission)
            Executor:ChatPrint("Permissions updated!")
        end)
end)