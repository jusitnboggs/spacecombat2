AddCSLuaFile()

include("administration.lua")
include("fun.lua")
include("health.lua")
include("teleportation.lua")
include("afk.lua")
include("help.lua")

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterGeneralCommands", function()
    -- Register commands
    Admin.RegisterCommandAlias("List", "Help")
    Admin.RegisterCommand("List", "List all commands usable by a user",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "optional_player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target or Executor

            if IsValid(Target) then
                Executor:ChatPrint("[Space Combat Administration] - "..Target:Nick().." has access to these commands:")
                for Command, Data in pairs(Admin.Commands) do
                    if Target:HasPermissions(Data.Permissions) then
                        Executor:ChatPrint("  !"..Command..": "..Data.Description)
                    end
                end
            end
        end)

    Admin.RegisterCommand("ListPermissions", "List all permissions a user has",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "optional_player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target or Executor

            if IsValid(Target) then
                Executor:ChatPrint("[Space Combat Administration] - "..Target:Nick().." has access to these permissions:")
                for Permission, _ in pairs(Target:GetPermissions()) do
                    Executor:ChatPrint("  - "..Permission)
                end
            end
        end)

    Admin.RegisterCommand("Arguments", "List all arguments for a command",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "optional_player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target or Executor

            if IsValid(Target) then
                --dostuff
            end
        end)
end)