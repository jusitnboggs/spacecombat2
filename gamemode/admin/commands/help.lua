AddCSLuaFile()

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterHelpCommands", function()
    Admin.RegisterPermission("ForceHelp")

    -- Register commands
    Admin.RegisterCommand("MOTD", "Open the MOTD",
        -- Permissions
        {
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)
            if IsValid(Executor) then
                Executor:SendLua("gui.OpenURL('https://diaspora-community.com/discussion/8/server-motd')")
            end
        end)

    Admin.RegisterCommandAlias("MOTD", "Rules")

    Admin.RegisterCommand("Downloads", "Open the Downloads page",
        -- Permissions
        {
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)
            if IsValid(Executor) then
                Executor:SendLua("gui.OpenURL('https://diaspora-community.com/discussion/9/redirect/p1')")
            end
        end)

    Admin.RegisterCommandAlias("Downloads", "Addons")

    Admin.RegisterCommand("Tutorials", "Open the Tutorials page",
        -- Permissions
        {
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)
            if IsValid(Executor) then
                Executor:SendLua("gui.OpenURL('https://www.youtube.com/playlist?list=PLf3I7tC1fyryhZGA1FH-UlQz8s2pHDFvu')")
            end
        end)

    Admin.RegisterCommand("ForceMOTD", "Open the MOTD for someone",
        -- Permissions
        {
            "ForceHelp"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                Target:SendLua("gui.OpenURL('https://diaspora-community.com/discussion/8/server-motd')")
            end
        end)

    Admin.RegisterCommand("ForceDownloads", "Open the Downloads for someone",
        -- Permissions
        {
            "ForceHelp"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                Target:SendLua("gui.OpenURL('https://diaspora-community.com/discussion/9/redirect/p1')")
            end
        end)

        Admin.RegisterCommand("ForceTutorials", "Open the Tutorials for someone",
        -- Permissions
        {
            "ForceHelp"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                Target:SendLua("gui.OpenURL('https://www.youtube.com/playlist?list=PLf3I7tC1fyryhZGA1FH-UlQz8s2pHDFvu')")
            end
        end)
end)