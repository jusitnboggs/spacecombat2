AddCSLuaFile()

local Admin = SC.Administration

local GimpSentences = {
	"im a nub",
	"i love it when the admin physguns meh :P",
	"DIS IS SPARTA!!!",
	"my g, why r u guys such nubs?",
	"k rly, u guys sux at teh wirmod!",
	"admin? cn u jail me?",
	"whr do babys cum fom?",
	"I LOV BEIN GIMPED BI TEH ADMINZZZ!!!",
	"mah cht r is brkn, i r blame teh Brandon.",
	"y u r mak me gimp? :(",
	"I are retarded.",
	"make me admin plox kthnx.",
	"I feel asleep.",
	"I am Error.",
	"I would very much appreciate it if you removed the gimp status from my player now.",
	"How do I type stuff into the chat box?",
	"Someone pull my lever!",
	"I'm, like, TURBO GAY right now.",
	"What is tree?",
	"I can has cheezburger?.",
	"I jizz, in, my pants.",
	"Please remove my stuff.",
	"I farted",
	"WOW NEW MAP",
	"how u make that inverted b?",
	"Man, my penis is so big if I laid it out on a keyboard it'd go all the way from A to Z",
	"what the fuck is wtf",
	"HOW THE FUCK CAN YOU TELL THAT I'M 13 BY LOOKING AT WHAT I'M WRITEING??????????????????????",
	"I put on my robe and wizard hat.",
	"whats oxygen for?",
	"ADMIN ABOOS",
	"HAHA! What does 'Gimped' mean?!",
	"Does a fusion generator cloak your ship??",
	"I blame Dubby!",
	"Wtf, I farted and it smelled like brocolli",
	"I wish I was your derivative so I could lie tangent to your curves.",
	"Your mother is so fat that the recursive function calculating her mass causes a stack overflow.",
	":O <========3",
	"brb, fire",
	"I <3 Steeveeo",
	"WHAT YOU SAY??",
	"Someone set up us the bomb!",
	"Wtf that's not what I wrote!",
	"Asgard hacks!",
	"I JUST LOST THE GAME",
	"Heheh, pull my finger.",
	"Why do all my chat lines get replaced with these stupid quotes?",
	"hrm. I couldn't get laid in a womens prison with a fist full of pardons",
	"Spiderman just sprays white fluid via a wrist action. We can all do that.",
	"how 2 rewind DVDs?",
	"!afk",
	"they see me rcon'in",
	"MY FISTS!! THEY ARE MADE OF HAM!!",
	"i like my whine with biscuits"
}

hook.Add("RegisterSC2Commands", "RegisterFunCommands", function()
    -- Register any permissions we need for these commands
    Admin.RegisterPermission("FunCommands", "The ability to use 'fun' commands.")
    Admin.RegisterPermission("PickupPlayers", "The ability to use pickup players using your physgun.")

    -- Register commands
    Admin.RegisterCommand("Gimp", "Makes the targeted player say silly things.",
        -- Permissions
        {
            "FunCommands"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Duration",
                Type = "optional_number"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target
            local Duration = Arguments.Duration

            if IsValid(Target) then
                Target.IsGimped = not Target.IsGimped

                if Duration then
                    timer.Simple(Duration, function()
                        if IsValid(Target) then
                            Target.IsGimped = false
                        end
                    end)
                end
            end
        end)
end)

if SERVER then
    hook.Add("PlayerSay", "SC2.Admin.FunCommandSay", function(Player, Text, Team)
        if Player.IsGimped then
            return GimpSentences[math.random(1, #GimpSentences)]
        end
    end)

	hook.Add("PhysgunPickup", "PhysgunPlayerPickup", function(Player, Entity)
        if Entity:IsPlayer() and Player:HasPermission("PickupPlayers") then
            Entity:Freeze(true)
            Entity:SetMoveType(MOVETYPE_NOCLIP)
            return true
        end
	end)

	hook.Add("PhysgunDrop", "PhysgunPlayerDrop", function(Player, Entity)
		if Entity:IsPlayer() then
			Entity:SetMoveType(MOVETYPE_WALK)
			Entity:Freeze(false)
		end
	end)
end