require("lip")

local Quiz = {}

Quiz.ShouldKickPlayerOnFailure = true
Quiz.KickWaitTime = 10

Quiz.Questions = {}
Quiz.Questions["Do you need to parent your props?"] = {"Yes", "No"}
Quiz.Questions["Is violence on spawn allowed?"] = {"Yes", "No"}
Quiz.Questions["Can you attack players who aren't on spawn?"] = {"Yes", "No"}
Quiz.Questions["Should you expect to be banned if you're a dick?"] = {"Yes", "No"}
Quiz.Questions["How do you know someone is an admin?"] = {"Scoreboard", "Asking in chat"}

Quiz.Answers = {}
Quiz.Answers["Do you need to parent your props?"] = "Yes"
Quiz.Answers["Is violence on spawn allowed?"] = "No"
Quiz.Answers["Can you attack players who aren't on spawn?"] = "Yes"
Quiz.Answers["Should you expect to be banned if you're a dick?"] = "Yes"
Quiz.Answers["How do you know someone is an admin?"] = "Scoreboard"

if not sql.TableExists("quiz") then
	local query = "CREATE TABLE quiz (unique_id varchar(255), taken int)"
	local result = sql.Query(query)
	if not sql.TableExists("quiz") then
		SC.Error("Something went wrong with the quiz query!\n")
        SC.Error(sql.LastError(result) .. "\n")
    end
end

if not sql.TableExists("quiz_failures") then
	local query = "CREATE TABLE quiz_failures (question_id INTEGER NOT NULL PRIMARY KEY, failcount BIGINT NOT NULL)"
	local result = sql.Query(query)
	if not sql.TableExists("quiz_failures") then
		SC.Error("Something went wrong with the quiz_failures query!\n")
		SC.Error(sql.LastError(result) .. "\n")
	end
end

hook.Add("InitPostEntity", "LoadQuizConfiguration", function()
    if file.Exists(SC.DataFolder.."/config/quiz.txt", "DATA") then
        local NewData = lip.load(SC.DataFolder.."/config/quiz.txt")

        if NewData and table.Count(NewData) > 0 and NewData.Answers and NewData.Settings then
            Quiz.Answers = NewData.Answers
            Quiz.ShouldKickPlayerOnFailure = NewData.Settings.ShouldKickPlayerOnFailure
            Quiz.KickWaitTime = NewData.Settings.KickWaitTime
            Quiz.Questions = {}

            sql.Begin()
            for section, data in pairs(NewData) do
                if string.lower(section:sub(0, 10)) == "questions/" then
                    local Question = section:sub(11)
                    Quiz.Questions[Question] = data
                    sql.Query(string.format("INSERT OR IGNORE INTO quiz_failures('question_id', 'failcount') VALUES (%s, 0)", util.CRC(Question)))
                end
            end
            sql.Commit()

            if table.Count(Quiz.Questions) ~= table.Count(Quiz.Answers) then
                SC.Error("Bad number of quiz answers detected! Users will not be able to finish the quiz successfully!")
            end
        else
            SC.Error("Malformed quiz configuration file detected!")
        end
    else
        local SaveData = {}

        SaveData["Settings"] = {
            ["ShouldKickPlayerOnFailure"] = Quiz.ShouldKickPlayerOnFailure,
            ["KickWaitTime"] = Quiz.KickWaitTime
        }

        SaveData["Answers"] = Quiz.Answers

        for i,k in pairs(Quiz.Questions) do
            SaveData["Questions/"..i] = k
        end

        file.CreateDir(SC.DataFolder.."/config/")
        lip.save(SC.DataFolder.."/config/quiz.txt", SaveData)
    end
end)

hook.Add("RegisterSC2Commands", "RegisterQuizCommands", function()
    local Admin = SC.Administration

    Admin.RegisterPermission("ResetQuiz", "The ability to force a player to take the quiz again")

    Admin.RegisterCommand("ResetQuiz", "Forces a player to take the quiz again",
        -- Permissions
        {
            "ResetQuiz"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                sql.Query(string.format("DELETE FROM TABLE quiz WHERE unique_id='%s'", Target:SteamID64()))

                net.Start("SC.GetQuizQuestions")
                net.WriteTable(Quiz.Questions)
                net.WriteBool(Quiz.ShouldKickPlayerOnFailure)
                net.WriteInt(Quiz.KickWaitTime, 8)
                net.WriteBool(true)
                net.Send(Target)
            end
        end)

        Admin.RegisterCommand("ViewQuizFailures", "View quiz failure results",
        -- Permissions
        {
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)

            if IsValid(Executor) then
                local ToSend = {}
                local Failures = sql.Query("SELECT * FROM quiz_failures")
                for _, Data in pairs(Failures) do
                    for Question, Answer in pairs(Quiz.Answers) do
                        if util.CRC(Question) == Data.question_id then
                            ToSend[Question] = Data.failcount
                        end
                    end
                end

                net.Start("SC.ViewQuizFailures")
                net.WriteTable(ToSend)
                net.Send(Executor)
            end
        end)
end)

net.Receive("SC.GetQuizQuestions", function(len, ply)
    local NeedsQuiz = true
    if sql.Query("SELECT * FROM quiz WHERE unique_id='"..ply:SteamID64().."'") then
        NeedsQuiz = false
    end

    net.Start("SC.GetQuizQuestions")
    net.WriteTable(Quiz.Questions)
    net.WriteBool(Quiz.ShouldKickPlayerOnFailure)
    net.WriteInt(Quiz.KickWaitTime, 8)
    net.WriteBool(NeedsQuiz)
    net.Send(ply)
end)

net.Receive("SC.CheckQuiz", function(len, ply)
    local Choices = net.ReadTable()
    local Failed = not table.Compare(Choices, Quiz.Answers)

    if Quiz.ShouldKickPlayerOnFailure and Failed then
        timer.Simple(Quiz.KickWaitTime, function()
            if IsValid(ply) then
                ply:Kick("You failed the quiz! Please try again later!")
            end
        end)
    end

    if Failed then
        sql.Begin()
        for Question, Answer in pairs(Choices) do
            if Quiz.Answers[Question] and Answer ~= Quiz.Answers[Question] then
                sql.Query(string.format("UPDATE quiz_failures SET failcount = failcount + 1 WHERE question_id = %s", util.CRC(Question)))
            end
        end
        sql.Commit()
    else
        sql.Query("INSERT INTO quiz ('unique_id','taken') VALUES ('"..ply:SteamID64().."','1')")
    end

    net.Start("SC.CheckQuiz")
    net.WriteBool(Failed)
    net.Send(ply)
end)

util.AddNetworkString("SC.GetQuizQuestions")
util.AddNetworkString("SC.CheckQuiz")
util.AddNetworkString("SC.ViewQuizFailures")