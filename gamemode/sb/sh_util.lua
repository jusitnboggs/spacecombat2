local GM = GM
local print = print
local tostring = tostring
local debug = debug
local type = type
local setmetatable = setmetatable
local getmetatable = getmetatable

local util = GM.util

--Table comparison function, this is as effecient as it gets sadly :(
function table.Compare(tbl1, tbl2)
	if table.Count(tbl1) == table.Count(tbl2) then --compare the length first, if they aren't the same we don't need to check every member
		for k, v in pairs(tbl1) do
			if tbl2[k] ~= v then return false end
		end
		for k, v in pairs(tbl2) do
			if tbl1[k] ~= v then return false end
		end
	else
		return false
	end
	return true
end

--Slightly slower version with support for sub tables
function table.Compare2(tbl1, tbl2)
	if table.Count(tbl1) == table.Count(tbl2) then
		for k, v in pairs(tbl1) do
			if type(v) == "table" and type(tbl2[k]) == "table" then
				if not table.Compare(v, tbl2[k]) then return false end
			else
				if v ~= tbl2[k] then return false end
			end
		end
		for k, v in pairs(tbl2) do
			if type(v) == "table" and type(tbl1[k]) == "table" then
				if not table.Compare(v, tbl1[k]) then return false end
			else
				if v ~= tbl1[k] then return false end
			end
		end
	else
		return false
	end
	return true
end

--Modified table.Compare that returns a table containing values not in the second table
function table.Difference(tbl1, tbl2)
	local ret = {}

	for k, v in pairs(tbl1) do
		if tbl2[k] ~= v then table.insert(ret, v) end
	end

	return ret
end

--Function to merge the values of several tables, automatically adds values that exist already.
function table.MergeValues(tbl1, ...)
	for _,tbl in pairs({...}) do
		for i,k in pairs(tbl) do
			if tbl1[i] and not (type(tbl1[i]) == "string") and not (type(tbl1[i]) == "table") then
				tbl1[i] = tbl1[i] + k
			elseif not tbl1[i] then
				tbl1[i] = k
			end
		end
	end
end

-- Utility function, automatically prints error
function PCallError(...)
	local errored, rA, rB, rC, rD, rE, rF, rG, rH = pcall(...)

	if not errored then
		ErrorNoHalt(rA)
		return false, rA
	end

	return true, rA, rB, rC, rD, rE, rF, rG, rH
end

--Fixing the networking, damnit garry stop failing
net.WriteVector = function(vec, HighPrecision)
	local Mode = 0
	local X = math.floor(vec.x * 100)
	local Y = math.floor(vec.y * 100)
	local Z = math.floor(vec.z * 100)

	if (math.abs(X) < 8388607) and (math.abs(Y) < 8388607) and (math.abs(Z) < 8388607) and not HighPrecision then
		Mode = 1
	end

	if Mode == 1 then
		if (math.abs(X) < 32767) and (math.abs(Y) < 32767) and (math.abs(Z) < 32767) then
			Mode = 2
		elseif (math.abs(X) < 2047) and (math.abs(Y) < 2047) and (math.abs(Z) < 2047) then
			Mode = 3
		end

		local Bits
		if Mode == 1 then
			Bits = 24
		elseif Mode == 2 then
			Bits = 16
		elseif Mode == 3 then
			Bits = 12
		end

		net.WriteUInt(Mode, 2)
		net.WriteInt(X, Bits)
		net.WriteInt(Y, Bits)
		net.WriteInt(Z, Bits)
	else
		net.WriteUInt(0, 2)
		net.WriteFloat(vec.x or 0)
		net.WriteFloat(vec.y or 0)
		net.WriteFloat(vec.z or 0)
	end
end

net.ReadVector = function()
	local Mode = net.ReadUInt(2)
	local Bits = 32
	if Mode == 1 then
		Bits = 24
	elseif Mode == 2 then
		Bits = 16
	elseif Mode == 3 then
		Bits = 8
	end

	if Bits < 32 then
		return Vector(net.ReadInt(Bits), net.ReadInt(Bits), net.ReadInt(Bits)) * 0.01
	else
		return Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
	end
end

if file.TFind == nil then
	function file.TFind(filename, func)
		local files, dirs = file.Find(filename, "GAME")

		func(filename, dirs, files)
	end
end

function file.Read(filename, path, mode)
	if path == true then path = "GAME" end
	if path == nil or path == false then path = "DATA" end
	if mode == nil then mode = "rb" end

	local f = file.Open(filename, mode, path)
	if not f then return end
	local str = f:Read(f:Size())
	f:Close()
	return str or ""
end

util.createReadOnlyTable = function(t)
	return setmetatable({}, {
		__index = t,
		__newindex = function()
			print("Attempt to update a read-only table")
			print(tostring(debug.traceback()))
		end,
		__metatable = false
	})
end

util.deepCopyTable = function(self, orig)
	local orig_type = type(orig)
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do
			copy[self:deepCopyTable(orig_key)] = self:deepCopyTable(orig_value)
		end
		setmetatable(copy, self:deepCopyTable(getmetatable(orig)))
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end

util.tableSlice = function(values,i1,i2)
	local res = {}
	local n = #values
	-- default values for range
	i1 = i1 or 1
	i2 = i2 or n
	if i2 < 0 then
		i2 = n + i2 + 1
	elseif i2 > n then
		i2 = n
	end
	if i1 < 1 or i1 > n then
		return {}
	end
	local k = 1
	for i = i1,i2 do
		res[k] = values[i]
		k = k + 1
	end
	return res
end

util.smoother = function(target, current, smooth)
	return current + math.Clamp((target - current) * RealFrameTime() / smooth, -1, 1)
end


nicenumber = {}

local numbers = {
	{
		name = "septillion",
		short = "sep",
		symbol = "Y",
		prefix = "yotta",
		zeroes = 10^24,
	},
	{
		name = "sextillion",
		short = "sex",
		symbol = "Z",
		prefix = "zetta",
		zeroes = 10^21,
	},
	{
		name = "quintillion",
		short = "quint",
		symbol = "E",
		prefix = "exa",
		zeroes = 10^18,
	},
	{
		name = "quadrillion",
		short = "quad",
		symbol = "P",
		prefix = "peta",
		zeroes = 10^15,
	},
	{
		name = "trillion",
		short = "T",
		symbol = "T",
		prefix = "tera",
		zeroes = 10^12,
	},
	{
		name = "billion",
		short = "B",
		symbol = "B",
		prefix = "giga",
		zeroes = 10^9,
	},
	{
		name = "million",
		short = "M",
		symbol = "M",
		prefix = "mega",
		zeroes = 10^6,
	},
	{
		name = "thousand",
		short = "K",
		symbol = "K",
		prefix = "kilo",
		zeroes = 10^3
	}
}

local one = {
	name = "ones",
	short = "",
	prefix = "",
	zeroes = 1
}

-- returns a table of tables that inherit from the above info
local floor = math.floor
function nicenumber.info(n, steps)
	if not n or n < 0 then return {} end
	if n > 10 ^ 300 then n = 10 ^ 300 end

	local t = {}

	steps = steps or #numbers

	local displayones = true
	local cursteps = 0

	for i = 1, #numbers do
		local zeroes = numbers[i].zeroes

		local nn = floor(n / zeroes)
		if nn > 0 then
			cursteps = cursteps + 1
			if cursteps > steps then break end

			t[#t+1] = setmetatable({value = nn},{__index = numbers[i]})

			n = n % numbers[i].zeroes

			displayones = false
		end
	end

	if n >= 0 and displayones then
		t[#t+1] = setmetatable({value = n},{__index = one})
	end

	return t
end

local sub = string.sub

-- returns string
-- example 12B 34M
function nicenumber.format(n, steps)
	local t = nicenumber.info(n, steps)

	steps = steps or #numbers

	local str = ""
	for i=1,#t do
		if i > steps then break end
		str = str .. t[i].value .. t[i].short .. " "
	end

	return sub(str, 1, -2) -- remove trailing space
end

-- returns string with decimals
-- example 12.34B
local round = math.Round
function nicenumber.formatDecimal(n, decimals)
	local t = nicenumber.info(n, 1)

	decimals = decimals or 2

	local largest = t[1]
	if largest then
		n = n / largest.zeroes
		return round(n, decimals) .. largest.short
	else
		return "0"
	end
end

-------------------------
-- nicetime
-------------------------
local times = {
	{ "y", 31556926 }, -- years
	{ "mon", 2629743.83 }, -- months
	{ "w", 604800 }, -- weeks
	{ "d", 86400 }, -- days
	{ "h", 3600 }, -- hours
	{ "m", 60 }, -- minutes
	{ "s", 1 }, -- seconds
}
function nicenumber.nicetime(n)
	n = math.abs(n)

	if n == 0 then return "0s" end

	local prev_name = ""
	local prev_val = 0
	for i=1,#times do
		local name = times[i][1]
		local num = times[i][2]

		local temp = floor(n / num)
		if temp > 0 or prev_name ~= "" then
			if prev_name ~= "" then
				return prev_val .. prev_name .. " " .. temp .. name
			else
				prev_name = name
				prev_val = temp
				n = n % num
			end
		end
	end

	if prev_name ~= "" then
		return prev_val .. prev_name
	else
		return "0s"
	end
end

function SC_DigiSpace(num)
    if not num or num == nil then num = 0 end
    local dspac = "0"

	while num >= 1000 do
		local remainder = math.Round((num/1000 - math.floor(num/1000))*1000)
		if remainder == 0 then remainder = "000"
		elseif remainder < 100 and remainder > 10 then remainder = "0"..remainder
		elseif remainder < 10 and remainder > 0 then remainder = "00"..remainder end

		num = math.floor(num/1000)

		if dspac == "0" then
			dspac = remainder
		else
			dspac = remainder..","..dspac
		end
	end

	if num > 0 then
		if dspac == "0" then dspac = math.Round(num) --The number was to small to go through the loop
		else dspac = math.Round(num)..","..dspac end
	end

	return dspac
end

function sc_ds(num)
    num = num or 0
	if num >= 1000000000 then
		return tostring(math.floor(num/10000000)/100).."B"
	elseif num >= 1000000 then
		return tostring(math.floor(num/10000)/100).."M"
	elseif num >= 1000 then
		return tostring(math.floor(num/10)/100).."K"
	else
		return tostring(math.floor(num*100)/100)
	end
end

function util.LoadINIFile(File)
    if file.Exists(File, "DATA") then
        local NewData = lip.load(File)

        if NewData and table.Count(NewData) > 0 then
            -- This table will contain the actual data
            local OutData = {}

            -- First parse the data for subtables
            for Section, Data in pairs(NewData) do
                -- Before we actually handle the section headers check for subtables in the data
                -- This only supports one level of subtables seperated by . right now.
                local ParsedData = {}
                for i,k in pairs(Data) do
                    local TableMatch, ValueMatch = string.match(i, "([%w%s]+)%.([%w%s]+)")
                    if TableMatch and ValueMatch then
                        ParsedData[TableMatch] = ParsedData[TableMatch] or {}
                        ParsedData[TableMatch][ValueMatch] = k
                    else
                        ParsedData[i] = k
                    end
                end

                -- Now parse the section headers for subtables
                if string.find(Section, "/") then
                    -- First make sure all the subtables for the section exist
                    local Subsections = string.Explode("/", Section)
                    local LastSection = OutData
                    for _,NewSection in pairs(Subsections) do
                        LastSection[NewSection] = LastSection[NewSection] or {}
                        LastSection = LastSection[NewSection]
                    end

                    -- Then set the data on the subsection
                    for i,k in pairs(ParsedData) do
                        LastSection[i] = k
                    end
                else
                    -- If we aren't dealing with a special table just copy the data over
                    OutData[Section] = ParsedData
                end
            end

            return true, OutData
        end
    end

    return false, {}
end