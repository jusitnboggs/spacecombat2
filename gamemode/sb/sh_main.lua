local GM = GM
local storagetypes = {}
local storagemodels = {}
local LastStorageID = 0
local resourcetypes = {}
local resourcestoragetypes = {}
local resourceinfo = {}
local resourceInfoPageBuilders = {}
local LastResourceID = 0

local itemRarityColors = {}
local itemRarityNames = {}
local itemRarityLevels = 0

local ResourceIDs = {}
local ResourceReverseLookup = {}

local StorageTypeIDs = {}
local StorageTypeReverseLookup = {}

function GM:GetStorageTypeID(String)
    if not StorageTypeIDs[String] then SC.Error("Unable to find storage type ID: "..tostring(String), 5) end
    return StorageTypeIDs[String]
end

function GM:GetStorageTypeFromID(ID)
    if not StorageTypeReverseLookup[ID] then SC.Error("Unable to find storage type with ID: "..tostring(ID).."\n\n"..debug.traceback().."\n", 5) end
    return StorageTypeReverseLookup[ID]
end

function GM:GetStorageTypeLookupTableByID()
    return table.Copy(StorageTypeReverseLookup)
end

function GM:GetStorageTypeLookupTable()
    return table.Copy(StorageTypeIDs)
end

function GM:GetResourceID(String)
    if not ResourceIDs[String] then SC.Error("Unable to find resource ID: "..tostring(String), 5) end
    return ResourceIDs[String]
end

function GM:GetResourceFromID(ID, ReturnResourceData)
    if not ResourceReverseLookup[ID] then SC.Error("Unable to find resource with ID: "..tostring(ID).."\n\n"..debug.traceback().."\n", 5) end
    if ReturnResourceData and ResourceReverseLookup[ID] then
        return resourcetypes[ResourceReverseLookup[ID]]
    else
        return ResourceReverseLookup[ID]
    end
end

function GM:GetResourceFromName(Name)
    if not resourcetypes[Name] then SC.Error("Unable to find resource with Name: "..tostring(Name).."\n\n"..debug.traceback().."\n", 5) end
    return resourcetypes[Name]
end

function GM:GetResourceLookupTableByID()
    return table.Copy(ResourceReverseLookup)
end

function GM:GetResourceLookupTable()
    return table.Copy(ResourceIDs)
end

function GM:GetStorageTypes()
	return table.Copy(storagetypes)
end

function GM:GetStorageModels()
	return table.Copy(storagemodels)
end

function GM:GetDefaultStorageModel(name)
    return storagemodels[name]
end

function GM:AddDefaultStorageModel(name, model)
    if not name or name == "" or not model or model == "" then return end
    storagemodels[name] = model
end

function GM:AddStorageType(name)
	if not storagetypes[name] then
		storagetypes[name] = true
		resourcestoragetypes[name] = {}
        LastStorageID = LastStorageID + 1

        StorageTypeIDs[name] = LastStorageID
        StorageTypeReverseLookup[LastStorageID] = name
	end
end

function GM:NewStorage(type, max, StorageMode)
	if type and storagetypes[type] then
		return GM.class.getClass("ResourceContainer"):new(type, max, {}, StorageMode)
	end
end

function GM:StorageTypeCount()
	return LastStorageID
end

function GM:GetResourceTypes()
	return table.Copy(resourcetypes)
end

function GM:GetResourcesOfType(type)
    return resourcestoragetypes[type]
end

function GM:AddResourceType(name, volumePerUnit, type, massPerUnit, itemIconModel, itemWorldModel, itemBaseRarity, itemArchetype, descriptionText)
    if not resourcetypes[name] and storagetypes[type] then
        LastResourceID = LastResourceID + 1

		resourcetypes[name] = GM.class.getClass("Resource"):new(name, volumePerUnit, type, nil, nil, LastResourceID, true)
		resourcestoragetypes[type][name] = resourcetypes[name]

        ResourceIDs[name] = LastResourceID
        ResourceReverseLookup[LastResourceID] = name

		resourceinfo[name] = {}
		resourceinfo[name].Volume = volumePerUnit or 1
		resourceinfo[name].Mass = massPerUnit or 1
		resourceinfo[name].Desc = descriptionText or "[PLACEHOLDER DESCRIPTION TEXT - FIXME]"
		resourceinfo[name].Icon = itemIconModel or "models/props_interiors/pot01a.mdl"
		resourceinfo[name].Model = itemWorldModel or "models/props_interiors/pot01a.mdl"
		resourceinfo[name].Rarity = itemBaseRarity or 1
		resourceinfo[name].Archetype = itemArchetype or "Item"
	end
end


function GM:GetResourceStorageType(name)
  if not resourcetypes[name] then return "" end

  return resourcetypes[name]:GetType()
end

function GM:NewResource(name, amount, max)
	if resourcetypes[name] then
    local res = resourcetypes[name]
		return GM.class.getClass("Resource"):new(res:GetName(), res:GetSize(), res:GetType(), amount, max, res:GetID())
	end
end

function GM:ResourceTypesCount()
	return LastResourceID
end


--Sets the factory function for building an item's information page to a named archetype.
--Note 1: If a builder function is not provided, the panel will use its internal builder (basically, just print description and amounts).
--Note 2: This is in Shared for now because this is where the other resource crap is; move to clientside-only if we decouple resourcing between client/server.
--	BuilderFunc = function( InventoryPanel dermaObject )
--EX:
--	name =			"Core Mod"
--	builderFunc =	function that generates a RichText element displaying bonuses in green,
--					detriments in red, and a Fitting Cost panel.
function GM:SetResourceInfoPageBuilder(name, builderFunc)
	if not isfunction(builderFunc) then
		error("[SC2 SHARED] - InfoPanel builder for " .. name .. " was not function type!")
		return
	end

	resourceInfoPageBuilders[name] = builderFunc
end

function GM:GetResourceInfoPageBuilder(name)
	return resourceInfoPageBuilders[name]
end

function GM:GetResourceInfoTable(name)
	if not resourceinfo[name] then return {} end

	return table.Copy(resourceinfo[name])
end


--Item Rarity Levels
function GM:DefineItemRarityLevel(level, name, color)
	if not itemRarityColors[level] or not itemRarityNames[level] then
		itemRarityNames[level] = name
		itemRarityColors[level] = color

		itemRarityLevels = itemRarityLevels + 1
	end
end

function GM:GetItemRarityColor(level)
	if itemRarityColors[level] then
		return itemRarityColors[level]
	else
		return Color(255, 255, 255, 255)
	end
end

function GM:GetItemRarityName(level)
	if itemRarityNames[level] then
		return itemRarityNames[level]
	else
		return "[UNDEFINED RARITY]"
	end
end



----------------------------------------------------------------------------------
--						RESOURCE DEFINITIONS START HERE							--
----------------------------------------------------------------------------------

--Rarity Settings
ITEM_RARITY_COMMON = 1
ITEM_RARITY_UNCOMMON = 2
ITEM_RARITY_RARE = 3
ITEM_RARITY_VERYRARE = 4
ITEM_RARITY_LEGENDARY = 5

GM:DefineItemRarityLevel(ITEM_RARITY_COMMON,	"Common",		Color(128, 128, 128, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_UNCOMMON,	"Uncommon",		Color(0, 255, 0, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_RARE,		"Rare",			Color(0, 0, 255, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_VERYRARE,	"Very Rare",	Color(196, 0, 196, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_LEGENDARY,	"Legendary",	Color(255, 128, 0, 255))


--Storages
GM:AddStorageType("Energy")
GM:AddStorageType("Cargo")
GM:AddStorageType("Liquid")
GM:AddStorageType("Gas")
GM:AddStorageType("Ammo")

-- Effectively, doesn't exist
GM:AddResourceType("Energy", 1, "Energy", 0, "models/items/car_battery01.mdl", "models/items/car_battery01.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Energy, voltage, amperage, watts, NRG, zapitude; a resource with many names, all of which determine if your guns are firing and your lifesupport will turn on.")

-- Cargo
GM:AddResourceType("Lifesupport Canister", 1, "Cargo", 1, "models/slyfo_2/acc_oxygenpaste.mdl", "models/slyfo_2/acc_oxygenpaste.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Lifesupport Canisters make it so you do not die when exposed to inhospitable climates, like the vacuum of space or the heat of a lava planet.\n\nDISCLAIMER: May not protect the user from actual lava.")
GM:AddResourceType("Empty Canister", 1, "Cargo", 0.25, "models/slyfo_2/acc_oxygenpaste.mdl", "models/slyfo_2/acc_oxygenpaste.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"It's light, almost as if there's nothing inside it...")
GM:AddResourceType("Carbon", 1, "Cargo", 2.25, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A black, dusty material; base material for organics. Can be burned for energy.")
GM:AddResourceType("Rocket Fuel Canister", 1, "Cargo", 1.06, "models/props_junk/propane_tank001a.mdl", "models/props_junk/propane_tank001a.mdl", ITEM_RARITY_UNCOMMON,	--Note: Basing this off of an empty canister (0.25 kg) being filled with a liter of kerosene (0.81kg/liter)
					"Resource",
					"A highly energetic fuel in a bottle. Can be attached to explosives to make ammunition.")

-- Gasses
--[[
    These values are based on Covalent Radius
    Uranium 2.58
    Carbon Dioxide 1.52
    Carbon Monoxide 1.47
    Methane 1.42
    Water 1.26
    Carbon 1.00
    Nitrogen 0.93
    Oxygen 0.86
    Hydrogen 0.40
]]--

--FIXME: I have no idea how to convert these covalent radii into meaningful mass numbers. Luckily mass is not important to gameplay, so these will all be 0.1kg for now. -Steeveeo

GM:AddResourceType("Oxygen", 0.86, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Tasty, life-giving oxygen. Most organics need this to function properly, with the exception of plants (plants are weird).")
GM:AddResourceType("Hydrogen", 0.40, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"The most abundant gas in the universe. Flammable.")
GM:AddResourceType("Nitrogen", 0.93, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A highly stable gas. Nitrogen has many uses, including coolant when liquified.")
GM:AddResourceType("Carbon Dioxide", 1.52, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Most organics produce this as a byproduct of their function. Has a heating effect when applied liberally to atmospheres.")
GM:AddResourceType("Carbon Monoxide", 1.47, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A highly poisonous gas. This description would not recommend ingesting or inhaling this resource.")
GM:AddResourceType("Methane", 1.42, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A flammable gas most commonly associated with Old Earth carbon mines and bovine-type organics.")
GM:AddResourceType("Steam", 1.26, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Vaporized water. Byproduct of most power generation assemblies.")

GM:AddResourceType("Helium-3", 0.5, "Gas", 0.1)
GM:AddResourceType("Helium-4", 0.5, "Gas", 0.1)
GM:AddResourceType("Tritium", 0.40, "Gas", 0.1)
GM:AddResourceType("Deuterium", 0.40, "Gas", 0.1)

-- Liquids
GM:AddResourceType("Water", 1.26, "Liquid", 1, "models/props_junk/metalgascan.mdl", "models/props_junk/metalgascan.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"It's somewhat moist.")
GM:AddResourceType("Methanol", 1.7, "Liquid", 1.7, "models/props_junk/gascan001a.mdl", "models/props_junk/gascan001a.mdl", ITEM_RARITY_UNCOMMON, -- The size of this is total guesswork, my brief search found no information about its radius
					"Resource",
					"A methane-based fuel. Toxic to humanoid organics.")
GM:AddResourceType("Liquid Nitrogen", 0.93, "Liquid", 0.75, "models/props_junk/metalgascan.mdl", "models/props_junk/metalgascan.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A common industrial refrigerant. Used to cool high-temperature devices.")

-- Volatile
-- TODO: Add a way to mark these so they can explode in the cargo bay
GM:AddResourceType("Radioactive Coolant", 1.26, "Cargo", 1.5, "models/slyfo/barrel_unrefined.mdl", "models/slyfo/barrel_unrefined.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"Contaminated waste products of nuclear reactions. Can be purified to extract useful water.")
GM:AddResourceType("Uranium", 2.58, "Cargo", 19.05, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A radioactive actinide used as fissile material for Fission Reactors.")
GM:AddResourceType("Polonium", 1.84, "Cargo", 9.4, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A highly radioactive metal used in generating neutrons and alpha particles. Commonly used in Radioisotope Thermoelectric Generators (RTG).")
GM:AddResourceType("Antihydrogen", 0.40, "Cargo", 0.1, "models/slyfo_2/mortarsys_cryo.mdl", "models/slyfo_2/mortarsys_cryo.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"Instantaneously annihilates when coming in contant with Hydrogen, emitting gamma radiation which can be harnessed for Energy generation.")
GM:AddResourceType("Antianubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_cryo.mdl", "models/slyfo_2/mortarsys_cryo.mdl", ITEM_RARITY_RARE,
					"Resource",
					"Instantaneously annihilates when coming in contant with Anubium, emitting gamma radiation which can be harnessed for Energy generation.")
GM:AddResourceType("Anubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_RARE,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Polonium Nitrate", 1, "Cargo", 1, "models/tiberium/small_tiberium_reactor.mdl", "models/tiberium/small_tiberium_reactor.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					"An inexplicable freak of nature. A seed crystal, under the right conditions, can explosively precipitate outwards. Constantly emits highly corrosive polonium nitroxide gas.")
GM:AddResourceType("Plutonium", 2.58, "Cargo", 19.05, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A radioactive actinide used as fissile material for Fission Reactors.")


-- Mining Materials
GM:AddResourceType("Veldspar", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"A naturally occuring alloy of Tritanium and Iron.")
GM:AddResourceType("Iron", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"One of the most commonly used metals.")
GM:AddResourceType("Ice", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"Solid state of water. It's somewhat cold.")
GM:AddResourceType("Tritanium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"A common metal used in ship construction.") --Perhaps we can just rename this Titanium and get it over with?
GM:AddResourceType("Titanite", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Nil for placeholder autoreplace test
GM:AddResourceType("Triidium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Nil because I actually cannot find any lore/science behind this
GM:AddResourceType("Trinium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Are we just making up names now?
GM:AddResourceType("Tronium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --This one's the name of a power supply chip...
GM:AddResourceType("Vibranium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Perhaps we should remove this in case Marvel wants to sue...
GM:AddResourceType("Tylium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --This one's not even used in any recipes!
GM:AddResourceType("Voltarium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_RARE,
					"Material",
					nil) --I actually like this one, but it doesn't have any use currently, so I'm not sure what to put for the "lore"
GM:AddResourceType("Nubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Solid Nubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Lithium-6", 1, "Cargo", 1)
GM:AddResourceType("Depleted Uranium", 2.58, "Cargo", 19.05, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"Depleted Uranium.")

--Wreckage Drops
GM:AddResourceType("Electronic Circuitry", 3, "Cargo", 0.1, "models/cheeze/wires/ram.mdl", "models/cheeze/wires/ram.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"Salvaged electronics. Can be broken down into useful components.")
GM:AddResourceType("Scrap Metal", 1, "Cargo", 2, "models/props_c17/oildrumchunk01d.mdl", "models/props_c17/oildrumchunk01d.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Charred metal salvaged from a ship wreckage. Can be melted down and recycled.")
GM:AddResourceType("Contaminated Coolant", 0.1, "Cargo", 0.2, "models/tiberium/tiny_chemical_storage.mdl", "models/tiberium/tiny_chemical_storage.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Liquid coolant contaminated by the meltdown of a ship's core.") --We should probably get rid of this one since Radioactive Coolant's already a thing in the normal resource tree
GM:AddResourceType("Radioactive Materials", 0.5, "Cargo", 1.5, "models/tiberium/tiny_chemical_storage.mdl", "models/tiberium/tiny_chemical_storage.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Various contaminated materials. Not much of value unless reprocessed.")
GM:AddResourceType("Nubium Generator Core", 15, "Cargo", 30, "models/tiberium/small_tiberium_storage.mdl", "models/tiberium/small_tiberium_storage.mdl", ITEM_RARITY_RARE,
					"Salvage",
					"A salvaged component used in ship cores. Should have some rather valuable materials inside.")
GM:AddResourceType("Damaged Shield Emitter", 7, "Cargo", 15, "models/slyfo/powercrystal.mdl", "models/slyfo/powercrystal.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"A salvaged component used to protect a ship from incoming enemy fire. This one is irreparably damaged.")
GM:AddResourceType("Plasma Conduits", 2, "Cargo", 3, "models/slyfo_2/acc_sci_tube1sml.mdl", "models/slyfo_2/acc_sci_tube1sml.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"Ship operations require a lot of superheated plasma, this damaged conduit ferries that plasma to where its needed.")
GM:AddResourceType("Destroyed Consoles", 5, "Cargo", 7, "models/props_lab/reciever01c.mdl", "models/props_lab/reciever01c.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"Why are these consoles always exploding and killing the cadets? You'd think the Safety Commission would do something about that...")

--Ammo
GM:AddResourceType("Tiny Capacitor Charge", 20, "Ammo", 2)
GM:AddResourceType("Small Capacitor Charge", 40, "Ammo", 4)
GM:AddResourceType("Medium Capacitor Charge", 80, "Ammo", 8)
GM:AddResourceType("Large Capacitor Charge", 160, "Ammo", 16)
GM:AddResourceType("X-Large Capacitor Charge", 320, "Ammo", 32)
GM:AddResourceType("Nanite Canister", 1, "Ammo", 2)

include("sh_generators.lua")