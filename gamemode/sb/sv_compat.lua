local GM = GM

---
--- Environment Stuffs
---

---
-- Planet data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> planet radius
-- Case03 -> planet gravity
-- Case04 -> planet atmosphere
-- Case05 -> planet night temperature
-- Case06 -> planet temperature
-- Case07 -> color_id
-- Case08 -> bloom_id
-- Case16 -> process sb1 flags
---

---
-- Planet2 data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> planet radius
-- Case03 -> planet gravity
-- Case04 -> planet atmosphere
-- Case05 -> planet pressure (ignore)
-- Case06 -> planet night temperature
-- Case07 -> planet temperature
-- Case08 -> process sb3 flags
-- Case09 -> oxygen percentage
-- Case10 -> co2 percentage
-- Case11 -> nitrogen percentage
-- Case12 -> hydrogen percentage
-- Case13 -> planet name
-- Case14 -> unknown
-- Case15 -> color_id
-- Case16 -> bloom_id
---

---
-- Star data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
--
-- radius -> 512
-- gravity -> 0 (so you don't fall when inside of it)
-- nighttemperature -> 10000
-- temperature -> 10000
---

---
-- Star2 data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> star radius
-- Case03 -> star night temperature
-- Case04 -> unknown
-- Case05 -> star temperature
-- Case06 -> star name
---

local function getKey( key )
	if type(key) ~= "string" then error("Expected String, got",type(key)) return key end
	if string.find(key,"Case") and tonumber( string.sub(key, 5) ) then
		return tonumber( string.sub(key,5) ) or key
	else
		return key
	end
end

local function Extract_Bit(bit, field)
    if not bit or not field then return false end
    if ((field <= 7) and (bit <= 4)) then
        if (field >= 4) then
            field = field - 4
            if (bit == 4) then return true end
        end
        if (field >= 2) then
            field = field - 2
            if (bit == 2) then return true end
        end
        if (field >= 1) then
            if (bit == 1) then return true end
        end
    end
    return false
end

local function spawnEnvironment( v )

	local obj = GM.class.getClass("Celestial"):new()
	local env
	local ent
	local r

	--PrintTable( v )

	local type = string.lower(v[1])

	if type == "planet" or type == "planet2" or type == "cube" then

		if type == "planet2" or type == "cube" then v[6] = v[7] end -- Override night temp as norm temp

		r = tonumber(v[2])
        local Resources = {}

        if type == "planet" then
            local habitat = Extract_Bit(1, tonumber(v[16]))
            Resources["Oxygen"] = (habitat and 0.21 or 0.001)
            Resources["Carbon Dioxide"] = (habitat and 0.045 or 0.0915)
            Resources["Nitrogen"] = (habitat and 0.60 or 0.035)
            Resources["Hydrogen"] = (habitat and 0.55 or 0)
            Resources["Carbon Monoxide"] = 0.045
            Resources["Methane"] = 0.035
            Resources["Water"] = (habitat and 0.275 or 0.15)
        else
            Resources["Oxygen"] = tonumber(v[9])
            Resources["Carbon Dioxide"] = tonumber(v[10])
            Resources["Nitrogen"] = tonumber(v[11])
            Resources["Hydrogen"] = tonumber(v[12])
            -- TODO: Distribute these dynamically based on planet temperatures and other resources
            Resources["Carbon Monoxide"] = 0.045
            Resources["Methane"] = 0.035
            Resources["Water"] = 0.275
        end

        --Name, Type, Radius, Gravity, Temperature, AtmosphereDensity, AtmosphereResources
        local EnvName = ((type == "planet2" or type == "cube") and v[13] or "Planet")
        local EnvType = (type == "cube") and "cube" or "planet"
        env = GM.class.getClass("Environment"):new(EnvName, EnvType, r, tonumber(v[3]), tonumber(v[6]), tonumber(v[4]), Resources)

	elseif type == "star" then

		r = 512
        local Resources = {}
		Resources["Hydrogen"] = 0.8
		Resources["Helium-3"] = 0.02
		Resources["Helium-4"] = 0.18

		env = GM.class.getClass("Environment"):new("Star", "star", r, 0, 10000, 0, Resources) -- grav, temp, atmos, radius, resources, name
		env:SetPressure(5000)

	elseif type == "star2" then

		r = tonumber(v[2])

        local Resources = {}
		Resources["Hydrogen"] = 0.8
		Resources["Helium-3"] = 0.02
		Resources["Helium-4"] = 0.18

		env = GM.class.getClass("Environment"):new((string.len(v[6] or "") > 0 and v[6] or "Star"), "star", r, 0, tonumber(v[5]), 0, Resources)
		env:SetPressure(5000)

	end

	if env and obj and r then
		obj:SetEnvironment(env) -- Bind Environment IMMEDIATELY!

		ent = ents.Create("sc_planet")
		ent:SetPos( v.ent:GetPos())
		ent:SetAngles( v.ent:GetAngles() )
		ent:Spawn()

        ent:SetMoveType( MOVETYPE_NONE ) -- We don't want these planets to move
		ent:SetSolid( SOLID_NONE ) -- We want people to be able to pass through it...

        if type == "cube" then
            ent:PhysicsInitBox(Vector(-r,-r,-r), Vector(r,r,r))
            ent:SetCollisionBounds(Vector(-r,-r,-r), Vector(r,r,r))
        else
            ent:PhysicsInitSphere(r)
            ent:SetCollisionBounds( Vector(-r,-r,-r), Vector(r,r,r) )
        end

        ent:SetTrigger( true )
        ent:DrawShadow( false ) -- That would be bad.

        local phys = ent:GetPhysicsObject()
        if phys:IsValid() then
            phys:EnableMotion(false) -- DON'T MOVE!
            phys:EnableGravity(false)
            phys:EnableDrag(false)
            phys:Wake()
		end

		ent:SetNotSolid(true) -- It's not solid ofc
		obj:setEntity(ent) -- Bind the entity to the celestial
	end
end

local env_data = {}
function GM:CreateMapEnvironmentsFromSB()
    local ents = ents.FindByClass("logic_case")

    for _, ent in pairs(ents) do
        local tbl = { ent = ent }
        local vals = ent:GetKeyValues()
        for k, v in pairs(vals) do
            tbl[ getKey( k ) ] = v
        end
        table.insert(env_data, tbl )
    end

    for k,v in pairs( env_data ) do
        spawnEnvironment( v )
    end
end