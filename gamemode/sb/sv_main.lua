local GM = GM
local BaseClass = GM:getBaseClass()
local EnvironmentsInitialized = false

util.AddNetworkString("SpaceCombat2-UpdatePlayerGravity")

include("sv_convar.lua")
include("sh_main.lua")
include("sv_compat.lua")
include("sv_generators.lua")

AddCSLuaFile("sh_main.lua")
AddCSLuaFile("sh_generators.lua")
AddCSLuaFile("cl_convar.lua")
AddCSLuaFile("cl_fonts.lua")
AddCSLuaFile("cl_main.lua")
AddCSLuaFile("cl_hud.lua")

require("lip")

function GM:PlayerNoClip(ply)
    if not IsValid(ply) then return false end
    if ply:InVehicle() then return false end
    local noclip = GetConVar("SB_NoClip"):GetBool()
    if not noclip then
        return false
    end

    if ply.GetEnvironment then
        return ply.GetEnvironment():GetNoclip(ply)
    end

	return GM:GetSpace():GetNoclip(ply)
end

-- The direction of the sun, obviously
local SunDir = Vector(0,0,-1)
function GM:GetSunDir()
    return SunDir
end

function GM:UpdateSunDir()
    local Suns = ents.FindByClass("env_sun")
    if #Suns > 0 then
        local EnvSun = Suns[1]
        local Values = EnvSun:GetKeyValues()
        local NewSunDir

        -- Check if Garry has modified the sun
        if Values["sun_dir"] then
            NewSunDir = -Values["sun_dir"]

        -- See if the sun has been setup correctly if we don't have a direction yet
        else
            if Values["use_angles"] and Values["use_angles"] ~= 0 then
                NewSunDir = EnvSun:GetAngles()
                NewSunDir.p = -(Values["pitch"] ~= 0 and Values["pitch"] or NewSunDir.p)
                NewSunDir = NewSunDir:Forward()
            else
                local Target = ents.FindByName("sun_target")
                if #Target > 0 and IsValid(Target[1]) then
                    Target = Target[1]
                    NewSunDir = (Target:GetPos() - EnvSun:GetPos()):Normalize()
                end
            end
        end

        -- Some maps don't setup their suns correctly at all, so we'll just use the angles of the entity by default
        if not NewSunDir then
            NewSunDir = EnvSun:GetAngles()
            NewSunDir.p = -(Values["pitch"] ~= 0 and Values["pitch"] or NewSunDir.p)
            NewSunDir = NewSunDir:Forward()
        end

        SunDir = NewSunDir
    end

	--no sun found, so just set a default angle
	if not SunDir or SunDir == Vector(0,0,0) then SunDir = Vector(0,0,-1) end
end

function GM:SortMapEnvironments()
    --We need to rebuild the environment tree after we setup the atmospheres
    GM.EnvironmentTree = {[GM:GetSpace()] = {}}

    local environments = {}
    for I=1, #GM.AtmosphereTable do
        table.insert(environments, GM.AtmosphereTable[I])
    end
    for I=1, #GM.StarTable do
        table.insert(environments, GM.StarTable[I])
    end

    --We want the smallest atmospheres to go last, because they're generally going to be inside of the bigger ones
    table.sort(environments, function(a, b) return a:GetRadius() > b:GetRadius() end)

    --Loop over the environments and build the tree
    for i,k in pairs(environments) do
        --We need a position, so we can only do this on environments with a valid celestial entity
        if k:GetCelestial() and IsValid(k:GetCelestial():getEntity()) then
            --Get the atmospheres at the position
            local final, all = GM:GetAtmosphereAtPoint(k:GetCelestial():getEntity():GetPos())
            local children = GM.EnvironmentTree

            --Loop over all of the atmospheres at the point
            for I=1, #all do
                children = children[all[I]]
            end

            --Once we reach the lowest level, add the new children table
            children[k] = {}
        end
    end

    --Temporary fix, this should really be handled in the other loop. Tried putting it in the other loop originally, but it didn't work.
    local function setchildren(parent, children)
        parent:setChildren(children)
        for i,k in pairs(children) do
            i:setParent(parent)
            setchildren(i, k)
        end
    end
    setchildren(GM:GetSpace(), GM.EnvironmentTree[GM:GetSpace()])
end

function GM:SpawnEnvironment(EnvironmentName, Data)
    local Radius = Data.Radius or 1024
    local Environment = GM.class.getClass("Environment"):new(
        EnvironmentName,
        Data.Type or "planet",
        Radius,
        Data.Gravity or 1,
        Data.Temperature or 288,
        Data.Atmosphere or 1,
        Data.EnvironmentResources or {}
    )

    local Celestial = GM.class.getClass("Celestial"):new()
    Celestial:SetEnvironment(Environment)

    local Planet = ents.Create("sc_planet")
    Planet:SetPos(Vector(Data.Location))
    Planet:Spawn()

    Planet:SetMoveType(MOVETYPE_NONE)
    Planet:SetSolid(SOLID_NONE)

    if Data.Type == "cube" then
        Planet:PhysicsInitBox(Vector(-Radius, -Radius, -Radius), Vector(Radius, Radius, Radius))
        Planet:SetCollisionBounds(Vector(-Radius, -Radius, -Radius), Vector(Radius, Radius, Radius))
    else
        Planet:PhysicsInitSphere(Radius)
        Planet:SetCollisionBounds(Vector(-Radius, -Radius, -Radius), Vector(Radius, Radius, Radius))
    end

    Planet:SetTrigger(true)
    Planet:DrawShadow(false)
    Planet:SetNotSolid(true)

    local PhysObj = Planet:GetPhysicsObject()
    if IsValid(PhysObj) then
        PhysObj:EnableMotion(false)
        PhysObj:EnableGravity(false)
        PhysObj:EnableDrag(false)
        PhysObj:Wake()
    end

    Celestial:setEntity(Planet) -- Bind the entity to the celestial
end

function GM:CreateMapEnvironments()
    -- Make sure this doesn't get called more than once on a gamemode reload
    if EnvironmentsInitialized then
        return
    end

    -- Cleanup previous environments
    local OldPlanets = ents.FindByClass("sc_planet")
    for _, Planet in pairs(OldPlanets) do
        Planet:Remove()
    end

    -- Create space environment
    GM.Space = GM.class.getClass("Space"):new()

    -- Register sun
    GM:UpdateSunDir()

    -- Check if we have a config file available for the map
    local Map = string.lower(game.GetMap())
    local MapConfigFile = string.format("data/%s/environments/%s.txt", SC.DataFolder, Map)
    if file.Exists(MapConfigFile, "GAME") then
        local ConfigData = lip.load(MapConfigFile, "GAME")
        for Section, Data in pairs(ConfigData) do
            if string.sub(Section, 1, string.len("Envrionment/")) == "Environment/" then
                local EnvironmentName = string.sub(Section, 13)
                Data.EnvironmentResources = ConfigData["EnvironmentResources/"..EnvironmentName]
                self:SpawnEnvironment(EnvironmentName, Data)
            end
        end
    else
        -- Spacebuild Map Compatibility
        local EnableSBCompatibility = GM.Config:Get("SpaceCombat2", "Compatibility", "EnableSpacebuildMapCompatibility", true)
        if EnableSBCompatibility then
            self:CreateMapEnvironmentsFromSB()
        end
    end

    self:SortMapEnvironments()

    -- Check if any environments were added, if not then force a habitable atmosphere
    local ForceHabitable = GM.Config:Get("SpaceCombat2", "Compatibility", "ForceHabitableEnvironmentIfNoPlanets", true)
    if ForceHabitable and table.Count(GM.EnvironmentTree[GM:GetSpace()]) == 0 then
        GM:GetSpace():SetHabitable(true)
    else
        GM:GetSpace():SetHabitable(false)
    end

    EnvironmentsInitialized = true

    hook.Run("SC.PostEnvironmentsCreated")
end

hook.Remove("SC.Config.PostLoad", "SC.LoadMapEnvironments")
hook.Add("SC.Config.PostLoad", "SC.LoadMapEnvironments", function()
    GM:CreateMapEnvironments()
end)

local function SpaceThink()
    local Space = GM:GetSpace()
    if Space then
        Space:Think()
    end
end
timer.Remove("SC2.SpaceThink")
timer.Create("SC2.SpaceThink", 1, 0, SpaceThink)

function GM:GetSpace()
    return GM.Space
end

function GM:OnEnterEnvironment(env, ent)
	if env:GetName() ~= "" then
		print(ent, "Entering: ",env:GetName(),"\n")
	end
end

function GM:OnLeaveEnvironment(env, ent)
	if env:GetName() ~= "" then
		print(ent, "Leaving: ",env:GetName(),"\n")
	end
end

-- A way to find what environment is at a point that does not rely on touch checks and
-- therefore must loop over the environment tree. Will return the last environment in
-- the tree that has the point, and all atmospheres that contained the point.
function GM:CheckEnvironments(point, tree, atmospheres)
    if not point or not tree or not next(tree) then return end
	for atmo,children in pairs(tree) do
		if atmo:ContainsPosition(point) then
            SC.Error("Environment tree contains point.", 2)

            if atmospheres then table.insert(atmospheres, atmo) end

            local child
            if next(children) ~= nil then
                SC.Error("Environment has children, checking if point is inside of them...", 2)
                child = self:CheckEnvironments(point, children, atmospheres)
                SC.Error("Finished checking children.", 2)
            end

			return child or atmo
		end
	end

    SC.Error("Environment tree does not contain point.", 2)
end

-- A wrapper for GM:CheckEnvironment that goes over all environments on the map and returns all atmospheres that had the point
function GM:GetAtmosphereAtPoint(point)
    local atmospheres = {}
    local ret = self:CheckEnvironments(point, GM.EnvironmentTree, atmospheres)

    if not ret then
        SC.Error("Unable to find environment for position "..tostring(point).."! What happened!?", 5)
    end

	return ret, atmospheres
end

local function SetupEntityEnvironment(e)
    if not IsValid(e) then return end

    if not EnvironmentsInitialized then
        timer.Simple(1, function()
            SetupEntityEnvironment(e)
        end)

        return
    end

	if not e.GetEnvironment then
		--Planets should be in their own atmospheres, so ignore them.
		if e:GetClass() ~= "sc_planet" then
			--This hook is called just after ents.Create, usually before ent:SetPos, so wait.
			timer.Simple(0.1, function()
				if not IsValid(e) then return end
				local atmo = GM:GetAtmosphereAtPoint(e:GetPos())

                --Check if this is valid, it CAN be invalid at the start of a map before things are fully initialized
                if atmo then
				    atmo:SetEnvironment(e, atmo)
                else
                    GM:GetSpace():SetEnvironment(e, GM:GetSpace())
                end
			end)
		end
	end
end

function GM:OnEntityCreated(e)
    SetupEntityEnvironment(e)
end

function GM:SetPlayerSpeed(ply, walk, run, override) --Adding the override parameter to prevent fighting over wtf the speed is, things outside of the gamemode should set this to true to enable their override and false to disable it
    if not ply.speedoverride or type(override) == "boolean" then
        ply.speedoverride = override
        BaseClass.SetPlayerSpeed(self,ply,walk,run)
    end
end

-- These functions are used by environments to figure out what speed players should move at
function GM:SetPlayerSpeedModifier(ply, walk, run)
    if IsValid(ply) and walk ~= nil and run ~= nil then
        ply.walkmodifier = walk
        ply.runmodifier = run
    end
end

function GM:GetPlayerSpeedModifier(ply)
    return ply.walkmodifier or 1, ply.runmodifier or 1
end

-- Synchronize gravity to the client when it changes
function GM:UpdatePlayerGravity(ply, gravity)
    if IsValid(ply) and gravity ~= nil then
        ply:SetGravity(gravity)
        net.Start("SpaceCombat2-UpdatePlayerGravity")
        net.WriteFloat(gravity)
        net.Send(ply)
    end
end

--Effect code pulled from Space Combat 2 for usage in SB4
function GM:CreateEffect(name, tab)
	SC.CreateEffect(name, tab)
end