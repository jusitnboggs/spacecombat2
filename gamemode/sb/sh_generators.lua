local GM = GM

-- This file contains basic generator definitions
-- In order to add a new generator, copy the following basic definition and paste it later in the file.
-- After pasting it simply edit the variables to make your desired generator, it will be added to the spawner tool automatically.
--[[
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Basic Generator")
Generator:SetClass("sc_generator")
Generator:SetDescription("This is a test generator, it does nothing")
Generator:SetCategory("Basic")
Generator:SetDefaultModel("models/props_borealis/bluebarrel001.mdl")
Generator:AddProduction("Energy", 1)
Generator:AddProduction("Oxygen", 1)
Generator:AddConsumption("Energy", 1, true)
Generator:AddConsumption("Oxygen", 1, true)
GM:RegisterGenerator(Generator)
]]--
-- It is worth noting that this basic definition does not include all possible variables.
-- Take a look at the GeneratorInfo class for more variables.

-------------------------
-- ENERGY GENERATORS
-------------------------

-- Steam Turbine
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Steam Turbine")
Generator:SetClass("sc_generator")
Generator:SetDescription("Force steam through a giant spinning death machine to produce power!")
Generator:SetCategory("Energy Generators")
Generator:SetBaseVolume(57478)
Generator:SetDefaultModel("models/ce_ls3additional/water_heater/water_heater.mdl")
Generator:SetMaxMultiplier(100)
Generator:SetMaxHeat(150)
Generator:SetDamagingMultiplier(40)
Generator:AddProduction("Energy", 250)
Generator:AddProduction("Water", 130)
Generator:AddConsumption("Steam", 135, true)
GM:RegisterGenerator(Generator)
-- End Steam Turbine

-- Solar Panel
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Solar Panel")
Generator:SetClass("sc_generator")
Generator:SetDescription("A tiny, pathetic solar panel that you point at the giant flaming ball of plasma in the sky to forcefully generate power.")
Generator:SetCategory("Energy Generators")
Generator:SetDefaultModel("models/ce_ls3additional/solar_generator/solar_generator_large.mdl")
Generator:AddProduction("Energy", 1)
Generator:SetHasSounds(false)
Generator:SetHasCycle(false)
Generator:SetWireIO({}, { "On", "Generating Energy", "Efficiency", "Sun Direction [VECTOR]" }, false)

-- idea taken from E2, defines precision in normalized vector comparison
-- this is required because otherwise issues arise when the vectors are nearly identical or nearly opposite
local delta = 0.0000100000000
local function vec_equal( v1, v2 )
	return 	v1.x - v2.x <= delta and v2.x - v1.x <= delta and
			v1.y - v2.y <= delta and v2.y - v1.y <= delta and
			v1.z - v2.z <= delta and v2.z - v1.z <= delta
end

local rad2deg = 180 / math.pi
local function AngleBetweenVectors( v1, v2 )
	if vec_equal( v1, v2 ) then return 0 end
	if vec_equal( v1, -v2 ) then return 180 end

	local ang = math.acos(v1:Dot(v2)) * rad2deg
	if ang == ang then -- check for infinity, shouldn't happen, but just to be sure
		return ang
	else
		return 0
	end
end

Generator:SetSetupFunction(function(self)
    if WireLib ~= nil then
        WireLib.TriggerOutput(self, "Sun Direction", -GM:GetSunDir())
    end

	local Fitting = self:GetFitting()

	local GeneratorClasses = {  2816,
								12000,
								20000,
								30000,
								60000,
								120000,
								240000,
								600000,
								2400000 }

	local Box = self:OBBMaxs() - self:OBBMins()
	local SA = Box.x * Box.y

	local AllClasses = {
        "Drone",
        "Fighter",
        "Frigate",
        "Cruiser",
        "Battlecruiser",
        "Battleship",
        "Dreadnaught",
        "Titan"
    }

    self.UsableClasses = {}

    for i,k in ipairs(AllClasses) do
        if SA <= GeneratorClasses[i] then
            table.insert(self.UsableClasses, k)
        end
    end

    if #self.UsableClasses == 0 then
        table.insert(self.UsableClasses, "Unusably Large")
    end

	self:SetGeneratorClass(self.UsableClasses[1])

	Fitting.Classes = self.UsableClasses
    self:SetFitting(Fitting)
end)

Generator:SetUpdateOutputsFunction(function(self)
    if WireLib ~= nil then
        local efficiency = math.floor( self:GetMultiplier() * 100 ) * (self:CanGenerate() and 1 or 0)

	    WireLib.TriggerOutput( self, "Generating Energy", self:CanGenerate() and (self:GetProduction()["Energy"].mul * self:GetBaseMultiplier() * self:GetMultiplier()) or 0 )
        WireLib.TriggerOutput( self, "Efficiency", efficiency )
    end
end)

Generator:SetCalculateBaseMultiplierFunction(function(self)
    local boxsize = self:OBBMaxs() - self:OBBMins()
	return math.floor((boxsize.x * boxsize.y) / 10)
end)

Generator:SetCalculateMultiplierFunction(function(self)
	--Calculate based on number of visible stars (TODO: Distance as well?)
	if self.VisibleStars then
		local mult = 0
		for sun,visible in pairs(self.VisibleStars) do
			if visible then
				local starPos = sun:GetPos()
				local dir = (starPos - self:GetPos()):GetNormalized()
				local angle = AngleBetweenVectors(self:GetUp(), dir)

				angle = 1 - angle / 90
				mult = mult + math.Clamp(1 + math.log10(math.Clamp(angle, 0.01, 1)), 0, 1)
			end
		end

		self:SetMultiplier(mult)

	--Standard env_sun calcs
	else
		local angle = AngleBetweenVectors(-self:GetUp(), GM:GetSunDir())

		angle = 1 - angle / 90

		self:SetMultiplier(math.Clamp(1 + math.log10(math.Clamp(angle, 0.01, 1)), 0, 1))
	end
end)

Generator:SetCanGenerateFunction(function(self)
    if self:GetBroken() or not (IsValid(self:GetProtector()) and self:GetFitting().CanRun) then
        return false
    end

    if self:GetGeneratorClass() == "Unusably Large" then
        return false
    end

    if not self.lasttrace or self.lasttrace < CurTime() + 0.5 then
		local top = self:LocalToWorld(self:OBBCenter() + self:OBBMaxs() * Vector(0, 0, 1))

		-- Check either against env_sun, or any stars defined on the map
		self.IsBlocked = true
		if #GAMEMODE.StarTable > 0 then
			--For calculating multiplier
			if self.VisibleStars == nil then self.VisibleStars = {} end

			--Check against all defined stars
			for k,v in pairs(GAMEMODE.StarTable) do
				local starEnt = v:GetCelestial():getEntity()
				local starPos = starEnt:GetPos()
				local dir = (starPos - top):GetNormalized()
				local trace = util.TraceLine( { start = top, endpos = top + (dir * 4000), filter = {self} } )

				if not trace.Hit or trace.HitSky then
					self.IsBlocked = false

					self.VisibleStars[starEnt] = true
				else
					self.VisibleStars[starEnt] = false
				end
			end
		else
			local trace = util.TraceLine( { start = top, endpos = top - GM:GetSunDir() * 4000, filter = {self} } )

			if not trace.Hit or trace.HitSky then
				self.IsBlocked = false
			end
		end

		self.lasttrace = CurTime()
	end

	return not self.IsBlocked and self:IsLinked()
end)

GM:RegisterGenerator(Generator)
-- End Solar Panel

-- Hydrostatic Generator
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Hydrostatic Generator")
Generator:SetClass("sc_generator")
Generator:SetDescription("Produces energy by harnessing the power of water to push a piece of paper in a circle!")
Generator:SetCategory("Energy Generators")
Generator:SetBaseVolume(69897)
Generator:SetDefaultModel("models/chipstiks_ls3_models/hydrogenerator/hydrogenerator.mdl")
Generator:AddProduction("Energy", 20)
Generator:SetHasMultiplier(false)
Generator:SetHasSounds(false)
Generator:SetHasCycle(false)

Generator:SetCanGenerateFunction(function(self)
    return self:WaterLevel() > 0
end)

Generator:SetCalculateMultiplierFunction(function(self)
    self:SetWantedMultiplier(self:WaterLevel())
end)

GM:RegisterGenerator(Generator)
-- End Hydrostatic Generator

-------------------------
-- COLLECTORS
-------------------------

-- Moisture Vaporator
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Moisture Vaporator")
Generator:SetClass("sc_generator")
Generator:SetDescription("Collects water vapor directly from the atmosphere. Super useful on hot planets where there isn't any water around.")
Generator:SetCategory("Collectors")
Generator:SetDefaultModel("models/slyfo/moisture_condenser.mdl")
Generator:SetBaseVolume(16860)
Generator:AddProduction("Water", 20)
Generator:AddConsumption("Water", 20, true, nil, nil, true)
Generator:AddConsumption("Energy", 75, true)

Generator:SetCanGenerateFunction(function(self)
	local CanGenerate = true

	if not self.GetEnvironment then CanGenerate = false end
	if self.GetEnvironment():GetName() == "Space" then CanGenerate = false end
    if not self:IsLinked() then CanGenerate = false end
    if self:GetBroken() then CanGenerate = false end
    if not self:GetFitting().CanRun then CanGenerate = false end
    if not self:CheckResources() then CanGenerate = false end

	return CanGenerate
end)

GM:RegisterGenerator(Generator)
-- End Moisture Vaporator

-- Air Compressor
local AirCompressorMinResources = {
	Oxygen = 0.06
}

local Generator = GM:NewGeneratorInfo()
Generator:SetName("Atmospheric Compressor")
Generator:SetClass("sc_generator")
Generator:SetDescription("Drains a planet's natural gaseous resources for your own uses.")
Generator:SetCategory("Collectors")
Generator:SetDefaultModel("models/ce_ls3additional/compressor/compressor.mdl")
Generator:AddConsumption("Energy", 50, true)
Generator:SetHasSounds(true)
Generator:SetLoopSound("vehicles/diesel_loop2.wav")
Generator:SetDeactivationSound("vehicles/crane/crane_extend_stop.wav")
Generator:SetBaseVolume(45603)
Generator:SetWireIO({}, { "Production [TABLE]", "Consumption [TABLE]" })

Generator:SetCanGenerateFunction(function(self)
	local CanGenerate = true

	if not self.GetEnvironment then CanGenerate = false end
	if self.GetEnvironment():GetName() == "Space" then CanGenerate = false end
	if not self:IsLinked() then CanGenerate = false end
	if self:WaterLevel() > 0 then CanGenerate = false end

	return (not self:GetBroken()) and IsValid(self:GetProtector()) and self:GetFitting().CanRun and self:CheckResources() and CanGenerate
end)

Generator:SetCalculateMultiplierFunction(function(self)
	if self.GetEnvironment and (not self.old_environment or self.old_environment ~= self:GetEnvironment() or CurTime() > (self.NextMultiplierUpdate or 0)) then
		self.old_environment = self:GetEnvironment()
        local Resources = {}
        local Container = self.old_environment:GetContainerOfType("Gas")
        if Container then
            Resources = Container:GetStored()
        end

		local Total = 0
		for Name in pairs(Resources) do Total = Total + Container:GetAmount(Name) end

		local ResourcesToGenerate = {}
		for Name in pairs(Resources) do
			local Amount = Container:GetAmount(Name)
			local Percent = Amount / Container:GetMaxAmount(Name)

			if Amount > 1 and (not AirCompressorMinResources[Name] or AirCompressorMinResources[Name] < Percent) then
                local Generate = math.min((Amount / Total) * 100, Amount)
				ResourcesToGenerate[Name] = Generate
                self:AddConsumption(Name, Generate, true, nil, nil, true)
                self:AddProduction(Name, Generate)
			end
		end

        for i,k in pairs(self.OldResourcesToGenerate or {}) do
            if not ResourcesToGenerate[i] then
                self:RemoveProduction(i)
                self:RemoveConsumption(i)
            end
        end

        self.OldResourcesToGenerate = ResourcesToGenerate
        self.NextMultiplierUpdate = CurTime() + 10

        local ID = SC.NWAccessors.GetNWAccessorID(self, "Production")
        SC.NWAccessors.SyncValue(self, ID)

        ID = SC.NWAccessors.GetNWAccessorID(self, "CycleActivationCost")
        SC.NWAccessors.SyncValue(self, ID)
	end
end)

Generator:SetUpdateOutputsFunction(function(self)
    if WireLib ~= nil then
        local WIRETABLE = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
        local Mult = self:GetBaseMultiplier() * self:GetMultiplier()
        local Production = table.Copy(WIRETABLE)
        local Consumption = table.Copy(WIRETABLE)

        for i,k in pairs(self:GetProduction()) do
            Production.s[i] = k.mul * Mult
            Production.stypes[i] = "n"
        end

        for i,k in pairs(self:GetCycleActivationCost()) do
            Consumption.s[i] = k.mul * Mult
            Consumption.stypes[i] = "n"
        end

        WireLib.TriggerOutput(self, "Production", Production)
        WireLib.TriggerOutput(self, "Consumption", Consumption)
    end
end)

GM:RegisterGenerator(Generator)
-- End Air Compressor

-- Water Pump
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Water Pump")
Generator:SetClass("sc_generator")
Generator:SetDescription("Utilizes a large attractor field arranged in a cone to draw liquids into storage. Put simply, it's a water vacuum.")
Generator:SetCategory("Collectors")
Generator:SetBaseVolume(22507)
Generator:SetDefaultModel("models/props_phx/life_support/gen_water.mdl")
Generator:SetMaxMultiplier(25)
Generator:SetMaxHeat(50)
Generator:SetDamagingMultiplier(10)
Generator:AddProduction("Water", 100)
Generator:AddConsumption("Water", 100, true, nil, nil, true)
Generator:AddConsumption("Energy", 75, true)

Generator:SetCanGenerateFunction(function(self)
    return self:WaterLevel() > 0
end)

Generator:SetCalculateMultiplierFunction(function(self)
    return self:WaterLevel()
end)

GM:RegisterGenerator(Generator)
-- End Water Pump

-------------------------
-- FUEL CELLS
-------------------------

-- Carbon Fuel Cell
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Carbon Fuel Cell")
Generator:SetClass("sc_generator")
Generator:SetDescription("Uses science to combine Carbon and Oxygen to create Energy and CO2. Runs super hot, you can cook dinner on this thing, but super efficient.")
Generator:SetCategory("Fuel Cells")
Generator:SetBaseVolume(11631)
Generator:SetDefaultModel("models/tiberium/tiny_tiberium_storage.mdl")
Generator:SetMaxMultiplier(20)
Generator:SetMaxHeat(50)
Generator:SetDamagingMultiplier(5)
Generator:AddProduction("Carbon Dioxide", 18)
Generator:AddProduction("Energy", 600)
Generator:AddConsumption("Carbon", 20, true)
Generator:AddConsumption("Oxygen", 40, true)
GM:RegisterGenerator(Generator)
-- End Carbon Fuel Cell

-- Hydrogen Fuel Cell
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Hydrogen Fuel Cell")
Generator:SetClass("sc_generator")
Generator:SetDescription("Explosively ignites hydrogen and oxygen. Byproducts are water and energy. Kind of a middle ground for efficiency.")
Generator:SetCategory("Fuel Cells")
Generator:SetBaseVolume(6021)
Generator:SetDefaultModel("models/props_phx/life_support/battery_small.mdl")
Generator:SetMaxMultiplier(50)
Generator:SetMaxHeat(100)
Generator:SetDamagingMultiplier(20)
Generator:AddConsumption("Hydrogen", 30, true)
Generator:AddConsumption("Oxygen", 15, true)
Generator:AddProduction("Water", 7.5)
Generator:AddProduction("Energy", 400)
Generator:SetHasSounds(true)
Generator:SetLoopSound("ambient/machines/refrigerator.wav")

GM:RegisterGenerator(Generator)
-- End Hydrogen Fuel Cell

-- Methanol Fuel Cell
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methanol Fuel Cell")
Generator:SetClass("sc_generator")
Generator:SetDescription("Burns Methanol and Oxygen to create Energy, CO2, and Water. Only runs at 30% efficiency, but runs cool enough to be multiplied heavily.")
Generator:SetCategory("Fuel Cells")
Generator:SetBaseVolume(23555)
Generator:SetDefaultModel("models/slyfo/crate_watersmall.mdl")
Generator:SetMaxMultiplier(100)
Generator:SetMaxHeat(200)
Generator:SetDamagingMultiplier(50)
Generator:AddProduction("Water", 20)
Generator:AddProduction("Carbon Dioxide", 10)
Generator:AddProduction("Energy", 30)
Generator:AddConsumption("Methanol", 30, true)
Generator:AddConsumption("Oxygen", 45, true)
GM:RegisterGenerator(Generator)
-- End Methanol Fuel Cell

-------------------------
-- SYNTHESIZERS
-------------------------

-- Methane Reformer
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methane Reformer")
Generator:SetClass("sc_generator")
Generator:SetDescription("Uses a reaction between steam and methane to produce Carbon Dioxide and Hydrogen.")
Generator:SetCategory("Synthesizers")
Generator:SetBaseVolume(86712)
Generator:SetDefaultModel("models/mandrac/nitrogen_tank/nitro_small.mdl")
Generator:AddProduction("Carbon Dioxide", 20)
Generator:AddProduction("Hydrogen", 60)
Generator:AddConsumption("Steam", 20, true)
Generator:AddConsumption("Methane", 20, true)
GM:RegisterGenerator(Generator)
-- End Methane Reformer

-- Methanator
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methanator")
Generator:SetClass("sc_generator")
Generator:SetDescription("The opposite of a Methane Reformer, takes Carbon Monoxide and Hydrogen to produce Methane and Water.")
Generator:SetCategory("Synthesizers")
Generator:SetBaseVolume(24869)
Generator:SetDefaultModel("models/lifesupport/generators/waterairextractor.mdl")
Generator:AddProduction("Methane", 20)
Generator:AddProduction("Water", 20)
Generator:AddConsumption("Energy", 20, true)
Generator:AddConsumption("Carbon Monoxide", 20, true)
Generator:AddConsumption("Hydrogen", 60, true)
GM:RegisterGenerator(Generator)
-- End Methanator

-- Methanol Synthesizer
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methanol Synthesizer")
Generator:SetClass("sc_generator")
Generator:SetDescription("Uses a catalyst to smash Carbon Monoxide and Hydrogen together and form Methane.")
Generator:SetCategory("Synthesizers")
Generator:SetBaseVolume(21907)
Generator:SetDefaultModel("models/props_pipes/valve002.mdl")
Generator:AddProduction("Methanol", 20)
Generator:AddConsumption("Energy", 20, true)
Generator:AddConsumption("Carbon Monoxide", 20, true)
Generator:AddConsumption("Hydrogen", 40, true)
GM:RegisterGenerator(Generator)
-- End Methanol Synthesizer

-- Carbon Monoxide Synthesizer
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Carbon Monoxide Synthesizer")
Generator:SetClass("sc_generator")
Generator:SetDescription("Attempts to forcefully create Carbon Monoxide from Carbon and Oxygen.")
Generator:SetCategory("Synthesizers")
Generator:SetBaseVolume(4725)
Generator:SetDefaultModel("models/props_pipes/valve001.mdl")
Generator:AddProduction("Carbon Monoxide", 20)
Generator:AddConsumption("Energy", 40, true)
Generator:AddConsumption("Carbon", 20, true)
Generator:AddConsumption("Oxygen", 20, true)
GM:RegisterGenerator(Generator)
-- End Carbon Monoxide Synthesizer

-------------------------
-- SPLITTERS
-------------------------

-- Water Splitter
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Water Splitter")
Generator:SetClass("sc_generator")
Generator:SetDescription("Uses the power of electricity to tear water molecules asunder. Comes with a vapor sorter to separate hydrogen from oxygen.")
Generator:SetCategory("Splitters")
Generator:SetDefaultModel("models/ce_ls3additional/water_air_extractor/water_air_extractor.mdl")
Generator:SetBaseVolume(11670)
Generator:AddConsumption("Energy", 100, true)
Generator:AddConsumption("Water", 20, true)
Generator:AddProduction("Oxygen", 20)
Generator:AddProduction("Hydrogen", 40)
Generator:SetHasSounds(true)
Generator:SetActivationSound("ambient/machines/thumper_startup1.wav")
Generator:SetLoopSound("ambient/machines/turbine_loop_1.wav")

GM:RegisterGenerator(Generator)
-- End Water Splitter

-- Carbon Dioxide Splitter
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Carbon Dioxide Splitter")
Generator:SetClass("sc_generator")
Generator:SetDescription("Takes Carbon Dioxide and Water, splits them up, and spews out a bunch of useful gasses.")
Generator:SetCategory("Splitters")
Generator:SetBaseVolume(17799)
Generator:SetDefaultModel("models/slyfo_2/power_unit.mdl")
Generator:AddProduction("Carbon Monoxide", 20)
Generator:AddProduction("Hydrogen", 40)
Generator:AddProduction("Oxygen", 40)
Generator:AddConsumption("Energy", 125, true)
Generator:AddConsumption("Carbon Dioxide", 20, true)
Generator:AddConsumption("Water", 20, true)
GM:RegisterGenerator(Generator)
-- End Carbon Dioxide Splitter

-- Methanol Splitter
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methanol Splitter")
Generator:SetClass("sc_generator")
Generator:SetDescription("Rips Methanol into pieces and gives you Carbon Dioxide and Water.")
Generator:SetCategory("Splitters")
Generator:SetBaseVolume(5191)
Generator:SetDefaultModel("models/slyfo_2/acc_sci_coolerator.mdl")
Generator:AddProduction("Carbon Dioxide", 20)
Generator:AddProduction("Water", 40)
Generator:AddConsumption("Methanol", 20, true)
Generator:AddConsumption("Oxygen", 30, true)
GM:RegisterGenerator(Generator)
-- End Methanol Splitter

-- Carbon Monoxide Splitter
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Carbon Monoxide Splitter")
Generator:SetClass("sc_generator")
Generator:SetDescription("Forcefully separate Carbon Monoxide into Oxygen and Carbon.")
Generator:SetCategory("Splitters")
Generator:SetBaseVolume(4593)
Generator:SetDefaultModel("models/slyfo_2/acc_sci_hoterator.mdl")
Generator:AddProduction("Carbon", 20)
Generator:AddProduction("Oxygen", 20)
Generator:AddConsumption("Carbon Monoxide", 20, true)
Generator:AddConsumption("Energy", 150, true)
GM:RegisterGenerator(Generator)
-- End Carbon Monoxide Splitter

-- Water Purifier (MOVE ME SOMEWHERE THAT MAKES SENSE)
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Water Purifier")
Generator:SetClass("sc_generator")
Generator:SetDescription("Passes Radioactive Coolant through a Nanocarbon Filter to purge its radioactivity and recycles it as Water.")
Generator:SetCategory("Splitters")
Generator:SetBaseVolume(5338)
Generator:SetDefaultModel("models/slyfo/rover_winch.mdl")
Generator:AddProduction("Water", 80)
Generator:AddConsumption("Energy", 100, true)
Generator:AddConsumption("Carbon", 1, true)
Generator:AddConsumption("Radioactive Coolant", 80, true)
GM:RegisterGenerator(Generator)
-- End Water Purifier

-------------------------
-- HEATERS
-------------------------

-- Water Heater
local WaterHeaterMult = 80
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Water Heater")
Generator:SetClass("sc_generator")
Generator:SetDescription("Cook up some boiling hot water to make epic steam clouds. Can also be used to purify coolant.")
Generator:SetCategory("Heaters")
Generator:SetBaseVolume(60250)
Generator:SetDefaultModel("models/ce_ls3additional/water_pump/water_pump.mdl")
Generator:AddProduction("Steam", WaterHeaterMult, true)
Generator:AddConsumption("Energy", 200, true)
Generator:AddConsumption("Water", WaterHeaterMult, true)

Generator:SetCalculateMultiplierFunction(function(self)
	if CurTime() > (self.NextMultiplierUpdate or 0) then
        self.NextMultiplierUpdate = CurTime() + 5

        local Changed = false
        local Consumption = self:GetCycleActivationCost(nil, true)
        if self:GetAmount("Radioactive Coolant") >= WaterHeaterMult * self:GetBaseMultiplier() then
            if Consumption["Water"] then
                Changed = true
                self:RemoveConsumption("Water")
                self:AddConsumption("Radioactive Coolant", WaterHeaterMult, true)
            end
        elseif Consumption["Radioactive Coolant"] then
            Changed = true
            self:RemoveConsumption("Radioactive Coolant")
            self:AddConsumption("Water", WaterHeaterMult, true)
        end

        if Changed then
            local ID = SC.NWAccessors.GetNWAccessorID(self, "CycleActivationCost")
            SC.NWAccessors.SyncValue(self, ID)
        end
    end
end)

GM:RegisterGenerator(Generator)
-- End Water Heater

-- Methanol Burner
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methanol Burner")
Generator:SetClass("sc_generator")
Generator:SetDescription("Burns Methanol to produce Steam.")
Generator:SetCategory("Heaters")
Generator:SetBaseVolume(13461)
Generator:SetDefaultModel("models/slyfo_2/pss_thrust.mdl")
--2 CH3OH + 3 O2 → 2 CO2 + 4 H2O
Generator:AddProduction("Steam", 540)
Generator:AddProduction("Carbon Dioxide", 270)
Generator:AddConsumption("Methanol", 270, true)
Generator:AddConsumption("Oxygen", 810, true)
GM:RegisterGenerator(Generator)
-- End Methanol Burner

-- Methane Burner
local Generator = GM:NewGeneratorInfo()
Generator:SetName("Methane Burner")
Generator:SetClass("sc_generator")
Generator:SetDescription("Burns Methane to produce Steam.")
Generator:SetCategory("Heaters")
Generator:SetBaseVolume(4729)
Generator:SetDefaultModel("models/slyfo_2/protorover_eng_plate.mdl")
--CH4 + 2 O2 → CO2 + 2 H2O
Generator:AddProduction("Steam", 270)
Generator:AddProduction("Carbon Dioxide", 135)
Generator:AddConsumption("Methane", 135, true)
Generator:AddConsumption("Oxygen", 540, true)
GM:RegisterGenerator(Generator)
-- End Methane Burner