local GM = GM

-- These files get sent to the client
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("diaspora.lua")
AddCSLuaFile("cl_help.lua")
AddCSLuaFile("cl_quiz.lua")
AddCSLuaFile("cl_hints.lua")
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("cl_search_models.lua")
AddCSLuaFile("cl_spawnmenu.lua")
AddCSLuaFile("cl_worldtips.lua")
AddCSLuaFile("player_extension.lua")
AddCSLuaFile("gui/IconEditor.lua")
AddCSLuaFile("gui/inventoryinfopanel.lua")
AddCSLuaFile("gui/inventorypanel.lua")
AddCSLuaFile("gui/inventoryitem.lua")
AddCSLuaFile("maprotate/client/cl_maptimer.lua")
AddCSLuaFile("classes/send.lua")
AddCSLuaFile("sb/cl_main.lua")
AddCSLuaFile("spacecombat2/cl_convar.lua")
AddCSLuaFile("spacecombat2/cl_config.lua")

include("classes/send.lua")
include("spacecombat2/sh_main.lua")
include('shared.lua')
include('commands.lua')
include('player.lua')
include("quiz.lua")
include("announcements.lua")
include('spawnmenu/init.lua')
include("sb/sv_main.lua")
include("spacecombat2/sv_main.lua")
include("admin/init.lua")
include("maprotate/server/maptimer.lua")

--networking strings
util.AddNetworkString("AddToChatBox")
util.AddNetworkString("PlayerSay")
util.AddNetworkString("SetOverlayText")
util.AddNetworkString("SetOverlayData")
util.AddNetworkString("Ship_Shield_UMSG")
util.AddNetworkString("Ship_Core_UMSG")
util.AddNetworkString("Ship_Core_Update")
util.AddNetworkString("Ship_Core_Select")
util.AddNetworkString("Ship_Core_OpenGlobalStorage")
util.AddNetworkString("Ship_Core_Mods")
util.AddNetworkString("sc_refinery_ui")
util.AddNetworkString("sc_createeffect")
util.AddNetworkString("sc_module_reactor_ui")
util.AddNetworkString("SC.ClientMissingString")
util.AddNetworkString("SC.CreateLight")
util.AddNetworkString("SC.Play3DSound")
util.AddNetworkString("SC.PlayerStorageSync")
util.AddNetworkString("SC.ShipStorageSync")

include("chat/chat.lua")

--
-- Make BaseClass available
--
DEFINE_BASECLASS("gamemode_base")


--[[---------------------------------------------------------
   Name: gamemode:OnPhysgunFreeze(weapon, phys, ent, player)
   Desc: The physgun wants to freeze a prop
-----------------------------------------------------------]]
function GM:OnPhysgunFreeze(weapon, phys, ent, ply)

	-- Don't freeze persistent props (should already be froze)
	if (ent:GetPersistent()) then return false end

	BaseClass.OnPhysgunFreeze(self, weapon, phys, ent, ply)

	ply:SendHint("PhysgunUnfreeze", 0.3)
	ply:SuppressHint("PhysgunFreeze")

end


--[[---------------------------------------------------------
   Name: gamemode:OnPhysgunReload(weapon, player)
   Desc: The physgun wants to unfreeze
-----------------------------------------------------------]]
function GM:OnPhysgunReload(weapon, ply)

	local num = ply:PhysgunUnfreeze()

	if (num > 0) then
		ply:SendLua("GAMEMODE:UnfrozeObjects("..num..")")
	end

	ply:SuppressHint("PhysgunReload")

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerShouldTakeDamage
   Return true if this player should take damage from this attacker
   Note: This is a shared function - the client will think they can
	 damage the players even though they can't. This just means the
	 prediction will show blood.
-----------------------------------------------------------]]
function GM:PlayerShouldTakeDamage(ply, attacker)

	-- The player should always take damage in single player..
	if (game.SinglePlayer()) then return true end

	-- Global godmode, players can't be damaged in any way
	if (cvars.Bool("sbox_godmode", false)) then return false end

	-- No player vs player damage
	if (attacker:IsValid() and attacker:IsPlayer()) then
		return cvars.Bool("sbox_playershurtplayers", true)
	end

	-- Default, let the player be hurt
	return true

end


--[[---------------------------------------------------------
   Show the search when f1 is pressed
-----------------------------------------------------------]]
function GM:ShowHelp(ply)

	ply:SendLua("hook.Run('StartSearch')")

end


--[[---------------------------------------------------------
   Desc: A ragdoll of an entity has been created
-----------------------------------------------------------]]
function GM:CreateEntityRagdoll(entity, ragdoll)

	-- Replace the entity with the ragdoll in cleanups etc
	undo.ReplaceEntity(entity, ragdoll)
	cleanup.ReplaceEntity(entity, ragdoll)

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerUnfrozeObject()
-----------------------------------------------------------]]
function GM:PlayerUnfrozeObject(ply, entity, physobject)

	local effectdata = EffectData()
		effectdata:SetOrigin(physobject:GetPos())
		effectdata:SetEntity(entity)
	util.Effect("phys_unfreeze", effectdata, true, true)

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerFrozeObject()
-----------------------------------------------------------]]
function GM:PlayerFrozeObject(ply, entity, physobject)

	if (DisablePropCreateEffect) then return end

	local effectdata = EffectData()
		effectdata:SetOrigin(physobject:GetPos())
		effectdata:SetEntity(entity)
	util.Effect("phys_freeze", effectdata, true, true)

end


--
-- Who can edit variables?
-- If you're writing prop protection or something, you'll
-- probably want to hook or override this function.
--
function GM:CanEditVariable(ent, ply, key, val, editor)

	-- Only allow admins to edit admin only variables!
	if (editor.AdminOnly) then
		return ply:IsAdmin()
	end

	-- This entity decides who can edit its variables
	if (isfunction(ent.CanEditVariables)) then
		return ent:CanEditVariables(ply)
	end

	-- default in sandbox is.. anyone can edit anything.
	return true

end

--GM.MakeEnt is basically a copy of WireLib.MakeWireEnt with our ownership stuff and without limit checks
function GM.MakeEnt(pl, Data, ...)
    local sent = scripted_ents.Get(Data.Class)
    if sent then
        Data.Class = sent.ClassName
    end

	local ent = ents.Create(Data.Class)
	if not IsValid(ent) then return false end

	duplicator.DoGeneric(ent, Data)
	ent:Spawn()
	ent:Activate()
	duplicator.DoGenericPhysics(ent, pl, Data) -- Is deprecated, but is the only way to access duplicator.EntityPhysics.Load (its local)

	ent.Owner = pl
	if ent.Setup then ent:Setup(...) end

	local phys = ent:GetPhysicsObject()
	if IsValid(phys) then
		if Data.frozen then phys:EnableMotion(false) end
		if Data.nocollide then phys:EnableCollisions(false) end
    end

    if ent.LoadSCInfo and Data.EntityMods then
        local Info = Data.EntityMods
        local SC2DupeInfo
        if not Info.SC2DupeInfo then
            if Info.WireDupeInfo then
                SC2DupeInfo = Info.WireDupeInfo.sc_data or {}
            else
                SC2DupeInfo = {}
            end
        else
            SC2DupeInfo = Info.SC2DupeInfo
        end

        ent:LoadSCInfo(SC2DupeInfo)
    end

	return ent
end

--playerspawn stuff
function GM:PlayerInitialSpawn(ply)
	self.BaseClass.PlayerInitialSpawn(self, ply)

	--This is here because players spawn before they're completely valid. You can thank Source logic for this one...
	timer.Simple(0.5, function() if IsValid(ply) then ply:KillSilent() end end)

	hook.Run("NotifyEvent", "PlayerSpawn", {Player=ply})
end

--[[---------------------------------------------------------
   Called on the player's every spawn
-----------------------------------------------------------]]
function GM:PlayerSpawn(ply)
    player_manager.SetPlayerClass(ply, "player_spacecombat")

	local spawners = ents.FindByClass("sc_player_start")
	for k,v in pairs(spawners) do
		if ply:getRace() == v.Race then
			ply:SetPos(v:GetPos())
			ply:SetAngles(v:GetAngles())
		end
	end

	self.BaseClass.PlayerSpawn(self, ply)
end

gameevent.Listen( "player_disconnect" )
hook.Add("player_disconnect", "SC.PlayerDisconnected", function(Data)
	hook.Run("NotifyEvent", "PlayerDisconnected", Data)
end)

gameevent.Listen( "player_connect" )
hook.Add( "player_connect", "SC.PlayerConnecting", function(Data)
	hook.Run("NotifyEvent", "PlayerConnecting", Data)
end)

-- Get the event text for generic gamemode events
function GM:GetEventText(EventName, EventData)
	if EventName == "PlayerSpawn" then
		if not IsValid(EventData.Player) then
			return
		end

		return {Color(0,255,0), EventData.Player:Nick(), " (", EventData.Player:SteamID(), ")", Color(255,255,255), " has joined the server!"}
	elseif EventName == "PlayerDisconnected" then
		if EventData.reason and string.len(EventData.reason) > 0 then
			return {Color(0,255,0), EventData.name, " (", EventData.networkid, ")", Color(255,255,255), " Disconnected. (Reason: ", EventData.reason, ")"}
		else
			return {Color(0,255,0), EventData.name, " (", EventData.networkid, ")", Color(255,255,255), " Disconnected."}
		end
	elseif EventName == "PlayerConnecting" then
		return {Color(0,255,0), EventData.name, " (", EventData.networkid, ")", Color(255,255,255), " is joining the server!"}
	end
end

function GM:NotifyEvent(EventName, EventData)
    local Message = hook.Run("GetEventText", EventName, EventData)

    if Message ~= nil then
        hook.Run("BroadcastEvent", EventName, Message)
    end
end

function GM:VehicleMove( ply, vehicle, mv )
	-- On duck toggle third person view
	if mv:KeyDown(IN_SPEED) and mv:KeyPressed(IN_ZOOM) and vehicle.SetThirdPersonMode then
		vehicle:SetThirdPersonMode(not vehicle:GetThirdPersonMode())
	end

	-- Adjust the camera distance with the mouse wheel
	local iWheel = ply:GetCurrentCommand():GetMouseWheel()
	if iWheel ~= 0 and vehicle.SetCameraDistance then
		-- The distance is a multiplier
		-- Actual camera distance = ( renderradius + renderradius * dist )
		-- so -1 will be zero.. clamp it there.
		local newdist = math.Clamp(vehicle:GetCameraDistance() - iWheel * 0.03 * (1.1 + vehicle:GetCameraDistance()), -1, 50)
		vehicle:SetCameraDistance(newdist)
	end
end