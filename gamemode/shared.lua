--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

AddCSLuaFile()

include("player_extension.lua")
include("player_class/player_spacecombat.lua")
include("drive/drive_sandbox.lua")
include("editor_player.lua")

--
-- Make BaseClass available
--
DEFINE_BASECLASS("gamemode_base")

--[[
 Note: This is so that in addons you can do stuff like

 if (!GAMEMODE.IsSandboxDerived) then return end

--]]

GM.IsSandboxDerived = true

cleanup.Register("props")
cleanup.Register("ragdolls")
cleanup.Register("effects")
cleanup.Register("npcs")
cleanup.Register("constraints")
cleanup.Register("ropeconstraints")
cleanup.Register("sents")
cleanup.Register("vehicles")


local physgun_limited = CreateConVar("physgun_limited", "0", FCVAR_REPLICATED)

--[[---------------------------------------------------------
   Name: gamemode:CanTool(ply, trace, mode)
   Return true if the player is allowed to use this tool
-----------------------------------------------------------]]
function GM:CanTool(ply, trace, mode)

	-- The jeep spazzes out when applying something
	-- todo: Find out what it's reacting badly to and change it in _physprops
	if (mode == "physprop" and trace.Entity:IsValid() and trace.Entity:GetClass() == "prop_vehicle_jeep") then
		return false
	end

	-- If we have a toolsallowed table, check to make sure the toolmode is in it
	if (trace.Entity.m_tblToolsAllowed) then

		local vFound = false
		for k, v in pairs(trace.Entity.m_tblToolsAllowed) do
			if (mode == v) then vFound = true end
		end

		if (not vFound) then return false end

	end

	-- Give the entity a chance to decide
	if (trace.Entity.CanTool) then
		return trace.Entity:CanTool(ply, trace, mode)
	end

	return true

end


--[[---------------------------------------------------------
   Name: gamemode:GravGunPunt()
   Desc: We're about to punt an entity (primary fire).
		 Return true if we're allowed to.
-----------------------------------------------------------]]
function GM:GravGunPunt(ply, ent)

	if (ent:IsValid() and ent.GravGunPunt) then
		return ent:GravGunPunt(ply)
	end

	return BaseClass.GravGunPunt(self, ply, ent)

end

--[[---------------------------------------------------------
   Name: gamemode:GravGunPickupAllowed()
   Desc: Return true if we're allowed to pickup entity
-----------------------------------------------------------]]
function GM:GravGunPickupAllowed(ply, ent)

	if (ent:IsValid() and ent.GravGunPickupAllowed) then
		return ent:GravGunPickupAllowed(ply)
	end

	return BaseClass.GravGunPickupAllowed(self, ply, ent)

end


--[[---------------------------------------------------------
   Name: gamemode:PhysgunPickup()
   Desc: Return true if player can pickup entity
-----------------------------------------------------------]]
function GM:PhysgunPickup(ply, ent)

	-- Don't pick up persistent props
	if (ent:GetPersistent()) then return false end

	if (ent:IsValid() and ent.PhysgunPickup) then
		return ent:PhysgunPickup(ply)
	end

	-- Some entities specifically forbid physgun interaction
	if (ent.PhysgunDisabled) then return false end

	local EntClass = ent:GetClass()

	-- Never pick up players
	if (EntClass == "player") then return false end

	if (physgun_limited:GetBool()) then

		if (string.find(EntClass, "prop_dynamic")) then return false end
		if (string.find(EntClass, "prop_door")) then return false end

		-- Don't move physboxes if the mapper logic says no
		if (EntClass == "func_physbox" and ent:HasSpawnFlags(SF_PHYSBOX_MOTIONDISABLED)) then return false  end

		-- If the physics object is frozen by the mapper, don't allow us to move it.
		if (string.find(EntClass, "prop_") and (ent:HasSpawnFlags(SF_PHYSPROP_MOTIONDISABLED) or ent:HasSpawnFlags(SF_PHYSPROP_PREVENT_PICKUP))) then return false end

		-- Allow physboxes, but get rid of all other func_'s (ladder etc)
		if (EntClass ~= "func_physbox" and string.find(EntClass, "func_")) then return false end


	end

	if (SERVER) then

		ply:SendHint("PhysgunFreeze", 2)
		ply:SendHint("PhysgunUse", 8)

	end

	return true

end


--[[---------------------------------------------------------
   Name: gamemode:EntityKeyValue(ent, key, value)
   Desc: Called when an entity has a keyvalue set
	      Returning a string it will override the value
-----------------------------------------------------------]]
function GM:EntityKeyValue(ent, key, value)

	-- Physgun not allowed on this prop..
	if (key == "gmod_allowphysgun" and value == '0') then
		ent.PhysgunDisabled = true
	end

	-- Prop has a list of tools that are allowed on it.
	if (key == "gmod_allowtools") then
		ent.m_tblToolsAllowed = string.Explode(" ", value)
	end

end

--[[---------------------------------------------------------
   Name: gamemode:CanProperty(pl, property, ent)
   Desc: Can the player do this property, to this entity?
-----------------------------------------------------------]]
function GM:CanProperty(pl, property, ent)

	--
	-- Always a chance some bastard got through
	--
	if (not IsValid(ent)) then return false end


	--
	-- If we have a toolsallowed table, check to make sure the toolmode is in it
	-- This is used by things like map entities
	--
	if (ent.m_tblToolsAllowed) then

		local vFound = false
		for k, v in pairs(ent.m_tblToolsAllowed) do
			if (property == v) then vFound = true end
		end

		if (not vFound) then return false end

	end

	--
	-- Who can who bone manipulate?
	--
	if (property == "bonemanipulate") then

		if (game.SinglePlayer()) then return true end

		if (ent:IsNPC()) then return GetConVarNumber("sbox_bonemanip_npc") ~= 0 end
		if (ent:IsPlayer()) then return GetConVarNumber("sbox_bonemanip_player") ~= 0 end

		return GetConVarNumber("sbox_bonemanip_misc") ~= 0

	end

    --
	-- Weapons can only be property'd if nobody is holding them
	--
	if (ent:IsWeapon() and IsValid(ent:GetOwner())) then
		return false
	end

	-- Give the entity a chance to decide
	if (ent.CanProperty) then
		return ent:CanProperty(pl, property)
	end

	return true

end

--[[---------------------------------------------------------
   Name: gamemode:CanDrive(pl, ent)
   Desc: Return true to let the entity drive.
-----------------------------------------------------------]]
function GM:CanDrive(pl, ent)
	return pl:IsAdmin() or pl:IsSuperAdmin()
end


--[[---------------------------------------------------------
	To update the player's animation during a drive
-----------------------------------------------------------]]
function GM:PlayerDriveAnimate(ply)

	local driving = ply:GetDrivingEntity()
	if not IsValid(driving) then return end

	ply:SetPlaybackRate(1)
	ply:ResetSequence(ply:SelectWeightedSequence(ACT_HL2MP_IDLE_MAGIC))

	--
	-- Work out the direction from the player to the entity, and set parameters
	--
	local DirToEnt = driving:GetPos() - (ply:GetPos() + Vector(0, 0, 50))
	local AimAng = DirToEnt:Angle()

	if (AimAng.p > 180) then
		AimAng.p = AimAng.p - 360
	end

	ply:SetPoseParameter("aim_yaw",			0)
	ply:SetPoseParameter("aim_pitch",		AimAng.p)
	ply:SetPoseParameter("move_x",			0)
	ply:SetPoseParameter("move_y",			0)
	ply:SetPoseParameter("move_yaw",		0)
	ply:SetPoseParameter("move_scale",		0)

	AimAng.p = 0
	AimAng.r = 0

	ply:SetRenderAngles(AimAng)
	ply:SetEyeTarget(driving:GetPos())

end

--[[ End of Sandbox

Beginning of Spacebuild 4 ]]--

GM.Name = "Space Combat 2"
GM.Author = "Diaspora Gaming Community"

include("sb/lcs.lua")
GM.LCS = LCS

GM.class = {} -- Used to create objects of classes defined in /classes
GM.convars = {} -- Used to store convars which will be used to configure spacebuild
GM.wrappers = {} -- Populated by sh_wrappers at a later date.
GM.constants = {} -- Populated by sh_const
GM.util = {} -- Populated by sh_util
GM.internal = {} -- Used for internal things like HUDs

SC = SC or {}

function GM:getBaseClass()
	return BaseClass
end

function FindPlayerByName(name)
    name = string.lower(name)
    for _,v in ipairs(player.GetAll()) do
        if string.find(string.lower(v:Name()),name,1,true) ~= nil then
            return v
        end
    end
end

-- Include consts/utils/wrappers before ANYTHING!!!!
include("sb/sh_const.lua")
include("sb/sh_util.lua")

include("config.lua")

include("classes/init.lua")
include("sb/sh_main.lua")

include("factions/shared.lua")

include("cl_scoreboard.lua")

include("drive_test.lua")
include("gui/charactermenu.lua")
include("gui/dcategorytree.lua")

include("debugger.lua")

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--SMALLBRIDGE TEXTURES
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Skin 1
list.Add( "OverrideMaterials", "spacebuild/Body" );
list.Add( "OverrideMaterials", "spacebuild/bodyskin" );
list.Add( "OverrideMaterials", "spacebuild/hazard" );
list.Add( "OverrideMaterials", "spacebuild/floor" );
list.Add( "OverrideMaterials", "spacebuild/sblight" );
list.Add( "OverrideMaterials", "spacebuild/sblight_d" );
list.Add( "OverrideMaterials", "spacebuild/fusion" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--skin 2
list.Add( "OverrideMaterials", "spacebuild/body2" );
list.Add( "OverrideMaterials", "spacebuild/body2skin" );
list.Add( "OverrideMaterials", "spacebuild/hazard2" );
list.Add( "OverrideMaterials", "spacebuild/floor2" );
list.Add( "OverrideMaterials", "spacebuild/sblight2" );
list.Add( "OverrideMaterials", "spacebuild/sblight2_d" );
list.Add( "OverrideMaterials", "spacebuild/fusion2" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--skin 3
list.Add( "OverrideMaterials", "spacebuild/body3" );
list.Add( "OverrideMaterials", "spacebuild/body3skin" );
list.Add( "OverrideMaterials", "spacebuild/hazard3" );
list.Add( "OverrideMaterials", "spacebuild/floor3" );
list.Add( "OverrideMaterials", "spacebuild/sblight3" );
list.Add( "OverrideMaterials", "spacebuild/sblight3_d" );
list.Add( "OverrideMaterials", "spacebuild/fusion3" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--skin 4
list.Add( "OverrideMaterials", "spacebuild/body4" );
list.Add( "OverrideMaterials", "spacebuild/body4skin" );
list.Add( "OverrideMaterials", "spacebuild/hazard4" );
list.Add( "OverrideMaterials", "spacebuild/floor4" );
list.Add( "OverrideMaterials", "spacebuild/sblight4" );
list.Add( "OverrideMaterials", "spacebuild/sblight4_d" );
list.Add( "OverrideMaterials", "spacebuild/fusion4" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--skin 5
list.Add( "OverrideMaterials", "spacebuild/body5" );
list.Add( "OverrideMaterials", "spacebuild/body5skin" );
list.Add( "OverrideMaterials", "spacebuild/hazard5" );
list.Add( "OverrideMaterials", "spacebuild/floor5" );
list.Add( "OverrideMaterials", "spacebuild/sblight5" );
list.Add( "OverrideMaterials", "spacebuild/sblight5_d" );
list.Add( "OverrideMaterials", "spacebuild/fusion5" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--cerus' textures
list.Add( "OverrideMaterials", "cmats/base_grey" );
list.Add( "OverrideMaterials", "cmats/base_metal_floor" );
list.Add( "OverrideMaterials", "cmats/base_metal_rust" );
list.Add( "OverrideMaterials", "cmats/base_tile" );
list.Add( "OverrideMaterials", "cmats/base_metal_yellow" );
list.Add( "OverrideMaterials", "cmats/base_metal_light" );
list.Add( "OverrideMaterials", "cmats/station_metal_b" );
list.Add( "OverrideMaterials", "cmats/station_metal_b_s" );
list.Add( "OverrideMaterials", "cmats/genfield" );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
