--=====================
-- Timed Map Rotator
--	By Steeveeo, updated by Lt.Brandon
--=====================

local GM = GM
GM.MapRotate = {}
local MapRotate = GM.MapRotate

MapRotate.Enabled = false

--Table of Boom
local EndOfUniverseExplosionTable = {
	"sc_shipsplosion_capital",
	"sc_shipsplosion_frigate",
	"impact_bigbertha",
	"impact_bigtorp"
}

--Settings
if not ConVarExists("maprotate_roundtime") then
	CreateConVar("maprotate_roundtime", 240, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

if not ConVarExists("maprotate_doEndOfUniverse") then
	CreateConVar("maprotate_doEndOfUniverse", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

if not ConVarExists("maprotate_voteDuration") then
	CreateConVar("maprotate_voteDuration", 60, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end

--Rotator Tables
MapRotate.MapList = {}
MapRotate.MapWeights = {}

--Initialization Functions
function GM.MapRotate.Enable()
	--If reinitializing, clean up first
	if MapRotate.Enabled then MapRotate.Disable() end

	MapRotate.Enabled = true

	--General Rotation Vars
	MapRotate.Time = GetConVarNumber("maprotate_roundtime")*60
	MapRotate.NextMap = ""
	MapRotate.PasswordApplied = false
	MapRotate.EndOfTheWorld = false

	--Vote Vars
	MapRotate.voteInProgress = false
	MapRotate.voteType = "time"
	MapRotate.voteMap = ""
	MapRotate.voteTime = 0
	MapRotate.votes = {}
	MapRotate.playersVoted = {}

	MapRotate.RandomMap()
	MapRotate.StartTimer()

	hook.Add("PlayerInitialSpawn", "MapRotate_Resync", MapRotate.ResyncClient)

	MapRotate.ResyncAll()

	ErrorNoHalt("[MAP ROTATOR] - Rotator Enabled.\n")
end

function GM.MapRotate.Disable()
	MapRotate.Enabled = false

	hook.Remove("MapRotate_Resync")
	timer.Destroy("MapRotationTimer")

	MapRotate.ResyncAll()

	ErrorNoHalt("[MAP ROTATOR] - Rotator Disabled.\n")
end

--REMOVEME: Delete the following call when config files are in place
timer.Simple(1, GM.MapRotate.Enable)

--------------
--Networking
--------------
util.AddNetworkString("MapRotate_Resync")			--Big fat resync message so we don't have to start multiple packets
util.AddNetworkString("MapRotate_UpdateTime")		--Sync client clock
util.AddNetworkString("MapRotate_UpdateMap")		--Sync nextmap field
util.AddNetworkString("MapRotate_MaxTime")			--Update hour dial
util.AddNetworkString("MapRotate_VoteStart")		--Tell clients about a new vote
util.AddNetworkString("MapRotate_VoteSubmitted")	--Client voted yes/no
util.AddNetworkString("MapRotate_VoteEnd")			--Vote ends

function GM.MapRotate.ResyncAll()
	net.Start("MapRotate_Resync")
		net.WriteBool(MapRotate.Enabled)
		net.WriteInt(MapRotate.Time, 32)
		net.WriteString(MapRotate.NextMap)
		net.WriteInt(GetConVarNumber("maprotate_roundtime"), 16)
	net.Broadcast()
end

function GM.MapRotate.ResyncClient(ply)
	net.Start("MapRotate_Resync")
		net.WriteBool(MapRotate.Enabled)
		net.WriteInt(MapRotate.Time, 32)
		net.WriteString(MapRotate.NextMap)
		net.WriteInt(GetConVarNumber("maprotate_roundtime"), 16)
	net.Send(ply)
end

--End Networking--

--------------
--Chat Stuff
--------------

local function BroadcastChat(msg)
	--Print to RCON
	ErrorNoHalt("[MAP ROTATOR] - " .. msg .. "\n")

	--Print to Players
	for k, v in pairs(player.GetAll()) do
		v:PrintMessage(HUD_PRINTTALK, "[MAP ROTATOR] - " .. msg)
	end
end

local function ChatResponse(ply, msg)
	--Console
	if not IsValid(ply) then
		ErrorNoHalt("[MAP ROTATOR] - " .. msg .. "\n")
	--Players
	else
		ply:PrintMessage(HUD_PRINTTALK, "[MAP ROTATOR] - " .. msg)
	end
end

--End Chat Stuff--

----------------------
--Map List Functions
----------------------

--Generate MapRotate Folder
if not file.Exists("maprotate/", "DATA") then
	file.CreateDir("maprotate")
end

--Write Maplist to File
if not file.Exists("maprotate/maplist.txt", "DATA") then
	--Build Map List
	--Derived from ULX, this should work to get the maps we need
	local SBMapList = file.Find("maps/sb_*.bsp", "GAME")
	local SC2MapList = file.Find("maps/sc2_*.bsp", "GAME")
	local CombinedMapList = table.Add(SBMapList, SC2MapList)
	local stringList = ""

	for _, map in pairs(CombinedMapList) do
		stringList = stringList .. map:sub( 1, -5 ):lower() -- Take off the .bsp
		stringList = stringList .. "|100\n" -- Add on default weighting
	end

	--Send to file
	file.Write("maprotate/maplist.txt", stringList)
end

--Maplist Updater
local function WriteMapList()

	--Loop through our data and record
	local stringList = ""
	for Index = 1, #MapRotate.MapList do
		stringList = stringList .. MapRotate.MapList[Index] .. "|" .. MapRotate.MapWeights[Index] .. "\n"
	end

	--Send to file
	file.Write("maprotate/maplist.txt", stringList)

	return
end

--Get Maplist from File
if file.Exists("maprotate/maplist.txt","DATA") then

	--Variables
	local fileData = file.Read("maprotate/maplist.txt","DATA")

	--Parse the file
	MapData = string.Explode("\n", fileData)

	--Sort through and parse data
	for Index = 1, #MapData do

		--Is this a valid entry?
		if(MapData[Index] ~= "") then
			--Get the two entries
			local Data = string.Explode("|",MapData[Index])
			local Map = Data[1]
			local Weight = tonumber(Data[2])

			--If this map is the one we're on, lower the weight, else raise the weight a bit
			if(Map == game.GetMap():lower()) then
				Weight = math.Clamp(Weight - 10,10,1000)
			else
				Weight = math.Clamp(Weight + 10,10,1000)
			end

			--Send to tables
			MapRotate.MapList[Index] = Map
			MapRotate.MapWeights[Index] = Weight
		end
	end

	--Update maplist file
	WriteMapList()

end

--Pick a Random Map from the List
function GM.MapRotate.RandomMap()
	if not MapRotate.Enabled then
		Error("[MAP ROTATOR] - MapRotate is not enabled!\n")
		return
	end

	--Generate the randomizer spectrum
	local RandomRanges = {}
	local RangeTotal = 0
	local CurPos = 0
	for Index = 1, #MapRotate.MapList do
		local Chance = MapRotate.MapWeights[Index]
		RangeTotal = RangeTotal + Chance

		--Save Range to table
		RandomRanges[Index] = {}
		RandomRanges[Index].Min = CurPos
		RandomRanges[Index].Max = RangeTotal-1

		--Move range cursor to front
		CurPos = RangeTotal
	end

	--Randomly choose a place in the RandomRange
	math.randomseed(os.time() + RealTime())
	local Rand = math.Rand(0, RangeTotal)

	--Find where that landed
	for Index = 1, #RandomRanges do

		--Is it in this range?
		if(Rand >= RandomRanges[Index].Min and Rand <= RandomRanges[Index].Max) then
			--Set to this map
			MapRotate.NextMap = MapRotate.MapList[Index]

			--Break out, since we have the map
			break
		end

	end

end

--End Map List Functions--

---------------------
-- Admin Concommands
---------------------

------Set Time----
--Functionality
function GM.MapRotate.SetTime(time)
	if not MapRotate.Enabled then
		Error("[MAP ROTATOR] - MapRotate is not enabled!\n")
		return
	end

	MapRotate.Time = time

	if MapRotate.PasswordApplied then
		game.ConsoleCommand("sv_password \"\"\n")
		MapRotate.PasswordApplied = false
	end
end

local function SetTimeCmd(ply, command, args)
	if not MapRotate.Enabled then
		ChatResponse(ply, "ERROR: MapRotate is not enabled on this server.")
		return
	end

	local nick = "Console"

	if IsValid(ply) then
		--Check for admin, else end
		if not ply:IsAdmin() then
			ChatResponse(ply, "You do not have access to this command!")
			return
		end
		nick = ply:Nick()
	end

	local SetTo = math.Clamp( tonumber(args[1]), 5, 240 )

	MapRotate.SetTime(math.floor(SetTo*60))
	BroadcastChat(nick .. " has set the timer to " .. SetTo .. " minutes!")

	return
end

concommand.Add("maprotate_setTime", SetTimeCmd)
------End Set Time----


------Set Map----
--Functionality
local function MapExists(map)
	if not file.Exists( "maps/" .. map .. ".bsp", "GAME" ) then
		return false
	end

	return true
end

function GM.MapRotate.SetMap(map)
	if not MapRotate.Enabled then
		Error("[MAP ROTATOR] - MapRotate is not enabled!\n")
		return
	end

	MapRotate.NextMap = map

	--Update Clients
	net.Start("MapRotate_UpdateMap")
		net.WriteString(MapRotate.NextMap)
	net.Broadcast()
end
local function SetMapCmd(ply, command, args)
	if not MapRotate.Enabled then
		ChatResponse(ply, "ERROR: MapRotate is not enabled on this server.")
		return
	end

	local nick = "Console"

	if IsValid(ply) then
		--Check for admin, else end
		if not ply:IsAdmin() then
			ChatResponse(ply, "You do not have access to this command!")
			return
		end
		nick = ply:Nick()
	end

	--Grab MapName
	local SetTo = tostring(args[1])

	--Remove .bsp if the user put it there
	SetTo = string.Replace(SetTo, ".bsp", "")

	--Attempt to find map in map folder
	if not MapExists(SetTo) then
		ChatResponse(ply, "Map not found!")
		return
	end

	--If all went well, set map
	MapRotate.SetMap(SetTo)
	BroadcastChat(nick .. " has set the next map to " .. SetTo .. "!")

	return
end

--SetMap Arguments
local function SetMapArgs(cmd, args)
	local maplist = file.Find("maps/*.bsp", "GAME")
	local mapTable = {}
	local stringList = ""

	for _, map in pairs(maplist) do
		table.insert(mapTable, cmd .. " " .. map:sub(1, -5):lower())
	end

	return mapTable
end
concommand.Add("maprotate_setMap", SetMapCmd, SetMapArgs)
------End Set Map----


------Print Time----
local function MapTime(ply,command,args)
	if not MapRotate.Enabled then
		Error("[MAP ROTATOR] - MapRotate is not enabled!\n")
		return
	end

	local nick = "Console"

	if IsValid(ply) then
		nick = ply:Nick()
	end

	--Decode Time into Clock Format
	local Hours
	local Minutes
	local Seconds
	local M_String
	local S_String
	Hours = math.floor(MapRotate.Time / 3600)
	Minutes = math.floor(MapRotate.Time / 60) - (Hours * 60)
	Seconds = math.floor(MapRotate.Time) - (Hours * 3600) - (Minutes * 60)

	--Add a placeholder for single digit numbers
	if(Minutes < 10) then
		M_String = "0" .. Minutes
	else
		M_String = tostring(Minutes)
	end

	if(Seconds < 10) then
		S_String = "0" .. Seconds
	else
		S_String = tostring(Seconds)
	end

	TimeString = Hours .. ":" .. M_String .. ":" .. S_String

	ChatResponse(ply, "Time Left: " .. TimeString)
	ChatResponse(ply, "Next Map: " .. MapRotate.NextMap)
	return
end

concommand.Add("maprotate_mapTime", MapTime)
------End Print Time----


----End Admin Concommands


---------------------
-- Player Commands
---------------------
local function TallyVotes(countUndecideds)
	local yesCount = 0
	local noCount = 0
	local total = 0

	--Tally Votes
	for k,v in pairs(MapRotate.votes) do
		if v then
			yesCount = yesCount + 1
		else
			noCount = noCount + 1
		end

		total = total + 1
	end

	if countUndecideds then total = #player.GetAll() end

	local percentYes = yesCount / total
	local percentNo = noCount / total

	return percentYes, percentNo, yesCount, noCount
end

local function FinalizeVote()
	local percentYes, percentNo, yesCount, noCount = TallyVotes(false)

	--Vote Passes
	if percentYes > 0.5 then
		BroadcastChat("Vote has passed " .. yesCount .. " to " .. noCount .. " (" .. math.Round(percentYes * 100) .. "%)!")

		--Set Map
		if MapRotate.voteType == "map" then
			MapRotate.SetMap(MapRotate.voteMap)

		--Set Time
		elseif MapRotate.voteType == "time" then
			MapRotate.SetTime(MapRotate.voteTime * 60)
		end
	--Vote Fails
	else
		BroadcastChat("Vote has failed " .. yesCount .. " to " .. noCount .. " (" .. math.Round(percentYes * 100) .. "%)")
	end

	MapRotate.voteInProgress = false

	--Notify clients
	net.Start("MapRotate_VoteEnd")
	net.Broadcast()
end

local function StartMapVote(map)
	MapRotate.voteType = "map"
	MapRotate.voteInProgress = true
	MapRotate.voteMap = map
	MapRotate.votes = {}
	MapRotate.playersVoted = {}

	--Vote Timeout
	timer.Create("MapRotate_VoteEnd", GetConVarNumber("maprotate_voteDuration"), 1, FinalizeVote)

	--Send to clients
	net.Start("MapRotate_VoteStart")
		net.WriteInt(GetConVarNumber("maprotate_voteDuration"), 16)
		net.WriteFloat(CurTime())
		net.WriteBool(true) --Is Map Vote
		net.WriteString(map)
	net.Broadcast()
end

local function StartTimeVote(time)
	MapRotate.voteType = "time"
	MapRotate.voteInProgress = true
	MapRotate.voteTime = time
	MapRotate.votes = {}
	MapRotate.playersVoted = {}

	--Vote Timeout
	timer.Create("MapRotate_VoteEnd", GetConVarNumber("maprotate_voteDuration"), 1, FinalizeVote)

	--Send to clients
	net.Start("MapRotate_VoteStart")
		net.WriteInt(GetConVarNumber("maprotate_voteDuration"), 16)
		net.WriteFloat(CurTime())
		net.WriteBool(false) --Is Map Vote
		net.WriteInt(time * 60, 32)
	net.Broadcast()
end

local function SubmitVote(ply, yes)
	--Record Votes
	table.insert(MapRotate.votes, yes)
	table.insert(MapRotate.playersVoted, ply)

	--Check if we need to end the vote
	if #MapRotate.votes >= #player.GetAll() then
		timer.Destroy("MapRotate_VoteEnd")
		FinalizeVote()
	end

	--Send to clients
	local percentYes, percentNo = TallyVotes(true)
	net.Start("MapRotate_VoteSubmitted")
		net.WriteFloat(percentYes)
		net.WriteFloat(percentNo)
	net.Broadcast()
end

hook.Add("RegisterSC2Commands", "RegisterMapTimerCommands", function()
    local Admin = SC.Administration

    -- Register commands
    Admin.RegisterCommand("Maplist", "List all maps available to the map rotator.",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
        },

        -- Callback
        function(ply, args)
            if not MapRotate.Enabled then
                ChatResponse(ply, "ERROR: MapRotate is not enabled on this server.")
                return
            end

            local maplist = table.concat(MapRotate.MapList, ", ")
            ChatResponse(ply, "These are the maps available to vote for: "..maplist)
        end)
    Admin.RegisterCommandAlias("Maplist","Maps")

    Admin.RegisterCommand("Votemap", "Starts a vote to change MapRotate's next map.",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
            {
                Name = "Map",
                Type = "string"
            }
        },

        -- Callback
        function(Executor, Arguments)
            if not MapRotate.Enabled then
                ChatResponse(Executor, "ERROR: MapRotate is not enabled on this server.")
                return
            end

            --Check if we're already voting on something
            if MapRotate.voteInProgress then
                ChatResponse(Executor, "Vote already in progress!")

            --Check if the desired map exists
            elseif not MapExists(Arguments.Map) then
                ChatResponse(Executor, "Map not found!")

            --Start Vote
            else
                BroadcastChat(Executor:Nick().." started a vote to change the next map to "..Arguments.Map..".")
                StartMapVote(Arguments.Map)
                SubmitVote(Executor, true)
            end
        end)
    Admin.RegisterCommandAlias("Votemap","rtv")

    Admin.RegisterCommand("Votetime", "Starts a vote to change MapRotate's remaining time.",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
            {
                Name = "Time",
                Type = "number"
            }
        },

        -- Callback
        function(Executor, Arguments)
            if not MapRotate.Enabled then
                ChatResponse(Executor, "ERROR: MapRotate is not enabled on this server.")
                return
            end

            --Check if we're already voting on something
            if MapRotate.voteInProgress then
                ChatResponse(Executor, "Vote already in progress!")

            --Check if the desired time is valid
            elseif Arguments.Time < 5 or Arguments.Time > GetConVarNumber("maprotate_roundtime") then
                ChatResponse(Executor, "Time must be between 5 and "..GetConVarNumber("maprotate_roundtime").." minutes!")

            --Start Vote
            else
                BroadcastChat(Executor:Nick().." started a vote to change the timer to "..Arguments.Time.." minutes.")
                StartTimeVote(Arguments.Time)
                SubmitVote(Executor, true)
            end
        end)

    Admin.RegisterCommand("Voteyes", "Responds with \"Yes\" to a vote in progress.",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)
            if not MapRotate.Enabled then
                ChatResponse(Executor, "ERROR: MapRotate is not enabled on this server.")
                return
            end

            --Only vote if there's something to vote on
            if not MapRotate.voteInProgress then
                ChatResponse(Executor, "No vote in progress!")

            --Check if we've already voted
            elseif table.HasValue(MapRotate.playersVoted, Executor) then
                ChatResponse(Executor, "You have already voted on this!")

            --Submit Vote
            else
                BroadcastChat(Executor:Nick() .. " voted yes.")
                SubmitVote(Executor, true)
            end
        end)

    Admin.RegisterCommand("Voteno", "Responds with \"no\" to a vote in progress.",
        -- Permissions
        {
            -- Usable by anyone
        },

        -- Arguments
        {
        },

        -- Callback
        function(Executor, Arguments)
            if not MapRotate.Enabled then
                ChatResponse(Executor, "ERROR: MapRotate is not enabled on this server.")
                return
            end

            --Only vote if there's something to vote on
            if not MapRotate.voteInProgress then
                ChatResponse(Executor, "No vote in progress!")

            --Check if we've already voted
            elseif table.HasValue(MapRotate.playersVoted, Executor) then
                ChatResponse(Executor, "You have already voted on this!")

            --Submit Vote
            else
                BroadcastChat(Executor:Nick() .. " voted no.")
                SubmitVote(Executor, false)
            end
        end)
end)

----End Player Commands

--Main Timer Loop
function GM.MapRotate.StartTimer()
	if not MapRotate.Enabled then
		Error("[MAP ROTATOR] - MapRotate is not enabled!\n")
		return
	end

	timer.Create("MapRotationTimer", 1, 0, function()
		--Are we nearing Time Up?
		if MapRotate.Time <= 1800 then
			if MapRotate.Time == 1800 then
				BroadcastChat("30 minutes left.")
			elseif MapRotate.Time == 900 then
				BroadcastChat("15 minutes left.")
			elseif MapRotate.Time == 600 then
				BroadcastChat("10 minutes left.")
			elseif MapRotate.Time == 300 then
				BroadcastChat("5 minutes left! Save up!")

				--OH GOD ALERT THE MASSES
				for k,v in pairs(player.GetAll()) do
					v:EmitSound("npc/attack_helicopter/aheli_damaged_alarm1.wav")
				end

				game.ConsoleCommand("sv_password 5min\n")
				MapRotate.PasswordApplied = true
			elseif MapRotate.Time == 60 then
				if GetConVar("maprotate_doEndOfUniverse"):GetBool() then
					BroadcastChat("1 minute left until End of the Universe! Final warning!")
				else
					BroadcastChat("1 minute left! Final warning!")
				end

				--AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
				for k,v in pairs(player.GetAll()) do
					v:EmitSound("npc/attack_helicopter/aheli_megabomb_siren1.wav")
				end
			elseif MapRotate.Time <= 0 and not MapRotate.EndOfTheWorld then
				if GetConVar("maprotate_doEndOfUniverse"):GetBool() and #player.GetAll() > 0 then
					BroadcastChat("WARNING: End of the Universe!")

					--Blow up all the cores, end of the universe!
					local deathTime = 5
					for k,v in pairs(ents.FindByClass("ship_core")) do
						if (v.DeathTime or 3) > deathTime then
							deathTime = v.DeathTime
						end

						v:ApplyDamage({wtf=1e30}, v, v, {Entity=v})
					end

					--Explosions everywhere, end of the universe!
					local seconds = 0
					while seconds < deathTime + 3 do
						local delay = math.Rand(0.25, 1.5)
						delay = math.Clamp(delay, 0, (deathTime + 3) - seconds)

						timer.Simple(seconds + delay, function()
							local e = EffectData()
							e:SetOrigin(VectorRand() * 16000)
							e:SetNormal(VectorRand())

							util.Effect(table.Random(EndOfUniverseExplosionTable), e)
						end)

						seconds = seconds + delay
					end

					--Fuck your eyes, end of the universe!
					timer.Simple(deathTime + 5, function()
						for i = 1, 3 do
							for k,v in pairs(player.GetAll()) do
								local e = EffectData()
								e:SetOrigin(v:GetPos())

								util.Effect("sc_shipsplosion_titan", e)
							end
						end
					end)

					--Actually go to the next map (40 seconds because titans take awhile to go boom)
					--....End of the universe!
					timer.Simple(deathTime + 10, function()
						game.ConsoleCommand("sv_password \"\"\n")
						game.ConsoleCommand("changelevel " .. MapRotate.NextMap .. "\n")
					end)

					MapRotate.EndOfTheWorld = true
				else
					--Boring, nonflashy end of the universe...
					game.ConsoleCommand("sv_password \"\"\n")
					game.ConsoleCommand("changelevel " .. MapRotate.NextMap .. "\n")
				end
			end
		end

		MapRotate.Time = MapRotate.Time - 1
		MapRotate.Time = math.max(MapRotate.Time, 0)

		--Send this on over to the client
		net.Start("MapRotate_UpdateTime")
			net.WriteInt(MapRotate.Time, 32)
		net.Broadcast()

	end)
end
