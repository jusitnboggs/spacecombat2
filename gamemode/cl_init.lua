--Functions that should exist
function FormatLine(str,font,size) --size equals width in pixes
	surface.SetFont(font)

	local start = 1
	local c = 1
	local endstr = ""
	local n = 0
	local lastspace = 0
	while(string.len(str) > c)do
		local sub = string.sub(str, start, c)
		if(string.sub(str, c, c) == " ") then
			lastspace = c
		end

		if(surface.GetTextSize(sub) >= size) then
			local sub2

			if(lastspace == 0) then
				lastspace = c
			end

			if(lastspace > 1) then
				sub2 = string.sub(str, start, lastspace - 1)
				c = lastspace
			else
				sub2 = string.sub(str, start, c)
			end
			endstr = endstr .. sub2 .. "\n"
			start = c + 1
			n = n + 1
		end
		c = c + 1
	end

	if(start < string.len(str)) then
		endstr = endstr .. string.sub(str, start)
	end

	return endstr, n
end


function GM.MaterialFromVMT(name,VMT)
	if(type(VMT) ~= "string" or type(name) ~= "string") then return Material("") end
	local t = util.KeyValuesToTable("\"material\"{"..VMT.."}")
	local shader,params = next(t)
	return CreateMaterial(name,shader,params)
end

function GM.MaterialCopy(name,filename)
	if(type(filename) ~= "string" or type(name) ~= "string") then return Material("") end
	filename = "../materials/"..filename:Trim():gsub(".vmt$","")..".vmt"
	return GM.MaterialFromVMT(name,file.Read(filename))
end

-- Hacks and redirects
GetControlPanel = controlpanel.Get

local MaterialMeta = FindMetaTable("IMaterial")
MaterialMeta.GetMaterialVector = MaterialMeta.GetVector
MaterialMeta.SetMaterialVector = MaterialMeta.SetVector
MaterialMeta.GetMaterialFloat = MaterialMeta.GetFloat
MaterialMeta.SetMaterialFloat = MaterialMeta.SetFloat
MaterialMeta.GetMaterialInt = MaterialMeta.GetInt
MaterialMeta.SetMaterialInt = MaterialMeta.SetInt
MaterialMeta.GetMaterialShader = MaterialMeta.GetShader
MaterialMeta.SetMaterialShader = MaterialMeta.SetShader
MaterialMeta.GetMaterialName = MaterialMeta.GetName
MaterialMeta.SetMaterialName = MaterialMeta.SetName
MaterialMeta.GetMaterialMatrix = MaterialMeta.GetMatrix
MaterialMeta.SetMaterialMatrix = MaterialMeta.SetMatrix
MaterialMeta.GetMaterialTexture = MaterialMeta.GetTexture
MaterialMeta.SetMaterialTexture = MaterialMeta.SetTexture
MaterialMeta.GetMaterialString = MaterialMeta.GetString
MaterialMeta.SetMaterialString = MaterialMeta.SetString

local oldsetmat = render.SetMaterial
function render.SetMaterial(mat,...)
	mat:SetMaterialInt("$spriterendermode",9)

	return oldsetmat(mat,...)
end

local EntityMeta = FindMetaTable("Entity")
local oldsetscale = EntityMeta.SetModelScale
function EntityMeta.SetModelScale(self, arg1, arg2)
	if arg2 == nil then arg2 = 0 end
	if type(arg1) == "Vector" then
		local scale = arg1
		local count = self:GetBoneCount() or -1
		if count > 1 then
			for i=0, count do
				self:ManipulateBoneScale(i, scale)
			end
		elseif self.EnableMatrix then
			local mat = Matrix()
			mat:Scale(Vector(scale.y,scale.x,scale.z)) -- Note: We're swapping X and Y because RenderMultiply isn't consistant with the rest of source
			self:EnableMatrix("RenderMultiply", mat)
		else
			-- Some entities, like ragdolls, cannot be resized with EnableMatrix, so lets average the three components to get a float
			oldsetscale(self,(scale.x+scale.y+scale.z)/3, arg2)
		end

		local propmax = self:OBBMaxs()
		local propmin = self:OBBMins()
		self:SetRenderBounds(Vector(scale.x*propmax.x, scale.y*propmax.y, scale.z*propmax.z), Vector(scale.x*propmin.x, scale.y*propmin.y, scale.z*propmin.z))
	elseif type(arg1) == "number" then
		return oldsetscale(self, arg1, arg2)
	else
		Error("SetModelScale - Expected number or vector, got "..type(arg1).."\n")
	end
end

local PlayerMeta = FindMetaTable("Player")
local OldGetInfoNum = PlayerMeta.GetInfoNum
function PlayerMeta.GetInfoNum(self, arg1, arg2)
	if arg2 == nil then arg2 = 0 end
	return OldGetInfoNum(self, arg1, arg2)
end

-- FIXME: Temporarily disabled scaling for sizes below 1440p
-- To be re-enabled when someone on a 720p screen can check this.
-- May need to implement a DPI scaling curve.
function SC2ScreenScale(N)
    return math.max(N * (ScrH() / 1440), N)
end

--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

include('shared.lua')
include('cl_spawnmenu.lua')
include('cl_hints.lua')
include('cl_worldtips.lua')
include('cl_search_models.lua')
include('gui/IconEditor.lua')
include('gui/inventoryinfopanel.lua')
include('gui/inventorypanel.lua')
include('gui/inventoryitem.lua')
--
-- Make BaseClass available
--
DEFINE_BASECLASS("gamemode_base")


local physgun_halo = CreateConVar("physgun_halo", "1", { FCVAR_ARCHIVE }, "Draw the physics gun halo?")

function GM:LimitHit(name)
	self:AddNotify("#SBoxLimit_"..name, NOTIFY_ERROR, 6)
	surface.PlaySound("buttons/button10.wav")
end

function GM:OnUndo(name, strCustomString)
	if not strCustomString then
		self:AddNotify("#Undone_"..name, NOTIFY_UNDO, 2)
	else
		self:AddNotify(strCustomString, NOTIFY_UNDO, 2)
	end

	-- Find a better sound :X
	surface.PlaySound("buttons/button15.wav")
end

function GM:OnCleanup(name)
	self:AddNotify("#Cleaned_"..name, NOTIFY_CLEANUP, 5)

	-- Find a better sound :X
	surface.PlaySound("buttons/button15.wav")
end

function GM:UnfrozeObjects(num)
	self:AddNotify("Unfroze "..num.." Objects", NOTIFY_GENERIC, 3)

	-- Find a better sound :X
	surface.PlaySound("npc/roller/mine/rmine_chirp_answer1.wav")
end

--[[---------------------------------------------------------
	Draws on top of VGUI..
-----------------------------------------------------------]]
function GM:PostRenderVGUI()
	BaseClass.PostRenderVGUI(self)
end

local PhysgunHalos = {}

--[[---------------------------------------------------------
   Name: gamemode:DrawPhysgunBeam()
   Desc: Return false to override completely
-----------------------------------------------------------]]
function GM:DrawPhysgunBeam(ply, weapon, bOn, target, boneid, pos)
	if (physgun_halo:GetInt() == 0) then return true end

	if (IsValid(target)) then
		PhysgunHalos[ ply ] = target
	end

	return true
end

hook.Add("PreDrawHalos", "AddPhysgunHalos", function()
	if not PhysgunHalos or table.Count(PhysgunHalos) == 0 then return end

	for k, v in pairs(PhysgunHalos) do
		if IsValid(k) then
		    local size = math.random(1, 2)
		    local colr = k:GetWeaponColor() + VectorRand() * 0.3

		    halo.Add(PhysgunHalos, Color(colr.x * 255, colr.y * 255, colr.z * 255), size, size, 1, true, false)
		end
	end

	PhysgunHalos = {}
end)


--[[---------------------------------------------------------
   Name: gamemode:NetworkEntityCreated()
   Desc: Entity is created over the network
-----------------------------------------------------------]]
function GM:NetworkEntityCreated(ent)
	--
	-- If the entity wants to use a spawn effect
	-- then create a propspawn effect if the entity was
	-- created within the last second (this function gets called
	-- on every entity when joining a server)
	--

	if ent:GetSpawnEffect() and ent:GetCreationTime() > (CurTime() - 1.0) then
		local ed = EffectData()
			ed:SetEntity(ent)
		util.Effect("propspawn", ed, true, true)
	end
end

function GM:AddNotify(str, type, length)
	notification.AddLegacy(str, type, length)
end

--Fonts
surface.CreateFont("dia_score", {font="coolvetica", size=20})
surface.CreateFont("TargetID2", {font="coolvetica", size=20})

--Includes
include("sb/cl_main.lua")
include("spacecombat2/sh_main.lua")
include("spacecombat2/cl_convar.lua")
include("spacecombat2/cl_config.lua")
include("cl_help.lua")
include("cl_quiz.lua")
include("shared.lua")
include("chat/cl_chat.lua")
include("diaspora.lua")
include("admin/shared.lua")
include("maprotate/client/cl_maptimer.lua")

function GM:InitPostEntity()
	self.BaseClass.InitPostEntity(self)


end

--Custom skin
function GM:ForceDermaSkin()
	return "Diaspora"
end

function GM:HUDShouldDraw(name)
	for k, v in pairs({"CHudHealth", "CHudBattery", "CHudAmmo", "CHudSecondaryAmmo", "CHudChat"}) do
		if name == v then return false end
	end
	return true -- Otherwise all the hud elements go bye bye :C
end

if WireLib and WireGPU_AddMonitor then
    WireGPU_AddMonitor("SBEP Console Big", "models/slyfo/consolescreenbig.mdl", 0.25, 0, 0, 0.05, -18.02, 18.02, -12.414, 13.109)
    WireGPU_AddMonitor("SBEP Console Medium", "models/slyfo/consolescreenmed.mdl", 0.15, 0.06, 0, 0.038, -13.95, 13.95, -9.505, 10.25)
    WireGPU_AddMonitor("SBEP Console Small", "models/slyfo/consolescreensmall.mdl", 0.1, 0.06, 0, 0.0288, -13.95, 13.95, -9.505, 10.25)
    WireGPU_AddMonitor("Combine Monitor", "models/props_combine/combine_monitorbay.mdl", -3, 4.25, -3.5, 0.115, -30.066, 36.884, -25, 30)
    WireGPU_AddMonitor("SBEP Computer Screen", "models/sbep_community/errject_smbwallcons.mdl", -41.9, 29.5, -2, 0.08, -30.531, 33.037, 9.777, 49.460, Angle(0, 90, 90))
    WireGPU_AddMonitor("SBEP Console Stand", "models/sbep_community/d12consolert.mdl", -20.75, 79, 0, 0.05, -27.85, 27.846, 66.407, 91.815, Angle(0, 90, 90))
    WireGPU_AddMonitor("Security Screen", "models/props_lab/securitybank.mdl", 13, 76.5, -10.25, 0.04, -4.134, 24.364, 66.975, 86.059, Angle(0, 90, 90))
    WireGPU_AddMonitor("TV Screen", "models/props_c17/tv_monitor01.mdl", 7, 0.5, 1.75, 0.023, -9.344, 5.691, -4.973, 6.232)
    WireGPU_AddMonitor("Tiberium Monitor", "models/tiberium/dispenser.mdl", 9, 13.65, 8.65, 0.026, -4, 4, -3, 3, Angle(0, 90, 90))


    list.Set("WireScreenModels", "models/props_lab/workspace002.mdl", true)
    list.Set("WireScreenModels", "models/slyfo/consolescreenbig.mdl", true)
    list.Set("WireScreenModels", "models/slyfo/consolescreenmed.mdl", true)
    list.Set("WireScreenModels", "models/slyfo/consolescreensmall.mdl", true)
    list.Set("WireScreenModels", "models/props_combine/combine_monitorbay.mdl", true)
    list.Set("WireScreenModels", "models/sbep_community/errject_smbwallcons.mdl", true)
    list.Set("WireScreenModels", "models/sbep_community/d12consolert.mdl", true)
    list.Set("WireScreenModels", "models/props_lab/securitybank.mdl", true)
    list.Set("WireScreenModels", "models/props_c17/tv_monitor01.mdl", true)
    list.Set("WireScreenModels", "models/tiberium/dispenser.mdl", true)
end

--[[---------------------------------------------------------
	Name: CalcVehicleView
-----------------------------------------------------------]]
function GM:CalcVehicleView(Vehicle, ply, view)
	if Vehicle.GetThirdPersonMode == nil or ply:GetViewEntity() ~= ply then
		-- This hsouldn't ever happen.
		return
	end

	-- If we're not in third person mode - then get outa here stalker
	if not Vehicle:GetThirdPersonMode() then return view end

	-- Don't roll the camera
	-- view.angles.roll = 0

	local mn, mx = Vehicle:GetRenderBounds()
	local radius = (mn - mx):Length()
	local radius = radius + radius * Vehicle:GetCameraDistance()

	-- Trace back from the original eye position, so we don't clip through walls/objects
	local TargetOrigin = view.origin + (view.angles:Forward() * -radius)
	local WallOffset = 4

	local tr = util.TraceHull({
		start = view.origin,
		endpos = TargetOrigin,
		filter = function(e)
			return e:IsWorld()
		end,
		mins = Vector(-WallOffset, -WallOffset, -WallOffset),
		maxs = Vector(WallOffset, WallOffset, WallOffset),
	})

	view.origin = tr.HitPos
	view.drawviewer = true

	-- If the trace hit something, put the camera there.
	if tr.Hit and not tr.StartSolid then
		view.origin = view.origin + tr.HitNormal * WallOffset
	end

	return view
end