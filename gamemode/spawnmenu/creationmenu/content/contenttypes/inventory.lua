--[[
	Space Combat Player Inventory - Q Menu Tab Container
	Author: Steve "Steeveeo" Green

	Note: This is just here so we can have an inventory panel in the
	Q menu. The InventoryPanel object is for use in any player-accessible
	inventory and is defined elsewhere.
]]--

spawnmenu.AddCreationTab( "#spawnmenu.category.inventory", function()

	local ctrl = vgui.Create( "InventoryPanel" )
	return ctrl

end, "icon16/picture.png", 100 )