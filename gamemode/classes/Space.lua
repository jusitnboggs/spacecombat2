local GM = GM

local BaseClass = GM.class.getClass("Environment")
local C = BaseClass:extends({
	name = "Space",
	gravity = 1/math.pow(10,30),
	atmosphere = 0,
	temperature = 14,
	pressure = 0,
	radius = 0,
	resources = {},
	entities = {},
    children = {},
	celestial = nil,
	habitable = false
})

function C:init()
    BaseClass.init(self, "Space", "space", 0, 1/math.pow(10,30), 14, 0, {})

    self:SetContainerSize("Gas", 0)
    self:SetContainerSize("Liquid", 0)
    self:SetContainerSize("Cargo", 0)
end

function C:updateEntities()
    local Entities = self:GetEntities()
    local ToRemove = {}
	for i,v in pairs(Entities) do
        if IsValid(v) then
            if v.GetEnvironment and v:GetEnvironment() ~= self then
				self:RemoveEntity(v)
			end
            self:updateEntity(v)
        else
            table.insert(ToRemove, i)
		end
    end

    for _, Bad in ipairs(ToRemove) do
        Entities[Bad] = nil
    end
end

function C:ContainsPosition(pos)
    return true --Space contains all positions, and is the root node of the environment graph
end

function C:SetHabitable(bool)
	self.habitable = bool

	self:SetName(bool and "Habitable Atmosphere" or "Space")
	self:SetGravity(bool and 1 or (1/math.pow(10,30)))
	self:SetTemperature(bool and 300 or 14)
	self:SetAtmosphere(bool and 1 or 0)
	self:SetRadius(0)
	self:SetPressure(self:GetGravity() * self:GetAtmosphere())
    self:SetNoclip(bool)
end

function C:GetHabitable()
	return self.habitable
end

function C:IsHostile()
    if not self.habitable then return self:super('IsHostile') end

	return true, 0
end

function C:GetPlayerSpeedModifier(ply)
	if self.habitable then return 1 end
	return 1/1e30
end

function C:GetNoclip(ply)
	local adminnoclip = GetConVar("SB_AdminSpaceNoclip"):GetBool()
    local planetonly = GetConVar("SB_PlanetNoClipOnly"):GetBool()

    return self.habitable or not planetonly or (adminnoclip and ply:IsAdmin())
end

GM.class.registerClass("Space", C)
