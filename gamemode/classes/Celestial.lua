-- Some base celestial stuff in here, mainly to do with ent tying in.
local GM = GM

local C = GM.LCS.class({
	env = nil,
	ent = nil
})

function C:init(ent, env)
	self:setEntity(ent or self:getEntity() )
	self:SetEnvironment(env or self:GetEnvironment() )
end

function C:getEntity()
	return self.ent
end

function C:setEntity( ent )
	if ent ~= nil and IsEntity( ent ) then
		if not ent.GetCelestial or ent:GetCelestial() ~= self then
			local this = self
			ent.GetCelestial = function()
				return this
			end
		end

		self.ent = ent
	else
		return false
	end
end

function C:GetEnvironment()
	return self.env
end

function C:SetEnvironment( env )
	if env ~= nil and env.is_A and env:is_A( GM.class.getClass("Environment")) then
		env:SetCelestial(self)
		self.env = env
	else
		return false
	end
end

function C:Think()
	self:GetEnvironment():Think()
end

GM.class.registerClass("Celestial", C)
