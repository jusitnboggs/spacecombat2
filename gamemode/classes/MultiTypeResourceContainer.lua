local GM = GM
local C = GM.LCS.class({
    Containers = {}
})

function C:init(ContainerTypes, ContainerStorageModes)
    self.Containers = {}

    if ContainerTypes then
        for Index, ContainerType in ipairs(ContainerTypes) do
            local ContainerMode
            if type(ContainerStorageModes) == "table" then
                ContainerMode = ContainerStorageModes[Index]
            elseif type(ContainerStorageModes) == "number" then
                ContainerMode = ContainerStorageModes
            else
                ContainerMode = CONTAINERMODE_DEFAULT
            end

            local Container = GM:NewStorage(ContainerType, 0, ContainerMode)
            self.Containers[ContainerType] = Container
        end
    end
end

function C:GetContainers()
    return self.Containers
end

function C:GetContainerOfType(Type)
    return self.Containers[Type]
end

function C:GetContainerForResource(Resource)
    if type(Resource) == "string" then
        Resource = GM:GetResourceFromName(Resource)
    end

    return self.Containers[Resource:GetType()]
end

function C:SetContainerSize(ContainerType, Size)
    local Container = self.Containers[ContainerType]
    if not Container then
        return false
    end

    return Container:Resize(Size)
end

function C:GetResource(Resource)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return
    end

    return Container:GetResource(Resource)
end

function C:GetAmount(Resource)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return 0
    end

    return Container:GetAmount(Resource)
end

function C:SetAmount(Resource, Amount)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:SetAmount(Resource, Amount)
end

function C:GetMaxAmount(Resource)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return 0
    end

    return Container:GetMaxAmount(Resource)
end

function C:SetMaxAmount(Resource, Amount, Force)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:SetMaxAmount(Resource, Amount, Force)
end

function C:IncreaseMaxAmount(Resource, Amount)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:IncreaseMaxAmount(Resource, Amount)
end

function C:DecreaseMaxAmount(Resource, Amount)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:DecreaseMaxAmount(Resource, Amount)
end

function C:SupplyResource(Resource, Amount)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:SupplyResource(Resource, Amount)
end

function C:ConsumeResource(Resource, Amount, Force)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:ConsumeResource(Resource, Amount, Force)
end

function C:AddResourceType(Resource, Max)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:AddResourceType(Resource, Max)
end

function C:RemoveResourceType(Resource)
    local Container = self:GetContainerForResource(Resource)
    if not Container then
        return false
    end

    return Container:RemoveResourceType(Resource)
end

function C:MergeStorage(Storage)
    -- TODO: Implement
    return false
end

function C:SplitStorage(Storage)
    -- TODO: Implement
    return false
end

function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteUInt(table.Count(self.Containers), 8)
    for i,k in pairs(self.Containers) do
        net.WriteUInt(GAMEMODE:GetStorageTypeID(i), 12)
        k:WriteCreationPacket()
    end
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    local ContainerCount = net.ReadUInt(8)
    if ContainerCount > 0 then
        self.Containers = {}
        for I = 1, ContainerCount do
            local ContainerType = GAMEMODE:GetStorageTypeFromID(net.ReadUInt(12))
            local NewContainer = GAMEMODE.class.getClass("ResourceContainer"):new()
            NewContainer:SetType(ContainerType)
            NewContainer:ReadCreationPacket()

            self.Containers[ContainerType] = NewContainer
        end
    end
end

function C:Serialize()
    -- TODO: Implement
    return {}
end

function C:DeSerialize(Data)
    -- TODO: Implement
end


GM.class.registerClass("MultiTypeResourceContainer", C)
