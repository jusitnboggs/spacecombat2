-- For resources such as Oxygen, Nitrogen, Carbon Dioxide etc
local GM = GM

local C = GM.LCS.class({
	name = "FAIL",
	amount = 0,
	max = 0,
    size = 1,
    type = "FAIL",
    id = -1,
    rarity = ITEM_RARITY_COMMON
})

function C:init(name, size, type, amount, max, id, default, rarity)
	self:SetName(name or self:GetName())
    self:SetMaxAmount(max or self:GetMaxAmount())
	self:SetAmount(amount or self:GetAmount())
    self:SetSize(size or self:GetSize())
    self:SetType(type or self:GetType())
    self:SetID(id or self:GetID(), default)
    self.rarity = rarity or ITEM_RARITY_COMMON
end

function C:GetName()
	return self.name
end

function C:SetName( n )
	self.name = n
end

function C:GetType()
	return self.type
end

function C:SetType( n )
	self.type = n
end

function C:GetID()
	return self.id
end

function C:GetRarity()
    return self.rarity
end

function C:SetID(n, IsDefault)
    if type(n) ~= "number" then
        SC.Error("Resource.SetID: Bad resource ID was set "..tostring(n), 5)
        return
    end

    self.id = n or -1

    if n > 0 and not IsDefault then
        local DefaultResource = GM:GetResourceFromID(n, true)
        if DefaultResource then
            self:SetName(DefaultResource:GetName())
            self:SetType(DefaultResource:GetType())
            self:SetSize(DefaultResource:GetSize())
            self.rarity = DefaultResource:GetRarity()
        else
            SC.Error("Resource.SetID: Bad resource ID was set "..tostring(n), 5)
        end
    end
end

function C:GetSize()
	return self.size
end

function C:SetSize( n )
	self.size = n
end

function C:GetAmount()
	return self.amount
end

function C:SetAmount( a )
	self.amount = math.Clamp(a, 0, self:GetMaxAmount())
	return
end

function C:SupplyResource(amount)
    self:SetAmount(self:GetAmount() + amount)
    return true
end

function C:ConsumeResource(amount, force)
    if self:GetAmount() < amount and not force then return false end
    self:SetAmount(self:GetAmount() - amount)
    return true
end

function C:GetMaxAmount()
	return self.max
end

function C:SetMaxAmount(NewMax)
    self.max = NewMax

    if self:GetAmount() > NewMax then
        self:SetAmount(NewMax)
    end
end

function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteInt(self.id, 16)
    net.WriteFloat(self.max)
    net.WriteFloat(self.amount)
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self.name = "FAIL"
    self.type = "FAIL"

    self:SetID(net.ReadInt(16))
    self.max = net.ReadFloat()
    self.amount = net.ReadFloat()

    self:init()
end

function C:Serialize()
    return {name = self.name, size = self.size, type = self.type, amount = self.amount, max = self.max}
end

function C:DeSerialize(data)
    self:init(data.name, data.size, data.type, data.amount, data.max)
end

GM.class.registerClass("Resource", C)