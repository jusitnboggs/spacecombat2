-- Class for the players suit, used for surviving in other environments

local GM = GM

local C = GM.LCS.class( {
    player = nil,-- Player assigned to the suit
    resources = { },-- Table of resources contained within the suit
    intake = nil,-- The resource required for respiration
    outtake = nil,-- The resource produced by respiration
	maxbreath = 30,--Seconds of air in lungs; player will be able to survive for this long without suit
	breath = 30
} )

function C:init(p, i, o)
    self:setPlayer(p or self:getPlayer())
    self:setIntake(i or GM:NewResource("Lifesupport Canister", 0, 900))
    self:setOuttake(o or GM:NewResource("Empty Canister", 45, 900))
end

function C:getPlayer()
    return self.player
end

function C:setPlayer(p)
    if IsValid(p) and p:IsPlayer() then
        self.player = p
    end
end

function C:getIntake()
    return self.intake
end

function C:setIntake(i)
    if i.is_A and i:is_A(GM.class.getClass("Resource")) then
        self.intake = i
        return
    end
    ErrorNoHalt("Expected Intake to be a Resource")
end

function C:getOuttake()
    return self.outtake
end

function C:setOuttake(o)
    if o.is_A and o:is_A(GM.class.getClass("Resource")) then
        self.outtake = o
        return
    end
    ErrorNoHalt("Expected Outtake to be a Resource")
end

function C:SetMaxBreath(m)
	self.maxbreath = math.max(0, m)
end

function C:GetMaxBreath()
	return self.maxbreath
end

function C:SetBreath(b)
	self.breath = math.Clamp(b, 0, self.maxbreath)
end

function C:GetBreath()
	return self.breath
end

function C:SupplyOuttake(amount)
    self:getOuttake():SupplyResource(amount)
end

function C:ConsumeOuttake(amount)
    self:getOuttake():ConsumeResource(amount)
end

function C:SupplyIntake(amount, override)
	--A breath of fresh...or not so fresh...I think it's air? I'm not so sure, but your lungs are now full of it
	self:SetBreath(self:GetMaxBreath())

	--Supplied from a device, like a dispenser
	if override then
		self:getIntake():SupplyResource(amount)
	--Supplied from the suit calcs (only supply what we can fill)
	else
		if self:getOuttake():GetAmount() > 0 and self:getIntake():GetAmount() < self:getIntake():GetMaxAmount() then
			self:getIntake():SupplyResource(math.min(amount, self:getOuttake():GetAmount()))
			self:ConsumeOuttake(amount)
		end
	end
end

function C:ConsumeIntake(amount)
    if self:getIntake():GetAmount() > amount then
        self:getIntake():ConsumeResource(amount)
        self:SupplyOuttake(amount)
    else
		--If the amount is 1, chances are we're just out of air or slightly cold or something, check breath first
		--TODO: Perhaps some sort of screenfade when running out of air?
		if amount <= 1 and self:GetBreath() > 0 then
			--If we're just underwater, full duration of breath, else kill within seconds to prevent planet-hopping without a suit.
			if self:getPlayer():WaterLevel() == 3 then
				self:SetBreath(self:GetBreath() - amount)
			else
				self:SetBreath(self:GetBreath() - amount * 10)
			end
		else
			local damage = DamageInfo()
			damage:SetDamage(5 * amount) --Scale damage based on environment hostility
			damage:SetDamageType(DMG_DROWN)
			damage:SetAttacker(self:getPlayer())
			damage:SetInflictor(self:getPlayer())

			self:getPlayer():TakeDamageInfo(damage)
		end
    end
end

GM.class.registerClass("PlayerSuit", C)