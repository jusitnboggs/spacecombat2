-- Class for environments which will be bound to celestial objects

local GM = GM
GM.AtmosphereTable = GM.AtmosphereTable or {}
GM.StarTable = GM.StarTable or {}
GM.EnvironmentTree = GM.EnvironmentTree or {}

local BaseClass = GM.class.getClass("MultiTypeResourceContainer")
local C = BaseClass:extends({
	name = "",
    type = "space",
	gravity = 0,
	atmosphere = 0,
    temperature = 0,
    basetemperature = 0,
	pressure = 0,
	radius = 0,
	noclip = true,
	entities = {},
    children = {},
    parent = nil,
	celestial = nil
})

function C:init(Name, Type, Radius, Gravity, Temperature, AtmosphereDensity, AtmosphereResources)
    self:SetName(Name)
    self:SetType(Type)
    self:SetRadius(Radius)
	self:SetGravity(Gravity)
	self:SetTemperature(Temperature)
	self:SetBaseTemperature(Temperature)
	self:SetAtmosphere(AtmosphereDensity)
    self:SetPressure(self:GetGravity() * self:GetAtmosphere())

    self.entities = {}
    self.children = {}

    BaseClass.init(self, {"Gas", "Liquid", "Cargo"}, CONTAINERMODE_AUTOENLARGE)

    if AtmosphereDensity > 0 then
        local Volume = self:GetVolume()
        local MaxStorage = Volume * Gravity * 0.00157
        local MaxAtmosphereStorage = MaxStorage * AtmosphereDensity

        self:SetContainerSize("Gas", MaxAtmosphereStorage)
        self:SetContainerSize("Liquid", MaxAtmosphereStorage)
        self:SetContainerSize("Cargo", MaxAtmosphereStorage)

        local Resources = {}
        local TotalAmount = 0
        for Name, Amount in pairs(AtmosphereResources) do
            TotalAmount = TotalAmount + Amount
            Resources[Name] = Amount
        end

        for Name, Amount in pairs(Resources) do
            if Amount > 0 then
                self:SupplyResource(Name, Amount / TotalAmount * MaxAtmosphereStorage)
            end
        end
    else
        if table.Count(AtmosphereResources) > 0 then
            SC.Error(Format("Someone added atmosphere resources to an environment with no atmosphere. Environment: %s", Name), 5)
        end
    end

    if self:GetType() == "planet" or self:GetType() == "cube" or self:GetType() == "custom" then
        GM.AtmosphereTable[#GM.AtmosphereTable + 1] = self
    end

    if self:GetType() == "star" then
        GM.StarTable[#GM.StarTable + 1] = self
    end
end

function C:GetName()
	return self.name
end

function C:describe()
	return self:GetName()
end

function C:GetType()
	return self.type
end

function C:SetType(n)
    if type(n) ~= "string" then error("Expected string, got"..type(n)) return end
	self.type = n
end

function C:SetName( n )
	if type(n) ~= "string" then error("Expected string, got"..type(n)) return end
	self.name = n
	return
end

function C:IsHostile()
    local hostile = false
    local intake = 0

    if (self:GetTemperature() < 261) or (self:GetTemperature() > 308) then
        hostile = true
        intake = intake + (math.abs(285 - self:GetTemperature()) * 0.005)
    end

    local oxygen = self:GetResource("Oxygen")
    local oxygenpercent = oxygen and (oxygen:GetAmount() / oxygen:GetMaxAmount()) or 0
    if oxygenpercent < 0.05 then
        hostile = true
        intake = intake + (0.25 * (5 / math.max(oxygenpercent * 100, 1)))
    end

    if self:GetPressure() > 1.5 then
        hostile = true
        intake = intake + (self:GetPressure() - 1.5)
    elseif self:GetPressure() < 0.5 then
        hostile = true
        intake = intake + ((1 - self:GetPressure()) / math.max(self:GetPressure(), 0.2))
    end

    return hostile, intake
end

function C:GetPlayerSpeedModifier(ply)
	local speedmod
	if self:GetGravity() > 1 then
		speedmod = math.Clamp(math.abs(5 - math.min(5, self:GetGravity() - 1)) / 5,0.3,1)
	else
		speedmod = math.Clamp(self:GetGravity(), 0.3, 1)
	end

	return speedmod
end

function C:updateEntity( e )
	-- This is where we set an ents gravity and physical vars

    if not e.HasGravityOverride then
	    e:SetGravity( math.max( self:GetGravity(), 0.000001 ) )
    end

	local phys = e:GetPhysicsObject()
	if IsValid(phys) and not e:IsPlayer() then
        if not e.HasGravityOverride then
		    if self:GetGravity() > 0.01 then
			    phys:EnableGravity( true )
		    else
			    phys:EnableGravity( false )
		    end
        end

        if not e.HasDragOverride then
		    if self:GetPressure() > 0.01 then
			    phys:EnableDrag( true )
		    else
			    phys:EnableDrag( false)
		    end
        end

        if not e.HasLifeSupportOverride then
            if self:GetPressure() > 2 then
                e:TakeDamage(self:GetPressure() * 50)
            end
        end
	end

	if e:IsPlayer() then
        if not e.HasGravityOverride then
            local speedmod = self:GetPlayerSpeedModifier(e)
            local WalkMod, RunMod = GM:GetPlayerSpeedModifier(e)

            GM:SetPlayerSpeed(e, 250 * speedmod * WalkMod, 500 * speedmod * RunMod)
			-- The .Scale member is provided by Player Resizer, doing it this way is temporary as we don't have a good way to override this remotely.
			GM:UpdatePlayerGravity(e, math.max(self:GetGravity() * math.max(e.Scale or 1, 0.2), 0.000001))
        end

        player_manager.RunClass(e, "HandleEnvironmentUpdate", self)

		if not self:GetNoclip(e) and e:GetMoveType() ~= MOVETYPE_WALK then
			e:SetMoveType(MOVETYPE_WALK)
		end
	end

end

function C:SetEnvironment( e, v )
	local this = v or GM:GetSpace()

    if e.GetEnvironment then
        if this == GM:GetSpace() then
            local parent = e:GetEnvironment():getParent()
			if parent then
				local parentcontains = parent:ContainsPosition(e:GetPos())
				if parentcontains then
					this = parent
					this = GM:CheckEnvironments(e:GetPos(), {[this] = this:getChildren()}) or this
				end
			end
        else
            this = GM:CheckEnvironments(e:GetPos(), {[this] = this:getChildren()}) or this
        end

        e:GetEnvironment():RemoveEntity(e)
    end

	e.GetEnvironment = function()
		return this
	end

    this:AddEntity(e)
end

function C:updateEntities()
    local Entities = self:GetEntities()
    local ToRemove = {}
	for i,v in pairs(Entities) do
		if IsValid(v) then
            self:updateEntity(v)
        else
            table.insert(ToRemove, i)
		end
    end

    for _, Bad in ipairs(ToRemove) do
        Entities[Bad] = nil
    end
end

function C:GetEntities()
	return self.entities
end

function C:HasEntity(o)
    if not IsValid(o) then return false end
	return self:GetEntities()[o:EntIndex()] ~= nil
end

function C:AddEntity(o)
    if not IsValid(o) then return end
	self:GetEntities()[o:EntIndex()] = o
end

function C:RemoveEntity(o)
    if not IsValid(o) then return end
	self:GetEntities()[o:EntIndex()] = nil
end

function C:GetCelestial()
	return self.celestial
end

function C:SetCelestial( p )
	if not p.is_A or not p:is_A( GM.class.getClass("Celestial") ) then error("Expected celestial to be a Celestial Object, got "..p:getClass() or type(p)) return end
	self.celestial = p
	return
end

function C:GetVolume()
	local radius = self:GetRadius()
	local volume = (4 / 3) * math.pi * (radius ^ 3)

	return volume
end


function C:GetGravity()
	return self.gravity
end

function C:SetGravity( g )
	if not tonumber(g) then error("Expected number, got "..type(g))return end
	self.gravity = tonumber(g)
	return
end

function C:GetTemperature()
	return self.temperature
end

function C:SetTemperature( t )
	if not tonumber(t) then error("Expected number, got "..type(t)) return end
	self.temperature = tonumber(t)
	return
end

function C:GetBaseTemperature()
	return self.basetemperature
end

function C:SetBaseTemperature( t )
	if not tonumber(t) then error("Expected number, got "..type(t)) return end
	self.basetemperature = tonumber(t)
	return
end

function C:GetAtmosphere()
	return self.atmosphere
end

function C:SetAtmosphere( a )
	if not tonumber(a) then error("Expected number, got "..type(a)) return end
	self.atmosphere = tonumber(a)
	return
end

function C:GetRadius()
	return self.radius
end

function C:SetRadius( r )
	if not tonumber(r) then error("Expected number, got "..type(r)) return end
	self.radius = tonumber(r)
	return
end

function C:GetPressure()
	return self.pressure
end

function C:SetPressure( p )
	if not tonumber(p) then error("Expected number, got "..type(p)) return end
	self.pressure =  tonumber(p)
	return
end

function C:setParent(e)
    self.parent = e or GM:GetSpace()
end

function C:getParent()
    return self.parent
end

function C:SetNoclip(bool)
    self.noclip = bool
end

function C:GetNoclip(ply)
    return self.noclip
end

function C:getChildren()
    return self.children
end

function C:setChildren(children)
    self.children = children or {}
end

local function CheckAABB(ent, pos)
    --Get the physics object, its needed to get the AABB vectors
    local phys = ent:GetPhysicsObject()

    --If we don't have one we obviously can't do this
    if not IsValid(phys) then return false end

    --Get our AABB vectors
    local Min, Max = ent:WorldSpaceAABB()

    --Check if its inside of us
    if not pos:WithinAABox(Min, Max) then return false end

    --Everything checks out, its inside of us
    return true
end

function C:ContainsPosition(pos)
    --If someone actually passes a nil pos, please rip their dick off
    if not pos then return false end

    --Make sure this is even something we can do with this environment
    if not self:GetCelestial() or not IsValid(self:GetCelestial():getEntity()) then return false end

    --Local variables to save space
    local ent = self:GetCelestial():getEntity()
    local shape = self:GetType()

    --Because there are multiple things that should be spheres
    if shape ~= "cube" and shape ~= "custom" then
      shape = "sphere"
    end

    --Check AABB first, if its not within this its definitely not in the environment
    if not CheckAABB(ent, pos) then return false end

    --Different shapes have different checks, spheres and cubes are easy, but custom shapes can be more involved
    if shape == "sphere" then
        return pos:DistToSqr(ent:GetPos()) < (self:GetRadius() ^ 2)
    elseif shape == "custom" then
        --TODO: Implement this, nothing uses it yet but it would be a wonderful feature for new maps to be able to use
        --Potentially able to find some good ways of doing it here: http://www.yaldex.com/game-programming/0131020099_ch22lev1sec1.html
        return false --Until this is implemented return false because we really don't know
    end --We don't need a shape check for cubes, the AABB check covers it

    --It was inside of us, return true
    return true
end

function C:Think()
	self:updateEntities()
end

GM.class.registerClass("Environment", C)
