local GM = GM
local BaseClass = GM.class.getClass("ProjectileComponent")
local C = BaseClass:extends({
    LastLocation = Vector(0, 0, 0)
})

local ZeroVector = Vector(0, 0, 0)

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if IsValid(self:GetParent()) then
        self:GetParent():SetCollisionComponent(self)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return true
end

function C:Think()
    local Parent = self:GetParent()
    if self.LastLocation ~= ZeroVector then
        local Data = {}
		Data.start = self.LastLocation
		Data.endpos = Parent:GetPos()
		local Trace = util.TraceLine(Data)

        if Trace.Hit then
            Parent:SetPos(Trace.HitPos - (Trace.Normal * 50))
            Parent:FireEvent("Collision", Trace)
            Parent:Remove()
        end
    end

    self.LastLocation = Parent:GetPos()
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "CollisionComponent"
end

-- Make sure you register your class so it can be created!
GM.class.registerClass("CollisionComponent", C)