local GM = GM
local C = GM.class.getClass("ProjectileComponent"):extends({
    Length = 0,
    LastHitLength = 0,
    UpdateRate = 0.1,
    NextThink = 0,
    Pulse = false,
    HasChecked = false
})

local BaseClass = C:getClass()

function C:init(Length, UpdateRate, Pulse)
    self.Length = Length or 0
    self.UpdateRate = UpdateRate or 0.1
    self.Pulse = Pulse or false
end

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if IsValid(self:GetParent()) then
        self:GetParent():SetCollisionComponent(self)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return CurTime() >= self.NextThink and not (self.Pulse and self.HasChecked)
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return true
end

function C:Think()
    local Parent = self:GetParent()
    local Data = {}
    Data.start = Parent:GetPos()
    Data.endpos = Parent:GetPos() + (Parent:GetAngles():Forward() * (self.Length > 0 and self.Length or 40000))
    Data.filter = {Parent:GetLauncherEntity()}
    local Trace = util.TraceLine(Data)

    if Trace.Hit then
        self.LastHitLength = Trace.Fraction * self.Length
        Parent:FireEvent("Collision", Trace)
    else
        self.LastHitLength = self.Length
    end

    self.HasChecked = true
    self.NextThink = CurTime() + self.UpdateRate
end

function C:Serialize()
    local Data = BaseClass.Serialize(self) or {}

    Data.Length = self.Length
    Data.UpdateRate = self.UpdateRate
    Data.Pulse = self.Pulse

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.Length = Data.Length or 0
    self.UpdateRate = Data.UpdateRate or 0.1
    self.Pulse = Data.Pulse or false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "BeamCollisionComponent"
end

-- Make sure you register your class so it can be created!
GM.class.registerClass("BeamCollisionComponent", C)