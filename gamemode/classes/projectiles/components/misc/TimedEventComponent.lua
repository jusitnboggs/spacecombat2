local C = GM.class.getClass("ProjectileComponent"):extends({
    EventName = "FAIL",
    EventData = {},
    Networked = false,
    Delay = 0,
    NextTrigger = 0,
    Triggered = false
})

local BaseClass = C:getClass()

function C:init(Name, Data, Delay, Networked)
    self.EventName = Name or "Fail"
    self.EventData = Data or {}
    self.Networked = Networked or false
    self.Delay = Delay or 10
    self.NextTrigger = self.Delay + CurTime()

    BaseClass.init(self)
end

function C:ResetTimer()
    self.NextTrigger = self.Delay + CurTime()
    self.Triggered = false
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return not self.Triggered
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return true
end

function C:Think()
    if not self.Triggered and self.NextTrigger <= CurTime() then
        self.Triggered = true
        if self.Networked then
            self:GetParent():FireNetworkedEvent(self.EventName, self.EventData)
        else
            self:GetParent():FireEvent(self.EventName, self.EventData)
        end
    end
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.EventName = self.EventName
    Data.EventData = self.EventData
    Data.Networked = self.Networked
    Data.Delay = self.Delay

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.EventName = Data.EventName or "Event"
    self.EventData = Data.EventData or {}
    self.Networked = Data.Networked or false
    self.Delay = Data.Delay or 10
    self.NextTrigger = self.Delay + CurTime()
    self.Triggered = false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "TimedEventComponent"
end

GM.class.registerClass("TimedEventComponent", C)