local C = GM.class.getClass("ProjectileComponent"):extends({
    Target = NULL
})

local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("TargetingComponent")

function C:init(Target)
    BaseClass.init(self)

    self.Target = Target
end

function C:OnProjectileEvent(Event, Info)
    if Event == "SetTargetEntity" and IsValid(Info.Target) then
        self.Target = Info.Target
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if self:GetParent() ~= nil then
        self:GetParent():SetTargetingComponent(self)
    end
end

function C:SetTarget(NewTarget)
    if IsValid(NewTarget) then
        self.Target = NewTarget

        if SERVER and IsValid(self:GetParent()) then
            self:GetParent():FireNetworkedEvent("SetTargetEntity", {Target=NewTarget})
        end
    end
end

function C:GetTarget()
    return self.Target
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

function C:Think()
    if IsValid(self.Target) then
        self:GetParent():FireEvent("SetTargetPosition", {Target=self.Target:GetPos()})
    end

    BaseClass.Think(self)
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "TargetingComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteEntity(self.Target)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.Target = net.ReadEntity()
end

GM.class.registerClass("TargetingComponent", C)