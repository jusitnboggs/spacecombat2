MININGLEVEL_COMMON = 1
MININGLEVEL_UNCOMMON = 2
MININGLEVEL_RARE = 3
MININGLEVEL_VERYRARE = 4
MININGLEVEL_LEGENDARY = 5

local EfficiencyLossPerMiningLevel = 0.5

local C = GM.class.getClass("DamageComponent"):extends({
    HasCollectionUpgrade = true,
    Efficiency = 0.7,
    InAtmosphereEfficiency = 0.7,
    MiningSpeed = 1.0,
    MiningLevel = MININGLEVEL_COMMON
})

local BaseClass = C:getClass()

function C:init(Damage)

    BaseClass.init(self, Damage)
end

function C:CalculateMiningEfficiency(MaterialMiningLevel, InAtmosphere)
    local Efficiency = self.Efficiency
    local MiningLevelDifference = math.abs(self.MiningLevel - MaterialMiningLevel)

    if MiningLevelDifference > 0 then
        for I=1, MiningLevelDifference do
            Efficiency = Efficiency * EfficiencyLossPerMiningLevel
        end
    end

    if InAtmosphere then
        Efficiency = Efficiency * self.InAtmosphereEfficiency
    end

    return Efficiency
end

function C:DealDamage(Target, HitData)
    local Parent = self:GetParent()
    local Launcher = Parent:GetLauncher()
    if Target.OnMined then
        if self.HasCollectionUpgrade then
            local ResourcesCollected = Target:OnMined(self.MiningSpeed)
            -- TODO: Implement InAtmosphere check
            local Efficiency = self:CalculateMiningEfficiency(Target:GetMiningLevel(), false)

            for _, Resource in ipairs(ResourcesCollected) do
                Resource:SetMaxAmount(Resource:GetMaxAmount() * Efficiency)

                -- FIXME: Update when Supply/Consume resource change to using objects
                if IsValid(Launcher) and IsValid(Launcher:GetCore()) then
                    Launcher:GetCore():SupplyResource(Resource:GetName(), Resource:GetAmount())
                end
            end
        else
            -- TODO: Implement resource rocks dropping
        end
    else
        BaseClass.DealDamage(self, Target, HitData)
    end
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.HasCollectionUpgrade = self.HasCollectionUpgrade
    Data.Efficiency = self.Efficiency
    Data.InAtmosphereEfficiency = self.InAtmosphereEfficiency
    Data.MiningSpeed = self.MiningSpeed
    Data.MiningLevel = self.MiningLevel

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.HasCollectionUpgrade = Data.HasCollectionUpgrade or false
    self.Efficiency = Data.Efficiency or 0.7
    self.InAtmosphereEfficiency = Data.InAtmosphereEfficiency or 0.7
    self.MiningSpeed = Data.MiningSpeed or 1.0
    self.MiningLevel = Data.MiningLevel or MININGLEVEL_COMMON
end


-- This should always return the name of the class!
function C:GetComponentClass()
    return "MiningComponent"
end

GM.class.registerClass("MiningComponent", C)