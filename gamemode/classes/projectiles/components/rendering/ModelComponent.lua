local GM = GM
local C = GM.class.getClass("ProjectileComponent"):extends({
    Model = "",
    ModelEntity = NULL,
    Offset = Vector(0,0,0),
    AngleOffset = Angle(0,0,0),
    Scale = 1
})
local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("ModelComponent")

function C:init(Model)
    self.Model = Model or ""

    if CLIENT then
        if IsValid(self.ModelEntity) then
            self.ModelEntity:Remove()
        end

        self.ModelEntity = ClientsideModel(self.Model, RENDERGROUP_OPAQUE)
    end
end

function C:GetModel()
    return self.Model
end

function C:SetModel(NewModel)
    self.Model = NewModel or ""

    if CLIENT then
        if IsValid(self.ModelEntity) then
            self.ModelEntity:Remove()
        end

        self.ModelEntity = ClientsideModel(self.Model, RENDERGROUP_OPAQUE)
        self.ModelEntity:SetModelScale(self.Scale)
    else
        self:GetParent():FireNetworkedEvent("ModelComponent.SetModel", {NewModel=self.Model})
    end
end

function C:GetScale()
    return self.Scale
end

function C:SetScale(NewScale)
    self.Scale = NewScale

    if CLIENT then
        if not IsValid(self.ModelEntity) then return end
        self.ModelEntity:SetModelScale(self.Scale)
    else
        self:GetParent():FireNetworkedEvent("ModelComponent.SetScale", {Scale=self.Scale})
    end
end

function C:GetOffset()
    return self.Offset
end

function C:SetOffset(NewOffset)
    self.Offset = NewOffset

    if SERVER then
        self:GetParent():FireNetworkedEvent("ModelComponent.SetOffset", {Offset=self.Offset})
    end
end

function C:GetAngleOffset()
    return self.AngleOffset
end

function C:SetAngleOffset(NewAngleOffset)
    self.AngleOffset = NewAngleOffset

    if SERVER then
        self:GetParent():FireNetworkedEvent("ModelComponent.SetAngleOffset", {Offset=self.AngleOffset})
    end
end

function C:OnProjectileEvent(Event, Info)
    if SERVER then return end
    if Event == "ModelComponent.SetModel" then
        self:SetModel(Info.NewModel)
    elseif Event == "Removed" then
        if IsValid(self.ModelEntity) then
            self.ModelEntity:Remove()
        end
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return true
end

-- If this returns true then the C:Draw function will be called. If it returns false, then it won't get called.
function C:ShouldDraw()
    return true
end

function C:Think()
    if SERVER or not IsValid(self.ModelEntity) then return end
    local Pos, Ang = LocalToWorld(self:GetOffset(),  self:GetAngleOffset(), self:GetParent():GetPos(), self:GetParent():GetAngles())
    self.ModelEntity:SetPos(Pos)
    self.ModelEntity:SetAngles(Ang)
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "ModelComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteVector(self.Offset)
    net.WriteAngle(self.AngleOffset)
    net.WriteFloat(self.Scale)

    -- This will cause the Model to be cached as a network string for later
    SC.WriteNetworkedString(self.Model)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self:SetOffset(net.ReadVector())
    self:SetAngleOffset(net.ReadAngle())
    self:SetScale(net.ReadFloat())

    -- Read the cached Model string
    self:SetModel(SC.ReadNetworkedString("", function(ID, String)
        if self then
            self:SetModel(String)
        end
    end))
end

function C:Serialize()
    -- Call the Serialize function on the base class first!
    local Out = BaseClass.Serialize(self)

    Out.Model = self.Model
    Out.Offset = self.Offset
    Out.AngleOffset = self.AngleOffset
    Out.Scale = self.Scale

    return Out
end

function C:DeSerialize(Data)
    -- Call the DeSerialize function from the base class first!
    BaseClass.DeSerialize(self, Data)
    self:SetModel(Data.Model)
    self:SetOffset(Data.Offset or Vector(0,0,0))
    self:SetAngleOffset(Data.AngleOffset or Angle(0,0,0))
    self:SetScale(Data.Scale or 1)
end

-- Make sure you register your class so it can be created!
GM.class.registerClass("ModelComponent", C)