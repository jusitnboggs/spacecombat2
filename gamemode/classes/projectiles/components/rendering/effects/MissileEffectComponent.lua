local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({})
local Shaft = Material("effects/ar2ground2");

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function C:Think()
	local Parent = self:GetParent()
    local particle = self:GetEmitter():Add("particles/flamelet"..math.random(1,5), Parent:GetPos() + (Parent:GetAngles():Forward() * -10))
	if particle then
		particle:SetVelocity(Parent:GetAngles():Forward() * -math.random(50 * self:GetScale()))
		particle:SetLifeTime(0)
		particle:SetDieTime(0.25 * self:GetScale())
		particle:SetStartAlpha(math.Rand(230, 255))
		particle:SetEndAlpha(0)
		particle:SetStartSize(27 * self:GetScale())
		particle:SetEndSize(0)
		particle:SetRoll(math.Rand(0, 360))
		particle:SetRollDelta(math.Rand(-10, 10))
		particle:SetColor(255 , 255 , 255)
	end

    local smoke = self:GetEmitter():Add("particles/smokey", Parent:GetPos())
	if smoke then
		smoke:SetPos(Parent:GetPos() + Parent:GetAngles():Forward() * -10 * self:GetScale())
		smoke:SetVelocity(Parent:GetAngles():Forward() * -math.random(50 * self:GetScale()))
		smoke:SetLifeTime(0)
		smoke:SetDieTime(0.5 * self:GetScale())
		smoke:SetStartAlpha(math.random(30) + 30)
		smoke:SetEndAlpha(0)
		smoke:SetStartSize((math.random(10) + 15) * self:GetScale())
		smoke:SetEndSize((math.random(15) + 45) * self:GetScale())
		smoke:SetRoll(math.random(359) + 1)
		smoke:SetRollDelta(math.random(-2, 2))
		smoke:SetColor(175, 175, 175)
	end
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function C:Draw()
    local Parent = self:GetParent()
	render.SetMaterial(Shaft)
	render.DrawBeam(Parent:GetPos(),Parent:GetPos() + (Parent:GetAngles():Forward() * (-250 * self:GetScale())),20,1,0,Color(255,180,180,255))
end

function C:GetComponentClass()
    return "MissileEffectComponent"
end
GM.class.registerClass("MissileEffectComponent", C)

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("MissileEffectComponent")