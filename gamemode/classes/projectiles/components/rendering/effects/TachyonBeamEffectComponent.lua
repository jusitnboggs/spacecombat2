local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Length = 0,
    MaxLength = 64000,
    NextShockEmit = 0,
	ShockEmitDelay = 0.1,
    NextFlareEmit = 0,
	FlareEmitDelay = 0.1
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("TachyonBeamEffectComponent")
local ZeroAngle = Angle(0, 0, 0)

local function ParentedParticle(p)
	if p.OffsetSpeed then
		local offsetSpeed = p.OffsetSpeed * FrameTime()
		p.Offset = p.Offset + offsetSpeed
	end

    local Pos, Ang = LocalToWorld(p.Offset, ZeroAngle, p.Parent:GetParent():GetPos(), p.Parent:GetParent():GetAngles())
	p:SetPos(Pos)
	p:SetNextThink(CurTime())
end

function C:init(Parent)
    BaseClass.init(self, Parent)

    self.NextShockEmit = CurTime()
    self.ShockEmitDelay = 0.1
    self.NextFlareEmit = CurTime()
    self.FlareEmitDelay = 0.1
end

local BeamMat
if CLIENT then
    BeamMat = GM.MaterialFromVMT(
    "sc_tachyonbeam",
    [["UnlitTwoTexture"

    {
        "$baseTexture"  "effects/mining_beam/laser_hi"
        "$Texture2" "particle/beam_smoke_01"
        "$nocull" "1"
        "$nodecal" "1"
        "$additive" "1"
        "$model" 1
        "$color2" "[4 1 0.4]"

        "Proxies"
        {
            "TextureScroll"
            {
                "texturescrollvar" "$texture2transform"
                "texturescrollrate" 0.1
                "texturescrollangle" 90
            }

            "Sine"
            {
                "sineperiod"	.06
                "sinemin"	.5
                "sinemax"	.9
                "resultVar"	$alpha
            }
        }
    }]]
    )
end

function C:Think()
    if self.MaxLength <= 0 then
        self.MaxLength = 40000
    end

    local Parent = self:GetParent()
    local StartPos = Parent:GetPos()
    local EndPos = Parent:GetPos() + (Parent:GetAngles():Forward() * self.MaxLength)
    local Emitter = self:GetEmitter()
    local Scale = self:GetScale()

    local Data = {}
    Data.start = StartPos
    Data.endpos = EndPos
    Data.filter = {Parent:GetLauncherEntity()}
    local Trace = util.TraceLine(Data)

    if Trace.Hit then
        self.Length = Parent:GetPos():Distance(Trace.HitPos)
        EndPos = Trace.HitPos
    else
        self.Length = self.MaxLength
    end

    --Shakey flare
	if CurTime() > self.NextFlareEmit then
		local size = 1200

		for i=1, 3 do
			local p = Emitter:Add("sprites/light_ignorez", StartPos)
			p:SetDieTime(math.Rand(0.1, 0.15))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size + math.Rand(-size * 0.5, 100) * Scale)
			p:SetColor(255, 150, 50)
			p.Offset = vector_origin
			p.Parent = self
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

		for i=1, 4 do
			local p = Emitter:Add("effects/flares/light-rays_001", StartPos)
			p:SetDieTime(math.Rand(0.1, 0.25))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size * 2 + math.Rand(-size * 0.5, 100) * Scale)
			p:SetColor(255, 100, 0)
			p.Offset = vector_origin
			p.Parent = self
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

        if not self.Dying then
            for i=1, 15 do
                local p = Emitter:Add("effects/flares/light-rays_001", EndPos)
                p:SetDieTime(math.Rand(0.1, 0.25))
                p:SetStartAlpha(255)
                p:SetEndAlpha(0)
                p:SetStartSize(0)
                p:SetEndSize(size * 6 + math.Rand(-size * 0.5, size * 0.5) * Scale)
                p:SetColor(255, 16 * i, 2 * i)
                p.Offset = Vector(self.Length, 0, 0)
                p.Parent = self
                p:SetThinkFunction(ParentedParticle)
                p:SetNextThink(CurTime())
            end
        end

		self.NextFlareEmit = CurTime() + self.FlareEmitDelay
	end

    --Sparky shaft stuff
    local LauncherEntity = Parent:GetLauncherEntity()
	if IsValid(LauncherEntity) and CurTime() > self.NextShockEmit then
		for i=1, 15 do
			local mins = LauncherEntity:OBBMins()
			local maxs = LauncherEntity:OBBMaxs()
			local pos = Vector(math.Rand(mins.x, maxs.x), math.Rand(mins.y, maxs.y), math.Rand(mins.z, maxs.z))
			local p = Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), LauncherEntity:LocalToWorld(pos))
			p:SetDieTime(math.Rand(0.2, 0.5))
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(300, 400) * Scale)
			p:SetEndSize(0)
			p:SetRoll(math.random(0, 360))
			p:SetRollDelta(math.Rand(-1, 1))
			p:SetColor(255, 150, 0)
			p.Offset = vector_origin
			p.Parent = self
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

		self.NextShockEmit = CurTime() + self.ShockEmitDelay
	end

    local Scale = Scale * 2
    --Flare
    for i=1, 30 do
        local velocity = (Vector(1, 0, 0) + (VectorRand() * 0.25)):GetNormalized() * math.Rand(100, 5000)
        local Pos = StartPos + (VectorRand() * 60 * Scale)
        local p = Emitter:Add("effects/spark", Pos)
        p:SetDieTime(math.Rand(0.7, 1.5))
        p:SetStartAlpha(255)
        p:SetEndAlpha(0)
        p:SetStartSize(math.random(5, 15) * Scale)
        p:SetEndSize(0)
        p:SetRoll(math.Rand(0, 360))
        p:SetVelocity(velocity)
        p:SetColor(255, 120, 75)

        p.Offset = vector_origin
        p.Parent = self
        p.OffsetSpeed = velocity
        p:SetThinkFunction(ParentedParticle)
        p:SetNextThink(CurTime())
    end

    --Flare
    for i=1, 15 do
        local velocity = (Vector(1, 0, 0) + (VectorRand() * 0.015)):GetNormalized() * math.random(2600,7000)
        local Pos = StartPos
        local p = Emitter:Add("effects/spark", Pos)
        p:SetDieTime(math.Rand(2.00, 3.5))
        p:SetStartAlpha(math.random(100,255))
        p:SetEndAlpha(0)
        p:SetStartSize(math.random(75, 100) * Scale)
        p:SetEndSize(0)
        p:SetRoll(math.Rand(0, 360))
        p:SetColor(255, 70, 0)

        p.Offset = vector_origin
        p.Parent = self
        p.OffsetSpeed = velocity
        p:SetThinkFunction(ParentedParticle)
        p:SetNextThink(CurTime())
    end

    --Sparks Long
    for i=1, 15 do
        local velocity = (Vector(1, 0, 0) + (VectorRand() * 0.013)):GetNormalized() * math.Rand(2000, 6000)
        local Pos = StartPos + (VectorRand() * 35 * Scale)
        local p = Emitter:Add("effects/shipsplosion/sparks_002", Pos)
        p:SetDieTime(math.Rand(0.6, 3.2))
        p:SetVelocity(velocity)
        p:SetStartAlpha(255)
        p:SetEndAlpha(0)
        p:SetStartLength(math.Rand(150, 300) * Scale)
        p:SetEndLength(p:GetStartLength() + (math.Rand(15, 17.5) * Scale))
        p:SetStartSize(math.Rand(50, 100) * Scale)
        p:SetEndSize(p:GetStartSize() + (math.Rand(200, 350) * Scale))
        --p:SetRoll(math.Rand(0, 360))
        p:SetColor(255, 120, math.random(0,100))

        p.Offset = vector_origin
        p.Parent = self
        p.OffsetSpeed = velocity
        p:SetThinkFunction(ParentedParticle)
        p:SetNextThink(CurTime())
    end

    --Sparks Long
    for i=1, 35 do
        local velocity = (Vector(1, 0, 0) + (VectorRand() * 0.015)):GetNormalized() * math.random(2600,4800)
        local Pos = StartPos + (VectorRand() * 35 * Scale)
        local p = Emitter:Add("effects/shipsplosion/sparks_002", Pos)
        p:SetDieTime(math.Rand(4, 8))
        p:SetStartAlpha(255)
        p:SetEndAlpha(0)
        p:SetStartLength(math.Rand(150, 500) * Scale)
        p:SetEndLength(p:GetStartLength() + (math.Rand(150, 300) * Scale))
        p:SetStartSize(math.Rand(60, 120) * Scale)
        p:SetEndSize(p:GetStartSize() + (math.Rand(250, 350) * Scale))
        --p:SetRoll(math.Rand(0, 360))
        p:SetColor(255, 255, math.random(0,100))

        p.Offset = vector_origin
        p.Parent = self
        p.OffsetSpeed = velocity
        p:SetThinkFunction(ParentedParticle)
        p:SetNextThink(CurTime())
    end
end

function C:Draw()
    local Parent = self:GetParent()
    local StartPos = Parent:GetPos()
    local EndPos = Parent:GetPos() + (Parent:GetAngles():Forward() * self.Length)
    local Scale = self:GetScale()

    render.SetMaterial(BeamMat)
    render.DrawBeam(EndPos, StartPos, (250 + (100 * math.sin(CurTime() * (25 + math.Rand(1, 15))))) * Scale, CurTime(), CurTime() + 1, Color(255, 120, 0, 200) )
    render.DrawBeam(EndPos, StartPos, 500 * Scale, CurTime(), CurTime() + 4, Color(255, 120, 0, 200) )
    render.DrawBeam(EndPos, StartPos, (100 + (100 * math.sin(CurTime() * 45))) * Scale, CurTime() * 2, CurTime() * 2 + 2, Color( 255, 200, 0, 255 ) )
    render.DrawBeam(EndPos, StartPos, 250 * Scale, CurTime() * 2, CurTime() * 2 + 8, Color( 255, 200, 0, 255 ) )

    return true
end

function C:GetComponentClass()
    return "TachyonBeamEffectComponent"
end

GM.class.registerClass("TachyonBeamEffectComponent", C)