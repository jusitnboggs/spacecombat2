local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Life = 1,
    Refract = 0,
    DieTime = 0,
    Resize = 0,
    Width = 1
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("MjolnirHitEffectComponent")

local matRefraction	= Material("refract_ring")

local tbolt2
if CLIENT then
    tbolt2 = GM.MaterialFromVMT(
        "MjolnirThunderBolt-2",
        [["UnLitGeneric"
        {
            "$basetexture"		"effects/tool_tracer"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
end

function C:init()
	self.DieTime = CurTime() + self.Life
    self.Width = math.floor(self:GetScale() * 8 / math.random(1, 4))

    if CLIENT then
        timer.Simple(0, function()
            if not self then return end

            -- Play sound
            sound.PlayFile("sound/weapons/gauss/fire1.wav", "3d", function(station)
                if IsValid(station) and self then
                    station:SetPos(self:GetOffsetPos())
                    station:Set3DFadeDistance(500 * self:GetScale(), 2000 * self:GetScale())
                    station:Play()
                end
            end)

            -- Create light
            local Light = DynamicLight(self:GetParent():GetID())
            local Col = self:GetColor()

            if Light then
                Light.pos = self:GetOffsetPos()
                Light.r = Col.r
                Light.g = Col.g
                Light.b = Col.b
                Light.brightness = 6
                Light.Decay = 1000
                Light.Size = 500 * self:GetScale()
                Light.DieTime = CurTime() + 0.65
            end
        end)
    end
end

function C:ShouldThink()
    return true
end

function C:Think()
	if self.Life <= 0 then
        -- Only hit effects should EVER do this!
        self:GetParent():Remove()
		return
	end

	self.Life = self.Life - FrameTime()
	self.Refract = self.Refract + 2.0 * FrameTime()
    self.Resize = 512 * self:GetScale() * self.Refract ^ 0.2
end

function C:Draw()
    local Pos = self:GetOffsetPos()
	local count = math.floor(self.Width)
    local Col = self:GetColor()

	for i=1,count do
		local points = 4
		local dir = self:GetParent():GetAngles():Forward()
		local nrm = VectorRand()
		local increment = self:GetScale() * 16
		local alpha = math.random(50, 100) + (100 * self.Life)

		render.SetMaterial(tbolt2)
		render.StartBeam(points + 2)
		render.AddBeam(Pos, 25 * self:GetScale(), 0, Color(Col.r, Col.g, Col.b, alpha))

		local pos = Pos
		for i=1,points do
			dir = (dir + nrm)
			pos = (dir + VectorRand()) * (increment / i + 1) + pos
			local tcoord = (1 / points) * i
			render.AddBeam(pos, 25 * self:GetScale(), tcoord / 1, Color(Col.r, Col.g, Col.b, alpha))
		end

		render.AddBeam(dir * increment + Pos, 25 * self:GetScale(), 1, Color(Col.r, Col.g, Col.b, alpha))
		render.EndBeam()
	end

	matRefraction:SetMaterialFloat("$refractamount", math.sin(self.Refract * math.pi) * 0.1)
    render.SetMaterial(matRefraction)
    render.UpdateRefractTexture()
    render.DrawSprite(Pos + (EyePos() - Pos):GetNormal() * (self.Refract ^ 0.3) * 2.8 , self.Resize, self.Resize)
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "MjolnirHitEffectComponent"
end

GM.class.registerClass("MjolnirHitEffectComponent", C)