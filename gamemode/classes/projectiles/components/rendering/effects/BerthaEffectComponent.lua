 --[[*******************************************
	Scalable Bertha Bolt

	Big green blob of doom.

	Author: Steeveeo
********************************************]]--
local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Scale = 4.5,
    Velocity = 1000,
    NextFlareEmit = CurTime(),
    FlareEmitDelay = 0.1,
    NextSparkEmit = CurTime(),
    SparkEmitDelay = 0.1,
    NextBlatEmit = CurTime(),
    BlatEmitDelay = 0.1
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("BerthaEffectComponent")

local Glow = Material("sprites/light_glow01")
local Shaft = Material("effects/ar2ground2")

function C:Think( )
    local Parent = self:GetParent()

	--Core Flicker Flares
	if CurTime() > self.NextFlareEmit then
		for i = 1, 2 do
			local p = self:GetEmitter():Add("effects/flares/light-rays_001", Parent:GetPos())
			p:SetDieTime(math.Rand(0.15, 1.5))
			p:SetVelocity(Parent:GetAngles():Forward() * self.Velocity)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.Rand(125, 300) * self.Scale)
			p:SetColor(200, 255, 200)
		end

		self.NextFlareEmit = CurTime() + self.FlareEmitDelay
	end

	--Trailing Sparks
	if CurTime() > self.NextSparkEmit then
		for i = 1, 5 do
			local p = self:GetEmitter():Add("effects/shipsplosion/sparks_00" .. math.random(1,4), Parent:GetPos())
			p:SetDieTime(math.Rand(2, 6))
			p:SetVelocity(-Parent:GetAngles():Forward() + (VectorRand() * 0.1))
			p:SetStartAlpha(64)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(75, 100) * self.Scale)
			p:SetEndSize(math.random(5, 15) * self.Scale)
			p:SetStartLength(0)
			p:SetEndLength(math.random(150, 300) * self.Scale)
			p:SetColor(0, math.random(128, 255), 0)
		end

		self.NextSparkEmit = CurTime() + self.SparkEmitDelay
	end

	--Trailing Particulates
	if CurTime() > self.NextBlatEmit then
		for i = 1, 5 do
			local p = self:GetEmitter():Add("effects/shipsplosion/fire_001", Parent:GetPos())
			p:SetDieTime(math.Rand(2, 4))
			p:SetVelocity(Parent:GetAngles():Forward() * (self.Velocity * math.Rand(0.25, 0.8)))
			p:SetStartAlpha(64)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(50, 75) * self.Scale)
			p:SetEndSize(math.random(5, 15) * self.Scale)
			p:SetColor(0, math.random(128, 255), 0)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
		end

		self.NextBlatEmit = CurTime() + self.BlatEmitDelay
	end
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function C:Draw( )
    local Parent = self:GetParent()
	render.SetMaterial(Shaft)
	render.DrawBeam(Parent:GetPos(), Parent:GetPos() + (Parent:GetAngles():Forward() * -150 * self.Scale), 75 * self.Scale, 1, 0, Color(0, 255, 0, 255))		--Edge
	render.DrawBeam(Parent:GetPos(), Parent:GetPos() + (Parent:GetAngles():Forward() * -150 * self.Scale), 50 * self.Scale, 1, 0, Color(100, 255, 100, 255))	--Core

	render.SetMaterial(Glow)
	render.DrawSprite(Parent:GetPos(), 275 * self.Scale, 275 * self.Scale, Color(0, 255, 0, 255))				--Edge
	render.DrawSprite(Parent:GetPos(), 275 * self.Scale / 2, 275 * self.Scale / 2, Color(255, 255, 255, 255))	--Core
end

function C:GetComponentClass()
    return "BerthaEffectComponent"
end
GM.class.registerClass("BerthaEffectComponent", C)