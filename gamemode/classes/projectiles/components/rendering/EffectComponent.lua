local GM = GM
local BaseClass = GM.class.getClass("ProjectileComponent")
local C = BaseClass:extends({
    Scale = 1,
    Color = Color(255, 255, 255, 255),
    Offset = Vector(0, 0, 0),
    Emitter = nil
})

local ZeroAngle = Angle(0, 0, 0)

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("EffectComponent")

function C:init(Parent)
	BaseClass.init(self)

    self:SetParent(Parent)
    self:SetMass(0)
end

function C:GetOffsetPos()
    local pos = LocalToWorld(self.Offset, ZeroAngle, self:GetParent():GetPos(), self:GetParent():GetAngles())
    return pos
end

function C:SetOffset(NewOffset)
    self.Offset = NewOffset
end

function C:GetColor()
    return self.Color
end

function C:SetColor(NewColor)
    self.Color = NewColor
end

function C:GetScale()
    return self.Scale
end

function C:SetScale(NewScale)
    self.Scale = NewScale
end

function C:GetEmitter()
    if not CLIENT then return end
    if not self.Emitter then
        self.Emitter = ParticleEmitter(self:GetOffsetPos())
    end

    return self.Emitter
end

function C:OnProjectileEvent(Event, Info)
    if self.Emitter and Event == "Removed" then
        self.Emitter:Finish()
        self.Emitter = nil
    end
end

function C:ShouldThink()
    return CLIENT
end

-- If this returns true then the C:Draw function will be called. If it returns false, then it won't get called.
function C:ShouldDraw()
    return true
end

function C:IsClientside()
    return true
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    if math.abs(self.Scale - 1) > 0.001 then
        net.WriteBool(true)
        net.WriteFloat(self.Scale)
    else
        net.WriteBool(false)
    end

    if self.Color.r + self.Color.g + self.Color.b + self.Color.a < 1020 then
        net.WriteBool(true)
        net.WriteColor(self.Color)
    else
        net.WriteBool(false)
    end

    if self.Offset:LengthSqr() > 0.001 then
        net.WriteBool(true)
        net.WriteVector(self.Offset)
    else
        net.WriteBool(false)
    end
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    if net.ReadBool() then
        self.Scale = net.ReadFloat()
    end

    if net.ReadBool() then
        self.Color = net.ReadColor()
    end

    if net.ReadBool() then
        self.Offset = net.ReadVector()
    end
end

function C:Serialize()
    -- Call the Serialize function on the base class first!
    local Out = BaseClass.Serialize(self)

    Out.Scale = self.Scale
    Out.Color = self.Color
    Out.Offset = self.Offset

    return Out
end

function C:DeSerialize(Data)
    -- Call the DeSerialize function from the base class first!
    BaseClass.DeSerialize(self, Data)

    self.Scale = Data.Scale
    self.Color = Data.Color
    self.Offset = Data.Offset
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "EffectComponent"
end

GM.class.registerClass("EffectComponent", C)