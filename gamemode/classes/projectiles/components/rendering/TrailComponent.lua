local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    NumSegments = 0,
    Material = "",
    Length = 500,
    Width = 100,
    Segments = {},
    LastSegment = Vector(0, 0, 0)
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("TrailComponent")

function C:init(Parent, Length, Width, NumSegments, Material, Col)
	BaseClass.init(self, Parent)

    if not Parent or not Length or not Width or not NumSegments or not Material then return end

    self.Length = Length or 500
    self.Width = Width or 100
    self.NumSegments = self.Length / 50
    self.Material = Material or ""
    self.Color = Col or Color(255, 255, 255, 255)
    self.Segments = {}

    for I=1, self.NumSegments do
        table.insert(self.Segments, self:GetOffsetPos())
    end

    self.LastSegment = self:GetOffsetPos()
end

function C:Draw()
    local DistBetweenSegments = self.Length / self.NumSegments
    local Mat = Material(self.Material)

    if self:GetOffsetPos():Distance(self.LastSegment) > DistBetweenSegments then
        self.LastSegment = self:GetOffsetPos()
        table.remove(self.Segments)
        table.insert(self.Segments, 1, self.LastSegment)
    end

    render.SetMaterial(Mat)
    --[[render.StartBeam(self.NumSegments)
    render.AddBeam(self.Segments[1], self.Width, 0, self.Color)
    for I = 1, (self.NumSegments - 2) do
        render.AddBeam(self.Segments[I + 1], self.Width, (1 / (self.NumSegments - 1)) * I, self.Color)
    end
    render.AddBeam(self.Segments[self.NumSegments], 0.1, 1, self.Color)
    render.EndBeam()]]--

    for I = 1, (self.NumSegments - 1) do
        render.DrawBeam(self.Segments[I], self.Segments[I + 1], self.Width * (1 - (1 / (self.NumSegments - 1)) * I), (1 / (self.NumSegments - 1)) * I, (1 / (self.NumSegments - 1)) * (I + 1), self.Color)
    end
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteFloat(self.Length)
    net.WriteFloat(self.Width)

    -- This will cause the Material to be cached as a network string for later
    SC.WriteNetworkedString(self.Material)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.Length = net.ReadFloat()
    self.Width = net.ReadFloat()
    self.NumSegments = self.Length / 50

    for I=1, self.NumSegments do
        table.insert(self.Segments, self:GetOffsetPos())
    end

    self.LastSegment = self:GetOffsetPos()

    -- Read the cached Material string
    self.Material = SC.ReadNetworkedString("", function(ID, String)
        if self then
            self.Material = String
        end
    end)
end

function C:Serialize()
    -- Call the Serialize function on the base class first!
    local Out = BaseClass.Serialize(self)

    Out.Material = self.Material
    Out.Length = self.Length
    Out.Width = self.Width

    -- Return the table so it gets networked
    return Out
end

function C:DeSerialize(Data)
    -- Call the DeSerialize function from the base class first!
    BaseClass.DeSerialize(self, Data)

    self.Material = Data.Material
    self.Length = Data.Length
    self.Width = Data.Width
    self.NumSegments = self.Length / 50
    self.Segments = {}

    for I=1, self.NumSegments do
        table.insert(self.Segments, self:GetOffsetPos())
    end

    self.LastSegment = self:GetOffsetPos()
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "TrailComponent"
end

GM.class.registerClass("TrailComponent", C)