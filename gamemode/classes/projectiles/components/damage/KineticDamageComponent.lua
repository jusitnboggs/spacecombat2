local C = GM.class.getClass("DamageComponent"):extends({

})

local BaseClass = C:getClass()

function C:GetDamage()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        SC.Error("Warning: KineticDamageComponent has no parent!", 5)
        return {KIN=0}
    end

    local MovementComponent = self:GetParent():GetMovementComponent()
    if not MovementComponent then
        SC.Error("Warning: KineticDamageComponent has no movement component!", 5)
        return {KIN=0}
    end

    return {KIN=Parent:GetMass()*MovementComponent:GetVelocity():Length()}
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "KineticDamageComponent"
end

GM.class.registerClass("KineticDamageComponent", C)