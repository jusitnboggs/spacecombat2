local GM = GM
local ProjectileComponentData = {}

GM.Projectiles = GM.Projectiles or {}
GM.Projectiles.Components = {}
GM.Projectiles.Components.LoadedComponents = ProjectileComponentData

if SERVER then
    util.AddNetworkString("SC.ProjectileComponentDataLoad")

    net.Receive("SC.ProjectileComponentDataLoad", function(Length, Client)
        -- TODO: Change this to actually use the Read/Write creation packet function
        -- It will perform better and be cleaner. For now this is functional though.
        local SerializedComponents = {}
        for i,k in pairs(ProjectileComponentData) do
            SerializedComponents[i] = k:Serialize()
        end

        net.Start("SC.ProjectileComponentDataLoad")
        net.WriteTable(SerializedComponents)
        net.Send(Client)
    end)
else
    net.Receive("SC.ProjectileComponentDataLoad", function()
        local SerializedComponents = net.ReadTable()
        SC.Print("Got "..table.Count(SerializedComponents).." serialized Projectile Components from the server!", 4)
        for i,k in pairs(SerializedComponents) do
            ProjectileComponentData[i] = GM.class.new("ProjectileComponentData")
            ProjectileComponentData[i]:DeSerialize(k)
            SC.Print("Loaded Projectile Component "..ProjectileComponentData[i]:GetName(), 4)
        end
        SC.Print("Finished loading Projetile Components", 4)
        hook.Run("SC.ProjectileComponentDataLoaded")
    end)
end

function GM.Projectiles.Components.ReloadProjectileComponentData()
    -- Server loads all files from disk for projectile Components
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/projectiles/components/*", "DATA")

        -- For each file load the data into a new projectile Component class
        for _,File in pairs(Files) do
            local NewProjectileComponentData = GM.class.new("ProjectileComponentData")
            if NewProjectileComponentData:LoadFromINI(File) then
                ProjectileComponentData[NewProjectileComponentData:GetName()] = NewProjectileComponentData
            else
                SC.Error("Failed to load projectile component from file "..File, 5)
            end
        end

        hook.Run("SC.ProjectileComponentDataLoaded")

    -- Clients request data from the server for projectile Components
    else
        -- Reset the existing data until we get the new stuff
        ProjectileComponentData = {}
        GM.Projectiles.Components.LoadedComponents = ProjectileComponentData

        -- Request new data from the server
        net.Start("SC.ProjectileComponentDataLoad")
        net.SendToServer()
    end
end

hook.Remove("InitPostEntity", "SC.LoadProjectileComponentData")
hook.Add("InitPostEntity", "SC.LoadProjectileComponentData", function()
    GM.Projectiles.Components.ReloadProjectileComponentData()
end)

hook.Remove("OnReloaded", "SC.ReloadProjectileComponentData")
hook.Add("OnReloaded", "SC.ReloadProjectileComponentData", function()
    GM.Projectiles.Components.ReloadProjectileComponentData()
end)

local C = GM.LCS.class({
    -- Name of the component
    Name = "Component",

    -- Description of the component displayed in the projectile creation interface
    Description = "",

    -- What class should be created when spawning the projectile
    Class = "ProjectileComponent",

    -- What family of components does this component belong to
    -- Used to determine what components can be placed in a slot
    Family = "Component",

    -- A list of factions that can use the component in projectiles
    Factions = {},

    -- A list of resources required to produce a projectile with this component
    ResourceCost = {},

    -- If the component should be hidden from the projectile creation tool
    Hidden = false,

    -- The amount of mass added to the projectile by the component
    Mass = 0,

    -- A table of data passed to the component class when it is created
    ClassData = {},

    -- A list of options available for this component in the projectile creation tool
    ToolOptions = {},
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetClass()
    return self.Class
end

function C:GetFamily()
    return self.Family
end

function C:IsHidden()
    return self.Hidden
end

function C:GetMass()
    return self.Mass
end

function C:GetFactions()
    return table.Copy(self.Factions)
end

function C:GetResourceCost()
    return table.Copy(self.ResourceCost)
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/projectiles/components/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:ValidateINIData(Data)
    -- TODO: Use this function to make sure that an INI file has valid data
    -- If it returns false it should also return an error string to be printed to the console
    return true
end

function C:ValidateToolOptions(Options)
    -- Make sure the options are a table
    if not Options or not type(Options) == "table" then
        return false
    end

    -- Loop over all the options and make sure everything matches the tool options set in the data
    for Name, Value in pairs(Options) do
        -- If the option isn't supposed to be here then the data isn't valid
        local ToolOptions = self.ToolOptions[Name]
        if not ToolOptions then
            return false
        end

        -- Check to make sure the data is of the right type
        if ToolOptions.Type == "Number" then
            -- Make sure it's a number
            if type(Value) ~= "number" then
                return false
            end

            -- Make sure it's not too low
            if ToolOptions.Min and Value < ToolOptions.Min then
                return false
            end

            -- Make sure it's not too high
            if ToolOptions.Max and Value > ToolOptions.Max then
                return false
            end
        elseif ToolOptions.Type == "Color" then
            -- Make sure it's a color
            if type(Value) ~= "table" or not Value.r or not Value.g or not Value.b then
                return false
            end
        elseif ToolOptions.Type == "String" then
            -- Make sure it's a string
            if type(Value) ~= "string" then
                return false
            end
        elseif ToolOptions.Type == "Vector" then
            -- Make sure it's a vector
            if type(Value) ~= "Vector" then
                return false
            end
        elseif ToolOptions.Type == "Angle" then
            -- Make sure it's a angle
            if type(Value) ~= "Angle" then
                return false
            end
        end
    end

    return true
end

function C:GetComponentData(Options, Modifiers)
    -- If the options we got from the tool aren't valid them don't return anything
    if not self:ValidateToolOptions(Options) then
        return false, {}
    end

    -- This table has the basic data needed to create a component
    local Data = {
        ComponentClass = self.Class,
        Modifiers = table.Copy(Modifiers or {}),
        Mass = self.Mass
    }

    -- Add any special data needed by our component class
    for Name, Value in pairs(self.ClassData) do
        Data[Name] = Value
    end

    -- Set the values of any configured data
    for Name, Info in pairs(self.ToolOptions) do
        Data[Name] = Options[Name] or Info.Default
    end

    return true, Data
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteString(self.Class)
    net.WriteString(self.Family)
    net.WriteTable(self.Factions)
    net.WriteBool(self.Hidden)
    net.WriteFloat(self.Mass)
    net.WriteTable(self.ClassData)
    net.WriteTable(self.ToolOptions)
    net.WriteTable(self.ResourceCost)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Class = net.ReadString()
    self.Family = net.ReadString()
    self.Factions = net.ReadTable()
    self.Hidden = net.ReadBool()
    self.Mass = net.ReadFloat()
    self.ClassData = net.ReadTable()
    self.ToolOptions = net.ReadTable()
    self.ResourceCost = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description,
            Class = self.Class,
            Family = self.Family,
            Factions = self.Factions,
            Hidden = self.Hidden,
            Mass = self.Mass,
        },

        ClassData = self.ClassData,
        ToolOptions = self.ToolOptions,
        ResourceCost = self.ResourceCost,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Component"
        self.Description = Data.Configuration.Description or ""
        self.Class = Data.Configuration.Class or "ProjectileComponent"
        self.Family = Data.Configuration.Family or "Component"
        self.Factions = Data.Configuration.Factions or {}
        self.Hidden = Data.Configuration.Hidden or false
        self.Mass = Data.Configuration.Mass or 0
    end

    self.ClassData = Data.ClassData or {}
    self.ToolOptions = Data.ToolOptions or {}
    self.ResourceCost = Data.ResourceCost or {}
end

GM.class.registerClass("ProjectileComponentData", C)