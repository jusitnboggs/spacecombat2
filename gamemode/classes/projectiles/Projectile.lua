local GM = GM
local ClearedIDs = {}
local Projectiles = {}
local ComponentNetworkIDs = {}
local ComponentReverseLookup = {}

GM.Projectiles = GM.Projectiles or {}
function GM.Projectiles.GetAllProjectiles()
    return Projectiles
end

function GM.Projectiles.GetProjectile(ID)
    return Projectiles[ID]
end

function GM.Projectiles.FindProjectilesNearPosition(Position, Distance)
    local Found = {}
    local SqrDist = Distance * Distance
    for i,k in pairs(Projectiles) do
        if k:GetPos():DistToSqr(Position) <= SqrDist then
            table.insert(Found, k)
        end
    end

    return Found
end

function GM.Projectiles.RegisterNetworkedComponent(Name)
    if type(Name) ~= "string" then error("Error! Name must be a string!") end

    local NewID = table.Count(ComponentNetworkIDs) + 1
    ComponentNetworkIDs[Name] = NewID
    ComponentReverseLookup[NewID] = Name
    return NewID
end

function GM.Projectiles.GetComponentNetworkID(Name)
    if not ComponentNetworkIDs[Name] then error("Unable to find component Network ID: "..Name) end
    return ComponentNetworkIDs[Name]
end

function GM.Projectiles.GetComponentClassFromID(ID)
    if not ComponentReverseLookup[ID] then error("Unable to find component with Network ID: "..ID) end
    return ComponentReverseLookup[ID]
end

local function GetNetUpdateDelay()
    local Count = table.Count(Projectiles)

    return 1 / math.min(33, 1 / (Count / 300))
end

if SERVER then
    util.AddNetworkString("SC.ProjectileNetworkedEvent")
    util.AddNetworkString("SC.ProjectileCreated")
    util.AddNetworkString("SC.ProjectileNetworkUpdate")
else
    net.Receive("SC.ProjectileNetworkedEvent", function()
        local Projectile = GM.Projectiles.GetProjectile(net.ReadUInt(16))
        if not Projectile then return end

        local Data
        local Event = SC.ReadNetworkedString("", function(ID, String)
            if Projectile then
                Projectile:FireEvent(String, Data)
            end
        end)

        Data = net.ReadTable()

        if Event ~= "" then
            Projectile:FireEvent(Event, Data)
        end
    end)

    net.Receive("SC.ProjectileCreated", function()
        local Projectile = GM.class.new("Projectile")
        Projectile:ReadCreationPacket()
    end)

    net.Receive("SC.ProjectileNetworkUpdate", function()
        local id = net.ReadUInt(16)
        local Projectile = GM.Projectiles.GetProjectile(id)
        if not Projectile then return end
        Projectile:ReadNetworkUpdate()
    end)
end

local C = GM.LCS.class({
    ID = 0,
    Position = Vector(),
    Angles = Angle(),
    Components = {},
    Owner = NULL,
    NetworkedPlayers = {},
    NextNetUpdate = 0,
    Removed = false,
    ShouldSendNetUpdates = false,
    MovementComponent = nil,
    TargetingComponent = nil,
    CollisionComponent = nil,
    Launcher = nil,
    LauncherEntity = NULL,
    Mass = 0
})

function C:init(Position, Angles, Components, ID)
    self:SetPos(Position or Vector(0, 0, 0))
    self:SetAngles(Angles or Angle(0, 0, 0))
    self:SetComponents(Components or {})

    if CLIENT then
        if not ID then return end
        Projectiles[ID] = self
        self.ID = ID
    else
        -- If we hit this then init already ran once and we shouldn't do this again
        if self.ID ~= 0 then return end

        -- Only the server can decide our ID, projectiles should never be created on the client directly
        self.ID = #ClearedIDs > 0 and table.remove(ClearedIDs) or (#Projectiles + 1)
        self.NetworkedPlayers = player.GetAll()

        Projectiles[self.ID] = self
    end
end

function C:SendToClient()
    if not SERVER then return end
    self.NetworkedPlayers = player.GetAll()
    net.Start("SC.ProjectileCreated")
    self:WriteCreationPacket()
    net.Send(self.NetworkedPlayers)
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteVector(self:GetPos())
    net.WriteAngle(self:GetAngles())
    net.WriteUInt(self:GetID(), 16)
    net.WriteFloat(self.Mass)
    net.WriteEntity(self:GetLauncherEntity())

    -- Write all of the component's to the packet
    for i,k in ipairs(self:GetComponents()) do
        if k:IsClientside() then
            net.WriteUInt(GM.Projectiles.GetComponentNetworkID(k:GetComponentClass()), 12)
            k:WriteCreationPacket()
        end
    end

    -- Signifies that all components were read
    net.WriteUInt(0, 12)
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self:init(net.ReadVector(), net.ReadAngle(), {}, net.ReadUInt(16))
    self.Mass = net.ReadFloat()
    self.LauncherEntity = net.ReadEntity()

    -- Create components until we hit an invalid ID
    while true do
        local ComponentNetworkID = net.ReadUInt(12)
        if ComponentNetworkID == 0 then break end

        local ComponentClass = GM.Projectiles.GetComponentClassFromID(ComponentNetworkID)
        local Component = GM.class.new(ComponentClass)
        self:AddComponent(Component)
        Component:ReadCreationPacket()
        Component:PostComponentInit()
    end

    self:Spawn()
end

function C:FireEvent(Event, Info)
    for i,k in ipairs(self:GetComponents()) do
        k:OnProjectileEvent(Event, Info)
    end

    if CLIENT and Event == "Removed" then
        self:Remove()
    end
end

function C:FireNetworkedEvent(Event, Info)
    if not SERVER then return end

    self:FireEvent(Event, Info)

    net.Start("SC.ProjectileNetworkedEvent")
    net.WriteUInt(self:GetID(), 16)
    SC.WriteNetworkedString(Event)
    net.WriteTable(Info)
    net.Broadcast()
end

function C:ReadNetworkUpdate()
    if not CLIENT then return end

    self:SetPos(net.ReadVector())
    self:SetAngles(net.ReadAngle())

    for i,k in ipairs(self:GetComponents()) do
        k:ReadNetworkUpdate()
    end
end

function C:SendNetworkUpdate()
    if not SERVER then return end

    if self.ShouldSendNetUpdates and self.NextNetUpdate <= CurTime() then
        net.Start("SC.ProjectileNetworkUpdate")
        net.WriteUInt(self:GetID(), 16)
        net.WriteVector(self:GetPos())
        net.WriteAngle(self:GetAngles())

        for i,k in ipairs(self:GetComponents()) do
            k:SendNetworkUpdate()
        end

        net.Send(self.NetworkedPlayers)

        self.NextNetUpdate = CurTime() + GetNetUpdateDelay()
    end
end

function C:GetLauncherEntity()
    if SERVER and IsValid(self.Launcher) then
        return self.Launcher:GetAttachedEntity()
    else
        return self.LauncherEntity
    end
end

function C:SetLauncherEntity(NewLauncherEntity)
    if IsValid(self.LauncherEntity) then
        SC.Error("Warning! Overriding LauncherEntity for projectile! (Are you sure this is right?)", 5)
    end

    self.LauncherEntity = NewLauncherEntity
end

function C:GetLauncher()
    return self.Launcher
end

function C:SetLauncher(NewLauncher)
    if not IsValid(NewLauncher) then
        return
    end

    if IsValid(self.Launcher) then
        SC.Error("Warning! Overriding Launcher for projectile! (Are you sure this is right?)", 5)
    end

    self.Launcher = NewLauncher
    self:SetLauncherEntity(self.Launcher:GetAttachedEntity())
end

function C:GetMovementComponent()
    return self.MovementComponent
end

function C:SetMovementComponent(NewMovementComponent)
    if self.MovementComponent then
        SC.Error("Warning! Overriding movement component for projectile! (Are you sure this is right?)", 5)
    end

    self.MovementComponent = NewMovementComponent
end

function C:GetTargetingComponent()
    return self.TargetingComponent
end

function C:SetTargetingComponent(NewTargetingComponent)
    if self.TargetingComponent then
        SC.Error("Warning! Overriding targeting component for projectile! (Are you sure this is right?)", 5)
    end

    self.TargetingComponent = NewTargetingComponent
end

function C:GetCollisionComponent()
    return self.CollisionComponent
end

function C:SetCollisionComponent(NewCollisionComponent)
    if self.CollisionComponent then
        SC.Error("Warning! Overriding collision component for projectile! (Are you sure this is right?)", 5)
    end

    self.CollisionComponent = NewCollisionComponent
end

function C:CalculateMass()
    if CLIENT then return end
    local Mass = 0
    for i,k in ipairs(self:GetComponents()) do
        Mass = Mass + k:GetMass()
    end

    self.Mass = Mass
end

function C:GetMass()
    return self.Mass
end

function C:SetPos(NewPos)
    self.Position = NewPos
end

function C:GetPos()
    return self.Position
end

function C:SetAngles(NewAngles)
    self.Angles = NewAngles
end

function C:GetAngles()
    return self.Angles
end

function C:AddComponent(Component)
    table.insert(self.Components, Component)
    Component:SetParent(self)
end

function C:RemoveComponent(Component)
    table.remove(self.Components, Component)
    Component:SetParent(nil)
end

function C:SetComponents(Components)
    self.Components = Components

    for i,k in ipairs(self.Components) do
        k:SetParent(self)
    end
end

function C:GetComponents()
    return self.Components
end

function C:GetID()
    return self.ID
end

function C:GetOwner()
    return self.Owner
end

function C:SetOwner(NewOwner)
    self.Owner = NewOwner
end

function C:Remove()
    if not IsValid(self) then return end

    if self.OnRemoved then
        self:OnRemoved()
    end

    Projectiles[self:GetID()] = nil
    table.insert(ClearedIDs, self:GetID())

    self:FireNetworkedEvent("Removed", {})
    self.ID = 0
    self.Removed = true

    for i,k in pairs(self.Components) do
        k:SetParent(nil)
        self.Components[i] = nil
    end
end

function C:Spawn()
    if self.Initialized then return end

    self:CalculateMass()
    self.Initialized = true
    self:FireEvent("Initialized", {})

    if SERVER then
        self:SendToClient()
    end
end

function C:IsValid()
    return self:GetID() ~= 0 and not self.Removed and self.Initialized
end

function C:Think()
    local LateThink = {}
    for i,k in ipairs(self:GetComponents()) do
        if k:ShouldThink() then
            if k:UsesLateThink() then
                table.insert(LateThink, k)
            else
                k:Think()
            end
        end
    end

    for i,k in ipairs(LateThink) do
        k:Think()
    end

    self:SendNetworkUpdate()
end

hook.Add("Think", "SC2ProjectileThink", function()
    for i,k in pairs(Projectiles) do
        if IsValid(k) then
            k:Think()
        end
    end
end)

function C:ShouldDraw()
    return CLIENT
end

function C:Draw()
    if not CLIENT then return end
    for i,k in ipairs(self:GetComponents()) do
        k:Draw()
    end
end

hook.Add("PreDrawEffects", "SC2ProjectileDraw", function()
    for i,k in pairs(Projectiles) do
        if IsValid(k) and k:ShouldDraw() then
            k:Draw()
        end
    end
end)

function C:Serialize()
    local SerializedComponents = {}
    for i,k in ipairs(self.Components) do
        table.insert(SerializedComponents, k:Serialize())
    end

    return {Position=self.Position, Angles=self.Angles, Components=SerializedComponents, ID=self.ID, Mass=self.Mass, Owner=self.Owner, LauncherEntity=self:GetLauncherEntity(), ShouldSendNetUpdates=self.ShouldSendNetUpdates}
end

function C:DeSerialize(Data)
    self:init(Data.Position, Data.Angles, {}, Data.ID)
    self:SetOwner(Data.Owner)
    self.ShouldSendNetUpdates = Data.ShouldSendNetUpdates or false
    self.Mass = Data.Mass or 0
    self.LauncherEntity = Data.LauncherEntity or NULL

    for i,k in ipairs(Data.Components) do
        local Component = GM.class.new(k.ComponentClass)
        self:AddComponent(Component)
        Component:DeSerialize(k)
        Component:PostComponentInit()
    end
end

GM.class.registerClass("Projectile", C)