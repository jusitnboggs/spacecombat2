local GM = GM
local ProjectileTypes = {}

GM.Projectiles = GM.Projectiles or {}
GM.Projectiles.Types = {}
GM.Projectiles.Types.LoadedTypes = ProjectileTypes

if SERVER then
    util.AddNetworkString("SC.ProjectileTypesLoad")

    net.Receive("SC.ProjectileTypesLoad", function(Length, Client)
        -- TODO: Change this to actually use the Read/Write creation packet function
        -- It will perform better and be cleaner. For now this is functional though.
        local SerializedTypes = {}
        for i,k in pairs(ProjectileTypes) do
            SerializedTypes[i] = k:Serialize()
        end

        net.Start("SC.ProjectileTypesLoad")
        net.WriteTable(SerializedTypes)
        net.Send(Client)
    end)
else
    net.Receive("SC.ProjectileTypesLoad", function()
        local SerializedTypes = net.ReadTable()
        SC.Print("Got "..table.Count(SerializedTypes).." serialized Projectile Types from the server!", 4)
        for i,k in pairs(SerializedTypes) do
            ProjectileTypes[i] = GM.class.new("ProjectileType")
            ProjectileTypes[i]:DeSerialize(k)
            SC.Print("Loaded Projectile Type "..ProjectileTypes[i]:GetName(), 4)
        end
        SC.Print("Finished loading Projetile Types", 4)
        hook.Run("SC.ProjectileTypesLoaded")
    end)
end

function GM.Projectiles.Types.ReloadProjectileTypes()
    -- Server loads all files from disk for projectile types
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/projectiles/types/*", "DATA")

        -- For each file load the data into a new projectile type class
        for _,File in pairs(Files) do
            local NewProjectileType = GM.class.new("ProjectileType")
            if NewProjectileType:LoadFromINI(File) then
                ProjectileTypes[NewProjectileType:GetName()] = NewProjectileType
            else
                SC.Error("Failed to load projectile type from file "..File, 5)
            end
        end

        hook.Run("SC.ProjectileTypesLoaded")

    -- Clients request data from the server for projectile types
    else
        -- Reset the existing data until we get the new stuff
        ProjectileTypes = {}
        GM.Projectiles.Types.LoadedTypes = ProjectileTypes

        -- Request new data from the server
        net.Start("SC.ProjectileTypesLoad")
        net.SendToServer()
    end
end

hook.Remove("SC.ProjectileComponentDataLoaded", "SC.LoadProjectileTypes")
hook.Add("SC.ProjectileComponentDataLoaded", "SC.LoadProjectileTypes", function()
    GM.Projectiles.Types.ReloadProjectileTypes()
end)

local C = GM.LCS.class({
    -- Name of the projectile type
    Name = "Projectile",

    -- Description of the projectile type displayed in the projectile creation interface
    Description = "",

    -- What class should be used to load the projectile type
    -- This is useful if you need to perform special validation on some projectile type
    -- TODO: Add support for this, it's not hooked up yet because nothing uses it
    Class = "ProjectileType",

    -- A table of slots available to the projectile type.
    -- The Key is the slot name, the value is a table of component families that are allowed to be used in that slot.
    -- The Key of the table of component families is the name of the family, and the value determines if that family is allowed.
    -- Setting a component family to false is used to filter out components that have multiple families.
    Slots = {},

    -- A table of components that will always be added to projectiles of this type
    -- It is mostly used for things like collision components.
    -- Key is the name of the slot the component is in. This isn't normally displayed to users.
    -- Component information is a table containing the component name, configuration, and modifiers.
    MandatoryComponents = {}
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetSlots()
    return table.Copy(self.Slots)
end

function C:GetNumberOfSlots()
    return table.Count(self.Slots)
end

function C:GetMandatoryComponents()
    return table.Copy(self.MandatoryComponents)
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/projectiles/types/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:ValidateRecipeConfiguration(Recipe)
    -- Todo: Load the data about the Projectile Type and make sure the components for the recipe are valid.
    -- If this returns false the recipe should be logged and removed immediately.
    return true
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteTable(self.Slots)
    net.WriteTable(self.MandatoryComponents)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Slots = net.ReadTable()
    self.MandatoryComponents = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description
        },
        Slots = self.Slots,
        MandatoryComponents = self.MandatoryComponents
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Projectile"
        self.Description = Data.Configuration.Description or ""
    end

    self.Slots = Data.Slots or {}
    self.MandatoryComponents = Data.MandatoryComponents or {}
end

GM.class.registerClass("ProjectileType", C)