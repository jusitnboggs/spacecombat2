local GM = GM
local LauncherTypes = {}

GM.Launchers = GM.Launchers or {}
GM.Launchers.Types = {}
GM.Launchers.Types.LoadedTypes = LauncherTypes

if SERVER then
    util.AddNetworkString("SC.LauncherTypesLoad")

    net.Receive("SC.LauncherTypesLoad", function(Length, Client)
        -- TODO: Change this to actually use the Read/Write creation packet function
        -- It will perform better and be cleaner. For now this is functional though.
        local SerializedTypes = {}
        for i,k in pairs(LauncherTypes) do
            SerializedTypes[i] = k:Serialize()
        end

        net.Start("SC.LauncherTypesLoad")
        net.WriteTable(SerializedTypes)
        net.Send(Client)
    end)
else
    net.Receive("SC.LauncherTypesLoad", function()
        local SerializedTypes = net.ReadTable()
        SC.Print("Got "..table.Count(SerializedTypes).." serialized Launcher Types from the server!", 4)
        for i,k in pairs(SerializedTypes) do
            LauncherTypes[i] = GM.class.new("LauncherType")
            LauncherTypes[i]:DeSerialize(k)
            SC.Print("Loaded Launcher Type "..LauncherTypes[i]:GetName(), 4)
        end
        SC.Print("Finished loading Projetile Types", 4)
        hook.Run("SC.LauncherTypesLoaded")
    end)
end

function GM.Launchers.Types.ReloadLauncherTypes()
    -- Server loads all files from disk for launcher types
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/launchers/types/*", "DATA")

        -- For each file load the data into a new launcher type class
        for _,File in pairs(Files) do
            local NewLauncherType = GM.class.new("LauncherType")
            if NewLauncherType:LoadFromINI(File) then
                LauncherTypes[NewLauncherType:GetName()] = NewLauncherType
            else
                SC.Error("Failed to load launcher type from file "..File, 5)
            end
        end

        hook.Run("SC.LauncherTypesLoaded")

    -- Clients request data from the server for launcher types
    else
        -- Reset the existing data until we get the new stuff
        LauncherTypes = {}
        GM.Launchers.Types.LoadedTypes = LauncherTypes

        -- Request new data from the server
        net.Start("SC.LauncherTypesLoad")
        net.SendToServer()
    end
end

hook.Remove("SC.LauncherModelsLoaded", "SC.LoadLauncherTypes")
hook.Add("SC.LauncherModelsLoaded", "SC.LoadLauncherTypes", function()
    GM.Launchers.Types.ReloadLauncherTypes()
end)

local C = GM.LCS.class({
    -- Name of the launcher type
    Name = "Launcher",

    -- Description of the launcher type displayed in the launcher creation interface
    Description = "",

    -- What class should be used for the launcher
    Class = "Launcher",

    -- A table of upgrade slots available to the launcher type.
    -- The Key is the slot name, the value is a table of upgrade families that are allowed to be used in that slot.
    -- The Key of the table of upgrade families is the name of the family, and the value determines if that family is allowed.
    -- Setting a upgrade family to false is used to filter out upgrades that have multiple families.
    Slots = {},

    -- A list of projectile families that can be fired from this launcher type
    CompatibleProjectileFamilies = {},

    -- A table of configuration data passed to the launcher when it is created
    LauncherConfiguration = {},
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetClass()
    return self.Class
end

function C:GetSlots()
    return table.Copy(self.Slots)
end

function C:GetNumberOfSlots()
    return table.Count(self.Slots)
end

function C:GetLauncherConfiguration()
    return table.Copy(self.LauncherConfiguration)
end

function C:IsProjectileRecipeCompatible(Recipe)
    if not self.CompatibleProjectileFamilies[Recipe:GetProjectileType()] then
        return false
    end

    if not Recipe:ValidateConfiguration() then
        return false
    end

    return true
end

function C:GetCompatibleProjectileRecipes(Owner)
    local AllRecipes = {}
    if IsValid(Owner) then
        AllRecipes[Owner:SteamID()] = GM.Projectiles.Recipes.LoadedRecipes[Owner:SteamID()] or {}
        AllRecipes[team.GetName(Owner:Team())] = GM.Projectiles.Recipes.LoadedRecipes[team.GetName(Owner:Team())] or {}
    end

    AllRecipes["NULL"] = GM.Projectiles.Recipes.LoadedRecipes["NULL"]

    local OutRecipes = {}
    for RecipeOwner, Recipes in pairs(AllRecipes) do
        OutRecipes[RecipeOwner] = {}
        for Name, Recipe in pairs(Recipes) do
            if self:IsProjectileRecipeCompatible(Recipe) then
                OutRecipes[RecipeOwner][Name] = Recipe
            end
        end
    end

    return OutRecipes
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/launchers/types/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:ValidateLauncherConfiguration(Upgrades)

    return true
end

function C:CreateLauncher(Owner, AttachedEntity, ProjectileRecipe, Upgrades)
    if Upgrades then
        if not self:ValidateLauncherConfiguration(Upgrades) then
            SC.Error("Tried to create launcher "..self.Name.." with invalid upgrade configuration!")
            return
        end
    end

    local Launcher = GM.class.new(self.Class)
    Launcher:SetLauncherType(self)

    if IsValid(Owner) then
        Launcher:SetOwner(Owner)
    end

    if IsValid(AttachedEntity) then
        Launcher:SetAttachedEntity(AttachedEntity)
    end

    if ProjectileRecipe then
        Launcher:SetRecipe(ProjectileRecipe)
    end

    if Upgrades then
        Launcher:ApplyUpgrades(Upgrades)
    end

    Launcher:Spawn()

    return Launcher
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteString(self.Class)
    net.WriteTable(self.CompatibleProjectileFamilies)
    net.WriteTable(self.Slots)
    net.WriteTable(self.LauncherConfiguration)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Class = net.ReadString()
    self.CompatibleProjectileFamilies = net.ReadTable()
    self.Slots = net.ReadTable()
    self.LauncherConfiguration = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description,
            Class = self.Class,
            CompatibleProjectileFamilies = self.CompatibleProjectileFamilies,
        },

        LauncherConfiguration = self.LauncherConfiguration,
        Slots = self.Slots,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Launcher"
        self.Description = Data.Configuration.Description or ""
        self.Class = Data.Configuration.Class or "Launcher"
        self.CompatibleProjectileFamilies = Data.Configuration.CompatibleProjectileFamilies or {}
    end

    self.LauncherConfiguration = Data.LauncherConfiguration or {}
    self.Slots = Data.Slots or {}
end

GM.class.registerClass("LauncherType", C)