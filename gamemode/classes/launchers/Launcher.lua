local GM = GM
GM.Launchers = GM.Launchers or {}

local Launchers = {}

local C = GM.LCS.class({
    Initialized = false,
    Removed = false,
    ID = 0,
    LauncherType = nil,
    ProjectileRecipe = nil,
    AttachedEntity = nil,
    Owner = nil,
    LauncherModel = nil,
    Core = nil,
    Upgrades = {},
    Position = Vector(0, 0, 0),
    Angle = Angle(0, 0, 0),
    Firing = false,
    NextThink = 0,
    NextFire = 0,
    LastFiringPosition = 0,

    -- Targeting
    HasTargeting = false,
    Target = nil,

    -- Fire Rate
    ShotsPerMinute = 60,

    -- Burst Fire
    HasBurst = false,
    ShotsPerBurst = 3,
    TimeBetweenBurstShots = 0.05,

    -- Magazine
    HasMagazine = false,
    Ammo = 0,
    MaxAmmo = 50,
    ReloadTime = 10,
    Reloading = false,
    ReloadingFinishedTime = 0,

    -- Resource Usage
    HasResourceUsage = false,
    ResourcesPerShot = {},
    ResourcesPerReload = {},

    -- Capacitor
    HasCapacitor = false,
    FillOnlyWhenFiring = true,
    MinimumFirePercentage = 0,
    Capacitor = 0,
    MaxCapacitor = 1000,
    CapacitorFillRate = 1000,
    CapacitorPerShot = 100,
    NextCapacitorFill = 0,

    -- Fitting
    HasFitting = false,
    Fitting = {
        CPU = 0,
        PG = 0,
        NumSlots = 1,
        Slot = "None",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    },

    -- Accuracy
    HasAccuracy = false,
    MinimumSpread = 0.01,
    MaximumSpread = 0.05,

    -- Heat
    HasHeat = false,
    Temperature = 0,
    MaxTemperature = 100,
    ThrottleTemperature = 70,
    ThrottledShotsPerMinute = 45,
    TemperaturePerShot = 1,
    CoolingRate = 1,
    Overheated = false,
    NextCooling = 0,

    -- Projectile Velocity
    HasProjectileVelocity = false,
    RelativeProjectileVelocity = Vector(3500, 0, 0),

    -- Projectile Offset
    UseAutoProjectileOffset = false,
    ProjectileOffset = 20,

    -- Sounds
    StartFiringSound = "sound/buttons/button1.wav",
    FireSound = "sound/spacecombat2/weapons/med_laser_01.wav",
    StopFiringSound = "sound/buttons/button6.wav",
    StartReloadSound = "sound/weapons/clipempty_rifle.wav",
    FinishReloadSound = "sound/buttons/lever6.wav",

    -- MuzzleFlash
    HasMuzzleFlash = true,
    UseMuzzleFlashLight = true,
    MuzzleFlashLightDuration = 0.15,
    MuzzleFlashLightSize = 250,
    MuzzleFlashLightStyle = 1,
    MuzzleFlashColor = Color(255, 180, 50),
    UseMuzzleFlashEffect = true,
    MuzzleFlashEffect = "launch_cannon",
    MuzzleFlashEffectScale = 1,

    -- Callbacks
    OnSpawnedCallback = function(Launcher) end,
    OnRemovedCallback = function(Launcher) end,
    OnStartedFiringCallback = function(Launcher) end,
    OnStoppedFiringCallback = function(Launcher) end,
    OnReloadingStartedCallback = function(Launcher) end,
    OnReloadingFinishedCallback = function(Launcher) end,
    PostProjectileFiredCallback = function(Launcher, Projectile) end,
    PreProjectileFiredCallback = function(Launcher, Projectile) end,
    OnOverheatedCallback = function(Launcher) end,
    OnCooledDownCallback = function(Launcher) end,
})

function C:init()
    -- Just making sure things don't share a reference to this table, it will cause issues if they do
    self.Fitting = {
        CPU = 0,
        PG = 0,
        Slot = "None",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    }
end

function C:IsProjectileRecipeCompatible(Recipe)
    return true
end

function C:GetCompatibleProjectileRecipes()
    local AllRecipes = {}
    if IsValid(self.Owner) then
        AllRecipes[self.Owner:SteamID()] = GM.Projectiles.Recipes.LoadedRecipes[self.Owner:SteamID()] or {}
        AllRecipes[team.GetName(self.Owner:Team())] = GM.Projectiles.Recipes.LoadedRecipes[team.GetName(self.Owner:Team())] or {}
    end

    AllRecipes["NULL"] = GM.Projectiles.Recipes.LoadedRecipes["NULL"]

    local OutRecipes = {}
    for Owner, Recipes in pairs(AllRecipes) do
        OutRecipes[Owner] = {}
        for Name, Recipe in pairs(Recipes) do
            if self:IsProjectileRecipeCompatible() then
                OutRecipes[Owner][Name] = Recipe
            end
        end
    end

    return OutRecipes
end

function C:SetRecipe(Recipe)
    assert(LCS.is_A(Recipe, "object"), "Recipe must be an object!")
    assert(Recipe:is_A(GM.class.getClass("ProjectileRecipe")), "Recipe must be a ProjectileRecipe!")

    if self:IsProjectileRecipeCompatible(Recipe) then
        self.ProjectileRecipe = Recipe
    end
end

function C:SetRecipeByName(RecipeName, RecipeOwnerID)
    local RecipeOwnerID = RecipeOwnerID or "NULL"
    local Recipe = GM.Projectiles.Recipes.LoadedRecipes[RecipeOwnerID]

    if not Recipe then
        return false
    end

    Recipe = GM.Projectiles.Recipes.LoadedRecipes[RecipeOwnerID][RecipeName]

    if not Recipe then
        return false
    end

    if self:IsProjectileRecipeCompatible(Recipe) then
        self.ProjectileRecipe = Recipe
        return true
    end

    return false
end

function C:GetProjectileRecipe()
    return self.ProjectileRecipe
end

function C:SetLauncherModel(Model)
    local ModelData = GM.Launchers.Models.LoadedModels[Model]
    if ModelData then
        self.LauncherModel = ModelData
    end
end

function C:GetLauncherModel()
    return self.ModelData
end

function C:SetLauncherType(Type)
    if not Type then return end

    if type(Type) == "string" then
        local TypeData = GM.Launchers.Types.LoadedTypes[Type]
        if TypeData then
            self.LauncherType = TypeData
        end
    else
        self.LauncherType = Type
    end

    local LauncherData = self.LauncherType:GetLauncherConfiguration()

    -- Update launcher configuration to match the values set in the launcher type
    -- Targeting
    if LauncherData.Targeting and LauncherData.Targeting.HasTargeting then
        self.HasTargeting = true
        self.Target = nil
    else
        self.HasTargeting = false
    end

    -- Fire Rate
    if LauncherData.FireRate then
        self.ShotsPerMinute = LauncherData.FireRate.ShotsPerMinute or 60
    else
        self.ShotsPerMinute = 60
    end

    -- Burst Fire
    if LauncherData.Burst and LauncherData.Burst.HasBurst then
        self.HasBurst = true
        self.ShotsPerBurst = LauncherData.Burst.ShotsPerBurst or 3
        self.TimeBetweenBurstShots = LauncherData.Burst.TimeBetweenBurstShots or 0.05
    else
        self.HasBurst = false
    end

    -- Magazine
    if LauncherData.Magazine and LauncherData.Magazine.HasMagazine then
        self.HasMagazine = true
        self.Ammo = 0
        self.MaxAmmo = LauncherData.Magazine.MaxAmmo or 50
        self.ReloadTime = LauncherData.Magazine.ReloadTime or 10
        self.Reloading = false
        self.ReloadingFinishedTime = 0
    else
        self.HasMagazine = false
    end

    -- Resource Usage
    if LauncherData.ResourceUsage and LauncherData.ResourceUsage.HasResourceUsage then
        self.HasResourceUsage = true
        self.ResourcesPerShot = LauncherData.ResourceUsage.ResourcesPerShot or {}
        self.ResourcesPerReload = LauncherData.ResourceUsage.ResourcesPerReload or {}
    else
        self.HasResourceUsage = false
    end

    -- Capacitor
    if LauncherData.Capacitor and LauncherData.Capacitor.HasCapacitor then
        self.HasCapacitor = true
        self.FillOnlyWhenFiring = LauncherData.Capacitor.FillOnlyWhenFiring or false
        self.MinimumFirePercentage = LauncherData.Capacitor.MinimumFirePercentage or 0
        self.Capacitor = 0
        self.MaxCapacitor = LauncherData.Capacitor.MaxCapacitor or 1000
        self.CapacitorFillRate = LauncherData.Capacitor.CapacitorFillRate or 100
        self.CapacitorPerShot = LauncherData.Capacitor.CapacitorPerShot or 100
        self.NextCapacitorFill = 0
    else
        self.HasCapacitor = false
    end

    -- Fitting
    if LauncherData.Fitting and LauncherData.Fitting.HasFitting then
        self.HasFitting = true
        self.Fitting = {
            CPU = LauncherData.Fitting.CPU or 0,
            PG = LauncherData.Fitting.PG or 0,
            NumSlots = LauncherData.Fitting.NumSlots or 1,
            Slot = LauncherData.Fitting.Slot or "None",
            Status = "Offline",
            CanRun = false,
            Classes = LauncherData.Fitting.Classes or {}
        }
    else
        self.HasFitting = false
    end

    -- Accuracy
    if LauncherData.Accuracy and LauncherData.Accuracy.HasAccuracy then
        self.HasAccuracy = true
        self.MinimumSpread = LauncherData.Accuracy.MinimumSpread or 0.01
        self.MaximumSpread = LauncherData.Accuracy.MaximumSpread or 0.015
    else
        self.HasAccuracy = false
    end

    -- Heat
    if LauncherData.Heat and LauncherData.Heat.HasHeat then
        self.HasHeat = true
        self.Temperature = 0
        self.MaxTemperature = LauncherData.Heat.MaxTemperature or 100
        self.ThrottleTemperature = LauncherData.Heat.ThrottleTemperature or 70
        self.ThrottledShotsPerMinute = LauncherData.Heat.ThrottledShotsPerMinute or 30
        self.TemperaturePerShot = LauncherData.Heat.TemperaturePerShot or 5
        self.CoolingRate = LauncherData.Heat.CoolingRate or 5
        self.Overheated = false
        self.NextCooling = 0
    else
        self.HasHeat = false
    end

    -- Projectile Velocity
    if LauncherData.ProjectileVelocity and LauncherData.ProjectileVelocity.HasProjectileVelocity then
        self.HasProjectileVelocity = true
        self.RelativeProjectileVelocity = LauncherData.ProjectileVelocity.RelativeProjectileVelocity or Vector(3500, 0, 0)
    else
        self.HasProjectileVelocity = false
    end

    -- Projectile Offset
    if LauncherData.ProjectileOffset then
        self.UseAutoProjectileOffset = LauncherData.ProjectileOffset.UseAutoProjectileOffset or false
        self.ProjectileOffset = LauncherData.ProjectileOffset.ProjectileOffset or 20
    end

    return LauncherData
end

function C:GetLauncherModel()
    return self.ModelData
end

function C:SetCore(Entity)
    if IsValid(Entity) and Entity.IsCoreEnt then
        self.Core = Entity
    end
end

function C:GetCore()
    return self.Core
end

function C:SetAttachedEntity(Entity)
    if IsValid(Entity) then
        self.AttachedEntity = Entity
        self:SetLauncherModel(self.AttachedEntity:GetModel())
        if IsValid(Entity:GetProtector()) then
            self:SetCore(Entity:GetProtector())
        end
    end
end

function C:GetAttachedEntity()
    return self.AttachedEntity
end

function C:SetOwner(NewOwner)
    if IsValid(NewOwner) then
        self.Owner = NewOwner
    end
end

function C:GetOwner()
    return self.Owner
end

function C:GetID()
    return self.ID
end

function C:GetWorldPosition()
    if IsValid(self.AttachedEntity) then
        return self.AttachedEntity:LocalToWorld(self.Position)
    else
        return self.Position
    end
end

function C:GetWorldAngles()
    if IsValid(self.AttachedEntity) then
        return self.AttachedEntity:LocalToWorldAngles(self.Angle)
    else
        return self.Angle
    end
end

function C:SetWorldPosition(NewPosition)
    if IsValid(self.AttachedEntity) then
        self.Position = self.AttachedEntity:WorldToLocal(NewPosition)
    else
        self.Position = NewPosition
    end
end

function C:SetWorldAngles(NewAngle)
    if IsValid(self.AttachedEntity) then
        self.Angle = self.AttachedEntity:WorldToLocalAngles(NewAngle)
    else
        self.Angle = NewAngle
    end
end

function C:SetRelativePosition(NewPosition)
    self.Position = NewPosition
end

function C:SetRelativeAngles(NewAngle)
    self.Angle = NewAngle
end

function C:GetRelativePosition()
    return self.Position
end

function C:GetRelativeAngles()
    return self.Angle
end

function C:ApplyAccuracy(StartAngle)
    if self.HasAccuracy then
        local Spread = math.Rand(self.MinimumSpread, self.MaximumSpread)
        local ModAngle = AngleRand()
        ModAngle:Mul(Spread)

        return StartAngle + ModAngle
    end

    return StartAngle
end

function C:GetFiringPositionOffset()
    if self.UseAutoProjectileOffset then
        -- TODO: Implement auto offset
    end

    return self.ProjectileOffset
end

function C:GetFiringPosition()
    if self.LauncherModel then
        local WorldPos = self:GetWorldPosition()
        local WorldAng = self:GetWorldAngles()

        local Index, Position = self.LauncherModel:GetNextFiringPosition(self.LastFiringPosition)
        self.LastFiringPosition = Index

        local Pos, Ang = LocalToWorld(Position, self.LauncherModel:GetFiringDirection(), WorldPos, WorldAng)
        return Pos, self:ApplyAccuracy(Ang)
    elseif IsValid(self.AttachedEntity) then
        local WorldAng = self:GetWorldAngles()

        --Figure out where we want to aim the projectile
		local BoxMax = self.AttachedEntity:OBBMaxs()
        local Offset = self:GetFiringPositionOffset()
		local WorldPos = self.AttachedEntity:LocalToWorld(self.Position + Vector(BoxMax.x + Offset, 0, 0))

        return WorldPos, self:ApplyAccuracy(WorldAng)
    else
        local WorldPos = self:GetWorldPosition()
        local WorldAng = self:GetWorldAngles()

        return WorldPos, self:ApplyAccuracy(WorldAng)
    end
end

function C:SetTarget(NewTarget)
    self.Target = NewTarget
end

function C:GetTarget()
    return self.Target
end

function C:ApplyUpgrades()

end

function C:OnSpawned()
end

function C:OnRemoved()
end

function C:OnStartedFiring()
end

function C:OnStoppedFiring()
end

function C:OnReloadingStarted()
end

function C:OnReloadingFinished()
end

function C:PostProjectileFired(Projectile)
end

function C:PreProjectileFired(Projectile)
end

function C:OnOverheated()
end

function C:OnCooledDown()
end

function C:StartFiring()
    if self.Firing then
        return
    end

    -- Force an update next tick to reduce input latency
    self.NextThink = CurTime()
    self.Firing = true
    self:OnStartedFiring()
    self:OnStartedFiringCallback()
    SC.Play3DSound(self.StartFiringSound, self:GetWorldPosition(), 200, 1500)
end

function C:StopFiring()
    if not self.Firing then
        return
    end

    self.Firing = false
    self:OnStoppedFiring()
    self:OnStoppedFiringCallback()
    SC.Play3DSound(self.StopFiringSound, self:GetWorldPosition(), 200, 1500)
end

function C:ApplyProjectileVelocity(Projectile)
    if self.HasProjectileVelocity then
        local MovementComponent = Projectile:GetMovementComponent()
        if not MovementComponent then
            MovementComponent = GAMEMODE.class.new("MovementComponent")
            Projectile:AddComponent(MovementComponent)
        end

        MovementComponent:AddRelativeVelocity(self.RelativeProjectileVelocity)
    end
end

function C:ApplyProjectileTarget(Projectile)
    if self.HasTargeting then
        local TargetingComponent = Projectile:GetTargetingComponent()
        if TargetingComponent then
            TargetingComponent:SetTarget(self.Target)
        end
    end
end

function C:FireBurst()
    -- Fire the first shot of the burst immediately
    self:Fire()

    -- Then queue up the rest of the shots
    for I=1, self.ShotsPerBurst - 1 do
        timer.Simple(I * self.TimeBetweenBurstShots, function()
            if IsValid(self) then
                self:Fire()
            end
        end)
    end
end

function C:ConsumeResources()
    if self.HasMagazine and self.Ammo < 0 then
        SC.Error("A weapon tried firing without any ammo available!", 5)
        return false
    end

    if self.HasCapacitor then
        if self.Capacitor / self.MaxCapacitor < self.MinimumFirePercentage then
            return false
        end

        if self.Capacitor < self.CapacitorPerShot then
            SC.Error("A weapon tried firing without any Capacitor available!", 5)
            return false
        end
    end

    if self.HasResourceUsage and not self.HasMagazine then
        if not self.Core:ConsumeResources(self.ResourcesPerShot) then
            SC.Error("A weapon started firing without all of it's resources available!", 5)
            return false
        end
    end

    if self.HasMagazine then
        self.Ammo = self.Ammo - 1
    end

    if self.HasCapacitor then
        self.Capacitor = self.Capacitor - self.CapacitorPerShot
    end

    return true
end

function C:Fire()
    if not self:ConsumeResources() then
        return
    end

    local Position, Angles = self:GetFiringPosition()
    local Success, Projectile = self.ProjectileRecipe:CreateProjectileFromRecipe(Position, Angles, self.Owner)

    if not Success then
        SC.Error("Failed to create projectile "..self.ProjectileRecipe:GetName().." at location "..tostring(Position).." for owner "..tostring(self.Owner), 5)
        return
    end

    Projectile:SetLauncher(self)
    self:ApplyProjectileVelocity(Projectile)
    self:ApplyProjectileTarget(Projectile)

    self:PreProjectileFired(Projectile)
    self:PreProjectileFiredCallback(Projectile)

    Projectile:Spawn()

    if self.HasHeat then
        self.Temperature = self.Temperature + self.TemperaturePerShot
    end

    if self.HasMuzzleFlash then
        if self.UseMuzzleFlashEffect then
            local effectdata = {}
            effectdata.Start = Position
            effectdata.Origin = Position
            effectdata.Normal = Angles:Forward()
            effectdata.Magnitude = math.random() + 2 / 2 + 0.25
            effectdata.Scale = self.MuzzleFlashEffectScale

            SC.CreateEffect(self.MuzzleFlashEffect, effectdata)
        end

        if self.UseMuzzleFlashLight then
            SC.CreateLight(
                self:GetID(),
                Position,
                self.MuzzleFlashColor,
                self.MuzzleFlashLightSize,
                self.MuzzleFlashLightDuration,
                nil,
                nil,
                self.MuzzleFlashLightStyle
            )
        end
    end

    SC.Play3DSound(self.FireSound, Position, 400, 2500)

    self:PostProjectileFired(Projectile)
    self:PostProjectileFiredCallback(Projectile)

    return Projectile
end

function C:IsThrottled()
    return self.HasHeat and self.Temperature > self.ThrottleTemperature
end

function C:IsOverheated()
    return self.HasHeat and self.Overheated
end

function C:ShouldApplyCooling()
    return self.HasHeat and CurTime() > self.NextCooling
end

function C:ApplyCooling()
    self.Temperature = math.max(self.Temperature - self.CoolingRate, 0)
end

function C:HasResources()
    if not self.HasResourceUsage then
        return true
    end

    if not IsValid(self.Core) then
        return false
    end

    if self.HasMagazine then
        if not self.Core:HasResources(self.ResourcesPerReload) then
            return false
        end

        local ResourcesPerShot = {}
        for Name, Amount in pairs(self.ResourcesPerShot) do
            ResourcesPerShot[Name] = Amount * self.MaxAmmo
        end

        if not self.Core:HasResources(ResourcesPerShot) then
            return false
        end
    else
        if self.HasBurst then
            local ResourcesPerShot = {}
            for Name, Amount in pairs(self.ResourcesPerShot) do
                ResourcesPerShot[Name] = Amount * self.ShotsPerBurst
            end

            if not self.Core:HasResources(ResourcesPerShot) then
                return false
            end
        else
            if not self.Core:HasResources(self.ResourcesPerShot) then
                return false
            end
        end
    end

    return true
end

function C:HasAmmo()
    if not self.HasMagazine then
        return self:HasResources()
    end

    if self.Ammo < 1 then
        return false
    end

    if self.HasBurst and self.Ammo < self.ShotsPerBurst then
        return false
    end

    return true
end

function C:StartReloading()
    if not self.HasMagazine or self:IsReloading() or self.Ammo >= self.MaxAmmo then
        return
    end

    -- Consume resources
    if self.HasResourceUsage then
        if not IsValid(self.Core) then
            SC.Error("A weapon started reloading without a core!", 4)
            return
        end

        if not self.Core:ConsumeResources(self.ResourcesPerReload) then
            SC.Error("A weapon started reloading without all of it's resources available!", 5)
            return
        end

        local ResourcesPerShot = {}
        for Name, Amount in pairs(self.ResourcesPerShot) do
            ResourcesPerShot[Name] = Amount * self.MaxAmmo
        end

        if not self.Core:ConsumeResources(ResourcesPerShot) then
            SC.Error("A weapon started reloading without all of it's resources available!", 5)
            return
        end
    end

    self.Reloading = true
    self.ReloadingFinishedTime = CurTime() + self.ReloadTime
    self:OnReloadingStarted()
    self:OnReloadingStartedCallback()

    SC.Play3DSound(self.StartReloadSound, self:GetWorldPosition(), 200, 1500)
end

function C:ReloadingFinished()
    self.Reloading = false
    self.Ammo = self.MaxAmmo
    self:OnReloadingFinished()
    self:OnReloadingFinishedCallback()

    SC.Play3DSound(self.FinishReloadSound, self:GetWorldPosition(), 200, 1500)
end

function C:IsReloading()
    return self.Reloading
end

function C:HasCapacitorToFire()
    if self.HasBurst then
        return not self.HasCapacitor or self.Capacitor >= self.CapacitorPerShot * self.ShotsPerBurst
    end

    return not self.HasCapacitor or self.Capacitor >= self.CapacitorPerShot
end

function C:FillCapacitor()
    local CapacitorNeeded = math.Clamp(self.MaxCapacitor - self.Capacitor, 0, self.CapacitorFillRate)
    if self.Core:ConsumeResource("Energy", CapacitorNeeded) then
        self.Capacitor = self.Capacitor + CapacitorNeeded
    end
end

function C:ShouldFillCapacitor()
    if self.FillOnlyWhenFiring and not self.Firing then
        return false
    end

    return self.HasCapacitor and IsValid(self.Core) and self.Capacitor < self.MaxCapacitor and CurTime() > self.NextCapacitorFill
end

function C:ShouldReload()
    return not self:HasAmmo() and self:HasResources() and not self:IsReloading()
end

function C:CanFire()
    return not self:IsReloading() and self:HasAmmo() and self:HasCapacitorToFire() and not self:IsOverheated() and CurTime() > self.NextFire
end

function C:ShouldFire()
    return self.Firing and self:CanFire()
end

function C:WantsToFire()
    return self.Firing
end

function C:HasCoreFitting()
    return not self.HasFitting or self.Fitting.CanRun == true
end

function C:ShouldThink()
    return CurTime() > self.NextThink and self:HasCoreFitting() and self.Initialized
end

function C:Think()
    if not IsValid(self.Owner) then
        if IsValid(self.AttachedEntity) then
            self.Owner = self.AttachedEntity.Owner
        end

        if not IsValid(self.Owner) then
            SC.Error("Invalid owner for launcher "..self:GetID(), 3)
            self.NextThink = CurTime() + 10
            return
        end
    end

    local FireDelay = self:UpdateFiring()
    self:CheckReload()
    self:UpdateCapacitor()
    self:UpdateCooling()

    local ThinkDelay = math.min(FireDelay, 1)

    self.NextThink = CurTime() + ThinkDelay
end

function C:CheckReload()
    -- Reload the magazine if we need to
    if self:ShouldReload() then
        self:StartReloading()
    else
        if self:IsReloading() and CurTime() > self.ReloadingFinishedTime then
            self:ReloadingFinished()
        end
    end
end

function C:UpdateFiring()
    local FireDelay = 60 / (self:IsThrottled() and self.ThrottledShotsPerMinute or self.ShotsPerMinute)

    -- If we should be firing, then start shooting
    if self:ShouldFire() then
        -- If we use burst fire, then call FireBurst instead of Fire
        if self.HasBurst then
            self:FireBurst()
            FireDelay = FireDelay + self.ShotsPerBurst * self.TimeBetweenBurstShots
        else
            self:Fire()
        end

        -- Set the next time we should fire a projectile
        self.NextFire = CurTime() + FireDelay
    end

    return FireDelay
end

function C:UpdateCapacitor()
    -- Refill the capacitor if we need to
    if self:ShouldFillCapacitor() then
        self:FillCapacitor()
        self.NextCapacitorFill = CurTime() + 1
    end
end

function C:UpdateCooling()
    -- Apply cooling if we should
    if self:ShouldApplyCooling() then
        self:ApplyCooling()

        if self:IsOverheated() then
            if self.Temperature <= self.ThrottleTemperature then
                self.Overheated = false
                self:OnCooledDown()
                self:OnCooledDownCallback()
            end
        elseif self.Temperature >= self.MaxTemperature then
            self.Overheated = true
            self:OnOverheated()
            self:OnOverheatedCallback()
        end

        self.NextCooling = CurTime() + 1
    end
end

function C:Spawn()
    self.Initialized = true
    self.ID = table.insert(Launchers, self)
    self:OnSpawned()
    self:OnSpawnedCallback()
end

function C:Remove()
    self.Removed = true
    Launchers[self.ID] = nil
    self:OnRemoved()
    self:OnRemovedCallback()
end

function C:IsValid()
    return self:GetID() ~= 0 and self.Initialized and not self.Removed
end

hook.Add("Think", "SC2LauncherThink", function()
    for i,k in pairs(Launchers) do
        if IsValid(k) and k:ShouldThink() then
            k:Think()
        end
    end
end)

function C:Serialize()
    return {
        LauncherType = self.LauncherType:GetName(),
        ProjectileRecipe = self.ProjectileRecipe:GetName(),
        ProjectileRecipeOwner = self.ProjectileRecipe:GetOwnerID(),
    }
end

function C:DeSerialize(Data)
    if Data.LauncherType ~= self.LauncherType:GetName() then
        SC.Error("Someone deserialized a launcher with a different launcher type than it was saved with!?", 5)
    end

    if Data.ProjectileRecipe and Data.ProjectileRecipeOwner then
        self:SetRecipeByName(Data.ProjectileRecipe, Data.ProjectileRecipeOwner)
    end
end

GM.class.registerClass("Launcher", C)