-- For Resources such as Oxygen, Nitrogen, Carbon Dioxide etc
local GM = GM

CONTAINERMODE_DEFAULT = 1
CONTAINERMODE_AUTOENLARGE = 2
CONTAINERMODE_AUTOSPLIT = 3
CONTAINERMODE_EQUALSPLIT = 4
CONTAINERMODE_CUSTOMSPLIT = 5

local C = GM.LCS.class({
	Type = "",
	Resources = {},
    UsedStorage = 0,
    Storage = 0,
    StorageMode = CONTAINERMODE_DEFAULT
})

function C:init(Type, Storage, Resources, StorageMode)
    self:SetType(Type or self:GetType())
    self:Resize(Storage or self.Storage)
    self.Resources = Resources or {}
    self.StorageMode = StorageMode or CONTAINERMODE_DEFAULT
    self:UpdateResourceSplit()
    self:CalculateUsedStorage()
end

function C:GetUsed()
    return self.UsedStorage
end

function C:GetSize()
    return self.Storage
end

function C:GetRemainingSize()
    return self:GetSize() - self:GetUsed()
end

function C:CalculateUsedStorage()
    self.UsedStorage = 0
    for Name, Resource in pairs(self:GetStored()) do
        self.UsedStorage = self.UsedStorage + (Resource:GetMaxAmount() * Resource:GetSize())
    end
end

function C:UpdateResourceSplit()

end
function C:Resize(Size, Force)
    if Size < self.UsedStorage then
        if Force then
            self:UpdateResourceSplit()
            self:CalculateUsedStorage()
        else
            return false
        end
    end

    self.Storage = Size
    return true
end

function C:GetType()
	return self.Type
end

function C:SetType(Type)
	self.Type = Type
end

function C:GetStored()
    return self.Resources
end

function C:GetResource(Name)
    return self.Resources[Name]
end

function C:GetAmount(Name)
    if not Name or not self.Resources[Name] then return 0 end
	return self.Resources[Name]:GetAmount()
end

function C:SetAmount(Name, Amount)
    if not self.Resources[Name] then
        -- Auto enlarging storages may not have all of their possible resources, so if needed add the new resource here
        if self.StorageMode == CONTAINERMODE_AUTOENLARGE or self.StorageMode == CONTAINERMODE_AUTOSPLIT then
            self:AddResourceType(Name, Amount)
        else
            SC.Error(string.format("Non-auto enlarging storage tried to supply %d of a bad resource %s!\n\n%s", Amount, Name, debug.traceback()), 5)
            return false
        end
    else
        if self.StorageMode == CONTAINERMODE_AUTOENLARGE and self.Resources[Name]:GetMaxAmount() < Amount and not self:SetMaxAmount(Amount) then
            return false
        end
    end

    self.Resources[Name]:SetAmount(Amount)

    return true
end

function C:GetMaxAmount(Name)
    if not Name then return 0 end

    if self.StorageMode == CONTAINERMODE_AUTOENLARGE then
        local DefaultResource = GM:GetResourceFromName(Name)
        if not DefaultResource then return 0 end
        if not DefaultResource:GetType() == self:GetType() then return 0 end

        local ExistingAmount = self:GetAmount(Name)
        return ExistingAmount + (self:GetRemainingSize() / DefaultResource:GetSize())
    else
        if not self.Resources[Name] then return 0 end
        return self.Resources[Name]:GetMaxAmount()
    end
end

function C:SetMaxAmount(Name, Amount, Force)
    if not Name or not self.Resources[Name] then return false end

    if self.StorageMode == CONTAINERMODE_AUTOENLARGE then
        if Amount <= 0 then
            self:RemoveResourceType(Name)
            return true
        end

        local Delta = (Amount - self:GetAmount(Name)) * self.Resources[Name]:GetSize()
        local Fits = (self:GetUsed() + Delta) <= self.Storage

        if Fits or Force then
            self.Resources[Name]:SetMaxAmount(Amount)
            self:CalculateUsedStorage()
            return true
        end
    else
        local Delta = (Amount - self.Resources[Name]:GetMaxAmount()) * self.Resources[Name]:GetSize()
        local Fits = (self:GetUsed() + Delta) <= self.Storage

        if Fits or Force then
            self.Resources[Name]:SetMaxAmount(Amount)
            self:CalculateUsedStorage()
            return true
        end
    end

    return false
end

function C:IncreaseMaxAmount(Name, Amount)
    if not self.Resources[Name] then
        return self:AddResourceType(Name, Amount)
    end

    return self:SetMaxAmount(Name, Amount + self:GetResource(Name):GetMaxAmount())
end

function C:DecreaseMaxAmount(Name, Amount)
    if not self.Resources[Name] then
        return
    end

    self:SetMaxAmount(Name, self:GetResource(Name):GetMaxAmount() - Amount)
end

function C:SupplyResource(Name, Amount)
    if not self.Resources[Name] then
        -- Auto enlarging storages may not have all of their possible resources, so if needed add the new resource here
        if self.StorageMode == CONTAINERMODE_AUTOENLARGE or self.StorageMode == CONTAINERMODE_AUTOSPLIT then
            if not self:AddResourceType(Name, Amount) then
                SC.Error(string.format("Auto enlarging storage failed to supply %d of a resource %s!\n\n%s", Amount, Name, debug.traceback()), 5)
                return false
            end
        else
            SC.Error(string.format("Non-auto enlarging storage tried to supply %d of a bad resource %s!\n\n%s", Amount, Name, debug.traceback()), 5)
            return false
        end
    else
        if self.StorageMode == CONTAINERMODE_AUTOENLARGE and not self:IncreaseMaxAmount(Name, Amount) then
            return false
        end
    end

    return self.Resources[Name]:SupplyResource(Amount)
end

function C:ConsumeResource(Name, Amount, Force)
    if not Name or not self.Resources[Name] then return false end

    if self.StorageMode == CONTAINERMODE_AUTOENLARGE then
        local ResourceConsumed = self.Resources[Name]:ConsumeResource(Amount, Force)
        if ResourceConsumed then
            self:DecreaseMaxAmount(Name, Amount)
        end

        return ResourceConsumed
    elseif self.StorageMode == CONTAINERMODE_AUTOSPLIT then
        local ResourceConsumed = self.Resources[Name]:ConsumeResource(Amount, Force)
        if ResourceConsumed and self:GetAmount(Name) <= 0 then
            self:RemoveResourceType(Name)
        end

        return ResourceConsumed
    end

    return self.Resources[Name]:ConsumeResource(Amount, Force)
end

function C:AddResourceType(Name, Max)
    if not Name or self.Resources[Name] ~= nil then return false end
    local resource = GM:NewResource(Name)
    if not resource or resource:GetType() ~= self.Type then return false end

    self.Resources[Name] = resource

    if self.StorageMode == CONTAINERMODE_AUTOSPLIT then
        local ResourceCount = table.Count(self.Resources)
        local ResourceMax = math.floor(self.Storage / ResourceCount)
        for Name, Resource in pairs(self.Resources) do
            self:SetMaxAmount(Name, math.floor(ResourceMax / Resource:GetSize()), true)
        end
    elseif Max == nil then
        self:SetMaxAmount(Name, self:GetRemainingSize() / resource:GetSize())
    else
        self:SetMaxAmount(Name, Max)
    end

    return true
end

function C:RemoveResourceType(Name)
    if not Name or not self.Resources[Name] then return false end

    self.Resources[Name] = nil

    if self.StorageMode == CONTAINERMODE_AUTOSPLIT then
        local ResourceCount = table.Count(self.Resources)
        local ResourceMax = self.Storage / ResourceCount
        for Name, Resource in pairs(self.Resources) do
            self:SetMaxAmount(Name, math.floor(ResourceMax / Resource:GetSize()), true)
        end
    end

    self:CalculateUsedStorage()

    return true
end

function C:MergeStorage(Storage)
    if Storage == nil or Storage:GetType() ~= self:GetType() then return end
    self:Resize(self.Storage + Storage.Storage)

    local res = Storage.Resources
    for i, k in pairs(res) do
        self:IncreaseMaxAmount(i, k:GetMaxAmount())
        self:SupplyResource(i, k:GetAmount())
        Storage:SetAmount(i, 0)
    end
end

function C:SplitStorage(Storage)
    if Storage == nil or Storage:GetType() ~= self:GetType() then return end

    for i,k in pairs(Storage.Resources) do
        local taken = math.Max(0, Storage:GetMaxAmount(i) - (self:GetMaxAmount(i) - self:GetAmount(i)))
        Storage:SetAmount(i, taken)
        self:ConsumeResource(i, taken)
        self:DecreaseMaxAmount(i, Storage:GetMaxAmount(i))
    end
end

function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteFloat(self.Storage)
    net.WriteUInt(self.StorageMode, 8)

    local ResourceCount = table.Count(self.Resources)
    net.WriteUInt(ResourceCount, 16)
    if ResourceCount > 0 then
        for i,k in pairs(self.Resources) do
            net.WriteInt(k:GetID(), 12)
            k:WriteCreationPacket()
        end
    end
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self.Storage = net.ReadFloat()
    self.StorageMode = net.ReadUInt(8)

    local TotalResources = net.ReadUInt(16)

    if TotalResources > 0 then
        for I = 1, TotalResources do
            local NewResourceID = GM:GetResourceFromID(net.ReadInt(12)) or "FAIL"
            local NewResource = GM.class.getClass("Resource"):new()
            NewResource:ReadCreationPacket()

            self.Resources[NewResourceID] = NewResource
        end
    end

    self:init(self.Type, self.Storage, self.Resources, self.StorageMode)
end

function C:Serialize()
    local OutResources = {}
    for i,k in pairs(self.Resources) do
        OutResources[i] = k:Serialize()
    end

    return {Type = self.Type, Storage = self.Storage, Resources = OutResources, StorageMode = self.StorageMode}
end

function C:DeSerialize(Data)
    local InResources = {}
    for i,k in pairs(Data.Resources) do
        local NewResource = GM.class.getClass("Resource"):new()
        NewResource:DeSerialize(k)
        InResources[i] = NewResource
    end
    self:init(Data.Type, Data.Storage, InResources, Data.StorageMode)
end

GM.class.registerClass("ResourceContainer", C)