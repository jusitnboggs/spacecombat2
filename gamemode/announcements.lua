local GM = GM
GM.Announcements = GM.Announcements or {}
local Announcements = GM.Announcements

Announcements.AnnouncementColor = Color(255, 80, 80, 255)
Announcements.AnnouncementTextColor = Color(255, 255, 255, 255)

local TypeHandlers = {}
function TypeHandlers.OnSpawn(Name, Data)
    if not Name or not Data then return end

    hook.Remove("PlayerInitialSpawn", "SC.Announcements.OnSpawn"..Name)
    hook.Add("PlayerInitialSpawn", "SC.Announcements.OnSpawn"..Name, function(NewPlayer)
        if IsValid(NewPlayer) then
            NewPlayer:ChatPrint({Data.Color or Announcements.AnnouncementColor, "[Announcement]", Data.TextColor or Announcements.AnnouncementTextColor, " - ", Data.Message}, "Notices")
        end
    end)
end

function TypeHandlers.Interval(Name, Data)
    if not Name or not Data then return end

    timer.Destroy("SC.Announcements.Interval"..Name)
    timer.Create("SC.Announcements.Interval"..Name, Data.Interval or 300, 0, function()
        for i,k in pairs(player.GetAll()) do
            k:ChatPrint({Data.Color or Announcements.AnnouncementColor, "[Announcement]", Data.TextColor or Announcements.AnnouncementTextColor, " - ", Data.Message}, "Notices")
        end
    end)
end

local MessageIndex = 0
local MessageQueue = {}
timer.Create("SC.Announcements.MessageQueue", 300, 0, function()
    if table.Count(MessageQueue) > 0 then
        local MessageData = MessageQueue[MessageIndex + 1]

        if not MessageData then
            MessageData = MessageQueue[1]
            MessageIndex = 1
        end

        for i,k in pairs(player.GetAll()) do
            k:ChatPrint({MessageData.Color or Announcements.AnnouncementColor, "[Announcement]", MessageData.TextColor or Announcements.AnnouncementTextColor, " - ", MessageData.Message}, "Notices")
        end

        MessageIndex = MessageIndex + 1
    end
end)

function TypeHandlers.MessageQueue(Name, Data)
    if not Data then return end

    table.insert(MessageQueue, Data)
end

function Announcements.SetupAnnouncement(Name, Data)
    if not Name or not Data or not Data.Type or not TypeHandlers[Data.Type] or not Data.Message then return end

    Data.Message = string.Replace(Data.Message, "\\n", "\n")

    TypeHandlers[Data.Type](Name, Data)
end

function Announcements.ReloadAnnouncements()
    local SaveData = {}

    if file.Exists(SC.DataFolder.."/config/announcements.txt", "DATA") then
        SaveData = lip.load(SC.DataFolder.."/config/announcements.txt")
    else
        SaveData["WelcomePlayer"] = {
            Type = "OnSpawn",
            Message = "Welcome to the server!"
        }

        SaveData["BetaReminder"] = {
            Type = "Interval",
            Message = "Reminder, this gamemode is in beta! Things can change at any time!",
            Interval = 900
        }

        file.CreateDir(SC.DataFolder.."/config/")
        lip.save(SC.DataFolder.."/config/announcements.txt", SaveData)
    end

    for Name, Data in pairs(SaveData) do
        Announcements.SetupAnnouncement(Name, Data)
    end
end
hook.Remove("InitPostEntity", "SC.LoadAnnouncementsFromDisk")
hook.Add("InitPostEntity", "SC.LoadAnnouncementsFromDisk", Announcements.ReloadAnnouncements)