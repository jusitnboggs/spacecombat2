AddCSLuaFile()

--[[
Space Combat Initialization
]]--

local StringNetworkIDs = {}
local StringReverseLookup = {}
local StringLookupFailed = {}

SC = SC or {}
SC.DataFolder = "spacecombat2"
SC.Version = "2.1.0 Beta"
SC.Plugins = file.Find("spacecombat/plugins/*.lua", "LUA")
SC.Debug = true
SC.DebugLevel = 3

include("sh_misc.lua")

Msg("\n\n".."==========================================\n")
Msg("== Space Combat Version:\t"..tostring(SC.Version).."\t==".."\n") -- You will see this if we loaded :)
for i,k in pairs(SC.Plugins) do
	Msg("Loading plugin "..k.."\n")
	include("spacecombat/plugins/"..k)
end
Msg("==========================================\n\n")

file.CreateDir(SC.DataFolder)

function SC.RegisterNetworkedString(String)
    if type(String) ~= "string" then error("Error! Name must be a string!") end
    if StringNetworkIDs[String] then return StringNetworkIDs[String] end
    if not SERVER then return 0 end

    local NewID = table.Count(StringNetworkIDs) + 1
    StringNetworkIDs[String] = NewID
    StringReverseLookup[NewID] = String

    return NewID
end

function SC.GetStringNetworkID(String)
    if not StringNetworkIDs[String] then SC.Error("Unable to find string Network ID: "..tostring(String), 5) end
    return StringNetworkIDs[String]
end

function SC.GetStringFromID(ID)
    if not StringReverseLookup[ID] then SC.Error("Unable to find string with Network ID: "..tostring(ID).."\n\n"..debug.traceback().."\n", 5) end
    return StringReverseLookup[ID]
end

function SC.GetStringLookupTableByID()
    return table.Copy(StringReverseLookup)
end

function SC.GetStringLookupTable()
    return table.Copy(StringNetworkIDs)
end

function SC.WriteNetworkedString(String)
    local ID = SC.GetStringNetworkID(String)

    SC.Error("SC.WriteNetworkedString ID: "..tostring(ID).." String: "..tostring(String), 3)

    -- If it already has an ID then just send that
    if ID then
        net.WriteBool(false)
        net.WriteUInt(ID, 16)

    -- Otherwise, create an ID for it and send the ID and String to the client
    else
        if SERVER then
            ID = SC.RegisterNetworkedString(String)
            net.WriteBool(true)
            net.WriteString(String)
            net.WriteUInt(ID, 16)
        else
            net.WriteBool(true)
            net.WriteString(String)
        end
    end
end

function SC.ReadNetworkedString(Default, OnFailed)
    local NewID = net.ReadBool()
    local String

    -- If its a new string, add it to the local table
    if NewID then
        if SERVER then
            -- A client wants to register a net network string
            local NewClientString = net.ReadString()

            -- Make sure the string is registered
            local ID = SC.RegisterNetworkedString(NewClientString)

            -- Send the registered string back to the client
            -- Delay this by a frame to prevent any issues from calling net.Start inside of another net function
            timer.Simple(0, function()
                net.Start("SC.ClientMissingString")
                net.WriteUInt(ID, 16)
                net.WriteString(NewClientString)
                net.Broadcast()
            end)

            return NewClientString
        else
            String = net.ReadString()
            local ID = net.ReadUInt(16)

            StringNetworkIDs[String] = ID
            StringReverseLookup[ID] = String
        end

    -- Otherwise, make sure we have it registered and return the string for X ID
    else
        local ID = net.ReadUInt(16)
        String = SC.GetStringFromID(ID)

        -- Request the string from the server if we don't have it for some reason
        if not String and CLIENT then
            -- Check if this string was marked as a failure already
            local HasFailed = StringLookupFailed[ID] ~= nil

            -- Make sure this string has been marked as a failure
            StringLookupFailed[ID] = StringLookupFailed[ID] or {}

            -- Register our OnFailed callback if we have one
            if OnFailed then
                table.insert(StringLookupFailed[ID], OnFailed)
            end

            -- Tell the server if this is a new failure
            if not HasFailed then
                -- Delay this by a frame to prevent any issues from calling net.Start inside of another net function
                timer.Simple(0, function()
                    net.Start("SC.ClientMissingString")
                    net.WriteUInt(ID, 16)
                    net.SendToServer()
                end)
            end

            -- Return default string
            return Default
        end
    end

    return String
end

--load all the particles because gmod doesn't for some weird reason - Lt.Brandon
if CLIENT then
	for i,k in pairs(file.Find("particles/*.pcf", "GAME")) do
		game.AddParticles("particles/"..k)
	end

	--Create a Space Combat 2 menu area, specifically this is for ship cores
	SC.Menus = {}
	SC.Menus["Options"] = function(Panel)
		Panel:ClearControls()
		Panel:SetName("Space Combat 2 - Options")
		Panel:CheckBox("Enable Core Shield Glow", "sc_coreglow")
		Panel:CheckBox("Enable Core Hit Sounds", "sc_coresounds")
		Panel:CheckBox("Enable Core Hit Effects", "sc_coreeffects")
	end

	SC.Menus["About"] = function(Panel)
		Panel:ClearControls()
		Panel:SetName("Space Combat 2 - About")
		Panel:Help("Space Combat 2 is an addon developed for the Diaspora Spacebuild community.\n\nFound a bug? Report it on our fourums at diaspora-community.com!")
		Panel:Help("Space Combat 2 Version: "..SC.Version)
	end

	hook.Add("PopulateToolMenu", "SpaceCombat2Menu", function()
		spawnmenu.AddToolMenuOption("Space Combat 2", "Options", "SC2Options", "Options", "", "", SC.Menus.Options)
		spawnmenu.AddToolMenuOption("Space Combat 2", "Options", "SC2About", "About", "", "", SC.Menus.About)
	end)

	--This isn't big enough to be in a plugin, and everything uses it for effects so I'm putting it here -Lt.Brandon
	net.Receive("sc_createeffect", function(len, ply)
		for i=1,net.ReadUInt(8) do
			local effect = EffectData()
            local name = SC.ReadNetworkedString("", function(ID, String)
                if effect then
                    util.Effect(String, effect)
                end
            end)

            local flags = net.ReadUInt(8)

			if SC.TestFlag(flags, 1) then effect:SetStart(net.ReadVector()) else effect:SetStart(vector_origin) end
			if SC.TestFlag(flags, 2) then effect:SetOrigin(net.ReadVector()) else effect:SetOrigin(vector_origin) end
			if SC.TestFlag(flags, 4) then effect:SetNormal(net.ReadVector()) else effect:SetNormal(vector_origin) end
			if SC.TestFlag(flags, 8) then effect:SetMagnitude(net.ReadFloat()) else effect:SetMagnitude(1) end
			if SC.TestFlag(flags, 16) then effect:SetScale(net.ReadFloat()) else effect:SetScale(1) end
			if SC.TestFlag(flags, 32) then effect:SetRadius(net.ReadFloat()) else effect:SetRadius(1) end
			if SC.TestFlag(flags, 64) then effect:SetEntity(net.ReadEntity()) end

            if name ~= "" then
                util.Effect(name, effect)
            end
		end
	end)

    net.Receive("SC.ClientMissingString", function()
        local ID = net.ReadUInt(16)
        local String = net.ReadString()

        SC.Error("SC.ClientMissingString ID: "..tostring(ID).." String: "..tostring(String), 3)

        if StringLookupFailed[ID] then
            StringNetworkIDs[String] = ID
            StringReverseLookup[ID] = String

            for i,k in pairs(StringLookupFailed[ID]) do
                k(ID, String)
            end

            StringLookupFailed[ID] = nil
        else
            SC.Error("SC.ClientMissingString Got mismatched string ID: "..tostring(ID).." Server String: "..tostring(String), 3)
        end
    end)
end

if SERVER then
    net.Receive("SC.ClientMissingString", function(Length, Client)
        local MissingID = net.ReadUInt(16)
        local MissingString = SC.GetStringFromID(MissingID)

        SC.Error("Server SC.ClientMissingString ID: "..tostring(MissingID).." String: "..tostring(MissingString), 3)

        if MissingString then
            net.Start("SC.ClientMissingString")
            net.WriteUInt(MissingID, 16)
            net.WriteString(MissingString)
            net.Send(Client)
        else
            SC.Error("WTF? Client requested unknown string #"..MissingID, 5)
        end
    end)

    local function SendEffect(name, tab)
        --Send the name of the effect to create
        SC.WriteNetworkedString(name)

        --Tell the client what kind of data is being send
        local flag = 0

        if tab.Start then flag = SC.SetFlag(flag, 1) end
        if tab.Origin then flag = SC.SetFlag(flag, 2) end
        if tab.Normal then flag = SC.SetFlag(flag, 4) end
        if tab.Magnitude then flag = SC.SetFlag(flag, 8) end
        if tab.Scale then flag = SC.SetFlag(flag, 16) end
        if tab.Radius then flag = SC.SetFlag(flag, 32) end
        if IsValid(tab.Entity) then flag = SC.SetFlag(flag, 64) end

        net.WriteUInt(flag, 8)

        --Actual data, if its being sent
		if tab.Start then net.WriteVector(tab.Start) end
		if tab.Origin then net.WriteVector(tab.Origin) end
		if tab.Normal then net.WriteVector(tab.Normal) end
		if tab.Magnitude then net.WriteFloat(tab.Magnitude) end
		if tab.Scale then net.WriteFloat(tab.Scale) end
		if tab.Radius then net.WriteFloat(tab.Radius) end
		if IsValid(tab.Entity) then net.WriteEntity(tab.Entity) end
    end

	function SC.CreateEffect(name, tab)
		net.Start("sc_createeffect")
			if type(name) == "table" then
				net.WriteUInt(#name, 8)
				for i, tab in pairs(name) do
					SendEffect(tab.Name, tab)
				end
			else
				net.WriteUInt(1, 8)
				SendEffect(name, tab)
			end
		net.Broadcast()
	end
end

SC.ShipClasses = {}
local ClassLUT = {}
function SC.AddShipClass(Name, Size, OctreeDepth, ExplosionEffect, EffectCounts)
	local NewClass = {}
	NewClass.Name = Name
	NewClass.Size = Size
	NewClass.OctreeDepth = OctreeDepth
	NewClass.ExplosionEffect = ExplosionEffect
	NewClass.EffectCounts = EffectCounts
	NewClass.SubClasses = {}
	local Index = table.insert(SC.ShipClasses, NewClass)
	ClassLUT[Name] = Index
end

function SC.GetShipClassTable(Name)
	return SC.ShipClasses[ClassLUT[Name]]
end

-- Use this number to easily adjust the volume required for subclasses without changing every number
local VolumeAdjustment = 0.18
function SC.AddSubClass(Name, Parent, Volume, Slots, Bonuses)
	local NewClass = {}
	NewClass.Name = Name
	NewClass.Volume = Volume * VolumeAdjustment
	NewClass.Slots = Slots
	NewClass.Bonuses = Bonuses
	table.insert(SC.GetShipClassTable(Parent).SubClasses, NewClass)
end

function SC.GetSubClasses(ParentClass)
	return SC.GetShipClassTable(ParentClass).SubClasses or {}
end

SC.AddShipClass("Drone", 0, 3, "FighterDeath", {0, 1, 0})
SC.AddShipClass("Fighter", 5, 3, "FighterDeath", {0, 1, 0})
SC.AddShipClass("Frigate", 20, 4, "FrigateDeath", {0, 0, 1})
SC.AddShipClass("Cruiser", 50, 4, "FrigateDeath", {1, 0, 1})
SC.AddShipClass("Battlecruiser", 80, 4, "CapitalDeath", {2, 1, 1})
SC.AddShipClass("Battleship", 110, 4, "CapitalDeath", {2, 2, 1})
SC.AddShipClass("Dreadnaught", 150, 5, "CapitalDeath", {2, 2, 2})
SC.AddShipClass("Titan", 200, 6, "TitanDeath", {2, 2, 3})

SC.AddSubClass("Light Drone", "Drone", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 2,
	["Industrial"] = 0,
    ["Upgrade"] = 1
}, {})

SC.AddSubClass("Medium Drone", "Drone", 92923, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 3,
	["Industrial"] = 0,
	["Upgrade"] = 1
}, {})

SC.AddSubClass("Heavy Drone", "Drone", 220263, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 4,
	["Industrial"] = 0,
	["Upgrade"] = 2
}, {})


SC.AddSubClass("Interceptor", "Fighter", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 1,
	["Tiny Weapon"] = 4,
	["Industrial"] = 0,
	["Upgrade"] = 3,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Fighter", "Fighter", 3441617, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 2,
	["Tiny Weapon"] = 4,
	["Industrial"] = 0,
	["Upgrade"] = 3,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Heavy Fighter", "Fighter", 16908664, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 1,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 1,
	["Small Weapon"] = 2,
	["Tiny Weapon"] = 2,
	["Industrial"] = 0,
	["Upgrade"] = 4,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Corvette", "Frigate", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 0,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 8,
	["Tiny Weapon"] = 4,
	["Industrial"] = 0,
	["Upgrade"] = 4,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Frigate", "Frigate", 92923661, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 1,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 2,
	["Small Weapon"] = 8,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 4,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Heavy Frigate", "Frigate", 220263494, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 4,
	["Small Weapon"] = 6,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 5,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Destroyer", "Cruiser", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 1,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 8,
	["Small Weapon"] = 6,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 5,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Cruiser", "Cruiser", 572599046, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 1,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 10,
	["Small Weapon"] = 4,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 5,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Heavy Cruiser", "Cruiser", 945154098, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 2,
	["Medium Weapon"] = 10,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 6,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Interdictor", "Battlecruiser", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 1,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 0,
	["Medium Weapon"] = 16,
	["Small Weapon"] = 4,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 5,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Battlecruiser", "Battlecruiser", 2113583107, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 2,
	["Medium Weapon"] = 16,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 6,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Heavy Battlecruiser", "Battlecruiser", 3441617108, {
	["Primary Reactor"] = 2,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 1,
	["Large Weapon"] = 4,
	["Medium Weapon"] = 10,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 7,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Light Battleship", "Battleship", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 18,
	["Medium Weapon"] = 10,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 7,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Battleship", "Battleship", 5234269420, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 0,
	["Large Weapon"] = 20,
	["Medium Weapon"] = 8,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 7,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Heavy Battleship", "Battleship", 7561232788, {
	["Primary Reactor"] = 2,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 2,
	["Large Weapon"] = 16,
	["Medium Weapon"] = 6,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 8,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Colossus", "Dreadnaught", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 4,
	["Large Weapon"] = 14,
	["Medium Weapon"] = 4,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 8,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Dreadnaught", "Dreadnaught", 12615457742, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 6,
	["Large Weapon"] = 16,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 8,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Leviathan", "Dreadnaught", 20071510979, {
	["Primary Reactor"] = 2,
	["Auxiliary Reactor"] = 2,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 10,
	["Large Weapon"] = 20,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 9,
    ["Targeting Computer"] = 1
}, {})


SC.AddSubClass("Behemoth", "Titan", 0, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 4,
	["Super Weapon"] = 0,
	["X-Large Weapon"] = 16,
	["Large Weapon"] = 12,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 10,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("Titan", "Titan", 36646338975, {
	["Primary Reactor"] = 1,
	["Auxiliary Reactor"] = 4,
	["Super Weapon"] = 1,
	["X-Large Weapon"] = 18,
	["Large Weapon"] = 16,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 10,
    ["Targeting Computer"] = 1
}, {})

SC.AddSubClass("World-Breaker", "Titan", 75550378775, {
	["Primary Reactor"] = 2,
	["Auxiliary Reactor"] = 4,
	["Super Weapon"] = 2,
	["X-Large Weapon"] = 22,
	["Large Weapon"] = 16,
	["Medium Weapon"] = 0,
	["Small Weapon"] = 0,
	["Tiny Weapon"] = 0,
	["Industrial"] = 0,
	["Upgrade"] = 10,
    ["Targeting Computer"] = 1
}, {})

SC.ArmorTypes = {}
function SC.AddArmorType(Name, HealthModifier, MassModifier, Regenerative, Resistances)
	local NewArmor = {}
	NewArmor.Name = Name
	NewArmor.HealthModifier = HealthModifier
	NewArmor.MassModifier = MassModifier
	NewArmor.Regenerative = Regenerative
	NewArmor.Resistances = Resistances
	SC.ArmorTypes[Name] = NewArmor
	list.Set("sc_armortypes", Name, NewArmor)
end

SC.AddArmorType("Standard", 1.0, 1.0, false, {
	EM		= 0.50,
	EXP		= 0.25,
	KIN		= 0.35,
	THERM	= 0.55
})

SC.AddArmorType("Energized", 0.75, 0.9, false, {
	EM		= 0.25,
	EXP		= 0.45,
	KIN		= 0.55,
	THERM	= 0.65
})

SC.AddArmorType("Reactive", 0.85, 0.9, false, {
	EM		= 0.50,
	EXP		= 0.20,
	KIN		= 0.65,
	THERM	= 0.45
})

SC.AddArmorType("Spaced", 0.85, 0.5, false, {
	EM		= 0.40,
	EXP		= 0.45,
	KIN		= 0.65,
	THERM	= 0.40
})

SC.AddArmorType("Solid Nubium", 1.25, 2.0, false, {
	EM		= 0.60,
	EXP		= 0.65,
	KIN		= 0.55,
	THERM	= 0.15
})

SC.AddArmorType("Bioregenerative", 1.75, 1.5, true, {
	EM		= 0.50,
	EXP		= 0.25,
	KIN		= 0.45,
	THERM	= 0.15
})

SC.ShieldTypes = {}
function SC.AddShieldType(Name, HealthModifier, RechargeExponent, Efficiency, Resistances)
	local NewShield = {}
	NewShield.Name = Name
	NewShield.HealthModifier = HealthModifier
	NewShield.RechargeExponent = RechargeExponent
	NewShield.Efficiency = Efficiency
	NewShield.Resistances = Resistances
	SC.ShieldTypes[Name] = NewShield
	list.Set("sc_shieldtypes", Name, NewShield)
end

SC.AddShieldType("Hybrid", 1.0, 0.85, 1.0, {
	EM		= 0.25,
	EXP		= 0.50,
	KIN		= 0.40,
	THERM	= 0.35
})

SC.AddShieldType("High Efficiency", 0.4, 0.845, 2.5, {
	EM		= 0.35,
	EXP		= 0.40,
	KIN		= 0.45,
	THERM	= 0.30
})

SC.AddShieldType("Overpowered", 1.5, 0.855, 0.75, {
	EM		= 0.55,
	EXP		= 0.25,
	KIN		= 0.30,
	THERM	= 0.45
})

SC.AddShieldType("Anti-Cyclonic", 0.4, 0.875, -0.1, {
	EM		= 0.75,
	EXP		= 0.25,
	KIN		= 0.15,
	THERM	= 0.35
})

SC.HullTypes = {}
function SC.AddHullType(Name, HealthModifier, CargoModifier, AmmoModifier, BonusSlots, Resistances)
	local NewHull = {}
	NewHull.Name = Name
	NewHull.HealthModifier = HealthModifier
	NewHull.CargoModifier = CargoModifier
	NewHull.AmmoModifier = AmmoModifier
	NewHull.BonusSlots = BonusSlots
	NewHull.Resistances = Resistances
	SC.HullTypes[Name] = NewHull
	list.Set("sc_hulltypes", Name, NewHull)
end

SC.AddHullType("Standard", 1.0, 1.0, 1.0, {}, {
	EM		= 0.20,
	EXP		= 0.20,
	KIN		= 0.20,
	THERM	= 0.20
})

SC.AddHullType("Reinforced", 1.4, 0.8, 0.8, {}, {
	EM		= 0.30,
	EXP		= 0.30,
	KIN		= 0.30,
	THERM	= 0.30
})

SC.AddHullType("Industrial", 0.6, 2.0, 0.2, {
	Industrial = 2
}, {
	EM		= 0.25,
	EXP		= 0.25,
	KIN		= 0.25,
	THERM	= 0.25
})

SC.AddHullType("Combat", 1.2, 0.7, 1.5, {
	["Auxiliary Reactor"] = 2,
	["Upgrade"] = 1
}, {
	EM		= 0.25,
	EXP		= 0.25,
	KIN		= 0.25,
	THERM	= 0.25
})

SC.AddHullType("Station", 4.0, 2.0, 2.0, {
	["Industrial"] = 4,
	["Upgrade"] = 2,
	["Auxiliary Reactor"] = 4
}, {
	EM		= 0.55,
	EXP		= 0.55,
	KIN		= 0.55,
	THERM	= 0.55
})

-- This will only be available if debug mode is enabled in code
if SC.Debug then
    SC.AddHullType("DEBUG", 100, 100, {
        ["Primary Reactor"] = 100,
        ["Auxiliary Reactor"] = 100,
        ["Super Weapon"] = 100,
        ["X-Large Weapon"] = 100,
        ["Large Weapon"] = 100,
        ["Medium Weapon"] = 100,
        ["Small Weapon"] = 100,
        ["Tiny Weapon"] = 100,
        ["Industrial"] = 100,
        ["Upgrade"] = 100
    }, {
        EM		= 0.99,
        EXP		= 0.99,
        KIN		= 0.99,
        THERM	= 0.99
    })
end