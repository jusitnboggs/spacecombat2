AddCSLuaFile()

local GM = GM

-- Utility/Other functions
function SC.ValidVector( v ) -- Just a little utilitarian function.
	return v ~= nil and v ~= vector_origin
end

--Bitwise flag functions
function SC.TestFlag(set, flag)
  return set % (2*flag) >= flag
end

function SC.SetFlag(set, flag)
  if set % (2*flag) >= flag then
    return set
  end
  return set + flag
end

function SC.ClearFlag(set, flag)
  if set % (2*flag) >= flag then
    return set - flag
  end
  return set
end

--This is a copy of the WireLib.ClassAlias function, I needed it for something and I don't want to rely on wiremod for everything incase it breaks.
function SC.ClassAlias(realclass, alias)
	scripted_ents.Alias(alias, realclass)
	-- Hack for Advdupe2, since scripted_ents.GetList() does not respect aliases
	hook.Add("Initialize", "Rename_"..alias, function()
		local tab = scripted_ents.GetStored(realclass).t -- Grab the registered entity
		scripted_ents.Register(tab, alias) -- Set alias to be defined as this ENT
		tab.ClassName = realclass -- scripted_ents.Register changes this to your argument, lets change it back
	end)
end

-- Messages that can easily be disabled.
function SC.Print(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		print(mess)
	end
	return
end

function SC.Msg(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		Msg(mess.."\n")
	end
	return
end

function SC.Error(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		ErrorNoHalt(mess.."\n")
	end
	return
end

function SC.GetCvarBool(cvar) --Mainly just to save space
	return GetConVar(cvar):GetBool()
end

--####  Math Stuff  ####--
function cubic_curve( a, b, c, d, X )
	return( (a*(X^3)) + (b*(X^2)) + (c*(X)) + d )
end

function SC.StringToVector(str)
	str = string.Trim(str) --Weird shit happens if you don't trim strings -Lt.Brandon
	local vecDims = string.Explode(",", str)
	local vector = Vector(tonumber(vecDims[1]), tonumber(vecDims[2]), tonumber(vecDims[3]))

	return vector
end

--Literally just string.Explode with a call to string.Trim for sanity's sake
function SC.StringToTable(str)
	str = string.Trim(str)
	local outTable = string.Explode(",", str)

	return outTable
end

function AuthGarbageCollector( ply, sid, uid )
	collectgarbage("collect")
end
hook.Add( "PlayerAuthed", "AuthGarbageCollector", AuthGarbageCollector)

--This is a garbage collector. Nothing cleans this table, and it's important, and can get really huge. ~Dubby
NextSBObjCleaning = CurTime() + 300
hook.Add("Think", "g_SBoxObjects Cleaner", function()
	if CurTime() > NextSBObjCleaning then
		NextSBObjCleaning = CurTime() + 300
		local count = 0
		for uid, tbl in pairs( g_SBoxObjects ) do
			for class, enttbl in pairs( tbl ) do
				for id, ent in pairs( enttbl ) do
					if ent == NULL then g_SBoxObjects[uid][class][id] = nil count = count + 1 end
				end
			end
		end
		if count > 0 then
			ErrorNoHalt( "Limiter: Found and removed "..tostring(count).." invalid object(s) from the player module's table.\n" )
		end
	end
end)

function SC.Play3DSound(Sound, Pos, FadeStart, FadeEnd)
    if not Sound or Sound == "" or not isvector(Pos) or not isnumber(FadeStart) or not isnumber(FadeEnd) then
        return
    end

    if CLIENT then
        if IsValid(LocalPlayer()) and LocalPlayer():GetPos():Distance(Pos) > FadeEnd then
            return
        end

        -- Play sound
        sound.PlayFile(Sound, "3d", function(Sound)
            if IsValid(Sound) then
                Sound:SetPos(Pos)
                Sound:Set3DFadeDistance(FadeStart, FadeEnd)
                Sound:Play()
            end
        end)
    else
        local SendToPlayers = {}
        for _, Player in ipairs(player.GetHumans()) do
            if Player:TestPVS(Pos) and Player:GetPos():DistToSqr(Pos) < (FadeEnd * FadeEnd) then
                table.insert(SendToPlayers, Player)
            end
        end

        net.Start("SC.Play3DSound")
        SC.WriteNetworkedString(Sound)
        net.WriteVector(Pos, false)
        net.WriteFloat(FadeStart)
        net.WriteFloat(FadeEnd)
        net.Send(SendToPlayers)
    end
end

function SC.CreateLight(ID, Pos, Col, Size, DieTime, Brightness, Decay, Style)
    if not isnumber(ID) or not isvector(Pos) or not IsColor(Col) then
        return
    end

    if CLIENT then
        if not GM.Config:Get("spacecombat_cl", "Effects", "EnableLightEffects", true) then
            return
        end

        local LightViewDistance = math.min(Size or 250, 256) * 15 * GM.Config:Get("spacecombat_cl", "Effects", "LightViewDistance", 1)
        if IsValid(LocalPlayer()) and LocalPlayer():GetPos():Distance(Pos) > LightViewDistance then
            return
        end

        -- Create light
        local Light = DynamicLight(ID)
        if Light then
            local Size = Size or 250
            local DieTime = DieTime or 0.65
            Light.pos = Pos
            Light.r = Col.r
            Light.g = Col.g
            Light.b = Col.b
            Light.brightness = Brightness or 6
            Light.Size = Size
            Light.Decay = Decay or (Size / DieTime)
            Light.DieTime = CurTime() + DieTime
            Light.Style = (Style or 0)
        end
    else
        local LightNetworkDistance = math.min(math.min(Size or 250, 256) * 15, GM.Config:Get("spacecombat", "Network", "LightDistance", 8192))
        LightNetworkDistance = LightNetworkDistance * LightNetworkDistance

        local SendToPlayers = {}
        for _, Player in ipairs(player.GetHumans()) do
            if Player:TestPVS(Pos) and Player:GetPos():DistToSqr(Pos) < LightNetworkDistance then
                table.insert(SendToPlayers, Player)
            end
        end

        local OptionalDataFlags = 0
        if isnumber(Size) then
            OptionalDataFlags = SC.SetFlag(OptionalDataFlags, 1)
        end

        if isnumber(DieTime) then
            OptionalDataFlags = SC.SetFlag(OptionalDataFlags, 2)
        end

        if isnumber(Brightness) then
            OptionalDataFlags = SC.SetFlag(OptionalDataFlags, 4)
        end

        if isnumber(Decay) then
            OptionalDataFlags = SC.SetFlag(OptionalDataFlags, 8)
        end

        if isnumber(Style) then
            OptionalDataFlags = SC.SetFlag(OptionalDataFlags, 16)
        end

        net.Start("SC.CreateLight")
        net.WriteUInt(ID, 16)
        net.WriteVector(Pos, false)
        net.WriteColor(Col)
        net.WriteUInt(OptionalDataFlags, 8)

        if isnumber(Size) then
            net.WriteFloat(Size)
        end

        if isnumber(DieTime) then
            net.WriteFloat(DieTime)
        end

        if isnumber(Brightness) then
            net.WriteUInt(Brightness, 8)
        end

        if isnumber(Decay) then
            net.WriteFloat(Decay)
        end

        if isnumber(Style) then
            net.WriteUInt(Style, 8)
        end

        net.Send(SendToPlayers)
    end
end

if CLIENT then
    net.Receive("SC.Play3DSound", function()
        SC.Play3DSound(SC.ReadNetworkedString(""), net.ReadVector(), net.ReadFloat(), net.ReadFloat())
    end)

    net.Receive("SC.CreateLight", function()
        local ID = net.ReadUInt(16)
        local Pos = net.ReadVector()
        local Col = net.ReadColor()
        local OptionalDataFlags = net.ReadUInt(8)

        local Size
        if SC.TestFlag(OptionalDataFlags, 1) then
            Size = net.ReadFloat()
        end

        local DieTime
        if SC.TestFlag(OptionalDataFlags, 2) then
            DieTime = net.ReadFloat()
        end

        local Brightness
        if SC.TestFlag(OptionalDataFlags, 4) then
            Brightness = net.ReadUInt(8)
        end

        local Decay
        if SC.TestFlag(OptionalDataFlags, 8) then
            Decay = net.ReadFloat()
        end

        local Style
        if SC.TestFlag(OptionalDataFlags, 16) then
            Style = net.ReadUInt(8)
        end

        SC.CreateLight(ID, Pos, Col, Size, DieTime, Brightness, Decay, Style)
    end)
end