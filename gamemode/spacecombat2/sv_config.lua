local GM = GM

hook.Remove("SC.Config.Register", "SC.LoadSC2Config")
hook.Add("SC.Config.Register", "SC.LoadSC2Config", function()
    local Config = GM.Config

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "EnableDamageProtection",
        Default = false
    })

    Config:Register({
        File = "spacecombat",
        Section = "General",
        Key = "EnableSandboxMode",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "General",
        Key = "EnableUnlimitedResources",
        Default = false
    })

    Config:Register({
        File = "spacecombat",
        Section = "Compatibility",
        Key = "EnableSpacebuildMapCompatibility",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "Compatibility",
        Key = "ForceHabitableEnvironmentIfNoPlanets",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "Network",
        Key = "LightDistance",
        Default = 8192
    })

    Config:Register({
        File = "spacecombat",
        Section = "Debug",
        Key = "EnableDebugMode",
        Default = true,
        ConVar = "sc_debug",
        OnChanged = function(File, Section, Key, NewValue)
            SC.Debug = NewValue
        end
    })

    Config:Register({
        File = "spacecombat",
        Section = "Debug",
        Key = "DebugPrintLevel",
        Default = 3,
        ConVar = "sc_debuglevel",
        OnChanged = function(File, Section, Key, NewValue)
            SC.DebugLevel = NewValue
        end
    })
end)