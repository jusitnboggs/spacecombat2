<p align="center">
  <a href="https://gitlab.com/TeamDiaspora/spacecombat2" rel="noopener">
 <img src="https://gitlab.com/TeamDiaspora/spacecombat2/raw/master/logo.png" alt="Project logo"></a>
</p>

---

<p align="center">
Space Combat 2 is a Garry's Mod gamemode based on the popular Spacebuild gamemode. In addition to having standard Spacebuild mechanics such as planetary environments and survival we have added numerous weapons, mining, and other new mechanics.
</p>

## 📝 Table of Contents

- [Getting Started](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [Security](./SECURITY.md)
- [Contributing](./CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🏁 Getting Started <a name = "getting_started"></a>
These instructions will get you a copy of the project up and running.

### Prerequisites
There are very few prerequesites for running Space Combat 2. Below is a list of the prerequesites and where to get them.

#### [Garry's Mod](https://store.steampowered.com/app/4000/Garrys_Mod/)
This is a Garry's Mod gamemode, so it should be obvious that this is a requirement. Head over to the [wiki](https://wiki.garrysmod.com/page/Downloading_a_Dedicated_Server) to see how to configure a server.

#### [MariaDB](https://mariadb.org/)
Head to the [download page](https://mariadb.org/download/) and grab the latest version for your platform. There are many tutorials available for configuring MariaDB. MySQL is also compatible with the gamemode if it is available.

#### [MySQLOO](https://github.com/FredyH/MySQLOO)
Head to the [releases page](https://github.com/FredyH/MySQLOO/releases) and grab the latest version. Follow the MySQLOO project installation instructions.

#### [SBEP Addon](https://gitlab.com/TeamDiaspora/sbep)
This addon is used for many of the default generator, weapon, and turret models. We recommend using our customized fork of the addon which has several changes to increase compatibility with the gamemode. Follow the instructions in the readme to install it.

### Recommended Addons

#### [Wire Addon](https://github.com/wiremod/wire)
Every entity in the game mode is compatible with wire. The Wire addon can be used to create impressively complex ships with custom programming and controls. Adding Wire can incur a notable performance cost if your users overuse it though!

#### [Advanced Duplicator 2](https://github.com/wiremod/advdupe2)
The standard Garry's Mod duplicator tool is not available in our gamemode. It is highly recommended that you install Advanced Duplicator 2 as a replacement. We will eventually ship our own solution for duplications.

#### [SProps](https://steamcommunity.com/sharedfiles/filedetails/?id=185573205)
SProps adds a large number of extra models to build ships and other contraptions out of. It is the go-to standard for building on most Sandbox servers.

### Installing
To install the gamemode start by cloning this repository into the following folder: `<Game Installation>/garrysmod/gamemodes/spacecombat2`

Then you will need to install the [default configuration repository](https://gitlab.com/TeamDiaspora/spacecombat2-data) into the following folder or create your own: `<Game Installation>/garrysmod/data/spacecombat2`

This can be done quickly using the following commands:

```bash
cd <Game Installation>/garrysmod/gamemodes
git clone https://gitlab.com/TeamDiaspora/spacecombat2.git spacecombat2
cd ../data
git clone https://gitlab.com/TeamDiaspora/spacecombat2-data.git spacecombat2
```

After the gamemode is installed you will need to configure some basic information for your server.

### Basic Configuration

These are the basic steps for configuring your installation. All configuration files are written using a modified version of the ini format. Due to limitations within Garry's Mod all configuration files are saved as `.txt` files.

#### MySQLOO Connection

1. Open `<Game Installation>/garrysmod/data/spacecombat2/config/mysql.txt`
2. Fill the file with the following information, replace the placeholder information with your own.

```ini
[Server Settings]
Address=127.0.0.1
Password=MyPassword
Username=MyUsername
Port=3306
Database=MyDatabase
```

#### Quiz Configuration

The gamemode supports giving users a short quiz when they first join the server. This is usually effective at weeding out players you may not want on your server.

1. Open `<Game Installation>/garrysmod/data/spacecombat2/config/quiz.txt`
2. Fill the file with the quiz configuration. Below is a sample configuration used on the Diaspora servers:

```ini
[Questions/Is parenting required?]
1=Yes
2=No

[Questions/How do you know someone is an admin?]
1=Scoreboard
2=Asking in chat

[Questions/What should you hit if you want help?]
1=Chat key
2=F2

[Questions/Should you expect to be banned if you're a dick?]
1=Yes
2=No

[Questions/Is violence on spawn allowed?]
1=Yes
2=No

[Questions/Can you attack players who aren't on spawn?]
1=Yes
2=No

[Answers]
Is violence on spawn allowed?=No
Can you attack players who aren't on spawn?=Yes
What should you hit if you want help?=F2
Should you expect to be banned if you're a dick?=Yes
How do you know someone is an admin?=Scoreboard
Is parenting required?=Yes

[Settings]
KickWaitTime=10
ShouldKickPlayerOnFailure=true
```

## 🎈 Usage <a name="usage"></a>
To use the gamemode after installation simply add `-gamemode spacecombat2` to your server's commandline. Some features of the gamemode require a compatible map to be loaded. You can find a list of all currently tested maps below.

### Compatible Maps

- gm_galactic_rc1
- gm_interplaneteryfunk
- sb_astria_beta_01
- sb_atlantis_b16
- sb_battlegrounds_r2
- sb_extinction_v1
- sb_forlorn_sb3_r3
- sb_gooniverse_v4
- sb_gooniverse
- sb_lostinspace
- sb_new_worlds_2
- sb_omen_v2
- sb_spacewar_v6
- sb_spatium
- sb_twinsuns_fixed
- sb_vacuum_rc1
- sb_voyages
- sb_discovery

### Additional Information

- For information about configuring the gamemode, please head over to the [configuration repository](https://gitlab.com/TeamDiaspora/spacecombat2-data)
- For information about how to play the gamemode after it is configured, please see the [tutorials](https://www.youtube.com/playlist?list=PLf3I7tC1fyryhZGA1FH-UlQz8s2pHDFvu).

## ⛏️ Built Using <a name = "built_using"></a>

- [Garry's Mod](https://github.com/Facepunch/garrysmod) - Sandbox gamemode and Lua framework
- [INP Spacebuild](https://github.com/awilliamson/inp-sb) - Original code
- [MariaDB](https://mariadb.org/about/) - Database
- [MySQLOO](https://github.com/FredyH/MySQLOO) - Database Connector
- [LuaMemorySnapshotDump](https://github.com/yaukeywang/LuaMemorySnapshotDump) - Leak Detection

## ✍️ Authors <a name = "authors"></a>

- [Brandon Garvin (@LtBrandon)](https://gitlab.com/LtBrandon) - Planning and Development
- [Steve Green (@Steeveeo)](https://gitlab.com/Steeveeo) - Mining and Visual Effects
- [Kanzuke (@Kanzuke)](https://gitlab.com/Kanzuke) - E2 Functions and Turret Designs
- [Divran (@Divran)](https://github.com/Divran) - Lifesupport Implementation
- And many other members of the Diaspora and McBuilds community since 2008

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- [The Diaspora Gaming Community](http://diaspora-community.com/) - Server hosting and support
- [Wire Team](https://github.com/wiremod/) - The creators of Wiremod
- [The SBEP Team](https://github.com/spacebuild/sbep/) - The largest collection of models and tools for building ships
- [The Spacebuild Team](https://github.com/spacebuild/spacebuild) - Creators of the original gamemode
